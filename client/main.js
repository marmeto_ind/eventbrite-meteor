import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Dashboard from '../imports/ui/pages/dashboard/Dashboard';
import UserSettingsPage from '../imports/ui/pages/settings/settings';
import HomePage from '../imports/ui/pages/homePage/home';
import allpopupdesign from '../imports/ui/pages/homePage/allpopup';
import VendorAddService from '../imports/ui/vendor/VendorAddServiceContainer';
import VendorSignup from '../imports/ui/pages/authentication/VendorSignup';
import VendorLogin from '../imports/ui/pages/authentication/VendorLogin';
import CommonLoginWrapper from '../imports/ui/components/CommonLoginPage/commonLoginWrapper';
import UserSignup from '../imports/ui/pages/authentication/UserSignup';
import ForgotPassword from '../imports/ui/components/signupLogin/forgotPassword/ForgotPassword';
import ResetPassword from '../imports/ui/components/signupLogin/forgotPassword/ResetPassword';
import UserChoiceContainer from '../imports/ui/pages/userChoice/UserChoiceContainer';
import UserServicePage from '../imports/ui/pages/userService/UserServiceContainer';
import SellerDashboard from '../imports/ui/pages/seller/Dashboard';
import UserSeller from '../imports/ui/pages/seller/Seller';
import UserChatContainer from '../imports/ui/pages/user/UserChatContainer';
import MobileHomeFirst from '../imports/ui/mobile/homeFirst/HomeFirst';
import MobileVendorSignup from '../imports/ui/mobile/vendorLoginSignUp/vendorSignUp';
import MobileHomeNewCouple from '../imports/ui/mobile/homeNewCouple/NewCouple';
import MobileHomeNewService from '../imports/ui/mobile/homeNewService/NewService';
import MobileUserCategory from '../imports/ui/mobile/userCategory/CategoryContainer';
import MobileUserChat from '../imports/ui/mobile/userChat/ChatContainer';
import MobileUserChats from '../imports/ui/mobile/userChats/ChatsContainer';
import MobileUserTables from '../imports/ui/mobile/userChats/TablesContainer';
import MobileUserGuestList from '../imports/ui/mobile/userGuestList/GuestListContainer';
import MobileUserMyEvent from '../imports/ui/mobile/userMyEvent/MyEventContainer';
import MobileUserSearchCategory from '../imports/ui/mobile/userMyEvent/SearchCategoryContainer';
import MobileUserSearchVendor from '../imports/ui/mobile/userSearchVendor/SearchVendorContainer';
import MobileUserSearchVendorGuest from '../imports/ui/mobile/userSearchVendor/SearchVendorContainerGuest';
import GuestContainer from '../imports/ui/mobile/userSearchVendor/SearchVendorGuest';
import MobileUserService from '../imports/ui/mobile/userService/ServiceContainer';
import MobileUserServiceGuest from '../imports/ui/mobile/userService/Service';
import MobileUserSettings from '../imports/ui/mobile/userSettings/SettingsContainer';
import MobileForgotPassword from '../imports/ui/mobile/mobileForgotPassword/mobileForgotPassword';
import MobileVendorAgendaContainer from '../imports/ui/mobile/vendorAgenda/VendorAgendaContainer';
import MobileVendorChat from '../imports/ui/mobile/vendorChat/ChatContainer';
import MobileVendorChats from '../imports/ui/mobile/vendorChats/ChatsContainer';
import MobileVendorDate from '../imports/ui/mobile/vendorDate/DateContainer';
import MobileVendorMyBusiness from '../imports/ui/mobile/vendorMyBusiness/MyBusinessContainer';
import MobileVendorServiceEdit from '../imports/ui/mobile/vendorServiceEdit/MyServiceEditContainer';
import MobileVendorSettings from '../imports/ui/mobile/vendorSettings/SettingsContainer';
import MobileVendorStatistics from '../imports/ui/mobile/vendorStatistics/StatisticsContainer';
import MobileAdminApprovals from '../imports/ui/mobile/adminApprovals/ApprovalContainer';
import MobileAdminApprovalService from '../imports/ui/mobile/adminApprovalService/ApprovalServiceContainer';
import MobileAdminChat from '../imports/ui/mobile/adminChat/ChatContainer';
import MobileAdminChats from '../imports/ui/mobile/adminChats/ChatsContainer';
import MobileAdminContactUs from '../imports/ui/mobile/adminContactUs/ContactUsContainer';
import MobileAdminNewServices from '../imports/ui/mobile/adminNewServices/NewServicesContainer';
import MobileAdminNewUsers from '../imports/ui/mobile/adminNewUsers/NewUsersContainer';
import MobileAdminSettings from '../imports/ui/mobile/adminSettings/SettingsContainer';
import MobileAdminStatistics from '../imports/ui/mobile/adminStatistics/StatisticsContainer';
import MobileAdminMail from '../imports/ui/mobile/adminMail/mail';
import MobileCommonLogin from '../imports/ui/mobile/homeLogin/MobileCommonLogin';
import MobileEvent from '../imports/ui/mobile/mobileEventPage/MobileEventContainer';
import CreateEvent from '../imports/ui/mobile/createEvent/createEventContainer';
import EditEvent from '../imports/ui/mobile/createEvent/editEventContainer';
import MobileAddBooking from '../imports/ui/mobile/createEvent/addBookingContainer';
import UserGuestList from '../imports/ui/pages/userGuestList/UserGuestListContainer';
import UserMyEvents from '../imports/ui/pages/userMyEvents/MyEventsContainer';
import VendorAgenda from '../imports/ui/pages/vendorAgenda/VendorAgendaContainer';
import VendorChat from '../imports/ui/pages/vendorChat/VendorChatContainer';
import VendorAddBooking from '../imports/ui/pages/vendorChat/VendorAddBookingContainer';
import VendorStats from '../imports/ui/pages/vendorStats/VendorStatsContainer';
import AdminEditPage from '../imports/ui/pages/admin/adminEdit/AdminEditContainer';
import AdminChatPage from '../imports/ui/pages/admin/adminChat/AdminChatContainer';
import AdminApprovalsPage from '../imports/ui/pages/admin/adminApprovals/AdminApprovalsContainer';
import AdminApprovalDetailsPage from '../imports/ui/pages/admin/adminApprovalDetails/AdminApprovalDetailsContainer';
import AdminStatsPage from '../imports/ui/pages/admin/adminStats/AdminStatsContainer';
import AdminCategoryEditAddPage from '../imports/ui/pages/admin/adminEditAdd/categoryEditAdd/CategoryEditAddContainer';
import AdminRegionEditAddPage from '../imports/ui/pages/admin/adminEditAdd/regionEditAdd/RegionEditAddContainer';
import AdminAttributeEditAddPage from '../imports/ui/pages/admin/adminEditAdd/attributeEditAdd/AttributeEditAddContainer';
import BannerEditAddPage from '../imports/ui/pages/admin/adminEditAdd/attributeEditAdd/BannerEditAddContainer';
import adminServiceEdit from '../imports/ui/pages/admin/adminEditAdd/attributeEditAdd/adminServiceEdit';
import AdminEventEditAddPage from '../imports/ui/pages/admin/adminEditAdd/eventEditAdd/EventEditAddContainer';
import NewServiceWebsite from '../imports/ui/mobile/homeNewService/NewServiceWebsite';

// import MyBusiness from '../imports/ui/vendor/MyBusiness';
// import PageNotFound from '../imports/ui/pages/not-found/PageNotFound';
// import CommonLogin from '../imports/ui/components/CommonLoginPage/commonLogin';
// import UserLogin from '../imports/ui/pages/authentication/UserLogin';
// import AdminSignup from '../imports/ui/pages/authentication/admin/AdminSignup';
// import AdminLogin from '../imports/ui/pages/authentication/admin/AdminLogin';
// import MobileHomeLogin from '../imports/ui/mobile/homeLogin/Login';
// import MobileVendorLogin from '../imports/ui/mobile/vendorLoginSignUp/vendorLogin';


import MobileIndex from '../imports/ui/mobile/Index';
import '../imports/startup/client';

function requireUserAuth(nextState, replace) {
    if (!Meteor.loggingIn() && !Meteor.userId()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname },
        });
    }
}

function requireVendorAuth(nextState, replace) {
    if (!Meteor.loggingIn() && !Meteor.userId()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname },
        });
    }
}

// function requireAdminAuth(nextState, replace) {
//     if(!Meteor.loggingIn() && !Meteor.userId()) {
//         replace({
//             pathname: '/login',
//             state: { nextPathname: nextState.location.pathname },
//         });
//     }
// }

const routes = (
    <MuiThemeProvider>
        <Router history={browserHistory}>
            <Route path="/" component={Dashboard} />
            <Route path="/home" component={HomePage} />
            <Route path="/user/settings" component={UserSettingsPage} />
            <Route path="/user/chat" component={UserChatContainer} onEnter={requireUserAuth} />
            <Route path="/user/choice" component={UserChoiceContainer} onEnter={requireUserAuth} />
            <Route path="/user/service/:id" component={UserServicePage} onEnter={requireUserAuth} />
            <Route path="/user/guest-list" component={UserGuestList} onEnter={requireUserAuth} />
            <Route path="/user/my-events" component={UserMyEvents} onEnter={requireUserAuth} />
            <Route path="/user/signup" component={UserSignup} />
            <Route path="/forgot/Password" component={ForgotPassword} />
            <Route path="/reset-password/:token" components={ResetPassword} />
            <Route path="/vendor/chat" component={VendorChat} onEnter={requireVendorAuth} />
            <Route path="/vendor/add-booking" component={VendorAddBooking} onEnter={requireVendorAuth} />
            <Route path="/vendor/agenda" component={VendorAgenda} onEnter={requireVendorAuth} />
            <Route path="/vendor/stats" component={VendorStats} onEnter={requireVendorAuth} />
            <Route path="/allpopup" component={allpopupdesign} />
            {/* <Route path="/vendor/add-service" component={VendorAddService} onEnter={requireVendorAuth} /> */}
            <Route path="/vendor/add-service" component={VendorAddService} />
            <Route path="/vendor/new-service" component={NewServiceWebsite} />
            <Route path="/vendor/signup" component={VendorSignup} />
            <Route path="/vendor/login" component={VendorLogin} />
            {/* <Route path="/admin/signup" component={AdminSignup} /> */}
            <Route path="/login" component={CommonLoginWrapper} />
            <Route path="/admin/edit" component={AdminEditPage} />
            <Route path="/admin/chat" component={AdminChatPage} />
            <Route path="/admin/approvals" component={AdminApprovalsPage} />
            <Route path="/admin/approval/details/:id" component={AdminApprovalDetailsPage} />
            <Route path="/admin/stats" component={AdminStatsPage} />
            <Route path="/admin/categoryEditAdd" component={AdminCategoryEditAddPage} />
            <Route path="/admin/regionEditAdd" component={AdminRegionEditAddPage} />
            <Route path="/admin/facilityEditAdd" component={AdminAttributeEditAddPage} />
            <Route path="/admin/bannerEditAdd" component={BannerEditAddPage} />
            <Route path="/admin/adminServiceEdit" component={adminServiceEdit} />
            <Route path="/admin/eventEditAdd" component={AdminEventEditAddPage} />
            <Route path="/seller" component={SellerDashboard} />
            <Route path="/user/seller" component={UserSeller} />
            <Route path="/mobile" component={MobileIndex} />
            <Route path="/mobile/login" component={MobileCommonLogin} />
            <Route path="/mobile/home/first" component={MobileHomeFirst} />
            <Route path="/mobile/home/login" component={MobileCommonLogin} />
            <Route path="/mobile/vendor/login" component={MobileCommonLogin} />
            <Route path="/mobile/vendor/signup" component={MobileVendorSignup} />
            <Route path="/mobile/home/new-couple" component={MobileHomeNewCouple} />
            <Route path="/mobile/home/new-service" component={MobileHomeNewService} />
            <Route path="/mobile/user/chat/:userId/:sellerId" component={MobileUserChat} />
            <Route path="/mobile/user/chat/:userId/:adminId/admin" component={MobileUserChat} />
            <Route path="/mobile/user/chats" component={MobileUserChats} />
            <Route path="/mobile/user/tables" component={MobileUserTables} />
             {/* added by NXP */}
            <Route path="/mobile/user/tables/:eventId/" component={MobileUserTables} />        
            <Route path="/mobile/user/guest-list" component={MobileUserGuestList} />
            {/* added by NXP */}
            <Route path="/mobile/user/guestlist/:eventId/:tableId/:eventName/" component={MobileUserGuestList} />
            <Route path="/mobile/event" component={MobileEvent} />
            <Route path="/mobile/event/create" component={CreateEvent} />
            <Route path="/mobile/event/edit/:eventId" component={EditEvent} />
            <Route path="/mobile/user/my-event/:eventId" component={MobileUserMyEvent} />
            <Route path="/mobile/user/categories" component={MobileUserMyEvent} />
            <Route path="/mobile/user/category/:categoryId/:eventId" component={MobileUserCategory} />
            <Route path="/mobile/user/search-event/:categoryId" component={MobileUserSearchVendor} />
            <Route path="/mobile/user/search-category" component={MobileUserSearchCategory} />
            <Route path="/mobile/user/search-event/guest" component={MobileUserSearchVendorGuest} />
            <Route path="/mobile/guest" component={GuestContainer} />
            <Route path="/mobile/user/service/:sellerId/:eventId" component={MobileUserService} />
            <Route path="/mobile/user/service/:sellerId/:eventId/guest" component={MobileUserServiceGuest} />
            <Route path="/mobile/user/settings" component={MobileUserSettings} />
            <Route path="/mobile/vendor/agenda" component={MobileVendorAgendaContainer} />
            <Route path="/mobile/vendor/chat/:userId/:sellerId" component={MobileVendorChat} />
            <Route path="/mobile/vendor/chats" component={MobileVendorChats} />
            <Route path="/mobile/vendor/date/:eventId" component={MobileVendorDate} />
            <Route path="/mobile/vendor/my-business" component={MobileVendorMyBusiness} />
            <Route path="/mobile/vendor/service-edit" component={MobileVendorServiceEdit} />
            <Route path="/mobile/vendor/settings" component={MobileVendorSettings} />
            <Route path="/mobile/vendor/statistics" component={MobileVendorStatistics} />
            <Route path="/mobile/vendor/add-booking" component={MobileAddBooking} />
            <Route path="/mobile/admin/login" component={MobileCommonLogin} />
            <Route path="/mobile/admin/approvals" component={MobileAdminApprovals} />
            <Route path="/mobile/admin/approval-service/:sellerId" component={MobileAdminApprovalService} />
            <Route path="/mobile/admin/chat/:userId/:sellerId" component={MobileAdminChat} />
            <Route path="/mobile/admin/chat/:userId/:user/user" component={MobileAdminChat} />
            <Route path="/mobile/admin/chats" component={MobileAdminChats} />
            <Route path="/mobile/admin/contact-us" component={MobileAdminContactUs} />
            <Route path="/mobile/admin/new-services" component={MobileAdminNewServices} />
            <Route path="/mobile/admin/new-users" component={MobileAdminNewUsers} />
            <Route path="/mobile/admin/settings" component={MobileAdminSettings} />
            <Route path="/mobile/admin/statistics" component={MobileAdminStatistics} />
            <Route path="/mobile/forgotPassword" component={MobileForgotPassword} />
            <Route path="/mobile/admin/mail" component={MobileAdminMail} />
            {/* <Route path="*" component={PageNotFound} /> */}
        </Router>
    </MuiThemeProvider>
);

Meteor.startup(() => {
    ReactDOM.render(routes, document.getElementById('app'));
});


// for mongo on mac ~/mongodb/bin/mongod --dbpath=/Users/hatprab/Desktop/eventbrite-meteor/data

// MONGO_URL='mongodb://marmeto:marmeto@ds251287.mlab.com:51287/heroku_m89c5hbb' meteor
