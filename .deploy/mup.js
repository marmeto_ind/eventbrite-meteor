module.exports = {
    servers: {
        one: {
            // TODO: set host address, username, and authentication method
            host: '18.188.47.119',
            username: 'ubuntu',
            pem: '/media/nawneet/Files\ Drive/ConProfile/pemFiles/eventbrite-client-server.pem'
            // password: 'server-password'
            // or neither for authenticate from ssh-agent
        }
    },

    app: {
        // TODO: change app name and path
        name: 'eventBrite',
        path: '/media/nawneet/Files\ Drive/Projects/eventbrite-meteor',

        servers: {
            one: {},
        },

        buildOptions: {
            serverOnly: true,
        },

        env: {
            // TODO: Change to your app's url
            // If you are using ssl, it needs to start with https://
            // ROOT_URL: 'http://ec2-18-188-47-119.us-east-2.compute.amazonaws.com',
            ROOT_URL: 'https://wendely.com',            
            MONGO_URL: 'mongodb://localhost/meteor',
            // MONGO_OPLOG_URL: 'mongodb://mongodb/local',
        },

        docker: {
            // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
            image: 'abernix/meteord:node-8.4.0-base',
        },

        // Show progress bar while uploading bundle to server
        // You might need to disable it on CI servers
        enableUploadProgressBar: true
    },

    mongo: {
        version: '3.4.1',
        servers: {
            one: {}
        }
    },

    // (Optional)
    // Use the proxy to setup ssl or to route requests to the correct
    // app when there are several apps

    proxy: {
      domains: 'wendely.com,www.wendely.com',

      ssl: {
        forceSSL: true,
        // Enable Let's Encrypt
        letsEncryptEmail: 'suvojit@marmeto.com'
      }
    }
};
