import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base'
import {check} from 'meteor/check';

if(Meteor.isServer) {
    Meteor.publish("users.totalCount", function () {
        return Meteor.users.find().count();
    })
}

Meteor.methods({
    'users.getTotalCount'() {
        return Meteor.users.find().count();
    },
    'users.remove'(user_id) {
        return Meteor.users.remove({"_id": user_id});
    },
    'users.getUsersCountInLastWeek'() {
        const oneDay = 24 * 60 * 60 * 1000;
        const oneWeekAgo = Date.now() - (7 * oneDay);

        return Meteor.users.find({"createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    },
    'users.getUsersCountInMonth'() {
        const oneDay = 24 * 60 * 60 * 1000;
        const oneMonthAgo = Date.now() - (30 * oneDay);

        return Meteor.users.find({"createdAt": {"$gt": new Date(oneMonthAgo)}}).count();
    },
    'users.getUserName'(userId) {
        return Meteor.users.findOne({"_id" : userId});
    },
    'users.getUser'(userId) {
        return Meteor.users.findOne({"_id" : userId});
    },
    'users.getAllUsers'() {
        const users = Meteor.users.find({"profile.role": "user"}).fetch();

        return users;
    },
    'users.getAdminId'() {
        return Meteor.users.findOne({"profile.role": "admin"});
    },
    'users.getUserByEmail'(email) {
        const user = Accounts.findUserByEmail(email);
        return !!user;
    },
    'users.sendResetPasswordEmail'(email) {
        check(email, String);
        const user = Accounts.findUserByEmail(email);

        if(user) {
            const userId = user._id;
            const sendPassResetEmail = Accounts.sendResetPasswordEmail(userId, email);
            console.log(sendPassResetEmail);
        } else {
        }
    }
});



