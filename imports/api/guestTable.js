import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import {Guests} from "./guests";

export const GuestTable = new Mongo.Collection("guests_table");

