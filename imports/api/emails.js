import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export const Emails = new Mongo.Collection("emails");

Meteor.methods({
    'emails.send'(option) {
        check(option, Object);
        let {user_type, event_type, user_id, email} = option;

        console.log(user_type);
        console.log(event_type);
        console.log(user_id);
        console.log(email);

        let email_subject = "";
        let email_body = '';

        // Set email subject and body for user signup
        if (event_type === "user_signup" && user_type === "user") {
            email_subject = "Welcome to Wendely user";
            email_body = "Welcome user to Wendely email body";
        } else if (event_type === "vendor_signup" && user_type === "vendor") {
            email_subject = "Welcome to Wendely vendor";
            email_body = "Welcome vendor to Wendely email body";
        } else if (event_type === "admin_signup" && user_type === "admin") {
            email_subject = "Welcome to Wendely Admin";
            email_body = "Welcome Admin to Wendely email body";
        }

        // Set email subject for forgot password
        if (event_type === "password_reset" && user_type === "user") {
            email_subject = "Password reset by user";
            email_body = "Password reset user email body";
        } else if (event_type === "password_reset" && user_type === "vendor") {
            email_subject = "Password reset by vendor";
            email_body = "Password reset vendor email body";
        } else if (event_type === "password_reset" && user_type === "admin") {
            email_subject = "Password reset by admin";
            email_body = "Password reset admin email body";
        }

        // Set email subject and body for account removal
        if (event_type === "account_remove" && user_type === "user") {
            email_subject = "User account removal";
            email_body = "User account removal email body text";
        } else if (event_type === "account_remove" && user_type === "vendor") {
            email_subject = "Vendor account removal";
            email_body = "Vendor account removal email body text";
        } else if (event_type === "account_remove" && user_type === "admin") {
            email_subject = "Admin account removal";
            email_body = "Admin account removal email body text";
        }

        Meteor.call(
            'sendEmail',
            'msuvojit@gmail.com',
            'nipor87@hotmail.com',
            email_subject,
            email_body,
            handleEmailError
        );
    }
});

function handleEmailError (err, result) {
    if (err) {console.log(err)}
    else {console.log(result)}
}

if(Meteor.isServer) {
    Meteor.publish('emails', emailsPublication);
    Meteor.publish('emails.all', emailsPublication);
}

function emailsPublication() {
    return Emails.find();
}