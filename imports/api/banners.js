import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';

export const Banners = new Mongo.Collection("banners");

if(Meteor.isServer) {
    Meteor.publish('banners', function bannersPublication() {
        return Banners.find();
    });
};