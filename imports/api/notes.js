import {Mongo} from 'meteor/mongo';

// Create notes collection
export const Notes = new Mongo.Collection('notes');