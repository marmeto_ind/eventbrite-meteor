import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import {Sellers} from "./sellers";

export const Messages = new Mongo.Collection('messages');

