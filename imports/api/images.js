import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';
import {check} from 'meteor/check';

var Images = new Mongo.Collection('images');

Images.allow({
    insert: function () { return true; },
    update: function () { return true; },
    remove: function () { return true; }
});
