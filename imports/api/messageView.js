import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';

export const MessageView = new Mongo.Collection("message_view");

if(Meteor.isServer) {
    Meteor.publish('message_view', function messageViewPublication() {
        return MessageView.find();
    });
    Meteor.publish('message_view.all', function messageViewPublication() {
        return MessageView.find();
    });
}

