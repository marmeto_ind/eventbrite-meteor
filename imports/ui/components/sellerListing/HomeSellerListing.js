import React from 'react';
import SellerList from '../seller/SellerList'

class HomeSellerListing extends React.Component {
    render() {
        // console.log(this.props);
        let categoryId = this.props.category;
        // console.log(categoryId);
        return (
            <div className="body_portion_b">
                {/*<img src={"/img/body_bac_k_b.jpg"} />*/}
                <SellerList category={categoryId}/>
            </div>
        )
    }
}

export default HomeSellerListing;