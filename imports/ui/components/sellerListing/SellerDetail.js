import React from 'react';
import SellerRating from './SellerRating';
import SellerBadge from './SellerBadge';

class SellerDetail extends React.Component {
    render() {
        return (
            <div className="dash-body_portion">
                <div className="overlap_div">
                    <div className="overlap_header">

                        <SellerRating/>

                        <div className="thumbup-btn">
                            <div className="btn">
                                <i className="fa fa-thumbs-up" aria-hidden="true"></i>
                            </div>
                        </div>

                        <SellerBadge/>

                    </div>
                    <div className="clearfix"></div>
                    <div className="text">
                        <p>sordartalje</p>
                        <h4>
                            {/*<Link to="/Event">*/}
                            panorama eventcenter
                            {/*</Link>*/}
                        </h4>
                    </div>
                    <div className="clearfix"></div>
                    <div className="social_fonts_main">
                        <div className="social_fonts_containter">

                            <span> <i className="fa fa-comment" aria-hidden="true"></i> </span>
                            <span> <i className="fa fa-play-circle-o" aria-hidden="true"></i> </span>
                            <span className="border-circle"><i className="fa fa-glass" aria-hidden="true"></i> </span>
                            <span><i className="fa fa-cutlery" aria-hidden="true"></i> </span>
                            <span><i className="fa fa-wifi" aria-hidden="true"></i></span>
                            <span>P</span>
                            <span><i className="fa fa-wheelchair" aria-hidden="true"></i></span>
                        </div>
                        <div className="check_dv">
                            <div className="btn">
                                <i className="fa fa-check" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellerDetail;