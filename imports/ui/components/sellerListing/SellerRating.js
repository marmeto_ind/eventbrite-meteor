import React from 'react';

class SellerRating extends React.Component {
    render() {
        return (
            <div className="rating">
                <span className="fa fa-star checked"></span>
                <span className="fa fa-star checked"></span>
                <span className="fa fa-star checked"></span>
                <span className="fa fa-star checked"></span>
                <span className="fa fa-star"></span>
            </div>
        )
    }
}

export default SellerRating;