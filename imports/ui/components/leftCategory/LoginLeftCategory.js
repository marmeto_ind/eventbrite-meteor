import React from 'react';
import LoginCategoryList from './LoginCategoryList';

class LoginLeftCategory extends React.Component {

    render() {
        return (
            <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                <ul className="nav">

                    <LoginCategoryList/>

                </ul>
            </div>
        )
    }
}

export default LoginLeftCategory;