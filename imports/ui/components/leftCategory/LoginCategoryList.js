import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../api/categories";

class CategoryDetail extends React.Component {

    render() {

        return (
            <div className="dropdown">
                <li className="">
                    {this.props.category.name}
                    <span className="circle_m">
                        <i className="fa fa-check" aria-hidden="true"></i>
                    </span>
                </li>
            </div>
        )
    }
}

class CategoryList extends React.Component {

    constructor(props) {
        super(props);
    }

    renderCategories() {
        return this.props.categories.map((category) => (
            <CategoryDetail
                key={category._id}
                category={category}
            />
        ));
    }

    render() {
        return (
            <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                <ul className="nav">
                    {this.renderCategories()}
                </ul>
            </div>
        )
    }
}

export default withTracker(() => {
    return {
        categories: Categories.find({}).fetch(),
    }
})(CategoryList);