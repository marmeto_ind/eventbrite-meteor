import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../api/categories";

class CategoryDetail extends React.Component {
    handleClick(event) {

        let categoryId = this.props.category._id;
        this.props.callbackFromParent(categoryId);
    }

    render() {

        return (
            <li onClick={this.handleClick.bind(this)} color="secondary">{this.props.category.name}</li>
        )
    }
}

class CategoryList extends React.Component {

    constructor(props) {
        super(props);
        // console.log(this.props);
    }    

    childCallback(data) {
        // console.log("log from category list")
        // console.log(data);
        // console.log(this.props);
        this.props.callbackFromParent(data)
    }

    renderCategories() {
        return this.props.categories.map((category) => (
            <CategoryDetail
                key={category._id}
                category={category}
                callbackFromParent={this.childCallback.bind(this)}
            />
        ));
    }

    render() {
        return (
            <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                <ul className="nav">
                    {this.renderCategories()}
                </ul>
            </div>
        )
    }
}

// export default CategoryList;

export default withTracker(() => {
    return {
        categories: Categories.find({}).fetch(),
    }
})(CategoryList);