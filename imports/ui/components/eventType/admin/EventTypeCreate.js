import React from 'react';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';

class EventTypeCreate extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();
        let newEventType = ReactDOM.findDOMNode(this.refs.newEventType).value.trim();

        const eventTypePictureFile = $("#eventTypePicture")[0].files[0];
        let eventTypePicture = '';

        if(eventTypePictureFile) {
            eventTypePicture = Images.insert(eventTypePictureFile, function (err, picture) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(picture._id);
                }
            });
        }

        Meteor.call('event_types.insert', newEventType);

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newEventType).value = '';
        console.log("submit button clicked");
    }

    render() {
        return (

            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Add new event type</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">
                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt=""/>
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newEventType"
                                                            placeholder="Enter Event Type"
                                                            ref="newEventType"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newEventType">New
                                                            Event Type</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="logo-picture form-group">
                                            <label htmlFor="eventTypePicture"><img id="hi" className={"pic-logo"}
                                                                                  src="/img/add.png"
                                                                                  alt=""/></label>
                                            <input
                                                type="file"
                                                ref={"eventTypePicture"}
                                                className="form-control-file"
                                                id="eventTypePicture"
                                            />
                                            <hr className="hr"/>
                                            <label className={"label-text"}>Event Type Picture</label>
                                            <h5 className={"text-color "}>(MAX 100MB)</h5>
                                        </div>

                                        {/*<RaisedButton*/}
                                            {/*label="Submit" primary*/}
                                            {/*className="btn-w-md category-submit-button"*/}
                                            {/*onClick={this._handleSubmit.bind(this)}*/}
                                        {/*/>*/}

                                        <button
                                            type="button"
                                            className="btn btn-primary btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        >Submit</button>
                                </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

export default EventTypeCreate;