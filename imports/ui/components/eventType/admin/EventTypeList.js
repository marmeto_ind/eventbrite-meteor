import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {EventType} from "../../../../api/eventType";
import { MuiDataTable } from 'mui-data-table';

function _handleDelete(data) {
    const eventTypeId = data._id;
    EventType.remove({_id: eventTypeId}, function (err) {
        if(err){
            console.log(err);
        } else {
            console.log("Record removed successfully");
            window.location.reload();
        }
    });
}

class EventTypeList extends React.Component {


    render() {
        const eventTypes = this.props.eventTypes;
        console.log(eventTypes);

        if(eventTypes.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Event Type</h3>
                    <div className={"new-button"}>
                        <a href="/admin/event-type/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No Data avilable</div>
                </div>
            )
        }

        let config = {
            paginated:true,
            search: 'name',
            data: eventTypes,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                {title: 'View', renderAs: function(data) {
                    return (<a href={"/admin/event-type/"+ data._id}>View</a>);
                }},
                {title: 'Update', renderAs: function(data) {
                    return (<a href={"/admin/event-type/update/"+ data._id}>Edit</a>);
                }},
                {title: 'Delete', renderAs: function(data) {
                    return (<button onClick={() => (_handleDelete(data))}>delete</button>);
                }},
            ],
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Event Type</h3>
                <div className={"new-button"}>
                    <a href="/admin/event-type/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <MuiDataTable config={config} />
            </div>

        )
    }
}


export default withTracker(() => {
    const eventTypes = EventType.find({}).fetch();
    return {
        eventTypes: eventTypes,
    };
})(EventTypeList);