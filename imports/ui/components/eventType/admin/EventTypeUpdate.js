import React from 'react';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {EventType} from "../../../../api/eventType";
import {browserHistory} from 'react-router';
// import RaisedButton from 'material-ui/RaisedButton';

class EventTypeUpdate extends React.Component {

    constructor(props) {
        super(props);
    }


    _handleSubmit(e) {
        e.preventDefault();
        let newEventType = ReactDOM.findDOMNode(this.refs.newEventType).value.trim();
        let eventTypeId = this.props.eventType._id;


        // Update the category to the database
        EventType.update({_id: eventTypeId},{name: newEventType}, function (err, numOfUpdate) {
            if(err) {
                console.log("Error Occured");
            } else {
                console.log(numOfUpdate);
                browserHistory.push('/admin/event-type');
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newEventType).value = '';
        console.log("submit button clicked");
    }

    render() {
        const category = this.props.category;
        // console.log(category);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Update event type</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newEventType"
                                                            // defaultValue={category.name}
                                                            ref="newEventType"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newEventType">Event Type</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {/*<RaisedButton*/}
                                            {/*label="Submit" primary*/}
                                            {/*className="btn-w-md category-submit-button"*/}
                                            {/*onClick={this._handleSubmit.bind(this)}*/}
                                        {/*/>*/}

                                        <button
                                            type="button"
                                            className="btn btn-primary btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        >Submit</button>
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    // console.log(data);
    const eventType = EventType.findOne({_id: data.eventTypeId});
    // console.log(eventType);
    return {
        eventType: eventType,
    };
})(EventTypeUpdate);
