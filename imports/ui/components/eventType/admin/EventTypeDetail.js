import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { withTracker } from 'meteor/react-meteor-data';
import {EventType} from "../../../../api/eventType";

class EventTypeDetail extends React.Component {

    render() {
        const eventType = this.props.eventType;

        if (!eventType) {
            return (
                <div>Loading</div>
            )
        } else {
            if(eventType.picture) {
                const collectionName = eventType.picture.collectionName;
                const imageName = eventType.picture.original.name;
                const imageId = eventType.picture._id;
                var imageUrl = '/uploads/images/'+collectionName+ '-'+ imageId+ '-'+ imageName;
            }

            return (
                <section className="container-fluid with-maxwidth chapter">
                    <QueueAnim type="bottom" className="ui-animate">
                        <div key="1">
                            <article className="article">
                                <h2 className="add-new-seller">View Event Type</h2>
                                <div className="box box-default">
                                    <div className="box-body padding-xl">
                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Name:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{eventType? eventType.name: ''}</h5>
                                            </div>
                                        </div>

                                        <div className={"row content-name"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Image:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>
                                                    {imageUrl?
                                                        <img src={imageUrl} alt="" width={"50px"} height={"50px"}/>
                                                        : ''}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </QueueAnim>
                </section>
            )
        }
    }
}


export default withTracker((data) => {

    const eventType = EventType.findOne({_id: data.eventTypeId});

    return {
        eventType: eventType,
    };

})(EventTypeDetail);
