import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import {EventType} from "../../../../api/eventType";
import RaisedButton from 'material-ui/RaisedButton';


class EachEventTypeInList extends React.Component {

    _handleDelete(e) {
        console.log("Delete button clicked");
        const eventTypeId = this.props.eventType._id;
        EventType.remove({_id: eventTypeId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("category successfully removed");
            }
        });
    }


    render() {
        // console.log(this.props.category);
        const eventType = this.props.eventType;

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {eventType.name}
                    <a href={"/admin/event-type/"+ eventType._id}><RaisedButton label="View" primary /></a>
                    <a href={"/admin/event-type/update/"+ eventType._id}><RaisedButton label="Edit" primary /></a>
                    <RaisedButton label="Delete" primary  onClick={this._handleDelete.bind(this)}/>
                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachEventTypeInList;