import React from 'react';
import './header.scss';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showAddNewUserIcon: false,
            showAddNewServiceIcon: false,
            showUserIcon: false,
            showSettingsIcon: false,
            userSignupActive: false,
            userLoginActive: false,
            newServiceActive: false,
            headerBackgroundActive: false,
        }
    }

    componentDidMount(){
        let pathname = window.location.pathname;
        if (pathname === "/user/signup"){
            this.setState({userSignupActive: true});
            this.setState({headerBackgroundActive:true});
        } else if (pathname === "/login"){
            this.setState({userLoginActive: true});
            this.setState({headerBackgroundActive:true});
        } else if (pathname === "/vendor/new-service"){
            this.setState({newServiceActive: true});
            this.setState({headerBackgroundActive:true});
        } else if (pathname === "/admin/signup"){
            this.setState({headerBackgroundActive:true});
        } else{
            this.setState({showActiveClass: false});
        }
    }

    logInpopUpFunction() {
        var logInpopUp = document.getElementById("mylogInpopUp");
        var vendorlogInpopUp = document.getElementById("vendormylogInpopUp");
        logInpopUp.classList.toggle("show");
        vendorlogInpopUp.classList.remove("show");
    };

    vendorlogInpopUpFunction() {
        var vendorlogInpopUp = document.getElementById("vendormylogInpopUp");
        var logInpopUp = document.getElementById("mylogInpopUp");
        vendorlogInpopUp.classList.toggle("show");
        logInpopUp.classList.remove("show");
    };

    render() {
        return(
            <header className={!this.state.headerBackgroundActive?'home-header':'home-header background_none'}>
                <div className={" header-container"}>
                    <div className={"header-div"}>
                        <div  id={"dashboard_headerLogo"} className={"pull-left col-md-8"}>
                            <div className={"row"}>
                                <div>
                                    <a href="/" className={"floatleft"} >
                                        <img className="header-logo" src="/img/white-logo.svg" alt="" />
                                        <h1 className="header-text">WENDELY</h1>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id={"dashboard_keyIcon"} className={"pull-right col-md-4 no-padding"}>
                            <nav className="home-header-nav home-header-nav-new">
                                <ul className={"floatright"}>
                                    <li className="home-header-nav-item signup">
                                        <a href="/user/signup" className={this.state.userSignupActive?'homeheaderNavlink signup_icon active':'homeheaderNavlink signup_icon'}>
                                            {/* <img src="/img/new-user.svg" alt="" /> */}
                                            &nbsp;
                                            <span className="toltips">New User</span>
                                        </a>
                                    </li>
                                    
                                    <li className="home-header-nav-item">
                    
                                        <a href="/vendor/new-service" title="Vendor Registration" className={this.state.newServiceActive?'homeheaderNavlink new_newservice active':'homeheaderNavlink new_newservice'}>
                                            {/* <img src="/img/new-service.svg" alt="" /> */}
                                            &nbsp;
                                            <span className="toltips">New Service</span>
                                        </a>
                                    </li>
                                    
                                    <li className="home-header-nav-item">
                                        <a href="/login" title="User Login" className={this.state.userLoginActive ?'homeheaderNavlink new_login_icon active':'homeheaderNavlink new_login_icon'}>
                                            {/* <img src="/img/key.svg" alt="" /> */}
                                            &nbsp;
                                            <span className="toltips">Log in</span>
                                        </a>
                                    </li>
                                    
                                    {this.state.showSettingsIcon?
                                    <li className="home-header-nav-item">
                                        <a href="/login" title="Settings" className={this.state.userLoginActive ?'homeheaderNavlink setting_icon':'homeheaderNavlink setting_icon'}>
                                            {/* <img src="/img/alternative.svg" alt="" /> */}
                                            &nbsp;
                                            <span className="toltips">Settings</span>
                                        </a>
                                    </li>
                                    :""}
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;

