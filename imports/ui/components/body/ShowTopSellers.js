import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Sellers} from "../../../api/sellers";
import {browserHistory} from 'react-router';

class GoldenStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/10.png" alt="" />
            </span>
        )
    }
}

class WhiteStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/11.png" alt="" />
            </span>
        )
    }
}

class ShowFacilityIcons extends React.Component {
    constructor(props){
        super(props);
        this.state={
            image: '',
        };
    }

    componentDidMount(){
        const self = this;
        const facility = this.props.facility;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                if (result) {
                    self.setState({image: result.image})
                }
            }
        })
    }

    render() {
        return (
            <span>
                <img className="icon-img" src={this.state.image} alt="" />
            </span >
        )
    }
}

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: '',
        };
    }

    handleSellerClick(e) {
        e.preventDefault();
        const sellerId = this.props.seller._id;
        browserHistory.push('/user/service/' + sellerId);
    }

    renderSellerFacilities(facilities) {
        return facilities.map((facility, index)=>(
            <ShowFacilityIcons facility={facility} key={index} />
        ))
    }

    componentDidMount(){
        const self = this;
        const sellerId = this.props.seller._id;
        Meteor.call('reviews.getSellerRating', sellerId, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({review: result});
            }
        })
    }

    renderRating(){
        const review = this.state.review;
        const extra = 5 - review;
        var reviewComponent = [];
 
        for (var i=0; i < review; i++) {
            reviewComponent.push(<GoldenStar key={i} />);
        }
        for (var j=0; j < extra; j++) {
            reviewComponent.push(<WhiteStar key={i + j} />);
        }

        return <div className="rating">{reviewComponent}</div>;
    }

    render() {
        const seller = this.props.seller;
        const mainImage = seller.profile.mainImage;

        const divStyle = {
            backgroundImage: `url("${mainImage}")`,
            backgroundSize: 'cover'
        };

        return (
            <div className="top-body-container relativeposition">
                <div className="content_container" style={divStyle}>
                    <div className="clearfix"></div>
                    <div className="vale_container home-tick guest_home_page dshboardPage">
                        <div className="mychoice_data  my-data" onClick={this.handleSellerClick.bind(this)}>
                            <div className="value-main-content">
                                <div className={"shadow-box-main-div"}>
                                    <div className={"shadow-box-div"}>
                                        <h3 className={"choice-text-style"}>{seller.name}</h3>
                                        <p className={"choice-text-style"}>{seller.profile.address}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="overlap_image_container">
                        {seller.profile.hotTag? <img src="/img/tags_icon/TAGS_HOT.png" alt="tag" />: ''}
                        {seller.profile.goodDealTag? <img src="/img/tags_icon/TAGS_GOOD_DEAL.png" alt="tag" />: ''}
                        {seller.profile.newTag? <img src="/img/tags_icon/TAGS_NEW.png" alt="tag" />: ''} 
                    </div>
                </div>
            </div>
        )
    }
}

class ShowSellerAdd extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const divStyle = {
            backgroundImage: 'url(/img/body_bac_k.jpg)',
            backgroundSize: 'cover',
        };

        return (
            <div  style={divStyle}  className="top-body-container relativeposition">
                <div className="clearfix"></div>
                <div className="vale_container home-tick  dshboardPage">
                    <div className="mychoice_data  my-data">
                        <div className="value-main-content">
                            <div className={"shadow-box-main-div"}>
                                <div className={"shadow-box-div"}>
                                    <h3 className={"choice-text-style"}>ADVERTISEMENT</h3>
                                    <p className={"choice-text-style"}>50% OFF</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ShowSellers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryName: 'ALL',
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const self = this;
    }

    renderSellers() {
        const sellers = this.props.sellers.slice(1, 5);
        return sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index} />
        ))
    }

    renderTopSeller() {
        const sellers = this.props.sellers.slice(0, 1);
        return sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index} />
        ))
    }

    render() {

        return (
            <div className="content_wrapper">
                <div className="venu_wrapper">
                    <div className={"dash-new"}>
                        {this.props.categoryName || "ALL"}
                    </div>
                    <div className="clearfix"></div>
                    {this.props.sellers.length > 0?
                    <div className="venu_container">
                        {this.renderTopSeller()}
                    </div>
                     :""}
                    <div className="advertisment_content">
                        <ShowSellerAdd />
                    </div>
                </div>
                <div className="services_wrapper">
                    <div>
                        <div className={"dash-new"}>
                            SERVICES
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    {this.renderSellers()}
                </div>
                <div className="clarfix"></div>
            </div>
        )
    }
}

export default ShowSellersContainer = withTracker((data) => {
    const categoryId = data.categoryId;
    const currentPage = data.currentPage;
    const recordToSkip = currentPage * 5;
    Meteor.subscribe('sellers');
    if(!categoryId) {
        var sellers = Sellers.find({"isApprove": true}, {sort: { createdAt: -1 }, skip: recordToSkip, limit: 5}).fetch();
        //console.log("Sellers by cat= ");
        //console.log(sellers);
    } else {
        var sellers = Sellers.find({"$and":[{"category._id": categoryId},{"isApprove": true}]}, {sort: { createdAt: -1 }, skip: recordToSkip, limit: 5}).fetch();
        //console.log("Sellers all= ");
        //console.log(sellers);
    }
    return {
        sellers: sellers,
    };
})(ShowSellers);
