import React from 'react';
import ReactPaginate from 'react-paginate';
import ShowSellers from './ShowSellers';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Sellers} from "../../../api/sellers";

class ShowSellersWithPagination extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            numOfPage: '',
            currentPage: 0,
        }
    }

    handlePageClick = (data) => {
        this.setState({
            currentPage: data.selected,
            numOfPage: this.props.numOfPage,
        });
    };

    render() {
        return (
            <div>
                
                <ShowSellers currentPage={this.state.currentPage} categoryId={this.props.categoryId}/>
                <div className={"floatright"}>

                {this.props.numOfPage > 0 ?


                    <ReactPaginate previousLabel={<img src="/img/left-arrow.png" alt=""/>}
                                   nextLabel={<img src="/img/right-arrow.png" alt=""/>}
                                   breakLabel={<a href="">...</a>}
                                   breakClassName={"break-me"}
                                   pageCount={0}
                                   marginPagesDisplayed={0}
                                   pageRangeDisplayed={0}
                                   onPageChange={this.handlePageClick}
                                   containerClassName={"pagination"}
                                   subContainerClassName={"pages pagination"}
                                   activeClassName={"active"} />
                    :
                    ''
                }
                </div>

            </div>
        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('sellers');
    let categoryId = '';
    let numOfPage = Math.floor((numOfSeller - 1) / 5 + 1);
    if(data.categoryId) {
        categoryId = data.categoryId;
    }
    if(!data.categoryId) {
        var numOfSeller = Sellers.find().count();
    } else {
        var numOfSeller = Sellers.find({"category._id": data.categoryId}).count();
    }
    return {
        numOfPage,
        categoryId
    }
})(ShowSellersWithPagination);
