import React from 'react';
import './body-part.scss';
import BodyTopBar from './BodyTopBar';
import BodySideBar from '../userLeftSideBar/LeftSideBar';
// import ShowSellersWithPagination from './ShowSellersWithPagination';
import ShowTopSellers from './ShowTopSellersWithPagination';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
// import { Meteor } from 'meteor/meteor';

class BodyPart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
            categoryId: '',
            categoryName: ''
        };
        this.parentCallBack = this.parentCallBack.bind(this);
    }

    _handleSearchKeyChange(event){
        let searchKey = event.target.value;
        this.setState({searchKey: searchKey});
    }

    _handleCategorySelect(category) {
        let {_id, name} = category;
        this.setState({categoryId: _id});
        this.setState({categoryName: name});
    }

    componentDidMount() {
        $("#mobile_topBar_createButton").click(function(){
            $("#userTopBar").toggle();
        });
    }

    parentCallBack(option) {
        let {searchKey } = option;
        this.setState({searchKey})
    }

    render(){
        
        let categories = this.props.categories;
        let sellers = this.props.sellers;

        return(
            <div className="body-cntnr main-container">
                <div  id={"mobile_topBar_createButton"} className="col-lg-12 col-xs-12">
                    <button>Create Event</button>
                </div>
                <div id={"userTopBar"}  className="col-lg-12 col-xs-12">
                    <div className="bottom-menu-style">
                        <BodyTopBar/>
                        <div className="clearfix"></div>
                        <div className={"row"}>
                        </div>
                    </div>
                </div>
                <div className="clearfix"></div>                
                <div className="clearfix"></div>
                <div className="body_container">
                    <BodySideBar
                        sellers={sellers}
                        categories={categories}
                        searchKey={this.state.searchKey}
                        parentCallBack={this.parentCallBack.bind(this)}
                        callbackFromParent={this._handleCategorySelect.bind(this)}
                    />
                    <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 body_portion_cn ">
                        <div className="guest_user_wrapper">
                            <div className="tab-content">
                                <div  id={"dashboard_homeImg"} className="body_portion welcome_msg">
                                    <p>WELCOME TO <span>WENDELY</span> WEDDING PLANER</p>
                                </div>
                            </div>
                            <div>
                                <div className={"dash-new"}>
                                    OFFERS
                                </div>
                                <div className="clearfix"></div>
                                {/* <div className={"no-padding "}>
                                    
                                    <img className={""} src={"/img/line.svg"} />
                                </div> */}
                            </div>
                            <div className="tab-content tab-content_carousel">
                                <div className="overlap_image_container">
                                    <img src="../img/tags_icon/TAGS_HOT.png" alt="tag" />
                                    <img src="../img/tags_icon/TAGS_GOOD_DEAL.png" alt="tag" />
                                    <img src="../img/tags_icon/TAGS_NEW.png" alt="tag" />
                                </div>
                                <Carousel>
                                    <div id={"dashboard_homeImg"} className="body_portion">
                                        
                                        <img src={"/img/body_bac_k.jpg"} />
                                    </div>
                                    <div id={"dashboard_homeImg"} className="body_portion">
                                        <img src={"/img/body_bac_k.jpg"} />
                                    </div>
                                    <div id={"dashboard_homeImg"} className="body_portion">
                                        <img src={"/img/body_bac_k.jpg"} />
                                    </div>
                                </Carousel>
                            </div>
                            <ShowTopSellers  categoryId={this.state.categoryId} categoryName={this.state.categoryName}/>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        )
    }
}

export default BodyPart;

