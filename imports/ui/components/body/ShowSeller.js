import React from 'react';

class ShowSeller extends React.Component {
    render() {

        const seller = this.props.seller;
        // console.log(seller);
        // const categoryName = seller.category.name;
        const sellerName = seller.name;

        return (
            <li color="secondary">
                <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                <span id={"seller"}>{sellerName}</span>
                {/*<span id={"category"}>{categoryName}</span>*/}
                <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
            </li>
        )
    }
}

export default ShowSeller;