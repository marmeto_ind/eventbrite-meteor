import React from 'react';
import RegionSelect from './RegionSelect';
// import EventTypeSelect from './EventTypeSelect';
// import DateSelect from './DateSelect';
import {Meteor} from 'meteor/meteor';
// import DatePicker from 'react-date-picker'
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { toast, ToastContainer } from 'react-toastify';
import {browserHistory} from 'react-router';

class BodyTopBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            region: '',
            eventType: '',
            date: 'WHEN?',
            guestNumber: '',
        };
        this._callbackFromRegionSelect = this._callbackFromRegionSelect.bind(this);
        this._callbackFromEventTypeSelect = this._callbackFromEventTypeSelect.bind(this);
        this._handleEventCreate = this._handleEventCreate.bind(this);
    }

    componentDidMount() {
        $('.region-dropdown .Select-input input').attr('readonly', true);
        $('.DayPickerInput input').attr('readonly', true);
    }

    _handleEventCreate(event){
        const user = Meteor.userId();
        const eventName = $('#eventName').val();
        const region = this.state.region;
        const date = this.state.date.toString();
        const eventType = this.state.eventType;
        const numberOfGuest = $('#numOfGuest').val();
        
        if(!user) {
            browserHistory.push("/login");
        } else if (!eventName) {
            this.errorNotify("Event Name cannot be empty");
        } else if (!region) {
            this.errorNotify("Region is not selected");
        } else if (!numberOfGuest) {
            this.errorNotify("Number of guest cannot be empty");
        } else if(date.length <= 4) {
            this.errorNotify("Date can not be empty");
        } else {
            Meteor.call('events.insert', eventName, region, eventType, date, numberOfGuest, user);
            this.successNotify("event created Successfully");
            browserHistory.push('/user/chat');
        }
    }

    _callbackFromRegionSelect(data) {
        this.setState({region: data.label});
    }

    _callbackFromEventTypeSelect(data) {
        this.setState({eventType: data.label});
    }

    _selectDate(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;
        if(day !== currentDate) {
            this.setState({date: day});
        }
    }

    successNotify = (message) => {
        toast.success(message, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (message) => {
        toast.error(message, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    sunday(day) {
        return day.getDay() === 0;
    }

    handleDayClick(day) {
        // console.log(day);
    }

    handleInputChange(event) {
        const value = event.target.value;
        if(value < 999999) {
            this.setState({guestNumber: event.target.value})
        }
    }

    render() {
        // const today = new Date();
        return (
            <div className="bottom-menu-container">
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <div className={"col-md-4 height_100"}>
                            <div className="search-container home-search-div height_100 padding-10 top-0">
                                <form className="height_100">
                                    <input id={"eventName"} className="dashboard-search placeholder-fix height_100  dashboard-search-input-box" 
                                    type="text" placeholder="COUPLE IN LOVE" name="event_name" />
                                </form>
                            </div>
                        </div>
                        <div className={"col-md-6  home-topBar-position no-padding height_100"}>
                            <ul className={"homePage_topBar_blockWidth"}>
                                <li className={"relativePosition height_100"}>
                                    <div className="fix-bar-option">
                                        <RegionSelect callbackFromParent={this._callbackFromRegionSelect}/>
                                    </div>
                                </li>
                                <li className={"dashboard-date homePageDateposition relativePosition height_100"}>
                                    <div className="fix-bar-option dateDropDown">
                                        <DayPickerInput
                                            onDayChange={day=> this._selectDate(day)}
                                            value={this.state.date}
                                            dayPickerProps={{ disabledDays: {before: new Date()} }}
                                        />
                                    </div>
                                </li>
                                <li className={"dashboard-guest  user_guest_padding relativePosition height_100"}>
                                    <div className="fix-bar-option guestDropDown">
                                        <form>
                                            <input
                                                id={"numOfGuest"}
                                                className="dashboard-guest guest-placeholder-fix"
                                                type="number"
                                                placeholder="HOW MANY?"
                                                name="num_of_guest"
                                                value={this.state.guestNumber}
                                                onChange={this.handleInputChange.bind(this)}
                                            />
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className={"col-md-2 home-header-col-style-right"}>
                                    
                            <button className="created_button" onClick={this._handleEventCreate}>CREATE</button>
                            <ToastContainer autoClose={1000000000000000}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BodyTopBar;

