import React, { Component } from 'react'
import 'font-awesome/css/font-awesome.min.css';
import EventDataSelected from '../eventTopBar/EventDataSelected';
import GuestLoginAdvertisement from '../advertisement/GuestLoginAdvertisement';
import SellerDetail from '../sellerListing/SellerDetail';
import LoginLeftCategory from '../leftCategory/LoginLeftCategory';
// import OnHoverItemBar from '../hoverItemBar/OnHoverItemBar';
import ChatItemBar from '../hoverItemBar/ChatItemBar';
// import ChatNoteItemBar from '../hoverItemBar/ChatNoteItemBar';

class DashboardBody extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    _callbackFromLoginLeftCategory(data) {
        console.log(data);
    }

    render(){
        return(
            <div>
                <EventDataSelected/>
                <div className="body-cntnr-dash main-container">
                    <div className="clearfix"></div>
                    <div className="body_container">

                        <GuestLoginAdvertisement/>

                        <div className="clearfix"></div>
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>
                            <div className="clearfix"></div>

                            <LoginLeftCategory/>

                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 body_portion_cn">
                            <ChatItemBar/>

                            <SellerDetail/>

                            <div className="spacer"> &nbsp; </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>
            </div>

        )
    }
}


export default DashboardBody;