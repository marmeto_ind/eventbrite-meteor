import React from 'react'
// import ReactDOM from 'react-dom'
// import DatePicker_m from 'react-date-picker';
import BannerSlider from './BannerSlider';
// import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
// import $ from 'jquery';
// import YouTube from 'react-youtube';
// import {
//     BrowserRouter as Router,
//     Route,
//     Link
// } from 'react-router-dom'


class Business extends React.Component {

    render(){
        const opts = {
            height: '200',
            width: '100%',
            playerVars: { // https://developers.google.com/youtube/player_parameters

            }
        };
        return (
            <div>

                <div className="body-cntnr-dash main-container">
                    <div className="clearfix"></div>
                    <div className="Business-profile-header">
                        <div className="col-lg-3">
                            <div className="header-user-name">
                                Hanna & Montana
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="header-item">
                                <ul>
                                    <li>Amsterdam</li>
                                    <li>Wedding</li>
                                    <li>Sat 26 fab 2018</li>
                                    <li>500</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3">
                            <div className="header-action-icon">
                                <ul>
                                    <li><i className="fas fa-comments"></i><span className="count">3</span></li>
                                    <li><i className="far fa-calendar-edit"></i> &nbsp;</li>
                                    <li><i className="fas fa-pencil-alt"></i>&nbsp;</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="top-body-container">
                        <div className="col-lg-4">
                            <div className="search_item">
                                <input type="text" placeholder="Search Venue" className="search_item_textfield" />

                            </div>
                        </div>
                        <div className="col-lg-8 place-header">
                            <h2>Venue</h2>
                            <hr className="hr" />
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="body_container_business">
                        <div className="col-lg-4">
                            <div className="left-side-menu-bussiness">
                                <ul>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">ahstatic</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                </ul>
                            </div>

                        </div>

                        <div className="Business-profile-conainer col-lg-8">
                            <div className="mydash_bannner">
                                <div className="body_portion_banner">
                                    <div className="banner">
                                        <BannerSlider />
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"></div>


                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 body_portion_ev res-no-padding">
                                <div className="event-right-side-element">
                                    <div className="upper-section">
                                        <div className="gallery">
                                            <img src="https://taj.tajhotels.com/content/dam/luxury/hotels/Taj_Mahal_Delhi/images/4x3/HotelFacade4x3.jpg" />
                                            <img src="https://www.kayak.co.in/rimg/himg/be/d0/c6/leonardo-1315494-Hotel_Exterior_-_Night_View_S-image.jpg?width=500&height=350&crop=true" />
                                            <img src="https://www.ahstatic.com/photos/6182_ho_00_p_2048x1536.jpg" />
                                            <img src="https://hotelimages.webjet.com.au/hotels/1000000/30000/22200/22136/22136_312_z.jpg" />
                                            <img src="https://media-cdn.tripadvisor.com/media/photo-o/0e/d5/8e/98/hotel-carlos-i.jpg" />
                                            <img src="https://www.safarihotelsnamibia.com/wp-content/uploads/2014/11/Safari-Court-Hotel-Pool.jpg" />
                                            <img src="http://streamafrica.com/wp-content/uploads/2016/01/hotels-4.jpg" />
                                        </div>
                                        <div className="circle-panorama">
                                            <div className="img_crcl_b"><img src="" /></div>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="button_evet">
                                        <button className="button_evet_left">Chat Now</button>
                                        <span className="hidden_space">&nbsp;</span>
                                        <button className="button_evet_right">My Choice <span className="circle_m"><i className="fa fa-check" aria-hidden="true"></i></span></button>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="black_container no-padding">
                                        <input type="checkbox" className="check" id="check" />
                                        <div className="check_box_item">
                                            <div className="freeparking div_same">freeparking</div>
                                            <div className="somking_area div_same">Somking area</div>
                                            <div className="free_w_f_i div_same">Free Wi-Fi</div>
                                            <div className="elavator div_same">Elavator</div>
                                            <div className="celling div_same">4,5 m to celling</div>
                                            <div className="m2 div_same">2000 m2</div>
                                            <div className="bar div_same">Bar</div>
                                            <div className="stage div_same">Stage</div>
                                            <div className="kitchen div_same">Kitchen</div>
                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container same-icon">
                                                <p className="star-print"><i className="far fa-star"></i></p>
                                                <div className="heading-text">
                                                    <h2>STANDARD PACK</h2>
                                                    <p>&nbsp;</p>
                                                </div>
                                                <h4>Baptzr/Main Courses/Dessert</h4>
                                                <b>Welcome drink / chocolate</b>
                                                <p>Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container same-icon">
                                                <p className="star-print"><i className="far fa-star"></i></p>
                                                <h2>STANDARD PACK</h2>
                                                <h4>Baptzr/Main Courses/Dessert</h4>
                                                <b>Welcome drink / chocolate</b>
                                                <p>Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container same-icon">
                                                <p className="star-print"><i className="far fa-star"></i></p>
                                                <h2>STANDARD PACK</h2>
                                                <h4>Baptzr/Main Courses/Dessert</h4>
                                                <b>Welcome drink / chocolate</b>
                                                <p>Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container same-icon">
                                                <p className="star-print"><i className="far fa-star"></i></p>
                                                <h2>STANDARD PACK</h2>
                                                <h4>Baptzr/Main Courses/Dessert</h4>
                                                <b>Welcome drink / chocolate</b>
                                                <p>Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                            </div>

                                        </div>
                                    </div>

                                    <div className="box-elemnt">

                                    </div>
                                    {/* <div className="box-elemnt">
                                        <YouTube
                                            videoId="2g811Eo7K8U"
                                            opts={opts}
                                            onReady={this._onReady}
                                        />
                                    </div> */}
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container email">
                                                <h2>Hanna.Montana@gmail.com</h2>
                                                <p>Mail</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container contact">
                                                <h2>+86 - 328746746643</h2>
                                                <p>Phone</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container website">
                                                <h2>Panoramafest.se</h2>
                                                <p>Website</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="black_container no-padding">
                                        <div className="check_box_item full-width-design">
                                            <div className="box-container credential">
                                                <h2>2365455 - 74465</h2>
                                                <p>Credential</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="box-section-bussiens">


                                </div>


                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>

                </div>

            </div>

        )
    }
}

export default Business;