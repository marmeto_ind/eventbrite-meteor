import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { withTracker } from 'meteor/react-meteor-data';
import {EventType} from "../../../api/eventType";

class EventTypeSelect extends React.Component {
    state = {
        selectedOption: '',
    };

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        console.log(selectedOption?`Selected: ${selectedOption.label}`: 'Option Cleared');
        this.props.callbackFromParent(selectedOption);
    };

    _createDropDownOption = (inputEventTypes) => {
        let dropDownOption = [];
        inputEventTypes.forEach(function (data) {
            dropDownOption.push({value: data._id, label: data.name});
        });
        return dropDownOption;
    };

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        const options = this._createDropDownOption(this.props.eventTypes);

        return (
            <div className={"region-dropdown eventDropDown"}>
                <Select name="form-field-name" value={value} onChange={this.handleChange}
                        options={options} placeholder={"Event"}
                />
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('event_types');

    return {
        eventTypes: EventType.find({}).fetch(),
    }
})(EventTypeSelect);
