import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';


class BannerSlider extends Component {
    render() {
        return (
            <Carousel autoPlay>
                <div>
                    <img src="https://fnqnaturetours.com.au/wp-content/uploads/2017/10/Rainforest-scenery-slider-1024x400.jpg" />
                </div>
                <div>
                    <img src="http://www.visitgeres.com/sites/visitgeres.com/files/banner/EN-Via%20Romana%20-%20Mata%20da%20Albergaria.1024x400.jpg" />
                </div>
                <div>
                    <img src="https://visittucsonorg.s3-us-west-1.amazonaws.com/s3fs-public/styles/slideshow/public/aravaipa-birders2_dan-sidle.jpg?itok=a2AYUc9v" />

                </div>
                <div>
                    <img src="http://tinyblackbird.com/wp-content/uploads/2014/10/Dusun-KL-Getaway-Hotels-via-TinyBlackBird.com_-1024x400.jpg" />
                </div>
                <div>
                    <img src="http://fnqnaturetours.com.au/wp-content/uploads/2017/11/Mossman-Gorge-Slider-1024x400.jpg" />
                </div>
                <div>
                    <img src="http://www.sankarpat.com/images/main3.jpg" />
                </div>
            </Carousel>
        )
    }
}

export default BannerSlider;