import React from 'react'
// import ReactDOM from 'react-dom'
// import DatePicker_m from 'react-date-picker';
// import BannerSlider from './BannerSlider';
// import $ from 'jquery';
// import YouTube from 'react-youtube';
// import {
//     BrowserRouter as Router,
//     Route,
//     Link
// } from 'react-router-dom'


class UserServiceBody extends React.Component {
render(){
return (
			<div className="body-cntnr main-container">
				<div className="clearfix"></div>
				<div className="col-lg-12 col-xs-12 bottom-menu-cntnr">
                    <div className="bottom-menu">
						<div className="col-lg-3">
                            <div className="header-user-name">
                                Hanna & Montana
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="header-item">
                                <ul>
                                    <li>Amsterdam</li>
                                    <li>Wedding</li>
                                    <li>Sat 26 fab 2018</li>
                                    <li>500</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3">
                            <div className="header-action-icon">
                                <ul>
                                    <li><i className="fas fa-comments"></i><span className="count">3</span></li>
                                    <li><i className="far fa-calendar-edit"></i> &nbsp;fds</li>
                                    <li><i className="fas fa-pencil-alt"></i>&nbsp;dfsdf</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
				<div className="body_container_business">
					<div className="userservice_container">
						
						<div className="col-lg-4">
							<div className="top-body-container">
								<div className="">
									<div className="search_item">
										<input type="text" placeholder="Search Vendor" className="search_item_textfield" />
									</div>
								</div>
								
							</div>
                            <div className="left-side-menu-bussiness">
                                <ul>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">ahstatic</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                    <li><a href="#">kbsjvbdfhb</a></li>
                                </ul>
                            </div>

                        </div>
						<div className="col-lg-8">
							<div className="top-body-container">
								<div className="place-header">
									<h4>My choice</h4>
									<hr className="hr" />
								</div>
								<div className="clearfix"></div>
								<div className="vale_container">
									<div className="mychoice_data">
										<div className="value-main-content">
											<div className="star"><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i></div>
											<h3>Assyriska Eventcenter</h3>
											<p>Sodertalje</p>
											<div className="icon_div">
												<span><i className="fal fa-comment"></i></span>
												<span><i className="fal fa-play-circle"></i></span>
												<span><i className="far fa-wifi"></i></span>
												<span><i className="far fa-utensils"></i></span>
												<span><i className="fal fa-comment"></i></span>
											</div>
											<div className="div-float">
												<p>Sold Out</p>
												<p>Best Deal</p>
												<p>Hot</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="clearfix"></div>
							<div className="top-body-container">
								<div className="place-header">
									<h4>Venues</h4>
									<hr className="hr" />
								</div>
								<div className="clearfix"></div>
								<div className="vale_container">
									<div className="mychoice_data">
										<div className="value-main-content">
											<div className="star"><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i></div>
											<h3>Assyriska Eventcenter</h3>
											<p>Sodertalje</p>
											<div className="icon_div">
												<span><i className="fal fa-comment"></i></span>
												<span><i className="fal fa-play-circle"></i></span>
												<span><i className="far fa-wifi"></i></span>
												<span><i className="far fa-utensils"></i></span>
												<span><i className="fal fa-comment"></i></span>
											</div>
											<div className="div-float">
												<p>Best Deal</p>
												<p>Hot</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="clearfix"></div>
							
							<div className="top-body-container">
								<br/>
								<div className="value_image">
									<img src="https://ksr-ugc.imgix.net/assets/000/413/492/3d4798e8f5e7352a8e7b493c0c818f50_original.jpg?w=680&fit=max&v=1362078301&auto=format&q=92&s=b9e9bb18383e06989a0de907e8f4a6ad" alt="images" />
								</div>
								<br/>
								<div className="clearfix"></div>
								<div className="vale_container">
									<div className="mychoice_data">
										<div className="value-main-content">
											<div className="star"><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i></div>
											<h3>Assyriska Eventcenter</h3>
											<p>Sodertalje</p>
											<div className="div-float-top">
												<p>Best Deal</p>
												<p>Best Deal</p>
												<p>Best Deal</p>
											</div>
											<div className="icon_div">
												<span><i className="fal fa-comment"></i></span>
												<span><i className="fal fa-play-circle"></i></span>
												<span><i className="far fa-wifi"></i></span>
												<span><i className="far fa-utensils"></i></span>
												<span><i className="fal fa-comment"></i></span>
											</div>
											<div className="div-float">
												<p>Best Deal</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div className="clearfix"></div>
				<br/>
			</div>
);

}

}

export default UserServiceBody;