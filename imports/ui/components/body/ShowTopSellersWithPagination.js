import React from 'react';
import ShowSellers from './ShowTopSellers';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Sellers} from "../../../api/sellers";

class ShowSellersWithPagination extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            numOfPage: '',
            currentPage: 0,
        }
    }

    handlePageClick = (data) => {
        this.setState({
            currentPage: data.selected,
            numOfPage: this.props.numOfPage,
        });
    };

    render() {
        return (
            <div>
                <ShowSellers 
                    currentPage={this.state.currentPage} 
                    categoryName={this.props.categoryName} 
                    categoryId={this.props.categoryId} />
                <div className={"floatright"}></div>
            </div>
        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('sellers');
    let categoryId = '';
    let numOfPage = Math.floor((numOfSeller - 1) / 5 + 1);
    if(data.categoryId) {
        categoryId = data.categoryId;
    }
    if(!data.categoryId) {
        var numOfSeller = Sellers.find().count();
    } else {
        var numOfSeller = Sellers.find({"category._id": data.categoryId}).count();
    }
    return {
        numOfPage,
        categoryId
    }
})(ShowSellersWithPagination);
