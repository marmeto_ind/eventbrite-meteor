import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import ShowSeller from './ShowSeller';

class BodySidebar extends React.Component {
    renderSellers() {
        const sellers = this.props.sellers;
        return sellers.map((seller, index) => (
            <ShowSeller seller={seller} key={index}/>
        ));
    }

    render() {
        return (
            <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">
                {/*<div className="responsive_btn_v">*/}
                {/*<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">*/}
                {/*<span className="icon-bar"></span>*/}
                {/*<span className="icon-bar"></span>*/}
                {/*<span className="icon-bar"></span>*/}
                {/*</button>*/}
                {/*</div>*/}

                <div className="clearfix"></div>
                <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                    <ul className="nav">

                        {this.renderSellers()}

                    </ul>
                </div>
            </div>
        )
    }
}

export default withTracker((data) => {
    const searchKey = data.searchKey;
    Meteor.subscribe('sellers');
    if(!searchKey) {
        return {
            sellers: Sellers.find({}, {fields: {"name": 1}}).fetch(),
        };
    }  else {
        return {
            sellers: Sellers.find({"name": {$regex : searchKey}},
                {fields: {"name": 1}}).fetch(),
        }
    }
}) (BodySidebar);

