import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Sellers} from "../../../api/sellers";
import {browserHistory} from 'react-router';

class GoldenStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/10.png" alt="" />
            </span>
        )
    }
}

class WhiteStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/11.png" alt="" />
            </span>
        )
    }
}

class ShowFacilityIcons extends React.Component {
    constructor(props){
        super(props);
        this.state={
            image: '',
        };
    }

    componentDidMount(){
        const self = this;
        const facility = this.props.facility;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                if (result) {
                    self.setState({image: result.image})
                }
            }
        })
    }

    render() {
        return (
            <span>
                <img className="icon-img" src={this.state.image} alt="" />
            </span >
        )
    }
}

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: '',
        };
    }

    handleSellerClick(e) {
        e.preventDefault();
        const sellerId = this.props.seller._id;
        browserHistory.push('/user/service/' + sellerId);
    }

    renderSellerFacilities(facilities) {
        // console.log(facilities);
        return facilities.map((facility, index)=>(
            <ShowFacilityIcons facility={facility} key={index} />
        ))
    }

    componentDidMount(){
        const self = this;
        const sellerId = this.props.seller._id;
        // console.log(sellerId);
        Meteor.call('reviews.getSellerRating', sellerId, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                // console.log(result);
                self.setState({review: result});
            }
        })
    }

    renderRating(){
        const review = this.state.review;
        const extra = 5 - review;
        var reviewComponent = [];
 
        for (var i=0; i < review; i++) {
            reviewComponent.push(<GoldenStar key={i} />);
        }
        for (var j=0; j < extra; j++) {
            reviewComponent.push(<WhiteStar key={i + j} />);
        }

        return <div className="rating">{reviewComponent}</div>;
    }

    render() {
        const seller = this.props.seller;
        const mainImage = seller.profile.mainImage;

        const divStyle = {
            backgroundImage: 'url(' + mainImage + ')',
            backgroundSize: 'cover',
            marginBottom: '20px',
        };

        return (
            <div  style={divStyle}  className="top-body-container relativeposition user_dashboard_wrapper">
                <div className="clearfix"></div>
                <div className="vale_container home-tick  dshboardPage">
                    <div className="mychoice_data  my-data" onClick={this.handleSellerClick.bind(this)}>
                        <div className="value-main-content">
                            <div className={"main-image"}>
                                {/*<img src={seller.profile.mainImage} alt="main image"/>*/}
                            </div>
                            <div className={"shadow-box-main-div"}>
                                <div className={"greyBackground"}>
                                </div>
                                <div className={"shadow-box-div"}>
                                    <div className="circle-panorama">
                                        <div className="img_crcl_b">
                                            <img className={"choice-circle-img"} src={seller.profile.logoImage} alt={"Thumbnail Images"} />
                                        </div>
                                    </div>

                                    <div className="circle-star-margin">
                                        {this.renderRating()}
                                    </div>
                                    <img src={"/img/mobile_searcBox_line.png"} />

                                    <h3 className={"choice-text-style"}>{seller.name}</h3>
                                    <p className={"choice-text-style"}>{seller.profile.address}</p>
                                    <div className="icon_div">
                                        {this.renderSellerFacilities(seller.profile.facilities)}
                                    </div>
                                </div>
                            </div>
                            <div className="div-float second-choice-div">
                                <p className={"second-best-deal best-deal"}>Best Deal</p>
                                <p className={"second-hot hot"}>Hot</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ShowSellers extends React.Component {
    renderSellers() {
        const sellers = this.props.sellers;

        return sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index} />
        ))
    }

    render() {
        return (
            <div>
                {this.renderSellers()}
            </div>
        )
    }
}

export default ShowSellersContainer = withTracker((data) => {
    const categoryId = data.categoryId;
    const currentPage = data.currentPage;
    const recordToSkip = currentPage * 5;
    Meteor.subscribe('sellers');
    if(!categoryId) {
        var sellers = Sellers.find({}, {sort: { createdAt: -1 }, skip: recordToSkip, limit: 5}).fetch();
    } else {
        var sellers = Sellers.find({"category._id": categoryId}, {sort: { createdAt: -1 }, skip: recordToSkip, limit: 5}).fetch();
    }
    return {
        sellers: sellers,
    };
})(ShowSellers);
