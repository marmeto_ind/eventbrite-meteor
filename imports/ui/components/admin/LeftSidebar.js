import React from 'react';

class LeftSidebar extends React.Component {

    render() {
        return (
            <div className={"left-sidebar list-top"}>

                {/*<a href="/admin/seller" className={"admin-menu"}>*/}
                    {/*<span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>*/}
                    {/*<span>Seller</span>*/}
                    {/*<span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>*/}
                {/*</a>*/}

                <a href="/admin/region" className={"admin-menu"}>
                    <span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    <span>Region</span>
                    <span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                </a>

                <a href="/admin/category" className={"admin-menu"}>
                    <span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    <span>Category</span>
                    <span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                </a>

                {/*<a href="/admin/event" className={"admin-menu"}>*/}
                    {/*<span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>*/}
                    {/*<span>Event</span>*/}
                    {/*<span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>*/}

                {/*</a>*/}

                <a  href="/admin/event-type" className={"admin-menu"}>
                    <span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    <span>Event</span>
                    <span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                </a>

                <a href="/admin/seller-facility" className={"admin-menu"}>
                    <span className={"category-icon1"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    <span>Facility</span>
                    <span className={"category-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                </a>
            </div>
        );
    }
}

export default LeftSidebar;