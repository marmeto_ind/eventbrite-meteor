import React from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import {Meteor} from 'meteor/meteor'
import {browserHistory} from 'react-router';

_handleLogOut = (e) => {
    e.preventDefault();
    // console.log("logout button clicked");
    Meteor.logout((err) => {
        if(err) {
            console.log(err);
        } else {
            console.log("logout successfull");
            browserHistory.push('/login');
        }
    })
};

_handleDashboardClick = (e) => {
    e.preventDefault();
    // console.log("Dashboard button clicked");
    browserHistory.push("/admin");
};

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <a href="#" className="logo"><img src="/img/logo_new.png" alt="" />
                </a>
                <h1 className="logo-text">WENDELY</h1>

                <nav className="header-nav">
                    <ul>
                        <li className="header-nav-item"><a href="#" className="header-nav-link"><img className="logo2" src="/img/user1.png" alt="" /></a></li>
                        <li className="header-nav-item"><a href="#" className="header-nav-link"></a><img className="logo1" src="/img/settings.png" alt="" /></li>
                    </ul>
                </nav>
            </header>

        )
    }
}

export default Header;