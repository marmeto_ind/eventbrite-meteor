import React from 'react';
import SellerDetail from './SellerDetail';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Categories} from "../../../api/categories";

class SellerList extends React.Component {

    renderSellers() {
        return this.props.sellers.map((seller) => (
            <SellerDetail key={seller._id} seller={seller}/>
        ));
    }

    render() {
        return (
            <div>
                {this.renderSellers()}
            </div>
        )
    }
}


export default withTracker((data) => {

    let categoryId = data.category;
    let category = Categories.findOne({_id: categoryId});

    if(category) {
        let categoryName = category.name;
        return {
            sellers : Sellers.find({"category": categoryName}).fetch(),
        };
    }

    return {
        sellers : Sellers.find({}).fetch(),
    };
})(SellerList);