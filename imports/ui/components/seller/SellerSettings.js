import React from 'react';
import ReactDOM from 'react-dom';
import {Sellers} from "../../../api/sellers";

class SellerAddBusiness extends React.Component {
    onSubmit(event) {
        event.preventDefault();

        let name = ReactDOM.findDOMNode(this.refs.name).value.trim();
        let category = ReactDOM.findDOMNode(this.refs.category).value.trim();
        let credential = ReactDOM.findDOMNode(this.refs.credential).value.trim();
        let address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        let phoneNumber = ReactDOM.findDOMNode(this.refs.phoneNumber).value.trim();
        let website = ReactDOM.findDOMNode(this.refs.website).value.trim();
        let subscription = ReactDOM.findDOMNode(this.refs.subscription).value.trim();
        let debitCard = ReactDOM.findDOMNode(this.refs.debitCard).value.trim();
        let timeZone = ReactDOM.findDOMNode(this.refs.timeZone).value.trim();

        Sellers.insert({
            name: name,
            category: category,
            credential: credential,
            address: address,
            phoneNumber: phoneNumber,
            website: website,
            subscription: subscription,
            debitCard: debitCard,
            timeZone: timeZone,
            createdAt: new Date(), // Data inserted time
        }, function (err) {
            console.log(err);
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.name).value = '';
        ReactDOM.findDOMNode(this.refs.category).value = '';
        ReactDOM.findDOMNode(this.refs.credential).value = '';
        ReactDOM.findDOMNode(this.refs.address).value = '';
        ReactDOM.findDOMNode(this.refs.phoneNumber).value = '';
        ReactDOM.findDOMNode(this.refs.website).value = '';
        ReactDOM.findDOMNode(this.refs.subscription).value = '';
        ReactDOM.findDOMNode(this.refs.debitCard).value = '';
        ReactDOM.findDOMNode(this.refs.timeZone).value = '';

    }

    render() {
        return (
            <div>
                <form>
                    <label htmlFor="">
                        Business Name:
                        <input type="text" ref="name" name="name" />
                    </label><br/>

                    <label htmlFor="">
                        Category:
                        <input type="text" ref="category" name="category" />
                    </label><br/>

                    <label htmlFor="">
                        ORGANISATION NR:
                        <input type="text" ref="credential" name="credential" />
                    </label><br/>

                    <label htmlFor="">
                        Address:
                        <input type="text" ref="address" name="address" />
                    </label><br/>

                    <label htmlFor="">
                        Phone Number:
                        <input type="text" ref="phoneNumber" name="phoneNumber" />
                    </label><br/>

                    <label htmlFor="">
                        Website:
                        <input type="text" ref="website" name="website" />
                    </label><br/>

                    <label htmlFor="">
                        Subscription:
                        <input type="text" ref="subscription" name="subscription" />
                    </label><br/>

                    <label htmlFor="">
                        Debit Card:
                        <input type="text" ref="debitCard" name="debitCard" />
                    </label><br/>

                    <label htmlFor="">
                        Time Zone:
                        <input type="text" ref="timeZone" name="timeZone" />
                    </label><br/>

                    <input type="submit" value="Submit" onClick={this.onSubmit.bind(this)}/>
                </form>
            </div>
        )
    }
}

export default SellerAddBusiness;