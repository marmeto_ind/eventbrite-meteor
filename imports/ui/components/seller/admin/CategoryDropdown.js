import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Categories} from "../../../../api/categories";


class EachCategory extends React.Component {

    render() {
        const category = this.props.category;
        // console.log(category);

        return (
            <option value={category._id}>{category.name}</option>
        )
    }
}


class CategoryDropdown extends React.Component {
    renderCategories() {
        // console.log(this.props);

        return this.props.categories.map((category) => (
            <EachCategory key={category._id} category={category}/>
        ));
    }



    render() {
        const style = {
            height: "100%",
        };

        return (
            <div className="form-group">
                <div className={"row input-name"}>
                    <div className={"col-md-1"}>
                        <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                    </div>
                    <div className={"col-md-11"}>
                        <div className={"row input-name-box "}>
                            <select className="form-control select-category" id="category" ref="category" style={style}>
                                {this.renderCategories()}
                            </select>
                        </div>
                        <div className={"row"}>
                            <label className={"input-name-text input-name-text1"} htmlFor="businessName">Category</label>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}


export default withTracker(() => {
    Meteor.subscribe('categories');

    return {
        categories: Categories.find({}).fetch(),
    };
})(CategoryDropdown);

