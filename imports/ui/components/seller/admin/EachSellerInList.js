import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import {Sellers} from "../../../../api/sellers";
import RaisedButton from 'material-ui/RaisedButton';

class EachSellerInList extends React.Component {
    _handleDelete(e) {
        console.log("Delete button clicked");
        const sellerId = this.props.seller._id;
        Sellers.remove({_id: sellerId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("category successfully removed");
            }
        });
    }

    render() {
        const seller = this.props.seller;

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {seller.name}

                    <a href={"/admin/seller/"+ seller._id}>
                        <RaisedButton label="View" primary />
                    </a>

                    <a href={"/admin/seller/update/"+ seller._id}>
                        <RaisedButton label="Edit" primary />
                    </a>

                    <RaisedButton label="Delete"
                                  primary
                                  onClick={this._handleDelete.bind(this)}
                    />
                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachSellerInList;