import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import QueueAnim from 'rc-queue-anim';
import EachSellerInList from './EachSellerInList';
import SellerHeader from './SellerHeader';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../api/sellers";
import RaisedButton from 'material-ui/RaisedButton';
import { MuiDataTable } from 'mui-data-table';
import RefreshLoading from '../../Loading';


class ShowSellers extends React.Component{
    renderEachSeller() {
        return this.props.sellers.map((seller) => (
            <EachSellerInList key={seller._id} seller={seller}/>
        ));
    }

    render() {
        return (
            <article className="article">
                <h2 className="article-title">Admin seller list page</h2>
                <a href={"/admin/seller/new"}><RaisedButton label="Add" primary /></a>
                <Table>
                    <TableHeader>
                        <SellerHeader/>
                    </TableHeader>
                    <TableBody>
                        {this.renderEachSeller()}
                    </TableBody>
                </Table>
            </article>
        )
    }
}

function _handleDelete(data) {
    console.log(data);
    const sellerId = data._id;
    Sellers.remove({_id: sellerId}, function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log('Record removed successfully');
            window.location.reload();
        }
    })
}

class SellerList extends React.Component {

    render() {
        const sellers = this.props.sellers;

        console.log(sellers);

        if(sellers.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Seller</h3>
                    <div className={"new-button"}>
                        <a href="/admin/seller/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No data available</div>
                </div>
            )
        }

        const config = {
            paginated:true,
            search: 'name',
            data: sellers,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                { title: 'Category', renderAs: function (data) {
                    return data.category.name;
                } },
                { property: 'address', title: 'Address' },
                {title: 'View', renderAs: function (data) {
                    return (<a href={"/admin/seller/"+ data._id}>View</a>);
                }},
                {title: 'Edit', renderAs: function (data) {
                    return (<a href={"/admin/seller/update/"+ data._id}>Edit</a>);
                }},
                {title: 'Delete', renderAs: function (data) {
                    return (<button onClick={() => _handleDelete(data)}>Delete</button>)
                }},
            ],
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Seller</h3>
                <div className={"new-button"}>
                    <a href="/admin/seller/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <MuiDataTable config={config} />
            </div>
        )
    }
}


export default withTracker(() => {
    return {
        sellers: Sellers.find({}).fetch(),
    };
})(SellerList);