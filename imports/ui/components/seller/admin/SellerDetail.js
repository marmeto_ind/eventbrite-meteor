import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../api/sellers";
import {Images} from "../../../../api/images";
import {Videos} from "../../../../api/videos";
import QueueAnim from 'rc-queue-anim';

class Facility extends React.Component {
    render() {
        return (
            <div>{this.props.facility.name}</div>
        )
    }
}

class ShowFacilities extends React.Component {
    renderFacilities() {
        const facilities = this.props.data;
        return facilities.map((facility) => (
            <Facility key={facility._id} facility={facility}/>
        ))
    };

    render() {
        return (
            <div>{this.renderFacilities()}</div>
        )
    }
}

class Image extends React.Component {
    render() {
        // console.log(this.props);

        const id = this.props.imageId;
        // console.log(this.props);
        // console.log(id);

        if(id) {
            const picture = Images.findOne({_id: id});
            // console.log(picture);

            const imageName = picture.name();
            const imageId = picture._id;
            const collectionName = picture.collectionName;
            const imageUrl = '/uploads/images/'+collectionName+ '-'+ imageId+ '-'+ imageName;

            // console.log(imageUrl);

            return (
                <div>
                    {imageUrl?
                        <img src={imageUrl} alt="" width={"50px"} height={"50px"}/>
                        : ''}
                </div>
            )
        }

    }
}

class SellerOtherImages extends React.Component {
    renderOtherImages(images) {
        return images.map((image, index) => (
            <Image key={index} imageId={image}/>
        ))
    }

    render() {
        const otherImages = this.props.data;
        return (
            <div>{this.renderOtherImages(otherImages)}</div>
        );
    }
}


class SellerDetail extends React.Component {
    
    render() {
        const seller = this.props.seller;
        console.log(seller);

        if(!seller) {
            return (
                <div className={"data-loading"}>Loading...</div>
            )
        } else {

            const sellerFacilities = seller.facilities;
            // console.log(seller);

            if(seller.logoPicture) {
                const logoPicture = Images.findOne({_id: seller.logoPicture});
                const imageName = logoPicture.name();
                const imageId = logoPicture._id;
                const collectionName = logoPicture.collectionName;
                var logoImageUrl = '/uploads/images/'+collectionName+ '-'+ imageId+ '-'+ imageName;
            }

            if(seller.mainPicture) {
                const logoPicture = Images.findOne({_id: seller.mainPicture});
                const imageName = logoPicture.name();
                const imageId = logoPicture._id;
                const collectionName = logoPicture.collectionName;
                var mainImageUrl = '/uploads/images/' + collectionName+ '-' + imageId+ '-' + imageName;
            }


            if(seller.sellerVideo) {
                const sellerVideo = Videos.findOne({_id: seller.sellerVideo});
                const videoName = sellerVideo.name();
                const videoId = sellerVideo._id;
                const collectionName = sellerVideo.collectionName;
                var mainVideoUrl = '/uploads/videos/' + collectionName+ '-' + videoId+ '-' + videoName;
            }


            return (
                <section className="container-fluid with-maxwidth chapter">
                    <QueueAnim type="bottom" className="ui-animate">
                        <div key="1">
                            <article className="article">
                                <h2 className="add-new-seller">View Seller</h2>
                                <div className="box box-default">
                                    <div className="box-body padding-xl">

                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Name: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.name? seller.name: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style"> Category: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.category? seller.category.name: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">  Credential: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.credential? seller.credential: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Debit Card Number: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.debit_card_number? seller.debit_card_number: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">First Pack: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.firstPack? seller.firstPack: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Second Pack: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.secondPack? seller.secondPack: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">  Third Pack:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}> {seller.thirdPack? seller.thirdPack: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style"> Subscription: </h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{seller.subscription? seller.subscription: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style"> Timezone:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}> {seller.timezone? seller.timezone: ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">SellerFacilities</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}> {seller.facilities? <ShowFacilities data={seller.facilities}/> : ''}</h5>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">LogoImage:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <div className={"logo-image"}>
                                                    {logoImageUrl?
                                                    <img src={logoImageUrl} alt="" width={"50px"} height={"50px"}/>
                                                    : ''}
                                                </div>
                                            </div>
                                        </div>



                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">MainImage:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <div className={"main-image"}>
                                                    {mainImageUrl?
                                                    <img src={mainImageUrl} alt="" width={"50px"} height={"50px"}/>
                                                    : ''}
                                                </div>
                                            </div>
                                        </div>


                                        <div className={"row content-name seller-detail-bottom"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">OtherPictures</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <div className={"other-images"}>
                                                    {seller.otherPictures ? <SellerOtherImages data={seller.otherPictures}/>: ''}
                                                </div>
                                            </div>
                                        </div>

                                        {/*<div className={"seller-video"}>*/}
                                            {/*<a href={mainVideoUrl} title="video">seller video</a>*/}
                                        {/*</div>*/}

                                    </div>
                                </div>
                            </article>
                        </div>
                    </QueueAnim>
                </section>
            )
        }

    }
}

export default withTracker((data) => {

    const sellerId = data.sellerId;

    return {
        seller: Sellers.findOne({_id: sellerId}),
    };
})(SellerDetail);