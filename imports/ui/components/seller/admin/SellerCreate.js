import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Sellers} from "../../../../api/sellers";
import {Images} from "../../../../api/images";
import {Videos} from "../../../../api/videos";
import $ from 'jquery';
import SellerFacilityComponent from '../../sellerFacility/SelectSellerFacility';
import CategoryDropdown from './CategoryDropdown';
import {browserHistory} from 'react-router';

class SellerCreate extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();

        // initialize all variable
        let businessName = '';
        let credentials = '';
        let address = '';
        let phoneNumber = '';
        let subscription = '';
        let debitCard = '';
        let timeZone = '';
        let firstPack = '';
        let secondPack = '';
        let thirdPack = '';
        let mainPicture = '';
        let logoPicture = '';
        let otherPictures = '';
        let sellerVideo = '';
        let sellerFacilities = '';

        // fetch all data from the form
        businessName = ReactDOM.findDOMNode(this.refs.businessName).value.trim();
        credentials = ReactDOM.findDOMNode(this.refs.credentials).value.trim();
        address = ReactDOM.findDOMNode(this.refs.address).value.trim();
        phoneNumber = ReactDOM.findDOMNode(this.refs.phoneNumber).value.trim();
        subscription = ReactDOM.findDOMNode(this.refs.subscription).value.trim();
        debitCard = ReactDOM.findDOMNode(this.refs.debitCard).value.trim();
        timeZone = ReactDOM.findDOMNode(this.refs.timeZone).value.trim();

        firstPack = ReactDOM.findDOMNode(this.refs.firstPack).value.trim();
        secondPack = ReactDOM.findDOMNode(this.refs.secondPack).value.trim();
        thirdPack = ReactDOM.findDOMNode(this.refs.thirdPack).value.trim();

        // upload main picture and save image link
        const mainPictureFile = $("#sellerMainPicture")[0].files[0];
        const sellerLogoPictureFile = $("#sellerLogoPicture")[0].files[0];
        const sellerOtherPictureFiles = $("#sellerOtherPictures")[0].files;
        const sellerVideoFile = $("#sellerVideo")[0].files[0];

        const categoryId = $("#category").val();
        const categoryName = $("#category option:selected").text();

        console.log(categoryId);
        console.log(categoryName);

        const category = { _id: categoryId, name: categoryName};

        // Upload picture to server if image is selected

        if(mainPictureFile) {
            mainPicture = Images.insert(mainPictureFile, function (err, mainPicture) {
                if (err) {
                    console.log(err);
                } else {
                }
            });
        }

        // upload logo if image is selected
        if(sellerLogoPictureFile) {
            logoPicture = Images.insert(sellerLogoPictureFile, function (err, logoPicture) {
                if (err) {
                    console.log(err);
                } else {
                    // console.log(mainPicture._id);
                }
            });
        }


        if(sellerOtherPictureFiles.length > 0) {

            otherPictures = [];

            for (var index = 0; index < sellerOtherPictureFiles.length; ++index) {
                let pictureFile = sellerOtherPictureFiles[index];
                const picture = Images.insert(pictureFile, function (err, picture) {
                    if(err) {
                        console.log(err);
                    }
                });
                // console.log(picture._id);
                console.log(picture);
                otherPictures.push(picture._id);
            }
        }

        // upload seller video if video file is selected
        if(sellerVideoFile) {

            sellerVideo = Videos.insert(sellerVideoFile, function (err, video) {
                if (err) {
                    console.log(err);
                }
            });
        }

        // Initialized seller facilities object
        sellerFacilities = [];
        const selectedFacilities = $("input:checked");

        // get the id the the selected facilities and save it
        for (let index = 0; index < selectedFacilities.length; ++index) {
            let item = selectedFacilities[index];
            let itemId = item.id;

            let jquerySelector = "label[for='" + itemId.toString() + "']";
            let value = $(jquerySelector).text();
            let facility = {};

            facility["_id"] = item.id;
            facility["name"] = value;

            sellerFacilities.push(facility);
            // console.log(sellerFacilities);
        }

        // Insert seller data to the database
        // Sellers.insert({
        //     name: businessName,
        //     category: category,
        //     credential: credentials,
        //     address: address,
        //     phone_number: phoneNumber,
        //     subscription: subscription,
        //     debit_card_number: debitCard,
        //     timezone: timeZone,
        //
        //     firstPack: firstPack,
        //     secondPack: secondPack,
        //     thirdPack: thirdPack,
        //
        //     mainPicture: mainPicture._id,
        //     logoPicture: logoPicture._id,
        //     otherPictures: otherPictures,
        //     sellerVideo: sellerVideo._id,
        //
        //     facilities: sellerFacilities,
        //
        // }, function (err) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log("seller added successfully");
        //         browserHistory.push('/admin/seller');
        //     }
        // });

        Meteor.call('sellers.insert', businessName, category);

    }


    render() {
        return (

            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <article className="article">
                        <h2 className="add-new-seller">Add new seller</h2>
                        <div className="box box-default">
                            <div className="box-body padding-xl ">

                                <form role="form">
                                    <div className="logo-picture main-picture form-group">
                                        <label htmlFor="sellerMainPicture"><img id="hi" className={"pic-logo"} src="/img/add.png" alt="" /></label>
                                        <input
                                            type="file"
                                            ref={"sellerMainPicture"}
                                            className="form-control-file"
                                            id="sellerMainPicture"
                                        />
                                        <hr className="hr"/>
                                        <label className={"label-text"}>Main Picture</label>
                                        <h5 className={"text-color "}>(MAX 100MB)</h5>
                                    </div>

                                    <div className="logo-picture form-group">
                                        <label  htmlFor="sellerLogoPicture"><img  className={"pic-logo"} src="/img/add.png" alt="" /></label>
                                        <input
                                            type="file"
                                            ref={"sellerLogoPicture"}
                                            className="form-control-file"
                                            id="sellerLogoPicture"
                                        />
                                        <hr className="hr"/>
                                        <label className={"label-text"}>Logo Picture</label>
                                        <h5 className={"text-color "}>(MAX 100MB)</h5>
                                    </div>

                                    <div className="logo-picture form-group">
                                        <div>
                                            <label htmlFor="sellerOtherPictures"><img  className={"pic-logo"} src="/img/add.png" alt="" /></label>
                                            <input
                                                type="file"
                                                multiple
                                                ref={"sellerOtherPictures"}
                                                className="form-control-file"
                                                id="sellerOtherPictures"

                                            />
                                        </div>

                                        <hr className="hr" />
                                        <label className={"label-text"}>Other Pictures</label>
                                        <h5 className={"text-color "}>(MAX 100MB)</h5>
                                    </div>

                                    <div className="logo-picture form-group">
                                        <label htmlFor="sellerVideo"><img  className={"pic-logo"} src="/img/add.png" alt="" /></label>
                                        <input
                                            type="file"
                                            ref={"sellerVideo"}
                                            className="form-control-file"
                                            id="sellerVideo"
                                        />
                                        <hr className="hr"/>
                                        <label className={"label-text"}>Video</label>
                                        <h5 className={"text-color "}>(MAX 100MB)</h5>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="businessName"
                                                        placeholder="Business Name"
                                                        ref="businessName"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Business Name</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <CategoryDropdown/>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="credentials"
                                                        placeholder="Enter Credentials"
                                                        ref="credentials"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Credentials</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="address"
                                                        placeholder="Enter Address"
                                                        ref="address"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Address</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="phoneNumber"
                                                        placeholder="Enter Phone Number"
                                                        ref="phoneNumber"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Number</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="subscription"
                                                        placeholder="Enter Subscription"
                                                        ref="subscription"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Subscription</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="debitCard"
                                                        placeholder="Enter Debit Card Number"
                                                        ref="debitCard"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Debit Card</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="timeZone"
                                                        placeholder="Enter Time Zone"
                                                        ref="timeZone"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Time Zone</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                   <textarea
                                                       className="form-control"
                                                       id="firstPack"
                                                       ref={"firstPack"}
                                                       rows="3"
                                                       placeholder={"Enter Pack Details"}
                                                   >
                                        </textarea>
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">First Pack</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                   <textarea
                                                       className="form-control"
                                                       id="secondPack"
                                                       ref={"secondPack"}
                                                       rows="3"
                                                       placeholder={"Enter Pack Details"}
                                                   >
                                        </textarea>
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Second Pack</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box "}>
                                                    <textarea
                                                        className="form-control"
                                                        id="thirdPack"
                                                        ref={"thirdPack"}
                                                        rows="3"
                                                        placeholder={"Enter Pack Details"}
                                                    >
                                        </textarea>
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="businessName">Third Pack</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <SellerFacilityComponent/>

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-w-md button1"
                                        onClick={this._handleSubmit.bind(this)}
                                    >Submit</button>

                                </form>

                            </div>
                        </div>
                    </article>
                </div>
                </QueueAnim>
            </section>
        )
    }
}


export default SellerCreate;