import React from 'react';
import {Images} from "../../../api/images";
import $ from 'jquery';


class SellerAddBusiness extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();
    }

    render() {
        return (
            <div>
                <h2>Vendor add business page</h2>

                <form>

                    <div className="form-group">
                        <label htmlFor="sellerMainPicture">Main Picture</label>
                        <input
                            type="file"
                            ref={"sellerMainPicture"}
                            className="form-control-file"
                            id="sellerMainPicture"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="sellerLogoPicture">Logo Picture</label>
                        <input
                            type="file"
                            ref={"sellerLogoPicture"}
                            className="form-control-file"
                            id="sellerLogoPicture"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="sellerOtherPictures">Other Pictures</label>
                        <input
                            type="file"
                            ref={"sellerOtherPictures"}
                            className="form-control-file"
                            id="sellerOtherPictures"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="sellerVideo">Video</label>
                        <input
                            type="file"
                            ref={"sellerVideo"}
                            className="form-control-file"
                            id="sellerVideo"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="firstPackName">First pack name</label>
                        <input
                            type="text"
                            ref={"firstPackName"}
                            className="form-control-file"
                            id="firstPackName"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="firstPackFirstOffer">First pack first offer</label>
                        <input
                            type="text"
                            ref={"firstPackFirstOffer"}
                            className="form-control-file"
                            id="firstPackFirstOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="firstPackSecondOffer">First pack second offer</label>
                        <input
                            type="text"
                            ref={"firstPackSecondOffer"}
                            className="form-control-file"
                            id="firstPackSecondOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="firstPackThirdOffer">First pack third offer</label>
                        <input
                            type="text"
                            ref={"firstPackThirdOffer"}
                            className="form-control-file"
                            id="firstPackThirdOffer"
                        />
                    </div>



                    <div className="form-group">
                        <label htmlFor="secondPackName">Second pack name</label>
                        <input
                            type="text"
                            ref={"secondPackName"}
                            className="form-control-file"
                            id="secondPackName"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="secondPackFirstOffer">Second pack first offer</label>
                        <input
                            type="text"
                            ref={"secondPackFirstOffer"}
                            className="form-control-file"
                            id="secondPackFirstOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="secondPackSecondOffer">Second pack second offer</label>
                        <input
                            type="text"
                            ref={"secondPackSecondOffer"}
                            className="form-control-file"
                            id="secondPackSecondOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="secondPackThirdOffer">Second pack third offer</label>
                        <input
                            type="text"
                            ref={"secondPackThirdOffer"}
                            className="form-control-file"
                            id="secondPackThirdOffer"
                        />
                    </div>


                    <div className="form-group">
                        <label htmlFor="ThirdPackName">Third pack name</label>
                        <input
                            type="text"
                            ref={"ThirdPackName"}
                            className="form-control-file"
                            id="ThirdPackName"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="thirdPackFirstOffer">Third pack first offer</label>
                        <input
                            type="text"
                            ref={"thirdPackFirstOffer"}
                            className="form-control-file"
                            id="thirdPackFirstOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="thirdPackSecondOffer">Third pack second offer</label>
                        <input
                            type="text"
                            ref={"thirdPackSecondOffer"}
                            className="form-control-file"
                            id="thirdPackSecondOffer"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="thirdPackThirdOffer">Third pack third offer</label>
                        <input
                            type="text"
                            ref={"thirdPackThirdOffer"}
                            className="form-control-file"
                            id="thirdPackThirdOffer"
                        />
                    </div>


                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack1"/>
                            <label className="form-check-label" htmlFor="sellerPack1">
                                4, 5 m ceiling height
                            </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack2"/>
                        <label className="form-check-label" htmlFor="sellerPack2">
                            2000 m2
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack3"/>
                        <label className="form-check-label" htmlFor="sellerPack3">
                            Free wifi
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack4"/>
                        <label className="form-check-label" htmlFor="sellerPack4">
                            kitchen
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack5"/>
                        <label className="form-check-label" htmlFor="sellerPack5">
                            Bar
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack6"/>
                        <label className="form-check-label" htmlFor="sellerPack6">
                            Smoking Area
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack7"/>
                        <label className="form-check-label" htmlFor="sellerPack7">
                            Free Parking
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack8"/>
                        <label className="form-check-label" htmlFor="sellerPack8">
                            Stage
                        </label>
                    </div>

                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" value="" id="sellerPack9"/>
                        <label className="form-check-label" htmlFor="sellerPack9">
                            Elevator
                        </label>
                    </div>

                    <input type="submit" onClick={this._handleSubmit.bind(this)}/>
                </form>
            </div>
        )
    }
}

export default SellerAddBusiness;

