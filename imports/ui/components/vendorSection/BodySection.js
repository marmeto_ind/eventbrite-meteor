import React from 'react';
import LeftCategory from './LeftCategory';
import VendorAgenda from './Agenda';

class BodyPart extends React.Component {

    render(){

        return(
            <div className="body-cntnr main-container">
                <div className="clearfix"></div>
                <div className="body_container seller-body">
                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">

                        <div className="clearfix"></div>
                        <LeftCategory/>

                    </div>
                    <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 body_portion_cn">
                        <div className="tab-content">
                            <VendorAgenda/>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>

            </div>

        )
    }
}

export default BodyPart;
