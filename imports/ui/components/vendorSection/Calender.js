import React from 'react';
import $ from 'jquery';
import 'jquery-ui';
import 'moment/min/moment.min'

import 'fullcalendar/dist/fullcalendar.css';
import 'fullcalendar/dist/fullcalendar';

class Calendar extends React.Component {
    componentDidMount(){

        $('#calendar').fullCalendar({header : {
            left : "prev",
            center : "title",
            right : "next"
        }});
    }

    render() {
        return (
            <div id="calendar"></div>
        )
    }
}

class App extends React.Component {
    render() {

        // let events = [
        //     {
        //         start: '2017-01-06',
        //         end: '2017-01-08',
        //         rendering: 'background',
        //         color: '#00FF00',
        //     },
        // ]

        return (
            <div className="App">
                <Calendar />
            </div>
        );
    }
}

export default App;