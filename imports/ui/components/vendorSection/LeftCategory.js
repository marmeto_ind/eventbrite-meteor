import React from 'react';

class CategoryList extends React.Component {

    render() {
        return (
            <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                <ul className="nav">
                    <li color="secondary">Agenda</li>
                    <li color="secondary">My Service</li>
                    <li color="secondary">Chat</li>
                    <li color="secondary">Statistics</li>
                    <li color="secondary">Settings</li>
                </ul>
            </div>
        )
    }
}

export default CategoryList;