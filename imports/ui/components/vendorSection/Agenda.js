import React from 'react';
import Calender from './Calender';

class Agenda extends React.Component {
    render() {
        return (
            <div className={"vendor-agenda"}>
                <div className={"booking-detail"}>
                    <div className={"user-name"}>Hanna Montana</div>
                    <div className={"booking-day"}>Saturday</div>
                    <div className={"booking-time"}>KL 21:00</div>
                    <div className={"pack"}>Exclusive Pack</div>
                    <div className={"num-of-guest"}>(430)</div>
                </div>
                <div className={"agenda-calender"}>
                    <Calender/>
                </div>
            </div>
        )
    }
}

export default Agenda;