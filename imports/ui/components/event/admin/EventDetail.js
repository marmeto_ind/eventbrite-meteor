import React from 'react';
import QueueAnim from 'rc-queue-anim';
// import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../../api/events";

class EventDetail extends React.Component {

    render() {
        const event = this.props.event;
        console.log(event);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">View Event</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">
                                    <div className={"row content-name seller-detail-bottom"}>
                                        <div className={"col-md-4 border-style"}>
                                            <h5 className="name-style">Name:</h5>
                                        </div>
                                        <div className={"col-md-8"}>
                                            <h5 className={"name-style"}>{event? event.name: ''}</h5>
                                        </div>
                                    </div>

                                    <div className={"row content-name seller-detail-bottom"}>
                                        <div className={"col-md-4 border-style"}>
                                            <h5 className="name-style"> Region: :</h5>
                                        </div>
                                        <div className={"col-md-8"}>
                                            <h5 className={"name-style"}>{event? event.region: ''}</h5>
                                        </div>
                                    </div>

                                    <div className={"row content-name seller-detail-bottom"}>
                                        <div className={"col-md-4 border-style"}>
                                            <h5 className="name-style">Number Of Guest:</h5>
                                        </div>
                                        <div className={"col-md-8"}>
                                            <h5 className={"name-style"}>{event? event.numOfGuest: ''}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    console.log(data);

    const event = Events.findOne({_id: data.eventId});
    console.log(event);
    return {
        event: event,
    };

})(EventDetail);
