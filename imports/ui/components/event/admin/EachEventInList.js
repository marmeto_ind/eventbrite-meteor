import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {Events} from "../../../../api/events";

class EachCategoryInList extends React.Component {
    _handleDelete(e) {
        console.log("Delete button clicked");
        const eventId = this.props.event._id;
        Events.remove({_id: eventId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("category successfully removed");
            }
        });
    }

    render() {
        const event = this.props.event;

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {event.name}

                    <a href={"/admin/event/"+ event._id}><RaisedButton label="View" primary /></a>
                    <a href={"/admin/event/update/"+ event._id}><RaisedButton label="Edit" primary /></a>
                    <RaisedButton label="Delete" primary  onClick={this._handleDelete.bind(this)}/>
                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachCategoryInList;