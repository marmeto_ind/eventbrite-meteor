import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../../api/events";
import {browserHistory} from 'react-router';

class EventUpdate extends React.Component {

    constructor(props) {
        super(props);
    }


    _handleSubmit(e) {

        e.preventDefault();

        let eventId = this.props.eventId;

        let region = ReactDOM.findDOMNode(this.refs.region).value.trim();
        let eventType = ReactDOM.findDOMNode(this.refs.eventType).value.trim();
        let date = ReactDOM.findDOMNode(this.refs.date).value.trim();
        let numOfGuest = ReactDOM.findDOMNode(this.refs.numOfGuest).value.trim();


        // Insert the new category to the database
        Events.update({
            _id: eventId
        },{
            region: region,
            eventType: eventType,
            date: date,
            numOfGuest: numOfGuest,
        }, function (err) {
            if(err) {
                console.log(err);
            } else {
                console.log("New Event inserted");
                browserHistory.push('/admin/event');
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.region).value = '';
        ReactDOM.findDOMNode(this.refs.eventType).value = '';
        ReactDOM.findDOMNode(this.refs.date).value = '';
        ReactDOM.findDOMNode(this.refs.numOfGuest).value = '';

        console.log("Event create submit button clicked");
    }

    render() {
        // const category = this.props.category;
        // console.log(category);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Update Event</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">
                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="region"
                                                        placeholder="Enter Region"
                                                        ref="region"
                                                    />
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="region">Region</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="eventType"
                                                            placeholder="Enter Event Type"
                                                            ref="eventType"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="eventType">Event Type</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="date"
                                                            placeholder="Enter Date"
                                                            ref="date"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="date">Date</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="numOfGuest"
                                                            placeholder="Enter Number Of Guest"
                                                            ref="numOfGuest"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="numOfGuest">Number of Guest</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <RaisedButton
                                            label="Submit" primary
                                            className="btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        />
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}


export default withTracker((data) => {
    // console.log(data);
    const event = Events.findOne({_id: data.eventId});
    console.log(event);

    return {
        event: event,
    };
})(EventUpdate);
