import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import EachEventInList from './EachEventInList';
import EventHeader from './EventHeader';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../../api/events";
import RaisedButton from 'material-ui/RaisedButton';
import { MuiDataTable } from 'mui-data-table';
import RefreshLoading from '../../Loading';


function _handleDelete(data) {
    // console.log(data);
    const eventId = data._id;
    Events.remove({_id: eventId}, function (err) {
        if(err){
            console.log("some error happened");
        } else {
            console.log("Event successfully removed");
            window.location.reload();
        }
    });
}

class EventList extends React.Component {



    render() {
        const events = this.props.events;
        // console.log(events);

        if(events.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Event</h3>
                    <div className={"new-button"}>
                        <a href="/admin/event/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No data available</div>
                </div>
            );
        }



        let config = {
            paginated:true,
            search: 'name',
            data: events,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                { property: 'region', title: 'Region' },
                { property: 'numOfGuest', title: 'No Of Guest' },
                {title: 'View', renderAs: function(data) {
                    return (<a href={"/admin/event/"+ data._id}>View</a>);
                }},
                {title: 'Update', renderAs: function(data) {
                    return (<a href={"/admin/event/update/"+ data._id}>Edit</a>);
                }},
                {title: 'Delete', renderAs: function(data) {
                    return (<button onClick={() => (_handleDelete(data))}>delete</button>);
                }},
            ]
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Event</h3>
                <div className={"new-button"}>
                    <a href="/admin/event/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <MuiDataTable config={config}/>
            </div>

        )
    }
}

export default withTracker(() => {
    return {
        events: Events.find({}).fetch(),
    };
})(EventList);