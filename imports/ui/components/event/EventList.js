import React from 'react';
import EventDetail from './EventDetail';


class EventList extends React.Component {
    constructor(props) {
        super(props);
    }

    _onSelect(e) {
        let eventValue = e.target.value;
        // console.log(eventValue);
        this.props.callBackFromParent(eventValue);
    }

    renderEvents() {
        return this.getEvents().map((event) => (
            <EventDetail key={event._id} event={event} />
        ));
    }

    render() {
        return (
            <div className="dropdown">
                <div className="dropdown-toggle bottom-menu-fur" data-toggle="dropdown">
                <select className="selectpicker dash-bottom-menu-ne-se" onChange={this._onSelect.bind(this)}>
                    {this.renderEvents()}
                </select>
                </div>
            </div>
        )
    }
}

export default EventList;