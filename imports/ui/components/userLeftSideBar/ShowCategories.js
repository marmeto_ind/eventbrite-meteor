import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Choices} from "../../../api/choice";
import {Sellers} from "../../../api/sellers";

class ShowCategories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryImage: ''
        };
    }

    _handleCategoryClick(event) {
        const category = this.props.category;
        const categoryId = category._id;
        if(this.props.callbackFromParent) {
            this.props.callbackFromParent(category);
        }
    }

    componentDidMount() {
        $(".userLeftBarlistMenu").click(function () {
            $(".userLeftBarlistMenu").removeClass("userLeftSideBarSelect");
            $(this).addClass("userLeftSideBarSelect");
        });
        const category = this.props.category;
        const categoryId = category._id;
        const userId = Meteor.userId();
        Meteor.call('choices.getDataByUserCategoryId', userId, categoryId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
            }
        })
        this.setState({categoryImage: this.props.category.image})
    }

    handleImageError() {
        this.setState({categoryImage: 'https://wendely-images.s3-us-east-2.amazonaws.com/CATERING.png'})
    }

    render() {
        const category = this.props.category;
        const categoryName = category.name;
        
        return (
            <li style={{background: 'linear-gradient(to right, #6b3e60 0%, #232322 50%, #6b3e60 100%)'}} className={"userLeftBarlistMenu"} color="secondary" onClick={this._handleCategoryClick.bind(this)}>
                <div className={"one-box"}>
                    <div className={"two-box"}>
                          <span className={"dashboard-left-icon img-top"} id={"left-icon"}>
                    <img
                        className="dashboard-list-logo"
                        src={this.state.categoryImage}
                        onError={this.handleImageError.bind(this)}
                        alt=""
                    />
                </span>
                    </div>
                    <div className={"three-box user_leftSideBar_threeBox_height"}>
                        <p  className={"dashboard-seller"} id={"seller"}>
                    {categoryName}
                </p>
                        <p className={"sellerNameBlock"}>
                    {this.props.sellerName? this.props.sellerName: ''}
                </p>
                    </div>
                    <div className={"four-box"}>
                         <span className={"dashboard-right-icon img-top "} id={"right-icon"}>
                    {
                        this.props.sellerName?
                            <img className="dashboard-list-logo" src="/img/show-tick.png" alt="" />
                            : ''
                    }
                </span>
                    </div>
                </div>
            </li>
        )
    }
}

export default withTracker((data)=> {
    const categoryId = data.category._id;
    const userId = Meteor.userId();
    Meteor.subscribe('choices.all');
    const choice = Choices.findOne({"user" : userId, "categoryId" : categoryId});

    if(choice) {
        Meteor.subscribe('sellers.all');
        const sellerId = choice.seller;
        const seller = Sellers.findOne({"_id" : sellerId});
        if(seller) {
            const sellerName = seller.name;
            return {
                sellerName: sellerName,
            }
        }
    }
    return {};
})(ShowCategories);