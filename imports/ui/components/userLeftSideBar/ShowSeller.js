import React from 'react';

class ShowSeller extends React.Component {
    render() {

        const seller = this.props.seller;
        // const categoryName = seller.category.name;
        // console.log(seller);
        const sellerName = seller.name;
        const sellerId = seller._id;

        return (
            <a href={"/user/service/" + sellerId}>
                <li style={{background: 'linear-gradient(to right, #602a34 0%, #221e1d 51%, #602a34 100%)'}} className={""} color="secondary">
                    <span className={"dashboard-left-icon"} id={"left-icon"}>
                        <img className="dashboard-list-logo" src={seller.profile.logoImage} alt="" />
                    </span>
                    <span  className={"dashboard-seller"} id={"seller"}>{sellerName}</span>
                    {/*<span id={"category"}>{categoryName}</span>*/}
                    {/*<span className={"dashboard-right-icon"} id={"right-icon"}>*/}
                        {/*<img className="dashboard-list-logo" src="/img/show-tick.png" alt="" />*/}
                    {/*</span>*/}
                </li>
            </a>
        )
    }
}

export default ShowSeller;