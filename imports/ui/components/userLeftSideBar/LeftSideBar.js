import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../api/categories";
import {Sellers} from "../../../api/sellers";
import ShowSeller from './ShowSeller';
import ShowCategories from './ShowCategories';
import './left-side-bar.css';

class LeftSideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCategories: 'true',
            showSellers: 'true',

        }
    }

    renderSellers() {
        const sellers = this.props.sellers;
        return sellers.map((seller, index) => (
            <ShowSeller seller={seller} key={index}/>
        ));
    }

    renderCategories() {
        const callbackFromParent = this.props.callbackFromParent;
        const categories = this.props.categories;
        return categories.map((category, index) => (
            <ShowCategories category={category} key={index} callbackFromParent={callbackFromParent} />
        ));
    }

    _handleSearchKeyChange(event){
        let searchKey = event.target.value;
        this.props.parentCallBack({searchKey})
    }

    _showCategories() {
        this.setState({
            showCategories: 'true',
            showSellers: 'false',
        })
    }

    _showSellers() {
        this.setState({
            showCategories: 'false',
            showSellers: 'true',
        })
    }

    render() {
        {this.props}
        return (
            <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">
                <div className="search_bar_wrapper">
                    <div className="form-group has-feedback ">
                        <input type="text"
                                id={"searchKey"}
                                className="search-vendor-control"
                                onChange={this._handleSearchKeyChange.bind(this)}
                                placeholder="SEARCH SERVICE"
                        />
                        <span className="form-control-feedback">
                            <img className="vendor-search-logo" src="/img/new_img/white_search.svg" alt="" />
                        </span>
                    </div>
                </div>
                <div className="clearfix"></div>
                <div className="menubar-left collapse navbar-collapse" id="myNavbar">
                    <ul className="nav dashboard-list">
                      {this.renderCategories()}
                    </ul>
                </div>
            </div>
        )
    }
}

export default withTracker((data) => {
    const searchKey = data.searchKey;

    Meteor.subscribe('sellers');
    Meteor.subscribe('categories.all');
    if(!data.categories) {
        var categories = Categories.find().fetch();
    } else {
        var categories = data.categories;
    }
    if(!data.sellers) {
        var sellers = Sellers.find().fetch();
    } else {
        var sellers = data.sellers;
    }

    if(searchKey) {
        let rgx1 = new RegExp('^' + searchKey, 'i');
        let rgx2 = new RegExp('(.)+'+searchKey+'(.)*', 'i');
        
        let categories1 = Categories.find({"name": {$regex : rgx1}}).fetch()
        let categories2 = Categories.find({"name": {$regex : rgx2}}).fetch()

        let result = categories1.concat(categories2);
        
        return {
            sellers: sellers,
            categories: result
        }
    } else {
        return {
            categories: categories,
            sellers: sellers
        }
    }
})(LeftSideBar);

