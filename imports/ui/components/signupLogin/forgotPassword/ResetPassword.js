import React from 'react';
import './forgotPassword.scss';
import $ from 'jquery';
import { Accounts } from 'meteor/accounts-base'


class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: '',
            successMessage: '',
        }
    }


    resetPassword(e) {
        e.preventDefault();
        const newPassword = $("#userNewPassword").val();
        const token = this.props.params.token;

        if(!newPassword) {
            this.setState({errorMessage: "Please enter password"});
        } else if(!token) {
            this.setState({errorMessage: "token is not valid"});
        } else {
            const self = this;
            Accounts.resetPassword(token, newPassword, function (err, result) {
                if(err) {
                    console.log(err);
                    self.setState({errorMessage: err.reason});
                } else {
                    if(result) {
                        self.setState({errorMessage: ""});
                        self.setState({successMessage: "password has been reset successfully"});
                        $("#userNewPassword").val("");
                    }
                }
            })
        }
    }

    backToHome(e) {
        browserHistory.push('/mobile/home/login');
    }

    render() {
        const success_msg_style = {
            color: '#f2d1d4 !important',
            margin: '0 !important',
            padding: '0 !important',
            paddingBottom: '15px'
        };

        return (
            // <div className="forgot-password-page page-login reset_page_background">
            //     <div className="main-body forgot-padding">
            //         <div key="1">
            //             <div className="body-inner body_background">

            //                 <div className="card card_background">
            //                     <div className="forgot-wrapper">
            //                         <div className="forgot-container">
            //                             <div className="forgot-div-wrapper">
            //                                 <div className="forgot-info">
            //                                     <div className="relative-div">
            //                                         <img className="home-login-img" src={"/img/logo_tick.png"} />
            //                                         <div className={"home-login-absolute-div-style"}><span>Reset Password</span></div>
            //                                     </div>
            //                                     <div className="login-form user-login-form-style">
                                                    // <div className={"error-message"}>{this.state.errorMessage}</div>
                                                    // <div className="email input-box-shadow">
                                                    //     <input id="userNewPassword" type="password" className="type-email"  placeholder="ENTER NEW PASSWORD" />
                                                    //     <p>Password</p>
                                                    // </div>
            //                                         <div className="relativePosition">
            //                                             <div  style={success_msg_style}>{this.state.successMessage}</div>
            //                                             <button className="text-style LogInbtnClass relativePosition">
            //                                                 <img className="logo-home-first-img" src={"/img/forgot_logo.png"} />
            //                                                 <div
            //                                                     className={"user-login-absolute-div-style forgot-top"}
            //                                                     onClick={this.resetPassword.bind(this)}
            //                                                 >
            //                                                     Reset
            //                                                 </div>
            //                                             </button>
            //                                         </div>

            //                                         <div style={{paddingTop:'10px'}} className="relative-div home-login-button-div">
                                                     
            //                                             <div className="text-style" onClick={this.backToHome.bind(this)}>
            //                                                 <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"} />
            //                                                 <div className={"absolute-div-style back_top"}>Back</div>
            //                                             </div>
            //                                         </div>
            //                                     </div>
            //                                 </div>
            //                             </div>
            //                         </div>
            //                     </div>
            //                 </div>

            //             </div>
            //         </div>
            //     </div>
            // </div>

            <div className="forgot-password-page page-login page_background background_position">
                <div className="div_center_vertically">
                    <div className="main-body usersignup-padding ">
                        <div key="1">
                            <div className="body-inner body_background">

                                <div className="card card_background">
                                    <div className="user-signup-wrapper">
                                        <div className="user-signup-container">
                                            <div className="user-signup-wrapper-content padding-top-0">
                                                <div className="user-signup-login-info">
                                                    <div className="login-header">
                                                        <span className="login-header-icon">
                                                            <img className="logo-login-img"
                                                                src={"/img/key_one.svg"}/></span>
                                                        <h4 id={"text1"} className={"user-signup-login-text"}>Reset Password</h4>
                                                    </div>
                                                    <h3 className="username_field">Please Enter Username</h3>
                                                    <div className="login-form user-login-form-style">
                                                        <form>
                                                        <div className={"error-message"}>{this.state.errorMessage}</div>
                                                        <div className="email">
                                                            <input id="userNewPassword" type="password" className="type-email"  placeholder="ENTER NEW PASSWORD" />
                                                            <p>Password</p>
                                                        </div>
                                                                                                                    
                                                            
                                                            <div className="sign_up_wrapper">
                                                                <button  className="LogInbtnClassSign" onClick={this.resetPassword.bind(this)}></button>
                                                            </div>
                                                            
                                                            <div className="relative-div home-login-button-div">
                                                                <div className="text-style" onClick={this.backToHome.bind(this)}>
                                                                    <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"}/>
                                                                    <div className={"absolute-div-style_wrapper"}>Back</div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ForgotPassword;