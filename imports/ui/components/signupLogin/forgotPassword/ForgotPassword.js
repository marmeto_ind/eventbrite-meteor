import React from 'react';
import './forgotPassword.scss';
import $ from 'jquery';
import { Accounts } from 'meteor/accounts-base';
import {browserHistory} from 'react-router';
import '../../CommonLoginPage/commonLogin.scss';


class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: '',
            successMessage: ''
        }
    }

    validateEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    
    resetPassword(e) {
        e.preventDefault();
        const userEmail = $("#userEmail").val();

        if(!userEmail) {
            this.setState({errorMessage: "Please enter valid email address"});
        }
        else if(!this.validateEmail(userEmail)) {
            this.setState({errorMessage: "Please enter valid email address"});
        } else {
            this.setState({errorMessage: ""});
            const self = this;
            Accounts.forgotPassword({email: userEmail}, function (err, result) {
                if (err) {
                    console.log(err);
                    self.setState({errorMessage: err.reason})
                } else {

                    // Send email to the customer regarding password change
                    let user_id = Meteor.userId();
                    let user_type=  Meteor.user().profile.role;

                    let emailOptions = {
                        user_type: user_type,
                        event_type: "password_reset",
                        user_id: user_id,
                        email: userEmail
                    };

                    Meteor.call('emails.send', emailOptions, function (err, result) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(result)
                        }
                    });

                    self.setState({successMessage: "We have sent one mail to reset your password"})
                }
            });
        }
    }

    backToHome(e) {
        browserHistory.push('/login');
    }

    render() {
        return (
            // <div className="forgot-password-page page-login page_background">
            //     <div className="main-body forgot-padding">
            //         <div key="1">
            //             <div className="body-inner body_background">

            //                 <div className="card card_background">
            //                     <div className="forgot-wrapper">
            //                         <div className="forgot-container">
            //                             <div className="forgot-div-wrapper">
            //                                 <div className="forgot-info">
            //                                     <div className="login-header">
            //                                         <span className="login-header-icon"><img className="logo-login-img" src={"/img/logo_tick.png"} /></span>
            //                                         <h4 className={"forgot-text"}>Forgot Password</h4>
            //                                     </div>
            //                                     <div className="login-form user-login-form-style">
            //                                         <div className="email input-box-shadow">
            //                                             <input id="userEmail" type="text" className="type-email"  placeholder="Email" />
            //                                             <p>Mail</p>
            //                                         </div>
            //                                         <div className="relativePosition">
            //                                             <button className="text-style LogInbtnClass">
            //                                                 <img className="logo-home-first-img" src={"/img/forgot_logo.png"} />
            //                                                 <div
            //                                                     className={"user-login-absolute-div-style forgot-top"}
            //                                                     onClick={this.resetPassword.bind(this)}
            //                                                 >
            //                                                     Reset
            //                                                 </div>
            //                                             </button>
            //                                         </div>
            //                                         <div className={"error-message"}>{this.state.errorMessage}</div>
            //                                         <div style={{paddingTop:'10px'}} className="relative-div home-login-button-div">
            //                                             {/*<input type="submit" className="text-style" value="New Couple" />*/}
            //                                             <div className="text-style" onClick={this.backToHome.bind(this)}>
            //                                                 <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"} />
            //                                                 <div className={"absolute-div-style back_top"}>Back</div>
            //                                             </div>

            //                                         </div>
            //                                     </div>
            //                                 </div>
            //                             </div>
            //                         </div>
            //                     </div>
            //                 </div>

            //             </div>
            //         </div>
            //     </div>
            // </div>
            <div className="forgot-password-page page-login page_background background_position">
                <div className="div_center_vertically">
                    <div className="main-body usersignup-padding ">
                        <div key="1">
                            <div className="body-inner body_background">

                                <div className="card card_background">
                                    <div className="user-signup-wrapper">
                                        <div className="user-signup-container">
                                            <div className="user-signup-wrapper-content padding-top-0">
                                                <div className="user-signup-login-info">
                                                    <div className="login-header">
                                                        <span className="login-header-icon">
                                                            <img className="logo-login-img"
                                                                src={"/img/key_one.svg"}/></span>
                                                        <h4 id={"text1"} className={"user-signup-login-text"}>Forgot Password</h4>
                                                    </div>
                                                    <h3 className="username_field">Please Enter Username</h3>
                                                    <div className="login-form user-login-form-style">
                                                        <h5 className="mandatory_text">Please enter username</h5>
                                                        <form>
                                                            <div className="email">
                                                                <input id="userEmail" type="text" className="type-email type-email-sign"  placeholder="Enter Username or mail" />
                                                                <p>Username</p>
                                                            </div>
                                                                                                                    
                                                            
                                                            <div className="sign_up_wrapper">
                                                                <button  className="LogInbtnClassSign" onClick={this.resetPassword.bind(this)}></button>
                                                            </div>
                                                            
                                                            <div className="relative-div home-login-button-div">
                                                                <div className="text-style" onClick={this.backToHome.bind(this)}>
                                                                    <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"}/>
                                                                    <div className={"absolute-div-style_wrapper"}>Back</div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ForgotPassword;