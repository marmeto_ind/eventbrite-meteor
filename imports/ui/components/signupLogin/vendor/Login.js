import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {Session} from 'meteor/session';

class Login extends Component {
    constructor(props) {
        super(props);
    }

    _handleLogin(e) {
        e.preventDefault();
        let email = $('#userEmail').val();
        let password = $('#userPassword').val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                console.log(user.profile.role === "vendor");
                if(user.profile.role === "vendor") {
                    console.log("successfully loggedIn");
                    const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;
                    Session.set({
                        userId: userId,
                        userProfile: userProfile,
                    });
                    browserHistory.push('/vendor/chat');
                } else {
                    Meteor.logout(function (err) {
                        throw err;
                    });
                    console.log("Not authorized");
                }
            } else {
                console.error(err);
                $("#userEmail").select();
                $(".vendor-not-exist").show();
            }
        })
    }

    render() {
        return (
            <div className="page-login page_background">
                <div className="main-body vendorlogin-padding">
                    <div key="1">
                        <div className="body-inner body_background">

                            <div className="card background_white">
                                <div className="user-signup-wrapper">
                                    <div className="user-signup-container">
                                        <div className="user-signup-div-wrapper">
                                            <div className="user-signup-login-info">
                                                <div className="login-header">
                                                    <span className="login-header-icon"><img className="logo-login-img" src={"/img/login-01.png"} /></span>
                                                    <h4 className={"user-login"}> Log in</h4>
                                                </div>
                                                <h3 className={"vendor-not-exist"}> Vendor does not exist</h3>
                                                <div className="login-form user-login-form-style">
                                                    <form>
                                                        <div className="email input-box-shadow">
                                                            <input id="userEmail" type="text" className="type-email"  placeholder="Email" />
                                                            <p>Mail</p>
                                                        </div>
                                                        <div className="password input-box-shadow">
                                                            <input id="userPassword" type="password" className="type-password" placeholder="********" />
                                                            <p>password</p>
                                                        </div>
                                                        {/*<div className="submit-button-dv">*/}
                                                            {/*<input*/}
                                                                {/*onClick={this._handleLogin.bind(this)}*/}
                                                                {/*type="submit"*/}
                                                                {/*className="submit-button"*/}
                                                                {/*value="Login" />*/}
                                                        {/*</div>*/}

                                                        <div className="">
                                                            <button  onClick={this._handleLogin.bind(this)} className="text-style LogInbtnClass"><img className="logo-home-first-img" src={"/img/login-01.png"} />
                                                                <div className={"user-login-absolute-div-style user-login-top"}>Login</div></button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="forgot-password additional-info">
                                <a href="/forgot/Password"
                                >Forgot your Password? Don't worry</a>
                            </div>

                            <div className="additional-info">
                                <a href="/vendor/signup"
                                >New to wendely? SIGN UP</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;