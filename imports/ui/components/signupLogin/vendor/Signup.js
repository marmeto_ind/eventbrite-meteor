import React, { Component } from 'react';
import  {Accounts} from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import {Session} from 'meteor/session';
import './vendor-signup-login.scss';

class SignUp extends Component {

    componentDidMount() {
        const self = this;

        // Validation before submit
        $(document).ready(function(){
            validateEmail = function(elementValue)
            {
                let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return emailPattern.test(elementValue);
            };

            $('#userEmail').change(function(){
                let value = $(this).val();
                let valid = validateEmail(value);
                if (!valid) {
                    $(".wrong-details").show();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".username_field").hide();
                } else {
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".username_field").hide();
                }
            });

            $('#userPassword').change(function () {
                let password = $("#userPassword").val();
                if (password.length < 4) {
                    $(".password_input_field_empty").show();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".username_field").hide();

                    console.log("blur");
                    console.log(password.length);

                }
                else {
                    $(".password_input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".username_field").hide();
                    console.log(password.length);
                    console.log("Ok");
                }
            });
        });
        //
    }

    _handleSignUp(e) {
        e.preventDefault();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();
        let name = $("#userName").val();
        let phoneNumber = $("#userNumber").val();

        // Validation on button Click

        validateEmail = function(elementValue)
        {
            let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            return emailPattern.test(elementValue);
        };


        let email_Value_check = validateEmail(email);

        if ( email.length === 0   && password.length === 0) {
            $(".password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".vendor-exist-text").hide();
            $(".input_field_empty").show();
            $(".username_field").hide();


            console.log(password.length);

        }

        else  if(!email_Value_check) {
            $(".wrong-details").show();
            $(".input_field_empty").hide();
            $(".vendor-exist-text").hide();
            $(".wrong-password").hide();
            $(".password_input_field_empty").hide();
            $(".username_field").hide();
            console.log("Wrong Email Pattern");

        }
        else if ( name.length === 0) {
            $(".password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".user_exist_text").hide();
            $(".input_field_empty").hide();
            $(".username_field").show();
            console.log(password.length);

        }

        else if (password.length < 4) {
            $(".password_input_field_empty").show();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".vendor-exist-text").hide();
            $(".input_field_empty").hide();
            $(".username_field").hide();
            console.log(password.length);

        }

        else {
            // Create account if validtaed
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    phoneNumber: phoneNumber,
                    role: "vendor",
                } }, (err) => {
                if(err) {
                    // throw err;
                    // console.log(err);
                    // $("#userEmail").addClass('user-active');
                    // $("#userEmail").select();
                    // var existText = document.getElementById("text1");
                    // existText.classList.remove("exist-text");
                    // $(".vendor-exist-text").show();



                    if(err.reason === "Password may not be empty") {
                        $(".wrong-password").show();
                        $(".vendor-exist-text").hide();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                    }

                    else if (err.reason === "Email already exists.") {

                        $(".vendor-exist-text").show();
                        $(".wrong-password").hide();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                    }
                } else {
                    console.log('vendor signup succesfull');
                    const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;

                    const email = user.emails[0].address;

                    let emailOptions = {
                        user_type: "vendor",
                        event_type: "vendor_signup",
                        user_id: user._id,
                        email: email
                    };

                    Meteor.call('emails.send', emailOptions, function (err, result) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(result)
                        }
                    });

                    Session.set({
                        userId: userId,
                        userProfile: userProfile,
                    });
                    browserHistory.push('/vendor/chat');
                }
            });
        }


    }

    render() {
        return (
            <div className="page-login page_background">
                <div className="main-body vendorsignup-padding">
                        <div key="1">
                            <div className="body-inner body_background">

                                <div className="card card_background">
                                    <div className="vendor-signup-wrapper">
                                        <div className="vendor-signup-container">
                                            <div className="vendor-signup-div-wrapper">
                                                <div className="vendor-signup-login-info">
                                                    <div className="login-header">
                                                    <span className="login-header-icon">
                                                       <img className="logo-login-img" src={"/img/login-01.png"} /></span>
                                                        <h4 className={"user-signup-login"}>Sign Up</h4>
                                                    </div>

                                                    <h3 className={"vendor-exist-text"}>Email Already Exists</h3>
                                                    <h3 className="wrong-password">Please enter your password correctly</h3>
                                                    <h3 className="wrong-details">Please enter valid email address</h3>
                                                    <h3 className="input_field_empty">Please enter email and password</h3>
                                                    <h3 className="password_input_field_empty">You must provide atleast 4 characters</h3>
                                                    <h3 className="username_field">Please Enter Username</h3>
                                                    <div className="login-form vendor-login-form-style">
                                                        <form>
                                                            <div className="username input-box-shadow">
                                                                <input id="userName" type="text" className="type-username" placeholder="Username" />
                                                                <p>username</p>
                                                            </div>
                                                            <div className="email input-box-shadow vendor_email_signup_pos">
                                                                <input id="userEmail" type="text" className="type-email"  placeholder="Email" />
                                                                <p>Mail</p>
                                                            </div>
                                                            <div className="password input-box-shadow">
                                                                <input id="userPassword"  maxLength={"32"} type="password" className="type-password" placeholder="***********" />
                                                                <p>password</p>
                                                            </div>
                                                            <div className="phoneNumber input-box-shadow">
                                                                <input id="userNumber" type="number" className="type-number" placeholder="Phone Number" />
                                                                <p className={"vendor-PhoneNumber"}>Phone Number</p>
                                                            </div>
                                                            {/*<div className="submit-button-dv">*/}
                                                                {/*<input*/}
                                                                    {/*onClick={this._handleSignUp.bind(this)}*/}
                                                                    {/*type="submit"*/}
                                                                    {/*className="submit-button"*/}
                                                                    {/*value="SignUp" />*/}
                                                            {/*</div>*/}

                                                            <div className="">
                                                                <button onClick={this._handleSignUp.bind(this)} className="text-style LogInbtnClass"><img className="logo-home-first-img" src={"/img/login-01.png"} />
                                                                    <div className={"user-login-absolute-div-style user-signup-top"}>Sign Up</div></button>

                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="additional-info">
                                    <a className={"signin-continue"} href="/login"
                                    >SignIn to continue</a>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default SignUp;