import React, { Component } from 'react';
import  {Accounts} from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import {Session} from 'meteor/session';
import TextField from 'material-ui/TextField';
import QueueAnim from 'rc-queue-anim';
import './adminSignUpLogin.scss';
import Header from '../../header/Header';
import Footer from '../../footer/Footer';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            showRepeatPassword: false,
            errorMsg: '',
            showErrorMsg: false
        }
    }

    componentDidMount() {
        const self = this;
        $(document).ready(function(){
            validateEmail = function(elementValue)
            {
                let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return emailPattern.test(elementValue);
            };

            $('#userEmail').change(function(){
                let value = $(this).val();
                let valid = validateEmail(value);
                if (!valid) {
                    $(".wrong-details").show();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".password_input_field_empty").hide();
                    $(".username_field").hide();
                    // $(this).css('color', 'red');
                    // $(this).css('font-weight', '600');
                } else {
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".password_input_field_empty").hide();
                    $(".username_field").hide();
                }
            });

            $('#userPassword').change(function () {
                let password = $("#userPassword").val();
                if (password.length < 4) {
                    $(".password_input_field_empty").show();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".username_field").hide();
                }
                else {
                    $(".password_input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".exist-text").hide();
                    $(".input_field_empty").hide();
                    $(".username_field").hide();
                }
            });

        });
    }

    _handleSignup(e) {
        e.preventDefault();

        let name = $("#userName").val();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();

        // Validation on Button click

        validateEmail = function(elementValue)
        {
            let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            return emailPattern.test(elementValue);
        };


        let email_Value_check = validateEmail(email);

        if ( email.length === 0   && password.length === 0) {
            $(".password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".vendor-exist-text").hide();
            $(".input_field_empty").show();
            $(".username_field").hide();
        }

        else  if(!email_Value_check) {
            $(".wrong-details").show();
            $(".input_field_empty").hide();
            $(".vendor-exist-text").hide();
            $(".wrong-password").hide();
            $(".password_input_field_empty").hide();
            $(".username_field").hide();
            console.log("Wrong Email Pattern");

        }
        else if ( name.length === 0) {
            $(".password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".user_exist_text").hide();
            $(".input_field_empty").hide();
            $(".username_field").show();
            console.log(password.length);

        }

        else if (password.length < 4) {
            $(".password_input_field_empty").show();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".vendor-exist-text").hide();
            $(".input_field_empty").hide();
            console.log(password.length);

        }

        else {
            // Create account if validtaed
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    role: "admin",
                }
            }, (err) => {
                if (err) {
                    console.log(err);
                    if (err.reason === "Password may not be empty") {
                        $(".wrong-password").show();
                        $(".exist-text").hide();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                        $(".username_field").hide();
                    }

                    else if (err.reason === "Email already exists.") {

                        $(".exist-text").show();
                        $(".wrong-password").hide();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                        $(".username_field").hide();
                    }
                } else {
                    console.log('Admin Signup Succesfull');
                    const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;

                    const email = user.emails[0].address;

                    let emailOptions = {
                        user_type: "admin",
                        event_type: "admin_signup",
                        user_id: user._id,
                        email: email
                    };

                    Meteor.call('emails.send', emailOptions, function (err, result) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(result)
                        }
                    });

                    Session.set({
                        userId: userId,
                        userProfile: userProfile,
                    });
                    browserHistory.push('/login');
                }
            });
        }
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    toggleShowRepeatPassword() {
        this.setState({showRepeatPassword: !this.state.showRepeatPassword})
    }

    render() {
        return (
            <div className="page-login page_background">
                <div className="signup_page_header">
                    <Header />
                </div>
                <div className="main-body admin-signup-padding usersignup-padding">
                    <QueueAnim type="bottom" className="ui-animate admin-signup-padding">
                        {/* <div key="1">
                            <div className="body-inner body_background">

                                <div className="card card_background">
                                    <div className="admin-signup-wrapper">
                                        <div className="admin-signup-container">
                                            <div className="admin-signup-div-wrapper">
                                                <div className="admin-signup-login-info">
                                                    <div className="login-header">
                                                        <span className="login-header-icon"><img className="logo-login-img" src={"/img/login-01.png"} /></span>
                                                        <h4 className={"admin-login"}> Sign up</h4>
                                                    </div>
                                                    <h3 className={"exist-text"}>Email Already Exists</h3>
                                                    <h3 className="wrong-password">Please enter your password correctly</h3>
                                                    <h3 className="wrong-details">Please enter valid email address</h3>
                                                    <h3 className="input_field_empty">Please enter email and password</h3>
                                                    <h3 className="password_input_field_empty">You must provide atleast 4 characters</h3>
                                                    <h3 className="username_field">Please Enter Username</h3>
                                                    <div className="login-form admin-login-form-style">
                                                        <form>
                                                            <div className="adminusername-div username input-box-shadow">
                                                                <input id="userName" type="text" className="type-username" ref={"name"} placeholder="UserName" />
                                                                <p>username</p>
                                                            </div>
                                                            <div className="email admin-email-margin input-box-shadow">
                                                                <input id="userEmail" type="text" className="type-email"  ref={"email"} placeholder="Email" />
                                                                <p>Mail</p>
                                                            </div>
                                                            <div className="password input-box-shadow">
                                                                <input id="userPassword" type="password"  maxLength={"32"} className="type-password" ref={"password"} placeholder="***********" />
                                                                <p>password</p>
                                                            </div>
                                                            

                                                            <div className="">
                                                                <button onClick={this._handleSignup.bind(this)} className="text-style LogInbtnClass"><img className="logo-home-first-img" src={"/img/login-01.png"} />
                                                                    <div className={"user-login-absolute-div-style admin-signup-top"}>Sign Up</div></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>
                                <div className="additional-info">
                                    <a className={"signin-continue"} href="/login"
                                    >SignIn to continue</a>
                                </div>

                            </div>
                        </div> */}
                            <div key="1">
                            <div className="body-inner body_background">

                                <div className="card card_background">
                                    <div className="user-signup-wrapper">
                                        <div className="user-signup-container">
                                            <div className="user-signup-wrapper-content padding-top-0">
                                                <div className="user-signup-login-info">
                                                    <div className="login-header">
                                                        <span className="login-header-icon">
                                                        <img className="logo-login-img"
                                                                src={"/img/newService/new_user_icon.svg"}/></span>
                                                        <h4 id={"text1"} className={"user-signup-login-text"}>New Admin</h4>
                                                    </div>

                                                    <div className="login-form user-login-form-style">
                                                        <h5 className="mandatory_text">Please enter username and password</h5>

                                                        {this.state.showErrorMsg?
                                                        <h3 className="exist-text">{this.state.errorMsg}</h3>
                                                        :""}

                                                        <form>
                                                            <div className="username-div">
                                                                <input id="userNameSign" type="text" className="type-username-sign"
                                                                    placeholder="Enter Username"/>
                                                                <p>username</p>
                                                            </div>
                                                            <div className="email">
                                                                <input id="userEmailsign" type="text" className="type-email-sign"
                                                                    placeholder="Email"/>
                                                                <p>Mail</p>
                                                            </div>
                                                            <div className="password password_new">
                                                                <input id="userPasswordSign" type={!this.state.showPassword? "password": "text"}
                                                                    className="type-password" maxLength={"32"}
                                                                    placeholder="********" />
                                                                <p>password</p>
                                                                <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"} 
                                                                    alt="show password" 
                                                                    onClick={this.toggleShowPassword.bind(this)}
                                                                    className="show_password" />
                                                            </div>
                                                            <div className="password password_reenter">
                                                                <input id="userPasswordSign" type={!this.state.showRepeatPassword? "password": "text"}
                                                                    className="type-password" maxLength={"32"}
                                                                    placeholder="********" />
                                                                <p>Repeat password</p>
                                                                <img src={!this.state.showRepeatPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png "} 
                                                                    alt="show password" 
                                                                    onClick={this.toggleShowRepeatPassword.bind(this)}
                                                                    className="show_password" />
                                                            </div>
                                                            
                                                            <div className="remember_div">
                                                                <label className="radio">
                                                                    <input name="radio" type="radio" />
                                                                    <span>Keep Me Logged In</span>
                                                                </label>
                                                            </div>
                                                            <div className="allready_login">
                                                                <img src="/img/new_user_icon.svg" />
                                                                <a href="/login">I'm already a member</a>
                                                            </div>
                                                            <div className="sign_up_wrapper">
                                                                <button onClick={this._handleSignup.bind(this)}  className="createbtnClassSign"></button>
                                                            </div>
                                                            <div className="app_store_wrapper">
                                                                <fieldset id="el01">
                                                                    <legend>Download App</legend>
                                                                    <ul>
                                                                        <li><a href="#"><img src="/img/signup/Apple_app.png" /></a></li>
                                                                        <li><a href="#"><img src="/img/signup/google_store.png" /></a></li>
                                                                    </ul>
                                                                </fieldset>
                                                            </div>
                                                            
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                   
                            </div>
                        </div>
                        
                    </QueueAnim>
                </div>
                <div className="sign_up_page">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Signup;