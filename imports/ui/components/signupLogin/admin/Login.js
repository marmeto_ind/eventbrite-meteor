import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import { Session } from 'meteor/session';
import './adminSignUpLogin.scss';

class Login extends Component {
    constructor(props) {
        super(props);
        // this.state = {errorMsg: 'jdafkdj'}
    }

    _handleLogin(e) {
        e.preventDefault();

        let email = $("#inputEmail").val();
        let password = $("#inputPassword").val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                // if(user.profile.role === "admin") {
                console.log("right credential");
                const userId = user._id;
                const userProfile = user.profile.role;
                Session.set({
                    userId: userId,
                    userProfile: userProfile,
                });
                    browserHistory.push('/admin/edit');
                // } else {
                //     Meteor.logout();
                //     alert("Invalid credential");
                // }
            } else {
                // console.log(err.reason);
                // // return err.reason;
                // Session.set('errorMessage', err.message);
                Meteor.logout();
                console.error(err);
                $("#inputEmail").select();
                $(".admin-not-exist").show();
            }
        });
    }

    render() {
        return (
            <div  className="page-login page_background">
                <div className="main-body admin-login-padding">
                    <div key="1">
                        <div className="body-inner body_background">

                            <div className="card background_white">
                                <div className="admin-signup-wrapper">
                                    <div className="admin-signup-container">
                                        <div className="admin-signup-div-wrapper">
                                            <div className="admin-signup-login-info">
                                                <div className="login-header">
                                                    <span className="login-header-icon"><img className="logo-login-img" src={"/img/login-01.png"} /></span>
                                                    <h4 className={"user-login"}> Log in</h4>
                                                </div>
                                                <h3 className={"admin-not-exist"}>Admin does not exist</h3>
                                                <div className="login-form admin-login-form-style">
                                                    <form>
                                                        <div className="email input-box-shadow">
                                                            <input id="inputEmail" type="email" className="type-email"  ref={"userEmail"} placeholder="Email" required />
                                                            <p>Mail</p>
                                                        </div>
                                                        <div className="password input-box-shadow">
                                                            <input id="inputPassword" type="password" className="type-password" ref={"userPassword"} placeholder="********" />
                                                            <p>password</p>
                                                        </div>
                                                        {/*<div className="submit-button-dv" onClick={this._handleLogin.bind(this)}>*/}
                                                            {/*<input  type="submit" className="submit-button" value="Login" />*/}
                                                        {/*</div>*/}
                                                        <div className="">
                                                            <button onClick={this._handleLogin.bind(this)} className="text-style LogInbtnClass"><img className="logo-home-first-img" src={"/img/login-01.png"} />
                                                                <div className={"user-login-absolute-div-style user-login-top"}>Login</div></button>

                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="forgot-password additional-info">
                                <a href="/forgot/Password"
                                >Forgot your Password? Don't worry</a>
                            </div>

                            {/*<div className="additional-info">*/}
                                {/*<a href=""*/}
                                {/*>New to wendely? SIGN UP</a>*/}
                            {/*</div>*/}

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Login;