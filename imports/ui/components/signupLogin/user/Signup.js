import React from 'react';
import { Accounts } from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import './user-signup-login.scss';
import { Session } from 'meteor/session';
import { Meteor } from "meteor/meteor";
import Header from '../../header/Header';
import Footer from '../../footer/Footer';

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            showRepeatPassword: false,
            errorMsg: '',
            showErrorMsg: false
        }
    }

    componentDidMount() {
        // const self = this;
    }

    validateEmail(elementValue) {
        let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };

    _handleSignUp(e) {
        e.preventDefault();
        let email = $("#userEmailsign").val();
        let password = $("#userPasswordSign").val();
        let name = $("#userNameSign").val();

        let email_Value_check = this.validateEmail(email);

        if (email.length === 0 && password.length === 0) {
            this.setState({ errorMsg: "Email and Password is empty" })
            this.setState({ showErrorMsg: true })
        } else if (email.length === 0) {
            this.setState({ errorMsg: "Email field is empty" })
            this.setState({ showErrorMsg: true })
        } else if (password.length === 0) {
            this.setState({ errorMsg: "Password field is empty" })
            this.setState({ showErrorMsg: true })
        } else if (!email_Value_check) {
            this.setState({ errorMsg: "Not a valid email" })
            this.setState({ showErrorMsg: true })
        } else if (name.length === 0) {
            this.setState({ errorMsg: "Name is empty" })
            this.setState({ showErrorMsg: true })
        } else if (password.length < 4) {
            this.setState({ errorMsg: "Password length is less than 4 character" })
            this.setState({ showErrorMsg: true })
        } else {
            this.setState({ showErrorMsg: false })

            // Create Account if validated
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    role: "user",
                }
            }, (err) => {
                console.log(err)

                if (err) {
                    this.setState({ errorMsg: err.reason })
                    this.setState({ showErrorMsg: true })

                } else {
                    const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;
                    const email = user.emails[0].address;

                    let emailOptions = {
                        user_type: "user",
                        event_type: "user_signup",
                        user_id: user._id,
                        email: email
                    };

                    Meteor.call('emails.send', emailOptions, function (err, result) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(result)
                        }
                    });

                    Session.set({
                        userId: userId,
                        userProfile: userProfile,
                    });
                    browserHistory.push('/home');
                }
            });
        }
    }

    toggleShowPassword() {
        this.setState({ showPassword: !this.state.showPassword })
    }

    toggleShowRepeatPassword() {
        this.setState({ showRepeatPassword: !this.state.showRepeatPassword })
    }

    render() {
        return (
            <div className="page-login page_background">
                <div className="signup_page_header">
                    <Header />
                </div>
                <div className="main-body usersignup-padding">
                    <div key="1">
                        <div className="body-inner body_background">

                            <div className="card card_background">
                                <div className="user-signup-wrapper-content padding-top-0">
                                    <div className="user-signup-container">
                                        <div className="user-signup-wrapper-content">
                                            <div className="user-signup-login-info">
                                                <div className="login-header">
                                                    <span className="login-header-icon">
                                                        <img className="logo-login-img"
                                                            src={"/img/newService/new_user_icon.svg"} /></span>
                                                    <h4 id={"text1"} className={"user-signup-login-text"}>New User</h4>
                                                </div>

                                                <div className="login-form user-login-form-style">
                                                    {this.state.showErrorMsg ? <h5 className="mandatory_text">Please enter username and password</h5> : ""}
                                                    {this.state.showErrorMsg ? <h3 className="exist-text">{this.state.errorMsg}</h3>: ""}
                                                    <form>
                                                        <div className="username-div">
                                                            <input id="userNameSign" type="text" className="type-username-sign"
                                                                placeholder="Enter Username" />
                                                            <p>username</p>
                                                        </div>
                                                        <div className="email">
                                                            <input id="userEmailsign" type="text" className="type-email-sign"
                                                                placeholder="Email" />
                                                            <p>Mail</p>
                                                        </div>
                                                        <div className="password password_new">
                                                            <input id="userPasswordSign" type={!this.state.showPassword ? "password" : "text"}
                                                                className="type-password" maxLength={"32"}
                                                                placeholder="********" />
                                                            <p>password</p>
                                                            <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"}
                                                                alt="show password"
                                                                onClick={this.toggleShowPassword.bind(this)}
                                                                className="show_password" />
                                                        </div>
                                                        <div className="password password_reenter">
                                                            <input id="userPasswordSign" type={!this.state.showRepeatPassword ? "password" : "text"}
                                                                className="type-password" maxLength={"32"}
                                                                placeholder="********" />
                                                            <p>Repeat password</p>
                                                            <img src={!this.state.showRepeatPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png "}
                                                                alt="show password"
                                                                onClick={this.toggleShowRepeatPassword.bind(this)}
                                                                className="show_password" />
                                                        </div>

                                                        <div className="remember_div">
                                                            <label className="radio">
                                                                <input name="radio" type="radio" />
                                                                <span>Keep Me Logged In</span>
                                                            </label>
                                                        </div>
                                                        <div className="allready_login">
                                                            <img src="/img/logo_tick.png" />
                                                            <a href="/login">I'm already a member</a>
                                                        </div>
                                                        <div className="sign_up_wrapper">
                                                            <button onClick={this._handleSignUp.bind(this)} className="createbtnClassSign"></button>
                                                        </div>
                                                        {/* <div className="app_store_wrapper">
                                                            <fieldset id="el01">
                                                                <legend>Download App</legend>
                                                                <ul>
                                                                    <li><a href="#"><img src="/img/signup/Apple_app.png" /></a></li>
                                                                    <li><a href="#"><img src="/img/signup/google_store.png" /></a></li>
                                                                </ul>
                                                            </fieldset>
                                                        </div> */}

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="sign_up_page">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Signup;
