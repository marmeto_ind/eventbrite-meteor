import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import './user-signup-login.scss';
import {Session} from 'meteor/session';

class Login extends Component {
    _handleLogin(e) {
        e.preventDefault();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                if(user.profile.role === "user") {
                    // console.log("right credential");
                    // const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;
                    Session.set('userId', userId);
                    Session.set('userProfile', userProfile);
                    // console.log(Session.get('userId'));
                    browserHistory.push('/home');
                } else {
                    Meteor.logout();
                    alert("Incorrect Credential");
                }
            }
            else {
                console.error(err);
                $("#userEmail").select();
                $("#userPassword").select();
                $(".user-not-exist").show();
            }
        });
    }

    render() {
        return (
            <div className="page-login page_background">
                <div className="main-body userlogin-padding">
                    <div key="1">
                            <div className="body-inner body_background">

                                <div className="card background_white">
                                    <div className="user-signup-wrapper">
                                        <div className="user-signup-container">
                                            <div className="user-signup-div-wrapper">
                                                <div className="user-signup-login-info">
                                                    <div className="login-header">
                                                        <span className="login-header-icon"><img className="logo-login-img" src={"/img/login-01.png"} /></span>
                                                        <h4 className={"user-login"}> Log in</h4>
                                                    </div>
                                                    <h3 className="user-not-exist">User Does not exist</h3>
                                                    <div className="login-form user-login-form-style">
                                                        <form>
                                                            <div className="email input-box-shadow">
                                                                <input id="userEmail" type="email" className="type-email"  placeholder="Email" />
                                                                <p>Mail</p>
                                                            </div>
                                                            <div className="password input-box-shadow">
                                                                <input id="userPassword" type="password" className="type-password" placeholder="********" />
                                                                <p>password</p>
                                                            </div>
                                                            {/*<div className="submit-button-dv">*/}
                                                                {/*<input onClick={this._handleLogin.bind(this)} type="submit" className="submit-button" value="Login" />*/}
                                                                {/*<div> <img className="logo-home-first-img" src={"/img/login-01.png"} /></div>*/}
                                                            {/*</div>*/}

                                                            <div className="">
                                                                <button onClick={this._handleLogin.bind(this)} className="text-style LogInbtnClass"><img className="logo-home-first-img" src={"/img/login-01.png"} />
                                                                    <div className={"user-login-absolute-div-style user-login-top"}>Login</div></button>

                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="forgot-password forgot-password-div additional-info">
                                    <a href="/forgot/Password"
                                    >Forgot your Password?</a>
                                </div>
                                <div className="additional-info forgot-password-div">
                                    <a href="/user/signup"
                                    >SIGN UP</a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        );
    }
}

export default Login;