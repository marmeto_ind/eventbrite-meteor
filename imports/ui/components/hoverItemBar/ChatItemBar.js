import React from 'react';
import ChatComponent from '../chat/ChatComponent';
import UserNote from '../UserNote/UserNote';

class ChatItemBar extends React.Component {
    componentDidMount() {
        $('.nested-ul-c li').click(function(){
            // alert('clicked');
            $('li.active').removeClass("active");
            $('li.active').children().removeClass("open")
            // $('li').children().removeClass("open")
            $('.padding-nested').removeClass('open')
            $(this).addClass("active");
            $('li.active').children().addClass("open");

        });

        $(document).ready(function(){

            $('.nested_menu_container ul.tabs li').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#"+tab_id).addClass('current');
            })
        })
    }

    render() {
        return (
            <div className="chat-bar">
                <h4>venu done?</h4>
                <ul className="nested-ul-c">
                    <li className="">
                        <div className="padding-nested no-border-right">
                            yes
                            <span className="circle">
                                <i className="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </div>
                    </li>

                    <li>
                        <div className="padding-nested">
                            No
                            <span className="circle circle-border-gay">
                                <i className="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </div>
                    </li>

                    <li className="comment_t dropdown-submenu">
                        <ChatComponent/>
                    </li>

                    <li className="user_t">
                        <div className="padding-nested ">
                            <div className="dropdown-toggle" type="button" data-toggle="dropdown">
                                <i className="fa fa-user" aria-hidden="true"></i>
                                <div className="count">3</div>
                            </div>
                            <ul className="dropdown-menu user_t_nested_dropdown_menu">
                                <UserNote/>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <div className="padding-nested no-border-right">
                            <i className="fa fa-caret-left" aria-hidden="true"></i>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ChatItemBar;