import React from 'react';
import EventList from '../event/EventList';

class SelectEvent extends React.Component {
    render() {
        return (
            <div className="dropdown">

                <div className="dropdown-toggle bottom-menu-fur" data-toggle="dropdown">
                    <EventList/>
                </div>

            </div>
        )
    }
}

export default SelectEvent;