import React from 'react';

class SelectGuest extends React.Component {
    _onChangeGuest(e){
        // console.log(e.target.value);
        let numOfGuest = e.target.value;
        // console.log(numOfGuest);
        this.props.callBackFromParent(numOfGuest);
    }

    render() {
        return (
            <div className="dropdown">

                <div className="dropdown-toggle bottom-menu-tw" type="button" data-toggle="dropdown">
                    <select
                        className="selectpicker dash-bottom-menu-ne"
                        onChange={this._onChangeGuest.bind(this)}
                    >
                        <option>50</option>
                        <option>100</option>
                        <option>200</option>
                        <option>300</option>
                        <option>400</option>
                        <option>500</option>
                    </select>
                </div>

            </div>
        )
    }
}

export default SelectGuest;