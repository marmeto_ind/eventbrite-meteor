import React from 'react';
import DatePicker from 'react-date-picker';

class SelectDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
        }
    }
    _onChangeDate(date) {
        // console.log(date);
        this.setState({date});
        let value = date;
        this.props.callBackFromParent(date);
    }

    render() {
        return (
            <div className="bottom-menu-thrd">
                <div className="bottom-menu-thrd">
                    <DatePicker onChange={this._onChangeDate.bind(this)}  value={this.state.date} />
                </div>
            </div>
        )
    }
}

export default SelectDate;