import React from 'react';
import RegionList from '../region/RegionList';

class SelectRegion extends React.Component {
    render() {
        return (
            <div className="dropdown">

                <div className="dropdown-toggle bottom-menu-ne" type="button" data-toggle="dropdown">
                    <RegionList/>
                </div>

            </div>
        )
    }
}

export default SelectRegion;