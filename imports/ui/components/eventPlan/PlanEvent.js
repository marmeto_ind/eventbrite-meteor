import React from 'react';
// import SelectDate from './SelectDate';
// import SelectGuest from './SelectGuest';
// import EventList from '../event/EventList';
// import RegionList from '../region/RegionList';
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';

class PlanEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            region: '',
            event: '',
            date: '',
            numberOfGuest: '',
        };
    }


    selectRegionCallback(region) {
        this.setState({region: region});
    }

    selectEventCallback(event) {
        this.setState({event: event});
    }

    selectDateCallback(date) {
        this.setState({date: date});
    }

    selectGuestCallback(numOfGuest) {
        this.setState({numberOfGuest: numOfGuest})
    }

    handleStartPlanning(event){

        let selectedRegion = this.state.region;
        let selectedEvent = this.state.event;
        let selectedDate = this.state.date;
        let selectedNumOfGuest = this.state.numberOfGuest;
        let userId = Meteor.user()._id;

        // Create the event
        Events.insert({
            name: selectedEvent,
            region: selectedRegion,
            date: selectedDate,
            numOfGuest: selectedNumOfGuest,
            userId: userId,
        }, function (err) {
            if(err) {
                console.log(err)
            } else {
                // console.log("successfully create event");
            }
        });
    }

    render() {

        return (
            <div className="bottom-menu">

                <div className="bottom-menu-container">
                    <ul className="bottom-menu-container-dv">
                        <li>
                            <div className="dropdown">
                                <div className="dropdown-toggle bottom-menu-ne" type="button" data-toggle="dropdown">
                                    <select className="selectpicker dash-bottom-menu-ne" onChange={this._onSelectItem.bind(this)}>
                                        {this.renderRegions()}
                                    </select>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="dropdown">
                                <div className="dropdown-toggle bottom-menu-fur" data-toggle="dropdown">
                                    <select className="selectpicker dash-bottom-menu-ne-se" onChange={this._onSelect.bind(this)}>
                                        {this.renderEvents()}
                                    </select>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="bottom-menu-thrd">
                                <div className="bottom-menu-thrd">
                                    <DatePicker onChange={this._onChangeDate.bind(this)}  value={this.state.date} />
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="dropdown">

                                <div className="dropdown-toggle bottom-menu-tw" type="button" data-toggle="dropdown">
                                    <select
                                        className="selectpicker dash-bottom-menu-ne"
                                    >
                                        <option>50</option>
                                        <option>100</option>
                                        <option>200</option>
                                        <option>300</option>
                                        <option>400</option>
                                        <option>500</option>
                                    </select>
                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
                <div className="clearfix"></div>
                <div className="spacer"></div>
                <div className="bottom-menu-container-buttn">

                    <div className="start-planning-button">
                        <input
                            type="submit"
                            id="submit_bttn"
                            value="start planning"
                            className="submit_bttn fa fa-search"
                            style={{"fontFamily": 'Calibri'}}
                        />

                        <i
                            className="fa fa-search search_icon"
                            aria-hidden="true"
                        ></i>

                    </div>
                </div>

            </div>
        )
    }
}

export default PlanEvent;