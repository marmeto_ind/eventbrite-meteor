import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {Regions} from "../../../../api/regions";


class EachRegionInList extends React.Component {

    _handleDelete(e) {
        console.log("Delete button clicked");
        const regionId = this.props.region._id;
        Regions.remove({_id: regionId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("Region successfully removed");
            }
        });
    }

    render() {
        const region = this.props.region;
        // console.log(region);

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {region.name}
                    <a href={"/admin/region/"+ region._id}><RaisedButton label="View" primary /></a>
                    <a href={"/admin/region/update/"+ region._id}><RaisedButton label="Edit" primary /></a>
                    <RaisedButton label="Delete" primary  onClick={this._handleDelete.bind(this)}/>
                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachRegionInList;