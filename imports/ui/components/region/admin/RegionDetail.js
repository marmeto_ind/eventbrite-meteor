import React from 'react';
import QueueAnim from 'rc-queue-anim';
// import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {Regions} from "../../../../api/regions";

class RegionDetail extends React.Component {

    render() {
        const region = this.props.region;

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">View Region Details</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl box-height">
                                    <div className={"row content-name"}>
                                        <div className={"col-md-4 border-style"}>
                                            <h5 className="name-style">Name</h5>
                                        </div>
                                        <div className={"col-md-8"}>
                                            <h5 className={"name-style"}>{region? region.name: ''}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}


export default withTracker((data) => {

    const region = Regions.findOne({_id: data.regionId});

    return {
        region: region,
    };

})(RegionDetail);
