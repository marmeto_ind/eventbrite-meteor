import React from 'react';
// import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {Regions} from "../../../../api/regions";
import {browserHistory} from 'react-router';

class RegionUpdate extends React.Component {

    constructor(props) {
        super(props);
    }


    _handleSubmit(e) {
        e.preventDefault();
        let newRegion = ReactDOM.findDOMNode(this.refs.newRegion).value.trim();
        // let categoryId = this.props.category._id;
        // console.log(this.props.region);
        let regionId = this.props.region._id;


        // console.log(regionId);

        // Update the category to the database
        Regions.update({_id: regionId},{name: newRegion}, function (err, numOfUpdate) {
            if(err) {
                console.log("Error Occured");
            } else {
                console.log(numOfUpdate);
                browserHistory.push('/admin/region');
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newRegion).value = '';
        console.log("submit button clicked");
    }

    render() {
        const region = this.props.region;
        // console.log(region);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Update Region</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newRegion"
                                                            // defaultValue={category.name}
                                                            ref="newRegion"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newRegion">Region</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/*<RaisedButton*/}
                                            {/*label="Submit" primary*/}
                                            {/*className="btn-w-md button1"*/}
                                            {/*onClick={this._handleSubmit.bind(this)}*/}
                                        {/*/>*/}

                                        <button
                                            type="button"
                                            className="btn btn-primary btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        >Submit</button>
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    const region = Regions.findOne({_id: data.regionId});

    return {
        region: region,
    };
})(RegionUpdate);
