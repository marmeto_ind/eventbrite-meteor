import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Regions} from "../../../../api/regions";
import { MuiDataTable } from 'mui-data-table';
import RefreshLoading from '../../Loading';
import {browserHistory} from 'react-router';


function _handleDelete(data) {
    console.log(data);
    const regionId = data._id;
    Regions.remove({_id: regionId}, function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log('Record removed successfully');
            window.location.reload();
        }
    })
}

class RegionList extends React.Component {

    render() {
        const regions = this.props.regions;

        if(regions.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Region</h3>
                    <div className={"new-button"}>
                        <a href="/admin/region/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No data available</div>
                </div>
            )
        }

        const config = {
            paginated:true,
            search: 'name',
            data: regions,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                {title: 'View', renderAs: function (data) {
                    return (<a href={"/admin/region/"+ data._id}>View</a>);
                }},
                {title: 'Edit', renderAs: function (data) {
                    return (<a href={"/admin/region/update/"+ data._id}>Edit</a>);
                }},
                {title: 'Delete', renderAs: function (data) {
                    return (<button onClick={() => _handleDelete(data)}>Delete</button>)
                }},
            ]
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Region</h3>
                <div className={"new-button"}>
                    <a href="/admin/region/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <MuiDataTable config={config}/>
            </div>
        )
    }
}

export default withTracker(() => {
    const regions = Regions.find({}).fetch();

    return {
        regions: regions,
    };
})(RegionList);