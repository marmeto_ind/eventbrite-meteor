import React from 'react';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';
// import {Regions} from "../../../../api/regions";
// import RaisedButton from 'material-ui/RaisedButton';
// import {browserHistory} from 'react-router';

class RegionCreate extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();
        let newRegion = ReactDOM.findDOMNode(this.refs.newRegion).value.trim();
        // console.log(newRegion);

        console.log(newRegion);

        Meteor.call('regions.insert', newRegion);

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newRegion).value = '';
        console.log("button clicked");
    }

    render() {
        return (


            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <article className="article">
                        <h2 className="add-new-seller">Add New Region</h2>
                        <div className="box box-default">
                            <div className="box-body padding-xl">

                                <form role="form">

                                    <div className="form-group">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo pic-logo1"} src="/img/add.png" alt="" />
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box input-name-box1"}>
                                                    <input
                                                        type="text"
                                                        className="form-control "
                                                        id="newRegion"
                                                        placeholder="Enter Region"
                                                        ref="newRegion"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"} htmlFor="newRegion">New Region</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-w-md button1"
                                        onClick={this._handleSubmit.bind(this)}
                                    >Submit</button>

                                </form>

                            </div>
                        </div>
                    </article>
                </div>
                </QueueAnim>
            </section>
        )
    }
}

export default RegionCreate;