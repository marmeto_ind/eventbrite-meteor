import React from 'react';
import RegionDetail from './RegionDetail';
import {Regions} from "../../../api/regions";
import { withTracker } from 'meteor/react-meteor-data';

class RegionList extends React.Component {
    renderRegions() {
        return this.getRegions().map((region) => (
            <RegionDetail key={region._id} region={region}/>
        ));
    }

    _onSelectItem(e) {
        // console.log(e.target.value);
        let value = e.target.value;
        this.props.callBackFromParent(value);
    }

    render() {
        return (
            <div className="dropdown">
                <div className="dropdown-toggle bottom-menu-ne" type="button" data-toggle="dropdown">
                <select className="selectpicker dash-bottom-menu-ne" onChange={this._onSelectItem.bind(this)}>
                    {this.renderRegions()}
                </select>
                </div>
            </div>
        )
    }
}

// export default RegionList;
export default withTracker(() => {
    return {
        regions: Regions.find({}).fetch(),
    };
})(RegionList);