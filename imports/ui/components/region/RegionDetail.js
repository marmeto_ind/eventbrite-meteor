import React from 'react';

class RegionDetail extends React.Component {
    handleClick(event){
        console.log('option clicked');
    }

    render() {
        return (
            <option onClick={this.handleClick.bind(this)}>{this.props.region.name}</option>
        )
    }
}

export default RegionDetail;