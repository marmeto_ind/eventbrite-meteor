import React from 'react';
import Search from './Search';
import SelectedLocation from './SelectedLocation';
import SelectedEvent from './SelectedEvent';
import SelectedDate from './SelectedDate';
import SelectedNumOfGuest from './SelectedNumOfGuest';

class EventDataSelected extends React.Component {
    state = {
        date: new Date(),
    }

    onChange = date => this.setState({ date });

    render() {
        return (
            <div className="col-lg-12 col-xs-12 no-padding">
                <div className="mydash_bootom-menu">

                    <div className="bottom-menu-container">
                        <ul className="bottom-menu-container-dv">

                            <Search/>

                            <SelectedLocation/>

                            <SelectedEvent/>

                            <SelectedDate/>

                            <SelectedNumOfGuest/>

                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default EventDataSelected;