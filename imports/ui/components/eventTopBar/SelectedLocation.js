import React from 'react';

class SelectedLocation extends React.Component {
    render() {
        return (
            <li>
                <div className="dropdown">

                    <div className="dropdown-toggle dash-bottom-menu-ne" type="button" data-toggle="dropdown">
                        <select className="selectpicker dash-bottom-menu-ne">
                            <option>Bengalore</option>
                            <option>Mumbai</option>
                            <option>Kolkata</option>
                            <option>Delhi</option>
                        </select>
                    </div>
                </div>
            </li>
        )
    }
}

export default SelectedLocation;