import React from 'react';

class Search extends React.Component {
    render() {
        return (
            <li className="no-border-right text-left">
                <button className="event_search">
                    <i className="fa fa-search search_icon" aria-hidden="true"></i>
                    <span className="desktop-hide">Search</span>
                </button>
            </li>
        )
    }
}

export default Search;