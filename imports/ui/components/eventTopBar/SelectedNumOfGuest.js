import React from 'react';

class SelectedNumOfGuest extends React.Component {
    render() {
        return (
            <li className="no-border-right">
                <div className="dropdown">

                    <div className="dropdown-toggle das-bottom-menu-tw" type="button" data-toggle="dropdown">
                        ~ &nbsp; <select className="selectpicker dash-bottom-menu-ne">
                        <option>50</option>
                        <option>100</option>
                        <option>200</option>
                        <option>300</option>
                        <option>400</option>
                        <option>500</option>
                    </select>
                    </div>

                </div>
            </li>
        )
    }
}

export default SelectedNumOfGuest;