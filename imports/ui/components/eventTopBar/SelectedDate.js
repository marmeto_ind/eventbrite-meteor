import React from 'react';
import DatePicker_m from 'react-date-picker';

class SelectedDate extends React.Component {
    state = {
        date: new Date(),
    }

    onChange = date => this.setState({ date });

    render() {
        return (
            <li>
                <div className="dropdown">

                    <div className="dropdown-toggle">
                        <DatePicker_m onChange={this.onChange}  value={this.state.date} />
                    </div>

                </div>
            </li>
        )
    }
}

export default SelectedDate;