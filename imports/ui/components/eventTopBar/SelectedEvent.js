import React from 'react';

class SelectedEvent extends React.Component {
    render() {
        return (
            <li>
                <div className="dropdown">

                    <div className="dropdown-toggle dash-bottom-menu-ne" type="button" data-toggle="dropdown">
                        <select className="selectpicker dash-bottom-menu-ne">
                            <option>Wedding</option>
                            <option>Marriage</option>
                            <option>Party</option>
                        </select>
                    </div>

                </div>
            </li>
        )
    }
}

export default SelectedEvent;