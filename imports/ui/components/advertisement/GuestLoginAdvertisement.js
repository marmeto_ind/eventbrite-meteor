import React from 'react';

class GuestLoginAdvertisement extends React.Component {
    render() {
        return (
            <div className="mydash_bannner">
                <div className="body_portion_banner">
                    <div className="banner">
                        <img src={"/img/banner.jpg"} />
                    </div>
                </div>
            </div>
        )
    }
}

export default GuestLoginAdvertisement;