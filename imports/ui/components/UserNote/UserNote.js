import React from 'react';
import UserNoteCreate from './UserNoteCreate';
import UserNoteList from './UserNoteList';

class UserNote extends React.Component {
    render() {
        return (
            <div className="nested_menu_container">
                <UserNoteCreate/>
                <UserNoteList/>
            </div>
        )
    }
}

export default UserNote;