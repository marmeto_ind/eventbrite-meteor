import React from 'react';
import './vendor-topBar.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from "meteor/meteor";

class TopBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            seller: '',
            review: ''
            
        }
    }


    componentWillMount() {
        const self = this;
        let userId = Meteor.userId();
        Meteor.call('seller.findOneByUserId', userId, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                self.setState({seller: result});
            }
        })

        Meteor.call('reviews.getSellerRatingByUserId', userId, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                self.setState({review: result})
            }
        })
    }

    componentDidMount() {
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length-1];

        if(url_last_part === 'chat' ) {
            $("#vendor-notifi").attr('src',"/img/new_img/chatIcon_one.svg");
        }

        else if(url_last_part === 'agenda') {
            $("#vendor-notepad").attr('src',"/img/new_img/black_calendar.svg");
        }

        else if(url_last_part === 'stats') {
            $("#vendor-edit").attr('src',"/img/new_img/black_graph.svg");
        }
    }

    render() {
        let seller = this.state.seller;

        return (
            <div className="col-lg-12 col-xs-12 bottom-menu-cntnr">
                <div className="bottom-menu ">

                    <div className="bottom-menu-container vendor_top_bar_wrap">
                        <div className="bottom-menu-container">
                            <div className={"row"}>
                                <div className={"col-md-12 col-xs-12"}>
                                    <div className={"col-md-3"}>
                                        <div  className="fix-bar-option common-event-name-div iconadd_before vendor">
                                            {Meteor.user().profile.name}
                                        </div>
                                    </div>
                                    <div className={"col-md-7"}>
                                        {/*<p className="addBookingIcon">*/}
                                            {/*<a href="/vendor/add-booking">*/}
                                                {/*<img src={"/img/tabBarImg/grey_calendar.svg"} />*/}
                                            {/*</a>*/}
                                        {/*</p>*/}

                                        {seller &&
                                        <ul>                                             
                                            <li className="chat-text-width">
                                                <div className="fix-bar-option address_icon">
                                                  {seller.profile.address}
                                                </div>
                                            </li>
                                            <li className="chat-text-width ">
                                                <div className="fix-bar-option review_icon">
                                                    {this.state.review}/5
                                                </div>
                                            </li>
                                            <li className="chat-text-width date" >
                                                <div className="fix-bar-option size_icon">                                                
                                                    {seller.profile.area} M2
                                                </div>
                                            </li>

                                            <li className="chat-text-width guest-no guest_icon" >
                                                <div className="fix-bar-option">
                                                    {event? event.numOfGuest: 'Guest'}
                                                </div>
                                            </li>
                                           
                                        </ul>
                                        }

                                    </div>
                                    <div id={"mobile_userRight_navBar"} className={"col-md-2  col-xs-5"}>
                                        <div className="row">
                                            <div className={"col-md-4 col-xs-4 relativeposition"}>
                                                <a   href="/vendor/chat">
                                                    <img id={"vendor-notifi"} className="create-add"
                                                         src="/img/new_img/pink_chat.svg"
                                                         alt="" />
                                                    <div className="vendor_chat_count_icon_style">
                                                        <span>{this.props.unseenMessage}</span>
                                                    </div>
                                                </a>

                                            </div>
                                            <div className={"col-md-4 col-xs-4"}>
                                                <a  href="/vendor/agenda">
                                                    <img  id={"vendor-notepad"} className="create-add"
                                                         src="/img/new_img/pink_calendar.svg"
                                                         alt="" />
                                                </a>
                                            </div>
                                            <div className={"col-md-4  col-xs-4"}>
                                                <a  href="/vendor/stats">
                                                    <img id={"vendor-edit"} className="create-add"
                                                         src="/img/new_img/pink_graph.svg"
                                                         alt="" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="bottom-menu-container-buttn">

                        <div className="start-planning-button">

                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default TopBar;

// export default withTracker((data) => {
//     let unseenMessage = '';
//     Meteor.subscribe('getUnseenMessage', Meteor.userId(), function (count) {
//         unseenMessage = count;
//     })
//     return {
//         unseenMessage: unseenMessage
//     }
// })(TopBar)
