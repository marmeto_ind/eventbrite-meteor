import React from 'react';
// import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
// import QueueAnim from 'rc-queue-anim';
// import EachCategoryInList from './EachCategoryInList';
// import CategoryHeader from './CategoryHeader';
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../../api/categories";
// import RaisedButton from 'material-ui/RaisedButton';
import { MuiDataTable } from 'mui-data-table';
// import RefreshLoading from '../../Loading';


// class ShowCategories extends React.Component{
//     renderEachCateory() {
//         // console.log(this.props.categories);
//
//         return this.props.categories.map((category) => (
//             <EachCategoryInList key={category._id} category={category}/>
//         ));
//     }
//
//     render() {
//         return (
//             <article className="article">
//                 <h2 className="article-title">Admin category list page</h2>
//                 <a href={"/admin/category/new"}><RaisedButton label="Add" primary /></a>
//                 <Table>
//                     <TableHeader>
//                         <CategoryHeader/>
//                     </TableHeader>
//                     <TableBody>
//                         {this.renderEachCateory()}
//                     </TableBody>
//                 </Table>
//             </article>
//         )
//     }
// }

function _handleDelete(data) {
    console.log(data);
    const categoryId = data._id;
    Categories.remove({_id: categoryId}, function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log('Record removed successfully');
            window.location.reload()
        }
    })
}

class CategoryList extends React.Component {

    render() {
        const categories = this.props.categories;
        // console.log(categories);

        if(categories.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Category</h3>
                    <div className={"new-button"}>
                        <a href="/admin/category/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No data available</div>
                </div>
            );
        }

        const config = {
            paginated:true,
            search: 'name',
            data: categories,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                {title: 'View', renderAs: function (data) {
                    return (<a href={"/admin/category/"+ data._id}>View</a>);
                }},
                {title: 'Edit', renderAs: function (data) {
                        return (<a href={"/admin/category/update/"+ data._id}>Edit</a>);
                }},
                {title: 'Delete', renderAs: function (data) {
                    return (<button onClick={() => _handleDelete(data)}>Delete</button>)
                }},
            ],
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Category</h3>
                <div className={"new-button"}>
                    <a href="/admin/category/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <div className={"data-table"}>
                    <MuiDataTable config={config} className={"mui-table"}/>
                </div>
            </div>

        )
    }
}

export default withTracker(() => {
    return {
        categories: Categories.find({}).fetch(),
    };
})(CategoryList);