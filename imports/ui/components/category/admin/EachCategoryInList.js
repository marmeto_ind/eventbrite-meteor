import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {Categories} from "../../../../api/categories";

const mWidthStyle = {
    minWidth: '135px'
};

class EachCategoryInList extends React.Component {

    _handleDelete(e) {
        console.log("Delete button clicked");
        const categoryId = this.props.category._id;
        Categories.remove({_id: categoryId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("category successfully removed");
            }
        });
    }

    render() {
        // console.log(this.props.category);
        const category = this.props.category;
        // console.log(category);

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {category.name}
                    <a href={"/admin/category/"+ category._id}><RaisedButton label="View" primary /></a>
                    <a href={"/admin/category/update/"+ category._id}><RaisedButton label="Edit" primary /></a>
                    <RaisedButton label="Delete" primary  onClick={this._handleDelete.bind(this)}/>

                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachCategoryInList;