import React from 'react';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
// import {Categories} from "../../../../api/categories";
// import {Images} from "../../../../api/images";
// import {browserHistory} from 'react-router';
import {Meteor} from 'meteor/meteor';

class CategoryCreate extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();
        let newCategory = ReactDOM.findDOMNode(this.refs.newCategory).value.trim();
        // const categoryPictureFile = $("#categoryPicture")[0].files[0];
        // var categoryPicture = '';

        // if (categoryPictureFile) {
        //     categoryPicture = Images.insert(categoryPictureFile);
        // }

        const err = Meteor.call('categories.insert', newCategory);
        if(!err) {
            ReactDOM.findDOMNode(this.refs.newCategory).value = "";
        }

    }

    render() {
        return (


            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Add new category</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">
                                        <div className="form-group category-create-form">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo pic-logo1"} src="/img/add.png"
                                                         alt=""/>
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box input-name-box1"}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newCategory"
                                                            placeholder="Enter Category"
                                                            ref="newCategory"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newCategory">New
                                                            Category</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {/*<div className="logo-picture form-group">*/}
                                            {/*<label htmlFor="categoryPicture">*/}
                                                {/*<img id="hi" className={"pic-logo"}*/}
                                                     {/*src="/img/add.png"*/}
                                                     {/*alt=""/>*/}
                                            {/*</label>*/}
                                            {/*<input*/}
                                                {/*type="file"*/}
                                                {/*ref={"categoryPicture"}*/}
                                                {/*className="form-control-file"*/}
                                                {/*id="categoryPicture"*/}
                                            {/*/>*/}
                                            {/*<hr className="hr"/>*/}
                                            {/*<label className={"label-text"}>Category Picture</label>*/}
                                            {/*<h5 className={"text-color "}>(MAX 100MB)</h5>*/}
                                        {/*</div>*/}

                                        <button
                                            type="button"
                                            className="btn btn-primary btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        >Submit</button>
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

export default CategoryCreate;