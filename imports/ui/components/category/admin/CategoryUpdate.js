import React from 'react';
// import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Categories} from "../../../../api/categories";
import {Images} from "../../../../api/images";
import { withTracker } from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

class CategoryUpdate extends React.Component {

    constructor(props) {
        super(props);
    }


    _handleSubmit(e) {
        e.preventDefault();
        let newCategory = ReactDOM.findDOMNode(this.refs.newCategory).value.trim();
        let categoryId = this.props.category._id;

        const categoryPictureFile = $("#categoryPicture")[0].files[0];
        let categoryPicture = '';

        if(categoryPictureFile) {
            // first console log mainPictureFile
            console.log(categoryPictureFile);
            categoryPicture = Images.insert(categoryPictureFile, function (err, picture) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(picture._id);
                }
            });
        }

        // console.log(categoryId);

        // Update the category to the database
        Categories.update({_id: categoryId},{
            name: newCategory,
            picture: categoryPicture,
        }, function (err, numOfUpdate) {
            if(err) {
                console.log(err);
            } else {
                console.log(numOfUpdate);
                browserHistory.push('/admin/category');
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newCategory).value = '';
        console.log("submit button clicked");
    }

    render() {
        const category = this.props.category;
        // console.log(category);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Update category</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">
                                        <div className="form-group category-create-form">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo pic-logo1"} src="/img/add.png"
                                                         alt=""/>
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box input-name-box1"}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newCategory"
                                                            // defaultValue={category.name}
                                                            ref="newCategory"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newCategory">
                                                            Category</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="logo-picture form-group">
                                            <label htmlFor="categoryPicture"><img id="hi" className={"pic-logo"}
                                                                                  src="/img/add.png"
                                                                                  alt=""/></label>
                                            <input
                                                type="file"
                                                ref={"categoryPicture"}
                                                className="form-control-file"
                                                id="categoryPicture"
                                            />
                                            <hr className="hr"/>
                                            <label className={"label-text"}>Category Picture</label>
                                            <h5 className={"text-color "}>(MAX 100MB)</h5>
                                        </div>

                                        {/*<RaisedButton*/}
                                            {/*label="Submit" primary*/}
                                            {/*className="btn-w-md category-submit-button"*/}
                                            {/*onClick={this._handleSubmit.bind(this)}*/}
                                        {/*/>*/}

                                        <button
                                            type="button"
                                            className="btn btn-primary btn-w-md button1"
                                            onClick={this._handleSubmit.bind(this)}
                                        >Submit</button>
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    const category = Categories.findOne({_id: data.categoryId});

    return {
        category: category,
    };
})(CategoryUpdate);
