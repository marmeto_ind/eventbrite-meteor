import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Categories} from "../../../../api/categories";
import {Images} from "../../../../api/images";
import { withTracker } from 'meteor/react-meteor-data';

class CategoryDetail extends React.Component {

    render() {
        const category = this.props.category;

        if(!category) {
            return (
                <div>Loading</div>
            )
        } else {
            if(category.picture) {
                const collectionName = category.picture.collectionName;
                const imageName = category.picture.original.name;
                const imageId = category.picture._id;
                var imageUrl = '/uploads/images/'+collectionName+ '-'+ imageId+ '-'+ imageName;
            }

            return (
                <section className="container-fluid with-maxwidth chapter">
                    <QueueAnim type="bottom" className="ui-animate">
                        <div key="1">
                            <article className="article">
                                <h2 className="add-new-seller">View category</h2>
                                <div className="box box-default">
                                    <div className="box-body padding-xl">

                                        Name: {category? category.name: ''}<br></br>
                                        Category Image: {imageUrl?
                                        <img src={imageUrl} alt="" width={"50px"} height={"50px"}/>
                                        : ''}

                                        <div className={"row content-name"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Name:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{category? category.name: ''}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </QueueAnim>
                </section>
            )
        }



    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    // console.log(data);
    const category = Categories.findOne({_id: data.categoryId});
    // console.log(category);
    return {
        category: category,
    };

})(CategoryDetail);
