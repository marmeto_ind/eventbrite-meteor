import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import {Messages} from "../../../api/messages";


class SendMessage extends React.Component {
    handleSubmit(event){
        event.preventDefault();

        let text = ReactDOM.findDOMNode(this.refs.textMessage).value.trim();
        let vendorId = this.props.vendorId;
        let userId = Meteor.user()._id;


        // Insert message to the database
        Messages.insert({
            text: text,
            sender: userId,
            receiver: vendorId,
            sendBy: "user",
            createdAt: new Date(), // Created time
        });

        ReactDOM.findDOMNode(this.refs.textMessage).value = '';

    }

    render() {
        return (
            <div className="submit_chat_cntr">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input
                        type="text"
                        ref="textMessage"
                        placeholder="Enter Message"
                        className="typing_comment"
                    />

                    <button className="submiting_chat_btn" onClick={this.handleSubmit.bind(this)}>
                        <i className="fa fa-arrow-right" aria-hidden="true"></i>
                    </button>
                </form>

            </div>
        )
    }
}

export default SendMessage;