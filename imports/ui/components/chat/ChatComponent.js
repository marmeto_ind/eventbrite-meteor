import React from 'react';
import ChatList from './ChatList';
import ChatDetail from './ChatDetail';

class ChatComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vendor: ''
        }
    }

    childCallbackFunction = (dataFromChild) => {
        // console.log("childCallBack function");
        // console.log(dataFromChild);
        this.setState({vendor: dataFromChild});
    }

    render() {
        console.log(this.state.vendor);
        return (
            <div className="padding-nested">
                <div className="dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton_m">
                    <i className="fa fa-comment" aria-hidden="true"></i>
                    <div className="count">7</div>
                </div>
                <ul className="dropdown-menu user_t_nested_dropdown_menu" aria-labelledby="dropdownMenuButton_m">
                    <div className="nested_menu_container">

                        <ChatList callbackFromChatList={this.childCallbackFunction}/>

                        <ChatDetail vendor={this.state.vendor}/>

                    </div>
                </ul>
            </div>
        )
    }
}

export default ChatComponent;