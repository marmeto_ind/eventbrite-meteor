import React from 'react';

class EachChatInList extends React.Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        event.preventDefault();
        // console.log(this.props);
        this.props.callbackFromParent(this.props.chat._id);
    }


    render() {


        return (
            <li className="tab-link current" data-tab="tab-1" onClick={this.handleClick}>
                <div className="circle_m_nested">&nbsp;</div>
                <div className="count"></div>
                {this.props.chat.name}
            </li>
        )
    }
}

export default EachChatInList;