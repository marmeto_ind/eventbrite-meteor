import React from 'react';
import EachChatInList from './EachChatInList';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";


class ChatList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedChat: ''
        }

    }

    childCallbackFunction = (dataFromChild) => {
        this.setState({selectedChat: dataFromChild});
        // console.log(dataFromChild);
        this.props.callbackFromChatList(dataFromChild);
    };

    renderChat() {
        return this.props.sellers.map((chat) => (
            <EachChatInList key={chat._id} chat={chat} callbackFromParent={this.childCallbackFunction}/>
        ));
    }

    render() {
        return (
            <ul className="tabs">
                { this.renderChat() }
            </ul>
        )
    }
}

// export default ChatList;

export default withTracker(() => {
    return {
        sellers: Sellers.find({}).fetch(),
    }
})(ChatList);