import React from 'react';
import EachMessage from './EachMessage';
import {withTracker} from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";

class MessageList extends React.Component {

    constructor(props) {
        super(props);
    }

    renderMessages() {

        return this.props.messages.map((message) => (
            <EachMessage key={message._id} message={message}/>
        ));
    }

    render() {
        // console.log(this.props.vendorId);
        var vendorId = this.props.vendorId;

        return (
            <div>
                {this.renderMessages()}
            </div>
        )
    }
}

export default withTracker((data) => {
    // console.log(data);
    let vendorId = data.vendorId;

    return {
        messages:Messages.find({"receiver": vendorId}).fetch(),
    };
})(MessageList);