import React from 'react';
import SendMessage from './SendMessage';
import MessageList from './MessageList';

class ChatDetail extends React.Component {

    render() {
        const vendorId = this.props.vendor;
        console.log(vendorId);

        return (
            <div className="main">
                <div id="tab-1" className="tab-content current">
                    <div className="chat_dv">

                        <MessageList vendorId={vendorId}/>

                        <SendMessage vendorId={vendorId}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default ChatDetail;