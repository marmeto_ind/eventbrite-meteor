import React, {Component} from 'react';

class EachMessage extends Component {
    render() {
        return (
            <div className="full_wdth">
                <div className={"frm_frnd send_by_" + this.props.message.sendBy}>{this.props.message.text}</div>
                <div className={"prf_icon profile_" + this.props.message.sendBy}></div>
                <div className={"clearfix"}></div>
            </div>
        )
    }
}

export default EachMessage;