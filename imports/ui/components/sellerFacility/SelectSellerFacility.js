import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../../api/sellerFacilities";
import './seller-facility.scss';

class EachSellerFacility extends React.Component {
    render() {
        const sellerFacility = this.props.sellerFacility;
        return (
            <li className="form-check">
                <input className="form-check-input" type="checkbox" value="" id={sellerFacility._id}/>
                <label className="form-check-label" htmlFor={sellerFacility._id}>
                    {sellerFacility.name}
                </label>
            </li>
        )
    }
}

class SelectSellerFacility extends React.Component {

    renderSellerFacilities() {
        return this.props.sellerFacilities.map((sellerFacility) => (
            <EachSellerFacility key={sellerFacility._id} sellerFacility={sellerFacility}/>
        ));
    }

    render() {
        return (
            <div className={"seller-facilities"}>
                {this.renderSellerFacilities()}
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('seller_facilities');
    const facilities= SellerFacilities.find({}).fetch();
    console.log(facilities);
    return {
        sellerFacilities: facilities,
    };
})(SelectSellerFacility);