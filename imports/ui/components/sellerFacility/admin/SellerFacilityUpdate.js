import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../../../api/sellers";
import {browserHistory} from 'react-router';

class SellerFacilityUpdate extends React.Component {

    _handleSubmit(e) {
        e.preventDefault();
        let newSellerFacility = ReactDOM.findDOMNode(this.refs.newSellerFacility).value.trim();
        // console.log(this.props.sellerFacilityId);
        let sellerFacilityId = this.props.sellerFacilityId;


        // console.log(sellerFacilityId);

        // Update the category to the database
        SellerFacilities.update({_id: sellerFacilityId},{name: newSellerFacility}, function (err, numOfUpdate) {
            if(err) {
                console.log(err);
            } else {
                console.log(numOfUpdate);
                browserHistory.push('/admin/seller-facility');
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newSellerFacility).value = '';
        console.log("submit button clicked");
    }

    render() {
        const sellerFacilityId = this.props.sellerFacilityId;
        // console.log(region);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="add-new-seller">Update Seller Facility</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">

                                        <div className="form-group">
                                            <div className={"row input-name"}>
                                                <div className={"col-md-1"}>
                                                    <img id="hi" className={"pic-logo"} src="/img/add.png" alt="" />
                                                </div>
                                                <div className={"col-md-11"}>
                                                    <div className={"row input-name-box "}>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="newSellerFacility"
                                                            // defaultValue={category.name}
                                                            ref="newSellerFacility"
                                                        />
                                                    </div>
                                                    <div className={"row"}>
                                                        <label className={"input-name-text"} htmlFor="newSellerFacility">Seller Facility</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <RaisedButton
                                            label="Submit" primary
                                            className="btn-w-md category-submit-button"
                                            onClick={this._handleSubmit.bind(this)}
                                        />
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    // console.log(data);

    const sellerFacilities = SellerFacilities.findOne({_id: data.sellerFacilityId});
    // console.log(sellerFacilities);

    return {
        sellerFacilities: sellerFacilities,
    };
})(SellerFacilityUpdate);
