import React from 'react';
import {browserHistory} from 'react-router';
import {Meteor} from 'meteor/meteor';
import { ToastContainer, toast } from 'react-toastify';

class SellerFacilityCreate extends React.Component {
    successNotification = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    failNotification = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    _handleSubmit(e) {
        e.preventDefault();
        let newSellerFacility = $("#newSellerFacility").val();
        // const facilityPictureFile = $("#facilityPicture")[0].files[0];
        // let facilityPicture = '';

        // // Upload image for seller facilities
        // if (facilityPictureFile) {
        //     // first console log mainPictureFile
        //     // console.log(facilityPictureFile);
        //     facilityPicture = Images.insert(facilityPictureFile, function (err, picture) {
        //         if (err) {
        //             console.log(err);
        //         } else {
        //             console.log(picture._id);
        //         }
        //     });
        // }

        if(!newSellerFacility) {
            this.failNotification("Facility is empty");
        } else {
            const result = Meteor.call('seller_facilities.insert', newSellerFacility, function (err) {
                console.log(err);
            });
            if(!result) {
                this.successNotification("Facility Created");
            }
        }
    }

    render() {
        return (
            <section className="container-fluid with-maxwidth chapter">
                <div key="1">
                    <article className="article">
                        <h2 className="add-new-seller">Add New Seller Facility</h2>
                        <div className="box box-default">
                            <div className="box-body padding-xl">

                                <form role="form">

                                    <div className="form-group category-create-form">
                                        <div className={"row input-name"}>
                                            <div className={"col-md-1"}>
                                                <img id="hi" className={"pic-logo pic-logo1"} src="/img/add.png"
                                                     alt=""/>
                                            </div>
                                            <div className={"col-md-11"}>
                                                <div className={"row input-name-box input-name-box1"}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="newSellerFacility"
                                                        placeholder="Enter Seller Facility"
                                                        ref="newSellerFacility"
                                                    />
                                                </div>
                                                <div className={"row"}>
                                                    <label className={"input-name-text"}
                                                           htmlFor="newSellerFacility">New Seller Facility</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {/*<div className="logo-picture form-group">*/}
                                        {/*<label htmlFor="facilityPicture">*/}
                                            {/*<img id="hi" className={"pic-logo"}*/}
                                              {/*src="/img/add.png"*/}
                                              {/*alt=""/>*/}
                                        {/*</label>*/}
                                        {/*<input*/}
                                            {/*type="file"*/}
                                            {/*ref={"facilityPicture"}*/}
                                            {/*className="form-control-file"*/}
                                            {/*id="facilityPicture"*/}
                                        {/*/>*/}
                                        {/*<hr className="hr"/>*/}
                                        {/*<label className={"label-text"}>Facility Picture</label>*/}
                                        {/*<h5 className={"text-color "}>(MAX 100MB)</h5>*/}
                                    {/*</div>*/}

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-w-md button1"
                                        onClick={this._handleSubmit.bind(this)}
                                    >Submit</button>

                                </form>

                            </div>
                        </div>
                    </article>
                </div>
                <ToastContainer/>
            </section>
        )
    }
}

export default SellerFacilityCreate;