import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import EachSellerFacilityInList from './EachSellerFacilityInList';
import SellerFacilityHeader from './SellerFacilityHeader';
import { withTracker } from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../../../api/sellers";
import RaisedButton from 'material-ui/RaisedButton';
import { MuiDataTable } from 'mui-data-table';

class ShowSellerFacilities extends React.Component{
    renderEachRegions() {

        return this.props.sellerFacilities.map((sellerFacility) => (
            <EachSellerFacilityInList key={sellerFacility._id} sellerFacility={sellerFacility}/>
        ));

    }

    render() {
        return (
            <article className="article">
                <h2 className="article-title">Admin Seller Facility List Page</h2>
                <a href={"/admin/seller-facility/new"}><RaisedButton label="Add" primary /></a>
                <Table>
                    <TableHeader>
                        <SellerFacilityHeader/>
                    </TableHeader>
                    <TableBody>
                        {this.renderEachRegions()}
                    </TableBody>
                </Table>
            </article>
        )
    }
}

function _handleDelete(data) {
    console.log(data);
    const sellerFacilityId = data._id;
    SellerFacilities.remove({_id: sellerFacilityId}, function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log('Record removed successfully');
            window.location.reload();
        }
    })
}

class SellerFacilityList extends React.Component {

    render() {
        const sellerFacilities = this.props.sellerFacilities;
        // console.log(sellerFacilities);

        if(sellerFacilities.length === 0) {
            return (
                <div className={"body-content"}>
                    <h3 className={"list-header"}>Seller Facility</h3>
                    <div className={"new-button"}>
                        <a href="/admin/seller-facility/new"
                           className="btn btn-primary active new-btn"
                           role="button" aria-pressed="true">New</a>
                    </div>
                    <div>No data available</div>
                </div>
            );
        }

        const config = {
            paginated:true,
            search: 'name',
            data: sellerFacilities,
            columns: [
                { property: '_id', title: 'ID' },
                { property: 'name', title: 'Name' },
                { title: 'View', renderAs: function (data) {
                    return (<a href={"/admin/seller-facility/"+ data._id}>View</a>);
                }},
                { title: 'Edit', renderAs: function (data) {
                    return (<a href={"/admin/seller-facility/update/"+ data._id}>Edit</a>);
                }},
                { title: 'Delete', renderAs: function (data) {
                    return (<button onClick={() => _handleDelete(data)}>Delete</button>)
                }},
            ],
        };

        return (
            <div className={"body-content"}>
                <h3 className={"list-header"}>Seller Facility</h3>
                <div className={"new-button"}>
                    <a href="/admin/seller-facility/new"
                       className="btn btn-primary active new-btn"
                       role="button" aria-pressed="true">New</a>
                </div>
                <MuiDataTable config={config} />
            </div>
        )
    }
}

export default withTracker(() => {
    const sellerFacilities = SellerFacilities.find({}).fetch();

    return {
        sellerFacilities: sellerFacilities,
    };
})(SellerFacilityList);