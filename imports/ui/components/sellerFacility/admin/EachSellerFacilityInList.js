import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {SellerFacilities} from "../../../../api/sellers";


class EachSellerFacilityInList extends React.Component {

    _handleDelete(e) {
        console.log("Delete button clicked");
        const sellerFacilityId = this.props.sellerFacility._id;
        SellerFacilities.remove({_id: sellerFacilityId}, function (err) {
            if(err){
                console.log("some error happened");
            } else {
                console.log("Seller Facility successfully removed");
            }
        });
    }

    render() {
        const sellerFacility = this.props.sellerFacility;
        // console.log(sellerFacility);

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>
                    {sellerFacility.name}
                    <a href={"/admin/seller-facility/"+ sellerFacility._id}><RaisedButton label="View" primary /></a>
                    <a href={"/admin/seller-facility/update/"+ sellerFacility._id}><RaisedButton label="Edit" primary /></a>
                    <RaisedButton label="Delete" primary  onClick={this._handleDelete.bind(this)}/>
                </TableRowColumn>
            </TableRow>
        )
    }
}

export default EachSellerFacilityInList;