import React from 'react';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../../../api/sellers";

class SellerFacilityDetail extends React.Component {

    render() {
        const sellerFacility = this.props.sellerFacility;

        if (!sellerFacility) {
            return (
                <div>Loading</div>
            )
        } else {

            if(sellerFacility.picture) {
                const collectionName = sellerFacility.picture.collectionName;
                const imageName = sellerFacility.picture.original.name;
                const imageId = sellerFacility.picture._id;
                var imageUrl = '/uploads/images/'+collectionName+ '-'+ imageId+ '-'+ imageName;
            }

            return (
                <section className="container-fluid with-maxwidth chapter">
                    <QueueAnim type="bottom" className="ui-animate">
                        <div key="1">
                            <article className="article">
                                <h2 className="add-new-seller">Seller Facility Details</h2>
                                <div className="box box-default">
                                    <div className="box-body padding-xl">

                                        <div className={"row content-name seller-detail-bottom "}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Name:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style"}>{sellerFacility? sellerFacility.name: ''}</h5>
                                            </div>
                                        </div>

                                        <div className={"row content-name"}>
                                            <div className={"col-md-4 border-style"}>
                                                <h5 className="name-style">Image:</h5>
                                            </div>
                                            <div className={"col-md-8"}>
                                                <h5 className={"name-style seller-image"}>{imageUrl?
                                                    <img src={imageUrl} alt="" width={"50px"} height={"50px"}/>
                                                    : ''}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </QueueAnim>
                </section>
            )
        }
    }
}


export default withTracker((data) => {

    const sellerFacility = SellerFacilities.findOne({_id: data.sellerFacilityId});

    return {
        sellerFacility: sellerFacility,
    };

})(SellerFacilityDetail);
