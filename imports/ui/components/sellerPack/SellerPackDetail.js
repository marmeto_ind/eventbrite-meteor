import React, { Component } from 'react';


class SellerPackDetail extends Component {
    render() {
        return (
            <div>
                {this.props.pack.name}{' '}
                {this.props.pack.firstOffer}{' '}
                {this.props.pack.secondOffer}{' '}
                {this.props.pack.thirdOffer}
            </div>
        );
    }
}

export default SellerPackDetail;