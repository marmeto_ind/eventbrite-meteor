import React from 'react';
import SellerPackDetail from './SellerPackDetail';
import {SellerPacks} from "../../../api/sellerPacks";

class SellerPackList extends React.Component {
    getSellerPacks() {
        return [
            {_id: 1, name: "standard pack", firstOffer: "first offer", secondOffer: "second offer", thirdOffer: "third offer"},
            {_id: 2, name: "generous pack", firstOffer: "first offer", secondOffer: "second offer", thirdOffer: "third offer"},
            {_id: 3, name: "exclusive pack", firstOffer: "first offer", secondOffer: "second offer", thirdOffer: "third offer"},
        ];
    }

    renderSellerPacks() {
        return this.getSellerPacks().map((pack) => (
            <SellerPackDetail key={pack._id} pack={pack} />
        ));
    }

    render() {
        return (
            <div>{this.renderSellerPacks()}</div>
        )
    }
}

export default SellerPackList;