import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import {Regions} from "../../../../api/regions";

class RegionCreate extends React.Component {
    _handleSubmit(e) {
        e.preventDefault();
        let newRegion = ReactDOM.findDOMNode(this.refs.newRegion).value.trim();
        console.log(newRegion);

        // Insert the new category to the database
        Regions.insert({name: newRegion}, function (err) {
            if(err) {
                console.log("Error Occured");
            } else {
                console.log("New region inserted");
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newRegion).value = '';
        console.log("button clicked");
    }

    render() {
        return (


            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <article className="article">
                        <h2 className="article-title">Add New Region</h2>
                        <div className="box box-default">
                            <div className="box-body padding-xl">

                                <form role="form">
                                    <div className="form-group">
                                        <label htmlFor="newCategory">New Region</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="newRegion"
                                            placeholder="Enter Region"
                                            ref="newRegion"
                                        />
                                    </div>
                                    <RaisedButton
                                        label="Submit" primary
                                        className="btn-w-md"
                                        onClick={this._handleSubmit.bind(this)}
                                    />
                                    {/*<div className="divider" />*/}
                                </form>

                            </div>
                        </div>
                    </article>
                </div>
                </QueueAnim>
            </section>
        )
    }
}

export default RegionCreate;