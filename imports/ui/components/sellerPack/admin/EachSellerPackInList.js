import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';

class EachRegionInList extends React.Component {

    render() {
        const region = this.props.region;
        // console.log(region);

        return (
            <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>{region.name}</TableRowColumn>
            </TableRow>
        )
    }
}

export default EachRegionInList;