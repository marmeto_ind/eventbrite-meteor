import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import QueueAnim from 'rc-queue-anim';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {Regions} from "../../../../api/regions";

class RegionUpdate extends React.Component {

    constructor(props) {
        super(props);
    }


    _handleSubmit(e) {
        e.preventDefault();
        let newRegion = ReactDOM.findDOMNode(this.refs.newRegion).value.trim();
        // let categoryId = this.props.category._id;
        // console.log(this.props.region);
        let regionId = this.props.region._id;


        // console.log(regionId);

        // Update the category to the database
        Regions.update({_id: regionId},{name: newRegion}, function (err, numOfUpdate) {
            if(err) {
                console.log("Error Occured");
            } else {
                console.log(numOfUpdate);
                // console.log("data successfully updated");
            }
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.newRegion).value = '';
        console.log("submit button clicked");
    }

    render() {
        const region = this.props.region;
        // console.log(region);

        return (
            <section className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <article className="article">
                            <h2 className="article-title">Update Region</h2>
                            <div className="box box-default">
                                <div className="box-body padding-xl">

                                    <form role="form">
                                        <div className="form-group">
                                            <label htmlFor="newRegion">Region</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="newRegion"
                                                // defaultValue={category.name}
                                                ref="newRegion"
                                            />
                                        </div>
                                        <RaisedButton
                                            label="Submit" primary
                                            className="btn-w-md"
                                            onClick={this._handleSubmit.bind(this)}
                                        />
                                        {/*<div className="divider" />*/}
                                    </form>

                                </div>
                            </div>
                        </article>
                    </div>
                </QueueAnim>
            </section>
        )
    }
}

// export default CategoryUpdate;

export default withTracker((data) => {
    const region = Regions.findOne({_id: data.regionId});

    return {
        region: region,
    };
})(RegionUpdate);
