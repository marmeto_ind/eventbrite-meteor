import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import QueueAnim from 'rc-queue-anim';
import EachSellerPackInList from './EachSellerPackInList';
import SellerPackHeader from './SellerPackHeader';
import { withTracker } from 'meteor/react-meteor-data';
import {SellerPacks} from "../../../../api/sellerPacks";

class ShowRegions extends React.Component{
    renderEachRegions() {

        return this.props.regions.map((region) => (
            <EachSellerPackInList key={region._id} region={region}/>
        ));

    }

    render() {
        return (
            <article className="article">
                <h2 className="article-title">Admin Seller Pack List Page</h2>
                <Table>
                    <TableHeader>
                        <SellerPackHeader/>
                    </TableHeader>
                    <TableBody>
                        {this.renderEachRegions()}
                    </TableBody>
                </Table>
            </article>
        )
    }
}


class SellerPackList extends React.Component {


    render() {
        const regions = this.props.regions;

        return (
            <div className="container-fluid with-maxwidth chapter">
                <QueueAnim type="bottom" className="ui-animate">
                    <div key="1">
                        <ShowRegions regions={regions}/>
                    </div>
                </QueueAnim>
            </div>
        )
    }
}

export default withTracker(() => {
    const sellerPacks = SellerPacks.find({}).fetch();
    console.log(sellerPacks);

    return {
        sellerPacks: sellerPacks,
    };
})(SellerPackList);