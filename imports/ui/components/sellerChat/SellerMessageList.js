import React from 'react';
import EachMessage from './SellerEachMessage';
import {withTracker} from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import {Meteor} from 'meteor/meteor';

class MessageList extends React.Component {

    constructor(props) {
        super(props);
    }

    renderMessages() {
        // console.log(this.props.messages);
        return this.props.messages.map((message) => (
            <EachMessage key={message._id} message={message} />
        ));
    }

    render() {
        // console.log(this.props.vendorId);
        var vendorId = this.props.vendorId;

        return (
            <div>
                {this.renderMessages()}
            </div>
        )
    }
}

export default withTracker((data) => {
    // console.log(data);
    let userId = data.userId;
    let vendorId = new Meteor.Collection.ObjectID("5a58fc07734d1d6161400665");

    // console.log(Messages.find({"receiver": vendorId}).fetch());

    return {
        messages:Messages.find({"receiver": vendorId, "sender": userId}).fetch(),
    };
})(MessageList);