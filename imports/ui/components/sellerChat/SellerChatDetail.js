import React from 'react';
import SendMessage from './SellerSendMessage';
import MessageList from './SellerMessageList';

class SellerChatDetail extends React.Component {

    render() {
        const userId = this.props.user;
        // console.log(userId);

        return (
            <div className="main">
                <div id="tab-1" className="tab-content current">
                    <div className="chat_dv">

                        <MessageList userId={userId}/>

                        <SendMessage userId={userId}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default SellerChatDetail;