import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import {Messages} from "../../../api/messages";


class SellerSendMessage extends React.Component {
    handleSubmit(event){
        event.preventDefault();

        let text = ReactDOM.findDOMNode(this.refs.textMessage).value.trim();
        let vendorId = new Meteor.Collection.ObjectID("5a58fc07734d1d6161400665");
        let userId = this.props.userId;


        Messages.insert({
            text: text,
            sender: userId,
            receiver: vendorId,
            sendBy: "seller",
            createdAt: new Date(), // Created time
        });

        ReactDOM.findDOMNode(this.refs.textMessage).value = '';

    }

    render() {

        // console.log(this.props);

        return (
            <div className="submit_chat_cntr">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input
                        type="text"
                        ref="textMessage"
                        placeholder="Enter Message"
                        className="typing_comment"
                    />

                    <button className="submiting_chat_btn" onClick={this.handleSubmit.bind(this)}>
                        <i className="fa fa-arrow-right" aria-hidden="true"></i>
                    </button>
                </form>



            </div>
        )
    }
}

export default SellerSendMessage;