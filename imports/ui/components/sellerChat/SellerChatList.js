import React from 'react';
import EachChatInList from './SellerEachChatInList';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';


class SellerChatList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedChat: ''
        }

    }

    childCallbackFunction = (dataFromChild) => {
        this.setState({selectedChat: dataFromChild});
        // console.log(dataFromChild);
        this.props.callbackFromChatList(dataFromChild);
    };


    renderChat() {
        // console.log(this.props.users);
        return this.props.users.map((user) => (
            <EachChatInList key={user._id} chat={user} callbackFromParent={this.childCallbackFunction}/>
        ));
    }

    render() {
        return (
            <ul className="tabs">
                { this.renderChat() }
            </ul>
        )
    }
}

// export default ChatList;

export default withTracker(() => {
    return {
        sellers: Sellers.find({}).fetch(),
        users: Meteor.users.find({}).fetch(),
    }
})(SellerChatList);