import React from 'react';
import ChatList from './SellerChatList';
import ChatDetail from './SellerChatDetail';

class SellerChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: ''
        }
    }

    childCallbackFunction = (dataFromChild) => {
        // console.log("childCallBack function");
        // console.log(dataFromChild);
        this.setState({user: dataFromChild});
    }

    render() {
        // console.log(this.state);
        return (
            <div className="nested_menu_container">

                <ChatList callbackFromChatList={this.childCallbackFunction}/>

                <ChatDetail user={this.state.user}/>
                <div className={"clearfix"}></div>

            </div>
        )
    }
}

export default SellerChat;