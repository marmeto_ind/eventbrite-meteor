import React from 'react';
import CompanyCopyright from './CompanyCopyright';
// import SelectLanguage from './SelectLanguage';

class Footer extends React.Component {
    render(){
        return(
            <div className="footer-div">
                <div className="footer-cntnr main-container">
                    <ul className="pull-left footer-content">
                        <CompanyCopyright/>
                        {/* <SelectLanguage/> */}
                    </ul>
                </div>
            </div>
        )
    }
}

export default Footer;