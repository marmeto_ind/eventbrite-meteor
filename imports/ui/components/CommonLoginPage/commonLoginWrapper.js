import React from 'react';
import CommonLogin from './commonLogin';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class CommonLoginContainer extends React.Component {

    render() {
        if(this.props.logginIn === true) {
            return (
                <div className="loader_wrapper">
                    <div className="loader_mobile"></div>
                </div>
            )
        } else {
            let user = Meteor.user();
            if (user && user.profile.role === "user") {
                browserHistory.push('/home');
                return (<div></div>);
            }

            if (user && user.profile.role === "vendor") {
                browserHistory.push('/vendor/chat');
                return (<div></div>);
            }

            if (user && user.profile.role === "admin") {
                browserHistory.push('/admin/edit');
                return (<div></div>);
            }

            return (<CommonLogin/>)

            // if(!Meteor.user()) {
            //     browserHistory.push('/login');
            //     return (<div></div>);
            // } else {
            //     const user = Meteor.user();
            //     if(user.profile.role !== "user") {
            //         browserHistory.push('/login');
            //         return (<div></div>);
            //     } else {
            //         return (
            //             <CommonLogin/>
            //         )
            //     }
            // }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(CommonLoginContainer);

