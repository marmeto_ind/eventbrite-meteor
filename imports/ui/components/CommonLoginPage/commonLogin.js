import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import './commonLogin.scss';
import { Session } from 'meteor/session';
import { Meteor } from 'meteor/meteor';
import Header from '../header/Header';
import Footer from '../footer/Footer';

class CommonLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            radioChecked: false
        }
        this.toggleRadioChecked = this.toggleRadioChecked.bind(this);
    }

    validateEmail(elementValue) {
        let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };

    componentDidMount() {
        const self = this;
    }

    checkFuntion(e) {
        console.log(e.target);
    }

    _handleLogin(e) {
        e.preventDefault();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();
        let valid = this.validateEmail(email);

        if (email.length === 0 && password.length === 0) {
            this.setState({ errorMsg: "Email and Password is empty" })
            this.setState({ showErrorMsg: true })
        } else if (email.length === 0) {
            this.setState({ errorMsg: "Email is empty" })
            this.setState({ showErrorMsg: true })
        } else if (password.length === 0) {
            this.setState({ errorMsg: "Password is empty" })
            this.setState({ showErrorMsg: true })
        } else if (!valid) {
            this.setState({ errorMsg: "Email is not valid" })
            this.setState({ showErrorMsg: true })
        } else if (password.length < 4) {
            this.setState({ errorMsg: "Password length is less than 4" })
            this.setState({ showErrorMsg: true })
        } else {
            this.setState({ showErrorMsg: false })

            // if (!this.state.radioChecked) {
            //     $(window).on('beforeunload', function () {
            //         // Alternatively you should be able to log out Meteor here (not tested), eg:
            //         Meteor.logout();
            //     });
            // }            

            // login if validated
            Meteor.loginWithPassword({ email: email }, password, function (err) {
                if (!err) {
                    const user = Meteor.user();

                    // For User Login
                    if (user.profile.role === "user") {
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set('userId', userId);
                        Session.set('userProfile', userProfile);
                        browserHistory.push('/home');
                    }

                    // For Vendor Login
                    else if (user.profile.role === "vendor") {
                        // console.log("successfully loggedIn");
                        const user = Meteor.user();
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set({
                            userId: userId,
                            userProfile: userProfile,
                        });
                        browserHistory.push('/vendor/chat');
                    }

                    // For Admin Login
                    else if (user.profile.role === "admin") {
                        console.log("right credential");
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set({
                            userId: userId,
                            userProfile: userProfile,
                        });
                        browserHistory.push('/admin/edit');
                    }
                    else {
                        Meteor.logout();
                        alert("Incorrect Credential");
                    }
                } else {
                    console.error(err);
                    this.setState({ errorMsg: err.reason })
                    this.setState({ showErrorMsg: true })
                }
            });
        }
    }

    toggleShowPassword() {
        this.setState({ showPassword: !this.state.showPassword })
    }

    toggleRadioChecked() {
        this.setState({ radioChecked: !this.state.radioChecked })
    }

    render() {

        return (
            <div className="page-login page_background">
                <div className="loginpage_header">
                    <Header />
                </div>
                <div className="main-body usersignup-padding">
                    <div key="1">
                        <div className="body-inner body_background">

                            <div className="card card_background">
                                <div className="user-signup-wrapper">
                                    <div className="user-signup-container">
                                        <div className="user-signup-wrapper-content padding-top-0">
                                            <div className="user-signup-login-info">
                                                <div className="login-header">
                                                    <span className="login-header-icon">
                                                        <img className="logo-login-img"
                                                            src={"/img/key_one.svg"} /></span>
                                                    <h4 id={"text1"} className={"user-signup-login-text"}>Sign In</h4>
                                                </div>

                                                <div className="login-form user-login-form-style">
                                                    {this.state.showErrorMsg ? <h5 className="mandatory_text">Please enter username and password</h5> : ""}
                                                    {this.state.showErrorMsg ? <h3 className="exist-text">{this.state.errorMsg}</h3> : ""}
                                                    <form>
                                                        <div className="email">
                                                            <input id="userEmail" type="text" className="type-email-sign"
                                                                placeholder="Enter Username or mail " />
                                                            <p>Username</p>
                                                        </div>
                                                        <div className="password password_new">
                                                            <input id="userPassword"
                                                                type={!this.state.showPassword ? "password" : "text"}
                                                                className="type-password" maxLength={"32"}
                                                                placeholder="******" />
                                                            <p>password</p>
                                                            <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"}
                                                                alt="show password"
                                                                className="show_password"
                                                                onClick={this.toggleShowPassword.bind(this)} />
                                                        </div>
                                                        <div className="remember_div">
                                                            <label className="radio" id={!this.state.radioChecked ? "no-check" : ""} >
                                                                <input name="radio" type="radio" checked="false" onClick={this.toggleRadioChecked} />
                                                                <span>Keep Me Logged In</span>
                                                            </label>
                                                        </div>
                                                        <div className="forgot_password">
                                                            <img src="/img/forgot_password.svg" />
                                                            <a href="/forgot/Password">Forgot Password?</a>
                                                        </div>
                                                        <div className="allready_login">
                                                            <img src="/img/new_user_icon.svg" />
                                                            <a href="/user/signup">I'M NOT A MEMBER YET</a>
                                                        </div>
                                                        <div className="sign_up_wrapper">
                                                            <button onClick={this._handleLogin.bind(this)} className="LogInbtnClassSign"></button>
                                                        </div>
                                                        {/* <div className="app_store_wrapper">
                                                            <fieldset id="el01">
                                                                <legend>Download App</legend>
                                                                <ul>
                                                                    <li><a href="#"><img src="/img/signup/Apple_app.png" /></a></li>
                                                                    <li><a href="#"><img src="/img/signup/google_store.png" /></a></li>
                                                                </ul>
                                                            </fieldset>
                                                        </div> */}

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer_loginpage">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default CommonLogin;
