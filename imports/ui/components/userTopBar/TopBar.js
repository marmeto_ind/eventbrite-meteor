import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Events} from "../../../api/events";
import './userTopBar.scss';

class TopBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            unseenMessage: '',
        };
    }

    componentWillMount() {
        let userId = Meteor.userId();
        let viewer = "user";
        let self = this;
        Meteor.call('user.getAllUnseenMessageCountByUser', userId, viewer, function (error, result) {
            if (error) {
                console.log(error)
            } else {
                self.setState({unseenMessage: result})
            }
        })
    }

    componentDidMount() {
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length-1];
        const self = this;

        if(url_last_part === 'chat' ) {
            $("#notification").attr('src',"/img/new_img/chatIcon.svg");
        } else if(url_last_part === 'my-events') {
                $("#notepadImg").attr('src',"/img/new_img/myevent_icon.svg");
        } else if(url_last_part === 'guest-list') {
            $("#pencilEdit").attr('src',"/img/new_img/black_list.svg");
        }
    }

    render() {
        const event = this.props.event;
        return (
            <div className="col-lg-12 col-xs-12 bottom-menu-cntnr">
                <div className="bottom-menu ">
                    <div className="bottom-menu-container">
                        <div className="bottom-menu-container">
                            <div className={"row"}>
                                <div className={"col-md-12  col-xs-12"}>
                                    <div className={"col-md-4 col-xs-12"}>
                                        <div  className="fix-bar-option padding  common-event-name-div">
                                            {event? event.name: 'Event Name'}
                                        </div>
                                    </div>
                                    <div className={"col-md-6 col-xs-12 no-padding"}>
                                        <ul className={"user_topBar_centerBlock_width"}>
                                            <li className="chat-text-width relativePosition ">
                                                <div className="fix-bar-option userTopBar_region">
                                                    {event? event.region: 'Region'}
                                                </div>
                                            </li>
                                            {/* <li className="chat-text-width relativePosition">
                                                <div className="fix-bar-option userTopBar_event">
                                                    {event? event.eventType: 'Event'}
                                                </div>
                                            </li> */}
                                            <li className="chat-text-width date relativePosition" >
                                                <div className="fix-bar-option userTopBar_date">
                                                    {event? event.date.substring(0,15): 'Date'}
                                                </div>
                                            </li>
                                            <li className="chat-text-width guest-no relativePosition" >
                                                <div className="fix-bar-option userTopBar_guest">
                                                    {event? event.numOfGuest: 'Guest'}
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id={"mobile_userRight_navBar"} className={"col-md-2  col-xs-5"}>
                                        <div className="row">
                                            <div className={"col-md-4  col-xs-4 relativeposition"}>
                                                <a href="/user/chat">
                                                    <img  id={"notification"} className="create-add notification-img"
                                                         src="/img/chat-notifi.png"
                                                         alt="" />
                                                    {this.state.unseenMessage > 0 && 
                                                    <div className="chat_count_icon_style">
                                                        <span>{this.state.unseenMessage}</span>
                                                    </div>}
                                                </a>
                                            </div>
                                            <div className={"col-md-4 col-xs-4"}>
                                                <a href="/user/my-events" onClick={this.changeImage}>
                                                    <img id={"notepadImg"} className="create-add notepadImg-Img" src="/img/tabBarImg/grey_eventHome.svg" alt="" />
                                                </a>
                                            </div>
                                            <div className={"col-md-4 col-xs-4 guest-icon-padding"}>
                                                <a href="/user/guest-list">
                                                    <img id={"pencilEdit"} className="create-add pencilEdit-Img"
                                                         src="/img/tabBarImg/greyList.svg"
                                                         alt="" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>

                </div>
            </div>
        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('events');
    const userEvent = Events.findOne({'user': Meteor.userId()}, {sort: {createdAt: -1, limit: 1}});
    if(userEvent) {
        Session.set("eventId", userEvent._id);
    }
    return {
        event: userEvent,
    };
})(TopBar);