import React from 'react';
import {Meteor} from 'meteor/meteor';
import { Messages } from "../api/messages";
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showComponent: false,
        };
        this._onButtonClick = this._onButtonClick.bind(this);
    }

    _onButtonClick() {
        this.setState({
            showComponent:true,
        });
    }

    showAllUser(e){
        e.preventDefault();
        Meteor.users.find().forEach(function (oneUser) {
            // console.log(oneUser);

        });
    }

    render() {
        return (
            <div className="chat-page">
                <h1>Welcome to chat page</h1>
                {/*<button onClick={this._onButtonClick}>New Chat</button>*/}
                {this.state.showComponent ?
                    <ShowAllUsers /> :
                    null}
                <ChatComponent/>
            </div>

        )
    }
}

// Show all users available
class ShowAllUsers extends React.Component {

    render(){

        var users = Meteor.users.find();
        var userList = users.map(function (user) {
            return <div key={user._id}>User Id: {user._id}</div>
        });

        return (
            <ul>
                {userList}
            </ul>
            );
    }
}

// Create a Message component
class Message extends React.Component {
    render() {
        return (
            <div>
                <div>{ this.props.message.text }</div>
            </div>

        );
    }
}

// Create the chat component
class ChatComponent extends React.Component {

    handleSubmit(event) {
        event.preventDefault();

        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

        // console.log(Messages);

        Messages.insert({
            text,
            createdAt: new Date(), // Current time
        });

        // Clear the form
        ReactDOM.findDOMNode(this.refs.textInput).value = '';
    }

    renderMessages() {
        // console.log(this.props.messages)
        return this.props.messages.map((message) => (
            <Message key={message._id} message={message}/>
            ));
    }

    render() {
        return (
            <div className="container">
                <h1>Welcome to chat page</h1>
                <div>{this.renderMessages()}</div>
                <form className="new-message" onSubmit={this.handleSubmit.bind(this)} >
                    <input
                        type="text"
                        ref="textInput"
                        placeholder="New messages"
                    />
                </form>
            </div>
            );
    }
}

export default withTracker(() => {
    return {
        messages: Messages.find({}).fetch(),
    };
})(ChatComponent);

