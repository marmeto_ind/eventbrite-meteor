import React from 'react';
import MessageList from './MessageList';
import {Meteor} from 'meteor/meteor';
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';
import {emojify} from 'react-emojione';

class ChatSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openEmoji: false,
        }
        this._handleMessageSend = this._handleMessageSend.bind(this);
        this._handleKeyPress = this._handleKeyPress.bind(this);
    }

    _handleMessageSend(event) {
        const {sellerId, adminId} = this.props;
        const message = $("#messageText").val();
        const userId = Meteor.userId();
        const sender = "user";

        if(message) {
            if (sellerId) {                
                Meteor.call('messages.insert', message, userId, sellerId, sender);
            } else if (adminId) {                
                Meteor.call('messages.adminInsert', message, userId, adminId, sender);
            }            
            $("#messageText").val("");
        }
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;
        console.log(emojiCode);
        $("#messageText").val(emojiCode);
    }

    _handleOpenEmoji() {
        console.log("open emoji");
        this.setState({openEmoji: !this.state.openEmoji});
    }

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#user_attachFile")[0].files[0];
        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {

                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                console.log(uploader.progress());
            }
        })
    }

    _handleKeyPress(event) {
        if (event.key === 'Enter') {
          this._handleMessageSend();
        }
    }

    render() {
        const {sellerId, adminId} = this.props;
        if(!sellerId && !adminId) {
            return (
                <div className={"sectionblock1 leftPading chat-no-vendor-selected"}>
                    <div className={"no-event-booked-style"}>No Vendor Selected</div>
                </div>
            )
        } else {
            return (
                <div className={"sectionblock1 no-padding padding-right-0 user_chat_wrapper"}>

                    <div className={"userchat-message-list"}>
                        <MessageList sellerId={sellerId} adminId={adminId}/>
                    </div>
                    {this.state.openEmoji === true ? <div className={"web_user_emoji_block"}>
                        <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)} />
                    </div>:
                    ''
                    }
                <div className={"userchatType-box"}>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item list-background add-message">
                            <div className={"float-lft list-div-left-background list-type-box"}>
                                <div className={"userChatBlock userChatBlockTypesection"}>                                                                    
                                    <div className={"block2"}>
                                        <label htmlFor="user_attachFile" >
                                            <span>
                                                <img
                                                    className={"list-img"}
                                                    src="/img/clip_chat.svg"
                                                    alt=""
                                                />
                                            </span>
                                        </label>
                                        <div onChange={this._handleFileUpload.bind(this)}>
                                            <input
                                                type="file"
                                                className="form-control-file"
                                                id="user_attachFile"
                                            />
                                        </div>
                                    </div>
                                    <div className={"block1"}>
                                        <span>
                                            <img className={"list-img"}
                                                 src="/img/emoji_chat.svg"
                                                 alt=""
                                                 onClick={this._handleOpenEmoji.bind(this)}
                                            />
                                        </span>
                                    </div>
                                    {/* <div className={"block1"}>
                                        <span>
                                            <img className={"list-img"}
                                                 src="/img/phone_chat.svg"
                                                 alt=""
                                                 onClick={this._handleOpenEmoji.bind(this)}
                                            />
                                        </span>
                                    </div> */}
                                    <div className={"block3"}>
                                        <input id={"messageText"}
                                               className="type-search"
                                               type="text"
                                               placeholder="WRITE A MESSAGE"
                                               name="message"
                                               onKeyPress={this._handleKeyPress}                                             
                                        />
                                    </div>
                                    <div className={"block4"}>

                                    <span className={"btn"} onClick={this._handleMessageSend.bind(this)}>
                                        <img className={"list-img chat-send"}  src="/img/send_chat.svg" alt="" />
                                    </span>

                                    </div>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                </div>
            )
        }
    }
}

export default ChatSection;