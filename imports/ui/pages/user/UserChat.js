import React from 'react';
import Header from '../../pages/header-username/header';
import Footer from '../../components/footer/Footer';
import './user-chat.scss'
import SellerList from './SellerList';
import ChatSection from './ChatSection';
import LeftSideBar from '../../components/userLeftSideBar/LeftSideBar';
import UserTopBar from '../../components/userTopBar/TopBar';

class UserChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
            sellerId: '',
            adminId: ''
        };
        this.callbackFromSellerList = this.callbackFromSellerList.bind(this);
    }

    _handleSearchKeyChange(event) {
        let searchKey = event.target.value;
        this.setState({searchKey: searchKey});
    }

    callbackFromSellerList({sellerId, adminId}) {
        this.setState({sellerId: sellerId});
        this.setState({adminId: adminId})
    }

    parentCallBack(option) {
        let {searchKey } = option;
        this.setState({searchKey})
    }

    render() {
        
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container position_relative">
                    <UserTopBar/>
                    <div className="clearfix"></div>
                    <div className="clearfix"></div>
                    <div className="body_container top_67_absolute">
                        <LeftSideBar 
                            searchKey={this.state.searchKey}
                            parentCallBack={this.parentCallBack.bind(this)}
                        />
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            <div className="page_heading_tex padding_left_right_0">
                                Chat
                            </div>
                            <div className={"row margin-0"}>
                                <div className={"col-md-12 display_flex  chat-container leftPading"}>
                                    <ChatSection sellerId={this.state.sellerId} adminId={this.state.adminId}/>
                                    <SellerList callbackFromParent={this.callbackFromSellerList}/>
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default UserChat;
