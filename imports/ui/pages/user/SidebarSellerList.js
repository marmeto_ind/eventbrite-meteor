import React from 'react';

class SidebarSellerList extends React.Component {
    render() {
        return (
            <div className="menubar-left collapse navbar-collapse left-menu-padding" id="myNavbar">
                <ul className="nav">
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                    <li color="secondary">
                        <span id={"left-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                        <span id={"seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><i className="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </li>
                </ul>
            </div>
        )
    }
}

export default SidebarSellerList;