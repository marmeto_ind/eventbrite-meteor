import React from 'react';
import {Meteor} from 'meteor/meteor';
import {emojify} from 'react-emojione';

class EachSellerInList extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            sellerImage: '',
            newMsgCount: '',
            numOfMessage: '',
            latestMessage: '',
            logoImage: ''
        }
    }

    componentWillMount() {
        const sellerId = this.props.user.seller;
        const userId = this.props.user.user;
        const admin = this.props.user.admin;
        const self = this;
        const viewer="user";

        Meteor.call('user.getUnseenMessage', {userId, sellerId, admin, viewer}, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                if (result) {
                    let {newMsgCount, numOfMessage, latestMessage, logoImage} = result;
                    self.setState({newMsgCount, numOfMessage, latestMessage})
                    self.setState({sellerImage: logoImage});
                }
            }
        })
    }

    _handleOnClick(event) {
        const {seller, admin} = this.props.user;        
        this.props.parentCallback({sellerId: seller, adminId: admin});
    }

    componentDidMount() {
        $(document).ready(function(){
            $(".sellerListMenu li").click(function () {
                $(".sellerListMenu li").removeClass("selected-list-active");
                $(this).addClass("selected-list-active");
            });
        });
    }

    showMessage(message) {
        if (message) {
            if(this.isUrl(message)) {
                return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
            } else {
                return emojify(message);
            }
        } else {
            return ""
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    handleImageError() {
        this.setState({sellerImage: 'https://wendely-images.s3-us-east-2.amazonaws.com/Skärmklipp.PNG'})
    }

    render() {
        const user = this.props.user;

        return (
            <li className="list-group-item list-right-padding-style btn li-background" onClick={this._handleOnClick.bind(this)}>
                <div className={"seller-main-width"}>
                    <div className={"seller-list-left-icon"}>
                        <span><img className={"list-img"} src={this.state.sellerImage} onError={this.handleImageError.bind(this)} alt="" /></span>
                    </div>

                    <div className={"seller-name capital-text"}>
                        <p className={"chat-vendor-name"}>{user? user.sellerName || user.adminName: ''}</p>
                        <p className={"venue-p-margin"}>{this.showMessage(this.state.latestMessage)}</p>
                    </div>
                    {
                        this.state.newMsgCount?
                            <div className={"seller-list-right-icon"}>
                                <span className={"span-number"}> {this.state.newMsgCount} </span>
                            </div>:
                            ""
                    }
                </div>
            </li>
        )
    }
}

export default EachSellerInList;
