import React from 'react';
import {Meteor} from 'meteor/meteor';
import EachSellerInList from './EachSellerInList';

class SellerList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerList: []
        }
    }

    componentWillMount() {
        let userId = Meteor.userId();
        let self = this;
        // check chatwith record between user and admin
        Meteor.call('chat_with.insertChatWithAdmin', userId, function(err, result) {
            if (err) {
                console.log(err)
            } else {
                // console.log(result)
            }
        })

        Meteor.call('user.getChatWithList', userId, function(err, result) {
            if (err) {
                console.log(err)
            } else {
                self.setState({sellerList: result});
            }
        })
    }

    renderChatUserList(users) {
        const callback = this.props.callbackFromParent;
        return users.map((user, index) => (
            <EachSellerInList key={index} user={user} parentCallback={callback}/>
        ))
    }

    render() {
        const sellerList = this.state.sellerList;

        return (
            <div  id={"chatListName"} className={"sectionblock2 rightPadding RightPadding"}>
                <ul  className="list-group list-group-flush sellerListMenu">
                    {this.renderChatUserList(sellerList)}
                </ul>
            </div>
        )
    }
}

export default SellerList;
