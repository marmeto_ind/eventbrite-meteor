import React from 'react';
import UserChat from './UserChat';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class UserChatContainer extends React.Component {

    render() {
        if(this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <UserChat/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(UserChatContainer);

