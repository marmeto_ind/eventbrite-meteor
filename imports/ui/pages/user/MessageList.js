import React from 'react';
import EachMessage from './EachMessage';
import { withTracker } from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import {Meteor} from 'meteor/meteor';

class MessageList extends React.Component {
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.sellerId !== this.props.sellerId) {
            const {userId, sellerId, adminId} = this.props;
            if (sellerId) {
                Meteor.call('message_view.upsert', userId, sellerId, "user", function (err, result) {
                    if (err) {
                        console.log("error occured")
                    } else {
                        console.log(result)
                    }
                })
            }
        }
      }

    renderMessages() {
        const messages = this.props.messages;
        return messages.map((message, index) => (
            <EachMessage key={index} message={message}/>
        ))
    }

    render() {
        return (
            <ul className="list-group list-group-flush">
                {this.renderMessages()}
            </ul>
        )
    }
}

export default withTracker((data) => {
    const userId = Meteor.userId();
    const {sellerId, adminId } = data;
    Meteor.subscribe('messages');

    if (sellerId) {
        const messages = Messages.find(
            {"user" : userId, "seller" : sellerId},
            {fields: {"message": 1, "createdAt": 1, "sender": 1}}
        ).fetch();
    
        return {
            messages,
            userId,
            sellerId
        }
    } else if (adminId) {
        const messages = Messages.find(
            {"user" : userId, "admin" : adminId},
            {fields: {"message": 1, "createdAt": 1, "sender": 1}}
        ).fetch();
        
        return {
            messages,
            userId,
            adminId
        }
    }    
})(MessageList);
