import React from 'react';
import Message from './Message';
import { withTracker } from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import {Meteor} from 'meteor/meteor';

class MessageList extends React.Component {
    renderMessages() {
        const messages = this.props.messages;
        return messages.map((message, index) => (
            <Message key={index} message={message}/>
        ))
    }

    render() {
        return (
            <ul className="list-group list-group-flush">
                {this.renderMessages()}
            </ul>
        )
    }
}

export default withTracker((data) => {
    const userId = data.userId;
    const sellerId = data.sellerId;
    Meteor.subscribe('messages');

    return {
        messages: Messages.find(
            {"user" : userId, "seller" : sellerId},
            {fields: {"message": 1, "createdAt": 1, "sender": 1}}
            ).fetch()
    }
})(MessageList);