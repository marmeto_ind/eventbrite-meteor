import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import {MessageView} from "../../../api/messageView";
import {Meteor} from 'meteor/meteor';
import {emojify} from 'react-emojione';

class ShowEachUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerId: '',
            count: '',
        };
    }

    _handleOnClick(event) {
        const chatUser = this.props.chatUser;
        const parentCallback = this.props.parentCallback;

        parentCallback(chatUser.user);
    }

    componentDidMount() {
        const self = this;
        const chatUser = this.props.chatUser;
        const chatUserId = chatUser.user;
        const loggedInUserId = Meteor.userId();

        Meteor.call('messages.getSellerMessageCountById', loggedInUserId, chatUserId, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({count: result});
            }
        });

        $(document).ready(function(){
            $(".vendorlistMenu li").click(function () {
                $(".vendorlistMenu li").removeClass("selected-list-active");
                $(this).addClass("selected-list-active");
            });
        });
    }

    showMessage(message) {
        if(this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    render() {
        const chatUser = this.props.chatUser;
        const parentCallback = this.props.parentCallback;

        return (
            <li className="list-group-item list-right-padding-style btn"
                onClick={this._handleOnClick.bind(this)}>
                <div className={"vendorchatList"}>
                    <div className={"FloatLeft"}>
                        <span><img className={"list-img"} src="/img/greyStatue.png" alt="" /></span>
                    </div>
                    <div className={"TextCenter"}>
                        <p className={"vendor-chat-name"}>{chatUser? chatUser.userName: ''}</p>
                        <p className={"venue-p-margin"}>{this.showMessage(this.props.latestMessage? this.props.latestMessage: '')}</p>
                    </div>
                    {
                        this.props.newMsgCount?
                            <div className={"FloatRight FloatRightMrgin top_0"}>
                                <span>{this.props.newMsgCount}</span>
                            </div>:
                            ""
                    }
                </div>
            </li>
        )
    }
}

export default withTracker((data) => {

    const chatUser = data.chatUser;
    const userId = chatUser.user;
    const sellerId = chatUser.seller;

    Meteor.subscribe('messages.all');

    const latestMessageCollection = Messages.findOne({"user" : userId, "seller" : sellerId, "sender" : "user"}, {sort:{createdAt:-1}});

    let latestMessage = "";
    if(latestMessageCollection) {
        latestMessage = latestMessageCollection.message;
    }

    Meteor.subscribe('message_view.all');

    let newMsgCount = 0;

    let msgView = MessageView.findOne({"user": userId, "seller": sellerId});

    let lastView = '';
    if (msgView) {
        lastView = msgView.updatedAt;
    }

    // Now check the number of message created after the time
    if (lastView) {
        newMsgCount = Messages.find({
            "user": userId,
            "seller": sellerId,
            "sender": "user",
            "createdAt": {$gt: lastView}
        }).count()
    } else {
        newMsgCount = Messages.find({"user": userId, "seller": sellerId, "sender": "user"}).count()
    }

    return {
        latestMessage,
        newMsgCount
    }
})(ShowEachUser);
