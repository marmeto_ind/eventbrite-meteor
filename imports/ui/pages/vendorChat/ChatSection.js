import React from 'react';
import MessageList from './MessageList';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';

class ChatSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openEmoji: false,
        }
        this._handleMessageSend = this._handleMessageSend.bind(this);
        this._handleKeyPress = this._handleKeyPress.bind(this);
    }

    componentDidMount() {
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;

        Meteor.call('message_view.upsert', userId, sellerId, "seller", function (err, result) {
            if (err) {
                console.log("error occured")
            } else {
                // console.log(result)
            }
        })
    }

    _handleMessageSend(event) {
        // event.preventDefault();
        const message = $("#messageText").val();
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;
        const seller = "seller";

        if(message) {
            Meteor.call('messages.insert', message, userId, sellerId, seller);
            $("#messageText").val("");
        }
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;
        console.log(emojiCode);
        $("#messageText").val(emojiCode);
    }

    _handleOpenEmoji() {
        console.log("open emoji");
        this.setState({openEmoji: !this.state.openEmoji});
    }

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#vendor_attachFile")[0].files[0];
        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                console.log(uploader.progress());
            }
        })
    }

    _handleKeyPress(event) {
        if (event.key === 'Enter') {
          this._handleMessageSend();
        }
    }

    render() {
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;

        if(!userId) {
            return (
                <div className={"sectionblock1  vendor-chat-txt"}>
                    <div className={"no-user-selected-style"}>No User Selected</div>
                </div>
                )
        } else {
            return (
                <div className={"sectionblock1 no-padding padding-right-0"}>
                    <div className="vendor_chat_wrapper">
                        <div className={"vendor-message-list box_shadow_unset"}>
                            <MessageList userId={userId} sellerId={sellerId}/>
                        </div>

                        {this.state.openEmoji === true ? <div className={"web_vendor_emoji_block"}>
                                <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)} />
                            </div>:
                            ''
                        }

                        <div className={"vendor-chatTypeBox"}>

                        <ul className="list-group list-group-flush vendor_chat_box_container padding-20p-30p">
                            <li className="list-group-item list-background">
                                <div className={"float-lft list-div-left-background list-type-box"}>
                                    <div className={"vendorChatBlock"}>
                                        <div className={"block2"}>
                                            <label htmlFor="vendor_attachFile" >
                                            <span>
                                                <img
                                                    className={"list-img"}
                                                    src="/img/clip_white.svg"
                                                    alt=""
                                                />
                                            </span>
                                            </label>
                                            <div onChange={this._handleFileUpload.bind(this)}>
                                                <input
                                                    type="file"
                                                    className="form-control-file"
                                                    id="vendor_attachFile"
                                                />
                                            </div>

                                        </div>
                                        <div className={"block1"}>
                                        <span>
                                            <img
                                                className={"list-img"}
                                                src="/img/smile_white.svg"
                                                alt=""
                                                onClick={this._handleOpenEmoji.bind(this)}/>
                                        </span>
                                        </div>
                                        
                                        <div className={"block3"}>
                                            <input id={"messageText"}
                                                className="type-search"
                                                type="text"
                                                placeholder="WRITE A MESSAGE"
                                                name="message"
                                                onKeyPress={this._handleKeyPress.bind(this)}
                                            />
                                        </div>
                                        <div className={"block4"}>
                                        <span className={"btn"} onClick={this._handleMessageSend.bind(this)}>
                                            <img className={"list-img"}  src="/img/send_image.svg" alt="" />
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        </div>
                    </div>

                </div>
            )
        }
    }
}

export default withTracker((data) => {
    const currentUserId = Meteor.userId();
    Meteor.subscribe('sellers');
    const seller = Sellers.findOne({"user" : currentUserId});
    let sellerId = '';
    if(seller) {sellerId = seller._id}
    return {
        sellerId: sellerId,
    }
})(ChatSection);

