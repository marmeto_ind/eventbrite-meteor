import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";

import Select from 'react-select';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {Regions} from "../../../api/regions";
import {EventType} from "../../../api/eventType";
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';
import {browserHistory} from "react-router";

class AddBookingSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openEmoji: false,
            date: 'Date',
            guestNumber: '',
            regions: [],
            events: [],
            selectedRegion: '',
            selectedEventType: '',
            eventId: ''
        }
    }

    componentDidMount() {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let date = url.searchParams.get("date");
        if(date) {
            this.setState({date: date});
        }
    }

    handleRegionChange(data) {
        this.setState({selectedRegion: data.value});
    }

    handleEventTypeChange(data) {
        this.setState({selectedEventType: data.value})
    }

    handleDayChange(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;

        if (day !== currentDate) {
            this.setState({date: day});
        }
    }

    _handleVendorAddBooking() {
        const user = Meteor.userId();
        const sellerId = this.props.sellerId;

        const eventName = $('#vendorEventName').val();
        const region = this.state.selectedRegion;
        const date = this.state.date.toString();
        const eventType = this.state.selectedEventType;
        const numberOfGuest = this.state.guestNumber;

        const self = this;

        if (!user) {
            browserHistory.push("/login");
        } else if (!eventName) {
            $(".event-name-err-msg").show().text("Event Name cannot be empty")
        } else if (!region) {
            $(".event-region-err-msg").show().text("Region is not selected")
        } else if (!eventType) {
            $(".event-type-err-msg").show().text("Event is not selected")
        }
        else if (date.length <= 4) {
            $(".event-date-err-msg").show().text("Date can not be empty")
        }
        else if (!numberOfGuest) {
            $(".event-guest-err-msg").show().text("Number of guest cannot be empty")
        } else {
            Meteor.call('events.insert',
                eventName,
                region,
                eventType,
                date,
                numberOfGuest,
                user, function (err, result) {
                    if(err) {
                        console.error(err);
                    } else {
                        self.setState({eventId: result});

                        // Now associate the event with the seller
                        Meteor.call('choices.insert', user, sellerId, "testcategory", result, function (err, result) {
                            if(err) {
                                console.log(err)
                            } else {
                                console.log("choice insert successful");
                                browserHistory.push("/vendor/agenda");
                            }
                        })
                    }
                });
        }
    }

    handleInputChange(event) {
        const value = event.target.value;

        if (value < 999999) {
            this.setState({guestNumber: event.target.value})
        }
    }

    render() {

        const errorStyle = {
            "display": "none",
            "color": "red",
            "textAlign": "center"
        };

        return (
            <div className={"sectionblock1  vendor-chat-txt mobile-create-event-block addBookingDiv"}>
                {/* <div className={"no-user-selected-style"}>No User Selected</div> */}

                <div className="create-event-div name">
                    <input
                        type="text"
                        className="create-event-input-class"
                        placeholder="Enter Event"
                        id={"vendorEventName"}
                    />
                    <p className={"create-event-p"}>Name</p>
                </div>
                <p className={"event-name-err-msg"}
                   style={errorStyle}></p>

                <div className="create-event-div event">
                    <div className={"mobile-event-dropdown-class region-dropdown"}>
                        <Select
                            name="form-field-name"
                            id={"vendoreventTypeName"}
                            placeholder={"Event"}
                            onChange={this.handleEventTypeChange.bind(this)}
                            options={this.props.eventDropdown}
                            value={this.state.selectedEventType}
                        />
                    </div>
                    <p className={"create-event-p"}>Type</p>
                </div>
                <p className={"event-type-err-msg"}
                   style={errorStyle}></p>

                {/*<div className="create-event-div region">*/}
                    {/*<div className={"mobile-region-dropdown-class region-dropdown"}>*/}
                        {/*<Select*/}
                            {/*name="form-field-name"*/}
                            {/*id={"vendorregionName"}*/}
                            {/*placeholder={"Region"}*/}
                            {/*onChange={this.handleRegionChange.bind(this)}*/}
                            {/*options={this.props.regionDropdown}*/}
                            {/*value={this.state.selectedRegion}*/}
                        {/*/>*/}
                    {/*</div>*/}
                    {/*<p className={"create-event-p"}>Region</p>*/}
                {/*</div>*/}
                {/*<p className={"event-region-err-msg"}*/}
                   {/*style={errorStyle}></p>*/}

                <div className="create-event-div mobile-date-class date">
                    <DayPickerInput
                        onDayChange={day => this.handleDayChange(day)}
                        value={this.state.date}
                        dayPickerProps={{disabledDays: {before: new Date()}}}
                    />
                    <p className={"create-event-p"}>Date</p>
                </div>
                <p className={"event-date-err-msg"}
                   style={errorStyle}></p>

                <div className="create-event-div guest">
                    <input
                        type="text"
                        className="create-event-input-class"
                        placeholder="Enter Guest"
                        value={this.state.guestNumber}
                        onChange={this.handleInputChange.bind(this)}
                    />
                    <p className={"create-event-p"}>Guest</p>
                </div>
                <p className={"event-guest-err-msg"}
                   style={errorStyle}></p>

                <div className={"mobile-create-button-div add-booking-btn"}>
                    <button className="mobile-create-event-button" onClick={this._handleVendorAddBooking.bind(this)}>
                        <span>Book</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default withTracker((data) => {
    const currentUserId = Meteor.userId();
    Meteor.subscribe('sellers');
    const seller = Sellers.findOne({"user": currentUserId});
    let sellerId = '';
    if (seller) {
        sellerId = seller._id
    }

    Meteor.subscribe('regions.all');
    Meteor.subscribe('event_types.all');

    const regions = Regions.find({}).fetch();
    const eventTypes = EventType.find({}).fetch();

    let regionDropdown = [];
    let eventDropdown = [];

    regions.map((region) => {
        const value = {value: region.name, label: region.name};
        regionDropdown.push(value);
    });

    eventTypes.map((eventType) => {
        const value = {value: eventType.name, label: eventType.name};
        eventDropdown.push(value);
    });

    return {
        regionDropdown: regionDropdown,
        eventDropdown: eventDropdown,
        sellerId: sellerId
    }
})(AddBookingSection);

