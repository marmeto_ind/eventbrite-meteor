import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {ChatWith} from "../../../api/chatWith";
import ShowEachUser from './ShowEachUserInList';

class ChatUserList extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        let userId = Meteor.userId();
        Meteor.call('chat_with.insertSellerChatWithAdmin', userId, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                // console.log(result);
            }
        })
    }

    renderUsers(users) {
        const parentCallback = this.props.parentCallback;
        return users.map((chatUser, index) => (
            <ShowEachUser key={index} chatUser={chatUser} parentCallback={parentCallback}/>
        ))
    }

    render() {
        const users = this.props.users;

        return (
            <div className={"sectionblock2 vendor-chatRightPadding"}>
                <ul className="list-group list-group-flush vendorlistMenu">
                    {this.renderUsers(users)}
                </ul>
            </div>
        )
    }
}

export default withTracker(() => {
    const user = Meteor.userId();
    let sellerId = '';
    Meteor.subscribe('sellers');
    Meteor.subscribe('chat_with');
    const seller = Sellers.findOne({"user": user});
    if (seller) {
        sellerId = seller._id
    }
    const users = ChatWith.find({"seller": sellerId}).fetch();
    return {
        users: users,
    }
})(ChatUserList)