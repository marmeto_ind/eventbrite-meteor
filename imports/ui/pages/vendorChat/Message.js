import React from 'react';
import {emojify} from 'react-emojione';

class Message extends React.Component {
    showMessage(message) {
        if(this.isUrl(message)) {
            const fileUrl = <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;

            return fileUrl;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(s);
    }

    render() {
        const message = this.props.message;
        
        if(message.sender == "seller") {
            return (
                <li className="list-group-item list-background">
                    <div className={"list-div-left-background send-by-seller"}>
                        <div className={"message"}>
                            <div className={"text"}>
                                <span>{this.showMessage(message.message)}</span>
                            </div>
                            <div className={"icon"}>
                                <img className={"list-img chat-list-logo"} src="/img/user-login.png" alt="" />
                            </div>
                        </div>
                    </div>
                </li>
            )
        } else {
            return (
                <li className="list-group-item list-background">
                    <div className={"list-div-left-background send-by-user"}>
                        <div className={"message"}>
                            <div className={"icon"}>
                                <img className={"list-img chat-list-logo"} src="/img/user-login.png" alt="" />
                            </div>
                            <div className={"text"}>
                                <span>{this.showMessage(message.message)}</span>
                            </div>
                        </div>
                    </div>
                </li>
            )
        }
    }
}

export default Message;