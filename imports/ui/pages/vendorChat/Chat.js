import React from 'react';
import './chat.scss';
import Header from '../header-username/header';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/vendorTopBar/TopBar';
import ChatSection from './ChatSection';
import ChatUserList from './ChatUserList';
import VendorLeftSideBar from "../vendorLeftSideBar/vendorLeftSideBar";

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
        };
        this.childCallback = this.childCallback.bind(this);
    }

    childCallback(data) {
        this.setState({userId: data})
    }

    render() {
        const userId = this.state.userId;
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container_vendor">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding vendorChat">
                            <div className={"vendorMyserviceTitleBlock"}>
                                <div className={"vendorMyServiceTtext vendorMyServiceTextDiv"}>
                                    MY SERVICE
                                </div>
                                <div className={"vendorMyServiceHr"}>
                                    <img className="svg-img-style" src="/img/line.png" />
                                </div>
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>
                            <div className="clearfix"></div>
                            <VendorLeftSideBar/>
                        </div>
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 vendor_chat_container">
                            <div className={"vendorChatTitleBlock"}>
                                <div className={"vendorChatText vendorChatTextDiv"}>
                                 CHAT
                                </div>
                                <div className={"vendorChatHr"}>
                                    <img className={"vendor_home_line_svg"} src={"/img/line.svg"} />
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <div className={"col-md-12 display_flex chatsection-padding padding_left_0"}>
                                <ChatSection userId={userId}/>
                                <ChatUserList parentCallback={this.childCallback}/>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Chat;














