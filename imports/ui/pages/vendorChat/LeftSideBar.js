import React from 'react';

class LeftSideBar extends React.Component {
    render() {
        return (
            <div className="menubar-left collapse navbar-collapse left-menu-padding" id="myNavbar">
                <ul className="nav agenda-left-slider">
                    <li className={"agenda-leftbar-list-item"} color="secondary">
                        <span id={"left-icon"}><img className="agenda-left-slider-img" src="/img/Church.png" alt="" /></span>
                        <span id={"vendor-agenda-seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><img className="agenda-left-slider-img agenda-right-img" src="/img/show-tick.png" alt="" /></span>
                    </li>
                    <li className={"agenda-leftbar-list-item"} color="secondary">
                        <span id={"left-icon"}><img className="agenda-left-slider-img" src="/img/Church.png" alt="" /></span>
                        <span id={"vendor-agenda-seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><img className="agenda-left-slider-img agenda-right-img" src="/img/show-tick.png" alt="" /></span>
                    </li>
                    <li className={"agenda-leftbar-list-item"} color="secondary">
                        <span id={"left-icon"}><img className="agenda-left-slider-img" src="/img/Church.png" alt="" /></span>
                        <span id={"vendor-agenda-seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><img className="agenda-left-slider-img agenda-right-img" src="/img/show-tick.png" alt="" /></span>
                    </li>
                    <li className={"agenda-leftbar-list-item"} color="secondary">
                        <span id={"left-icon"}><img className="agenda-left-slider-img" src="/img/Church.png" alt="" /></span>
                        <span id={"vendor-agenda-seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><img className="agenda-left-slider-img agenda-right-img" src="/img/show-tick.png" alt="" /></span>
                    </li>
                    <li className={"agenda-leftbar-list-item"} color="secondary">
                        <span id={"left-icon"}><img className="agenda-left-slider-img" src="/img/Church.png" alt="" /></span>
                        <span id={"vendor-agenda-seller"}>panorama eventcenter</span>
                        <span id={"category"}>venue</span>
                        <span id={"right-icon"}><img className="agenda-left-slider-img agenda-right-img" src="/img/show-tick.png" alt="" /></span>
                    </li>
                </ul>
            </div>
        )
    }
}

export default LeftSideBar;