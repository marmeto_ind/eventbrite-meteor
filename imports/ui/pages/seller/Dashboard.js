import React from 'react';
import SellerHeader from '../../components/header/vendor/Header';
import SellerFooter from '../../components/footer/vendor/Footer';
import SellerBody from '../../components/vendorSection/BodySection';

class Dashboard extends React.Component {
    render() {
        return (
            <div className={"seller-dashboard"}>
                <SellerHeader/>
                <SellerBody/>
                <SellerFooter/>
            </div>
        )
    }
}

export default Dashboard;