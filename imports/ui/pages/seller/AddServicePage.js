import React from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import SellerAddBusiness from '../../components/seller/SellerAddBusiness';

class AddServicePage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <SellerAddBusiness/>
                <Footer/>
            </div>
        )
    }
}

export default AddServicePage;
