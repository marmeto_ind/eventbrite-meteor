import React from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import SellerSettings from '../../components/seller/SellerSettings';

class SellerSettingsPage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <SellerSettings/>
                <Footer/>
            </div>
        )
    }
}

export default SellerSettingsPage;
