import React from 'react';
import UserService from './UserService';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class UserServiceContainer extends React.Component {

    render() {
        const sellerId = this.props.sellerId;
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    // Meteor.logout();
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <UserService sellerId={sellerId}/>
                    )
                }
            }
        }
    }
}

export default withTracker((data) => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
        sellerId: data.params.id
    }
})(UserServiceContainer);

