import React from 'react';
import Header from '../header-username/header';
import './user-service.scss';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/userTopBar/TopBar';
import LeftSideBar from '../../components/userLeftSideBar/LeftSideBar';
import Business from './Business';
import {Meteor} from 'meteor/meteor';

class UserService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
            seller: '',
        };
    }

    componentDidMount() {
        const self = this;
        const userId = Meteor.userId();
        const sellerId = this.props.sellerId;
        if (sellerId) {
            Meteor.call("seller_views.insert", userId, sellerId);

            Meteor.call('seller.findOne', sellerId, function (err, result) {
                if(err) {
                    console.log(err)
                } else {
                    self.setState({seller: result});
                }
            });
        }        
    }

    _handleSearchKeyChange(event){
        let searchKey = event.target.value;
        this.setState({searchKey: searchKey});
    }

    render() {        
        const sellerId = this.props.sellerId;
        
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>                  
                    <div className="clearfix"></div>
                    <div className="body_container">
                        <LeftSideBar searchKey={this.state.searchKey}/>
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 no-padding">
                            <div className="page_heading_tex">
                                Vendor &nbsp;&nbsp;
                            </div>
                            {
                                this.state.seller? 
                                <Business sellerId={sellerId} seller={this.state.seller} />
                                :""
                            }
                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default UserService;