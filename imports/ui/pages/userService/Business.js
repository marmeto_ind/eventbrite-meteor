import React from 'react';
// import YouTube from 'react-youtube';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import $ from 'jquery';
import { withTracker } from 'meteor/react-meteor-data';
// import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {Choices} from "../../../api/choice";
// import ReactPlayer from 'react-player';
// import {SellerFacilities} from "../../../api/sellerFacilities";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class GoldenStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/10.png" alt="" />
            </span>
        )
    }
}

class WhiteStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/11.png" alt="" />
            </span>
        )
    }
}

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage: '',
        }
    }

    componentDidMount() {
        $(".userServicePagePosition").css('top','0');
        const facility = this.props.facility;
        // console.log(facility);
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    // console.log(result);
                    self.setState({facilityImage: result.image});
                }
            }
        });
    }

    render() {
        const facility = this.props.facility;
        const facilityImage = this.state.facilityImage;
        const FacilitymainImage = facilityImage? facilityImage: "/img/newService/PARKING.png";

        return (
            <li className="active">
                <img src={FacilitymainImage} alt="" />
                <br />
                <span>{facility.name}</span>
            </li>
        )
    }
}

class ShowImage extends React.Component {
    render() {
        const image = this.props.image;
        // const showTag = this.props.showTag;
        return (
            <div id={"dashboard_homeImg"} className="body_portion">
                {/* {showTag()} */}
                <img src={image} className="slider_image_sell" />
            </div>
        )
    }
}

class Business extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            seller: '',
            review: '',
        }
    }

    renderOtherImages({images, seller}) {
        return images.map((image, index) => (
            <ShowImage image={image} key={index} showTag={this.showTag}/>
        ))
    }

    _handleChatButtonClick(e) {
        const userId = Meteor.userId();

        const sellerId = this.props.sellerId;
        const sellerName = this.props.seller.name;
        const userName = Meteor.user().profile.name;

        Meteor.call('chat_with.insert', userId, sellerId, sellerName, userName);
        browserHistory.push('/user/chat');
    }

    _handleAddChoice(e) {
        const userId = Meteor.userId();
        const seller = this.props.seller;
        const sellId = this.props.sellerId;
        const eventId = this.props.eventId;
        const categoryId = seller.category._id;

        /*
        choices.insert'(user, seller, categoryId, eventId)
        */

        Meteor.call('choices.insertOrRemove', userId, sellId, categoryId, eventId);

        // Meteor.call('choices.insert', userId, sellId, categoryId, eventId);
    }

    renderRating(){
        const review = this.state.review;
        const extra = 5 - review;
        var reviewComponent = [];

        for (var i=0; i < review; i++) {
            reviewComponent.push(<GoldenStar key={i} />);
        }
        for (var j=0; j < extra; j++) {
            reviewComponent.push(<WhiteStar key={i + j} />);
        }

        return <div className="rating">{reviewComponent}</div>;
    }

    renderFacilities() {
        const seller = this.props.seller;
        const facilities = seller.profile.facilities;

        if(!facilities) {
            return (<div></div>)
        } else {
            return facilities.map((facility, index) => (
                <ShowEachFacility facility={facility} key={index}/>
            ))
        }
    }

    componentDidMount() {
        const self = this;
        const eventId = Session.get("eventId");
        const userId = Meteor.userId();
        const sellerId = this.props.sellerId;

        // // console.log(sellerId);
        // Meteor.call('seller.findOne', sellerId, function (err, result) {
        //     if(err) {
        //         console.error(err)
        //     } else {
        //         // console.log(result);
        //         self.setState({seller: result});
        //     }
        // });

        Meteor.call('reviews.getSellerRating', sellerId, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({review: result});
            }
        })

    }

    showTag(){
        const seller = this.props.seller;
        return (
        <div className="overlap_image_container">
            {seller.profile.hotTag? <img src="/img/tags_icon/TAGS_HOT.png" alt="tag" />: ''}
            {seller.profile.goodDealTag? <img src="/img/tags_icon/TAGS_GOOD_DEAL.png" alt="tag" />: ''}
            {seller.profile.newTag? <img src="/img/tags_icon/TAGS_NEW.png" alt="tag" />: ''}            
        </div>
        )        
    }

    render() {

        const seller = this.props.seller;
        const choiceId = this.props.choiceId;     

        if(!seller) {
            return (
                <div></div>
            )
        } else {
            return (
                <div className="Business-profile-conainer no-padding  user-service-page user_vendor_wrapper">
                    <div className="mydash_bannner seller-mainImage-div">
                        {/* <div className="body_portion_banner">
                            <div className="banner">
                                <img className={"service-main-image"} src={seller?
                                    seller.profile.mainImage: ""
                                } />
                            </div>
                        </div>

                        <div className="div-float second-choice-div userservice-bestDeal">
                            <p className={"second-best-deal best-deal"}>Best Deal</p>
                            <p className={"second-hot hot"}>Hot</p>
                        </div>

                        <div className={"seller-name-div"}>
                            <div className={"userservice-name-div"}>
                                <p className={"seller-name-text"}>{seller.name}</p>
                                <p className={"seller-address-text"}>{seller.profile.address}</p>
                            </div>
                            <div className={"userservice-star-div"}>
                                <div className="service--star user-service-logoimgStar">
                                    {this.renderRating()}
                                </div>
                            </div>

                        </div> */}
                        <Carousel>
                            {this.renderOtherImages({images: seller.profile.moreImages, seller})}
                            
                            <div id={"dashboard_homeImg"} className="body_portion">
                                {this.showTag()}
                                <img src={seller.profile.mainImage} className="slider_image_sell" />
                            </div>
                            <div id={"dashboard_homeImg"} className="body_portion">
                                {this.showTag()}
                                <img src={seller.profile.logoImage} className="slider_image_sell" />
                            </div>
                        </Carousel>
                    </div>
                    <div className="clearfix"></div>


                    <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 body_portion_ev res-no-padding">
                        <div className="event-right-side-element">
                            {/* <div className="upper-section">
                                <div className="gallery slideShowScroll">
                                    {seller?
                                        this.renderOtherImages(seller.profile.moreImages):
                                        []
                                    }

                                </div>
                                <div className="circle-panorama">
                                    <div className="img_crcl_b user-service-logoimg">
                                        <img className={"circle-image"}
                                             src={seller? seller.profile.logoImage: ''}
                                             alt={""}
                                        />
                                    </div>
                                </div>
                            </div> */}
                            <div className="clearfix"></div>
                            
                            <div className="button_evet ">
                                <button className="button_evet_left"
                                        onClick={this._handleAddChoice.bind(this)}>
                                    Choose
                                    <span className="circle_m">
                                        {
                                            choiceId?
                                            <img className="mychoice-star-logo" src="/img/mychoice-tick.png" alt="" />
                                            : ''
                                        }
                                    </span>
                                </button>
                                <button className="button_evet_right"
                                        onClick={this._handleChatButtonClick.bind(this)}>
                                    Chat
                                </button>
                                
                                <div className="center_circle_box">
                                    <img className={"circle-image"}
                                             src={seller? seller.profile.logoImage: ''}
                                             alt={""}
                                        />
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            {/* <div className="black_container user-service-box ">
                                <div className="userServiceDivBlock">
                                    {this.renderFacilities()}
                                </div>
                            </div> */}

                            <div className="black_container_box no-padding">
                                <div className="check_box_item full-width-design">
                                    <div className="box-container same-icon">
                                        <p className="star-print">
                                            <span>
                                           <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                        </span>
                                        </p>
                                        {/* <div className="heading-text">
                                            <h2>STANDARD PACK</h2>
                                        </div> */}
                                        <div className="heading-text">
                                            <h2>Panorama service Center</h2>
                                            <h3 className="loacation">{seller.profile.standardPack}</h3>
                                            {/* <h4>Marmeto</h4>
                                            <h5>India</h5> */}
                                        </div>
                                        {/* <h4 className={"pack-text"}>{seller.profile.standardPack}</h4> */}
                                        {/*<b className={"pack-small-text"}>Welcome drink / chocolate</b>*/}
                                        {/*<p className={"pack-small-text"}>Lorem ipsum Lorem ipsum Lorem ipsum </p>*/}
                                    </div>
                                </div>
                            </div>

                            <div className="black_container_box no-padding ">
                                <div className="facility_wrapper">
                                    <ul>
                                        {this.renderFacilities()}
                                        {/* <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li> */}
                                        {/* <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li> */}
                                        {/* <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li>
                                        <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li> */}
                                        {/* <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li>
                                        <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li>
                                        <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li> */}
                                        {/* <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li>
                                        <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li>
                                        <li className="active">
                                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/PARKING.png" alt="" />
                                            <br />
                                            <span>parking</span>
                                        </li> */}
                                    </ul>
                                    {/* {this.renderFacilities()} */}

                                </div>
                            </div>

                            <div className="black_container_box no-padding">
                                <div className="check_box_item full-width-design">
                                    <div className="box-container same-icon">
                                        <p className="star-print">
                                            <span>
                                           <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                        </span>
                                        </p>
                                        {/* <div className="heading-text">
                                            <h2>STANDARD PACK</h2>
                                        </div> */}
                                        <div className="heading-text">
                                            <h2>STANDARD PACK</h2>
                                            <h3>{seller.profile.standardPack}</h3>
                                            {/* <h4>Marmeto</h4>
                                            <h5>India</h5> */}
                                        </div>
                                        {/* <h4 className={"pack-text"}>{seller.profile.standardPack}</h4> */}
                                        {/*<b className={"pack-small-text"}>Welcome drink / chocolate</b>*/}
                                        {/*<p className={"pack-small-text"}>Lorem ipsum Lorem ipsum Lorem ipsum </p>*/}
                                    </div>
                                </div>
                            </div>
                            <div className="black_container_box no-padding">
                                <div className="check_box_item full-width-design">
                                    <div className="box-container same-icon">
                                        <p className="star-print">
                                        <span>
                                           <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                        </span>
                                            <span>
                                           <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                        </span>
                                        </p>
                                        {/* <div className="heading-text">
                                            <h2>GENEROUS PACK</h2>
                                        </div> */}
                                        <div className="heading-text">
                                            <h2 className="generous">GENEROUS PACK</h2>
                                            <h3>{seller.profile.generousPack}</h3>
                                            {/* <h4>Marmeto</h4>
                                            <h5>India</h5> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="black_container_box no-padding">
                                <div className="check_box_item full-width-design">
                                    <div className="box-container same-icon">
                                        <p className="star-print">
                                            <span>
                                               <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                            </span>
                                                <span>
                                               <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                            </span>
                                                <span>
                                               <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                            </span>
                                        </p>
                                        <div className="heading-text">
                                            <h2 className="exclusive">EXCLUSIVE PACK</h2>
                                            <h3>{seller.profile.exclusivePack}</h3>
                                            {/* <h4>Marmeto</h4> */}
                                            {/* <h5>India</h5> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="black_container_box no-padding backgroundNone">
                                <div className="check_box_item full-width-design">
                                    <div className="box-container same-icon">
                                        <div className="heading-text">
                                            <h2 className="moreifo_text">More Info</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            {/* <div className="box-elemnt">
                            </div>
                            <div className="box-elemnt">
                                
                                <ReactPlayer
                                    url={seller.profile.video}
                                    controls={true}
                                    width='100%'
                                    height='100%'
                                />
                            </div> */}
                            <div className="black_container_box black_container_box_input no-padding relativePosition">
                                <div className="check_box_item service-email-width-design">
                                    <div className="box-container box-container-textBox email">
                                        <h2 className={"service-bottom-text-box"}>
                                            {seller.profile.email}
                                        </h2>
                                        <p>Mail</p>
                                    </div>
                                </div>
                            </div>
                            <div className="black_container_box black_container_box_input no-padding relativePosition">
                                <div className="check_box_item service-call-width-design">
                                    <div className="box-container box-container-textBox contact">
                                        <h2 className={"service-bottom-text-box"}>
                                            {seller.profile.phone}
                                            </h2>
                                        <p>Phone</p>
                                    </div>
                                </div>
                            </div>
                            <div className="black_container_box black_container_box_input no-padding relativePosition">
                                <div className="check_box_item service-internet-width-design">
                                    <div className="box-container box-container-textBox website">
                                        <h2 className={"service-bottom-text-box"}>
                                            {seller.profile.website}
                                            </h2>
                                        <p>Website</p>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="black_container_box black_container_box_input no-padding relativePosition">
                                <div className="check_box_item service-phone-width-design">
                                    <div className="box-container credential">
                                        <h2 className={"service-bottom-text-box"}>{seller.profile.credential}</h2>
                                        <p>Credential</p>
                                    </div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            )
        }
    }
}

export default withTracker((data) => {
    const sellerId = data.sellerId;
    const eventId = Session.get("eventId");
    const userId = Meteor.userId();

    Meteor.subscribe('choices.all');
    const choice = Choices.findOne({
        "user" : userId,
        "seller" : sellerId,
        "eventId" : eventId,
    });

    return {
        eventId: eventId,
        choiceId: choice?choice._id:'',
    }
})(Business);
