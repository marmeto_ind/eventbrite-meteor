import React from 'react';

class ShowEachUserChoice extends React.Component {
    render() {
        // console.log(this.props.user);
        const name = this.props.user.name;
        const createdAt = this.props.user.createdAt.toString();

        // console.log(createdAt);

        return (
            <li className="list-group-item list-right-padding-style">
                <div className={"vendorStatsList"}>
                    <div className={"vendorStatsFloatLeft"}>
                        <span>
                            <img className={"list-img"} src="/img/greyStatue.png" alt="" />
                        </span>
                    </div>
                    <div className={"vendorStatsTextCenter"}>
                        <p>{name}</p>
                        {/*<p className={"vendor-stats-p"}>Couple</p>*/}
                    </div>
                    <div className={"vendorStatsFloatRight"}>
                        <p>{createdAt.substring(0, 4)}</p>
                        <p className="eventTime">{createdAt.substring(16, 21)}</p>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachUserChoice;