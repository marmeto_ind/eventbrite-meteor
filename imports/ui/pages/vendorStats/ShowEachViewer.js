import React from 'react';

class ShowEachViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
        }
    }

    componentDidMount() {
        const user = this.props.user;
        const self = this;
        
        Meteor.call('users.getUserName', user.userId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                // console.log(result);
                self.setState({user: result});
            }

        })
    }

    render() {
        const user = this.props.user;
        const viewer = this.state.user;

        return (
            <li className="list-group-item list-right-padding-style">
                <div className={"vendorStatsList"}>
                    <div className={"vendorStatsFloatLeft"}>
                        <span><img className={"list-img"} src="/img/greyStatue.png" alt="" /></span>
                    </div>
                    <div className={"vendorStatsTextCenter"}>
                        <p>{viewer? viewer.profile.name: ''}</p>
                        <p className={"vendor-stats-p"}>Couple</p>
                    </div>
                    <div className={"vendorStatsFloatRight"}>
                        <p>{user.createdAt.toString().substring(0,4)}</p>
                        <p className="eventTime">{user.createdAt.toString().substring(16,21)}</p>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachViewer;