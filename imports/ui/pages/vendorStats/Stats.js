import React from 'react';
import './stats.scss';
import Header from '../header-username/header';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/vendorTopBar/TopBar';
import VendorLeftSideBar from "../vendorLeftSideBar/vendorLeftSideBar";
import ShowViewer from './ShowViewer';
import ShowUserChoice from './ShowUserChoice';
import VendorChoice from './VendorChoice';
import VendorView from './VendorView';
import VendorReview from './VendorReview';

class Stats extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>
                    {/* <div className="clearfix"></div> */}
                    <div className="clearfix"></div>
                    <div className="body_container_wrapper chat-body_wrapper">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding vendorStats">
                            <div className={"vendorMyserviceTitleBlock"}>
                                <div className={"vendorMyServiceTtext vendorMyServiceTextDiv"}>
                                    MY SERVICE
                                </div>
                                <div className={"vendorMyServiceHr"}>
                                    <img className="svg-img-style" src="/img/line.png" />
                                </div>
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>
                            <div className="clearfix"></div>
                            <VendorLeftSideBar/>
                        </div>
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 vendor_stats_container">
                            <div className={"col-md-12 display_flex padding no-padding"}>
                                <div className={"sectionblock1 leftPading"}>
                                    <div className={"vendorStatsHrTitleBlock"}>
                                        <div className={"vendorStatsText vendorStatsTextDiv"}>
                                            STATISTICS
                                        </div>
                                        {/* <div className={"vendorStatsHr"}>
                                            <img className="svg-img-style" src="/img/line.png" />
                                        </div> */}
                                    </div>
                                    <div className="clearfix"></div>
                                    <VendorChoice/>
                                    <VendorView/>
                                    <VendorReview/>
                                </div>
                                <div className={"sectionblock2 vendor-right-list-padding"}>
                                    {/* <div className={"vendorLatestHrTitleBlock"}>
                                        <div className={"vendorLatestText vendorLatestTextDiv"}>
                                            Latest Choices
                                        </div>
                                        <div className={"vendorLatestHr"}>
                                            <img className={"svg-img-style"} src={"/img/line.png"} />
                                        </div>
                                    </div> */}
                                    <div className="clearfix"></div>
                                    {/* <div>
                                        <ShowUserChoice/>
                                    </div> */}
                                    {/*<div className={"upcoming"}>*/}
                                        {/*<div className={"col-md-12 upcoming-padding"}>*/}
                                            {/*<div className={"col-md-6 upcoming-text latest-viewers"}>*/}
                                                {/*LATEST VIEWERS*/}
                                            {/*</div>*/}
                                            {/*<div className={"col-md-6 viewers-hr-box"}>*/}
                                                {/*<hr/>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                    {/*</div>*/}
                                    {/* <div className={"vendorLatestViewersHrTitleBlock"}>
                                        <div className={"vendorLatestViewersText vendorLatestViewersTextDiv"}>
                                            Latest Viewers
                                        </div>
                                        <div className={"vendorLatestViewersHr"}>
                                            <img className={"svg-img-style"} src={"/img/line.png"} />
                                        </div>
                                    </div> */}
                                    <div className="clearfix"></div>
                                    {/* <ShowViewer/> */}
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Stats;