import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Choices} from "../../../api/choice";
import {Meteor} from 'meteor/meteor';
import ShowEachUserChoice from './ShowEachUserChoice';

class ShowUserChoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
        };
    }

    componentDidMount() {
        const self = this;
        const userId = Meteor.userId();
        Meteor.call('choices.getLatestUsers', userId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                // console.log(result);
                self.setState({users: result})
            }
        })
    }

    renderUserChoice() {
        if(this.state.users.length === 0) {
            // console.log("no user");
            return (
                <div className={"no-event-booked-style"}>No user present</div>
            )
        } else {
            // console.log(this.state.users);
            return this.state.users.map((user, index) => (
                <ShowEachUserChoice user={user} key={index} />
            ))
        }
    }

    render() {
        return (
            <ul className="list-group list-group-flush ">
                {this.renderUserChoice()}
            </ul>
        )
    }
}

export default ShowUserChoice;

// export default withTracker(() => {
//     Meteor.subscribe('choices.all');
//     const sellerId = Meteor.userId();
//     const users = Choices.find({"seller": sellerId}).fetch();
//     // console.log(users);
//     return {
//         users: users,
//     }
// })(ShowUserChoice);