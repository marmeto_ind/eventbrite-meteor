import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {SellerViews} from "../../../api/sellerViews";

class VendorView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const self = this;
        const userId = Meteor.userId();
        Meteor.call('seller_views.getReviewStats', userId, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                if(result) {
                    self.setState({
                        totalNumOfView: result.totalNumOfView,
                        numOfViewInLastWeek: result.numOfViewInLastWeek,
                        numOfViewInMonth: result.numOfViewInMonth,
                    })
                }
            }
        })
    }

    render() {

        return (
            <div className={"third-div views-background"}>
                <ul className="list-group vendor-stats-list">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/svg_img/greyEye.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {this.state.numOfViewInMonth} V/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {this.state.numOfViewInLastWeek} VIEW LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {this.state.totalNumOfView} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (430)
                    </li>
                </ul>
            </div>
        )
    }
}

export default VendorView;
