import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {SellerViews} from "../../../api/sellerViews";
import {Meteor} from 'meteor/meteor';
import ShowEachViewer from './ShowEachViewer';
import {Sellers} from "../../../api/sellers";

class ShowViewer extends React.Component {

    renderSellerViewer() {
        // console.log(this.props.users);

        if(this.props.users.length === 0) {
            return (
                <div className={"no-event-booked-style"}>No viewer</div>
            )
        } else {
            return this.props.users.map((user, index) => (
                <ShowEachViewer user={user} key={index} />
            ));
        }
    }

    render() {
        return (
            <div>
                <ul className="list-group list-group-flush ">
                    {this.renderSellerViewer()}
                </ul>
            </div>
        )
    }
}

// export default ShowViewer;

export default withTracker(() => {
    Meteor.subscribe('seller_views.all');
    const userId = Meteor.userId();

    Meteor.subscribe('sellers.all');
    const seller = Sellers.findOne({"user" : userId});

    let users = [];
    if(seller) {
        const sellerId = seller._id;
        users = SellerViews.find({"sellerId": sellerId}, {sort: {createdAt: -1},limit: 3}).fetch();
    }

    // console.log(seller);

    return {
        users: users,
    }
})(ShowViewer);