import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';

class ShowEachEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerName: '',
            eventTypeImage: '',
        };
    }

    componentDidMount() {
        const self = this;
        const event = this.props.event;
        const eventId = event._id;

        Meteor.call('events.getSellerName', eventId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                self.setState({sellerName: result});
            }
        });

        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    // console.log(result);
                    self.setState({eventTypeImage: result});
                }
            }
        })
    }

    render() {
        const event = this.props.event;
        // console.log(event);

        return (
            <div className={"second-div vendor_agenda_upcoming_event_div"}>
                <ul className="list-group">
                    <li className="list-group-item div-left-icon">
                        <img
                            className="create-add all-div-icon vendor_agenda_upcoming_event_img "
                            src={this.state.eventTypeImage? this.state.eventTypeImage: "/img/greyStatue.png"}
                            alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1 agenda-all-div-text1">
                        <p> {event.name} </p>
                    </li>
                    <li className="list-group-item text-list-div-style">
                        <hr className={"agenda-hr"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        <p className={"event-agenda-date-style"}> {event.date} </p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        <p>{this.state.sellerName}</p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4 agenda-all-div-text1">
                        <p> ({event.numOfGuest}) </p>
                    </li>
                </ul>
            </div>
        )
    }
}


class UpcomingEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
        }
    }

    componentDidMount() {
        // console.log("component did mount");
        // console.log(this.props);
        const self = this;
        Meteor.call('events.getUpcomingEventsOfSeller', Meteor.userId(), function (err, result) {
            if(err) {
                console.error(err);
            } else {
                // console.log(result);
                self.setState({events: result});
            }
        });
    }

    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps);
    //     const self = this;
    //     Meteor.call('events.getUpcomingEventsOfSeller', Meteor.userId(), function (err, result) {
    //         if(err) {
    //             console.error(err);
    //         } else {
    //             // console.log(result);
    //             self.setState({events: result});
    //         }
    //     });
    // }

    renderEvents(events) {
        return events.map((event, index) => (
            <ShowEachEvent event={event} key={index} />
        ))
    }

    render() {
        // console.log(this.props);
        // const events = this.props.events;
        // console.log(events);
        const events = this.state.events;

        return (
            <div className={"agenda-comping-events"}>
                {this.renderEvents(events)}
            </div>
        )
    }
}

// export default UpcomingEvents;

export default withTracker(() => {
    // Meteor.subscribe('events.all');
    // const events = Events.find().fetch();

    return {
        // events: events,
    }
})(UpcomingEvents);