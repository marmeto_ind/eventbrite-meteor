import React from 'react';
// import { withTracker } from 'meteor/react-meteor-data';
// import {Events} from "../../../api/events";
// import {Meteor} from 'meteor/meteor';
// import {browserHistory} from "react-router";

class ShowEachEvent extends React.Component {
    render() {
        const event = this.props.event;
        const eventTag = {
            fontSize: '11px',
        };

        return (
            <li className="list-group-item">
                <div className="left_image">
                    <img src="/img/guset_topBar.png" />
                </div>
                <div className="content_box">
                    <h3>{event.name}</h3>
                    <h4>{event.date}</h4>
                    <h5>{event.region}</h5>
                    <h6>({event.numOfGuest})</h6>
                </div>
            </li>
        )
    }
}

class ShowEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
        }
    }

    renderEvent() {
        return this.props.events.map((event, index) => (
            <ShowEachEvent key={index} event={event}/>
        ))
    }

    render() {
        const events = this.props.events;
        
        if(events.length === 0) {
            return (
                <div className={"no-event-booked-style"}>No Event booked</div>
            )
        } else {
            return (                
                <ul className="list-group">
                    {this.renderEvent()}
                </ul>
            )
        }
    }
}

export default ShowEvent;
