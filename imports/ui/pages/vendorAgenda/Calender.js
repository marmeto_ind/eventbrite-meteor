import React from 'react';
import $ from 'jquery';
import 'jquery-ui';
import 'moment/min/moment.min';
import 'fullcalendar/dist/fullcalendar.css';
import 'fullcalendar/dist/fullcalendar';
import moment from "moment";
import Calendar from './ShowCalendar';

class App extends React.Component {
    render() {
        const parentCallback = this.props.parentCallback;
        const parentCallbackSetDate = this.props.parentCallbackSetDate;

        return (
            <div className="App">
                <Calendar parentCallback={parentCallback} parentCallbackSetDate={parentCallbackSetDate} />
            </div>
        );
    }
}

export default App;