import React from 'react';
import $ from 'jquery';
import 'jquery-ui';
import 'moment/min/moment.min';
import 'fullcalendar/dist/fullcalendar.css';
import 'fullcalendar/dist/fullcalendar';
import moment from "moment";
import { withTracker } from 'meteor/react-meteor-data';

class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: '',
            dates: [],
            complete: false
        }
    }

    handleDayClick(date, jsEvent, view) {
        const parentCallback = this.props.parentCallback;
        this.setState({selectedDate: date.toDate().toString().substring(0, 15)});

        parentCallback(this.state.selectedDate);
        $(this).css('background-color', 'red');
    }

    handleSpecificDayIcon(date,cell) {
        _.each(this.state.dates, function (data) {
            const specificEventDate = moment(data).add(12, 'hours');
            if (date.isSame(specificEventDate, "day")) {
                cell.css("background-image", "url('/img/greyStatue.png')");
                cell.css("background-size", "40px 40px");
                cell.css("position", "relative");
                cell.css("background-repeat", "no-repeat");
                cell.css("left", "10px");
            }
        })
    }

    componentDidMount(){
        const self = this;
        const userId = Meteor.userId();

        const parentCallbackSetDate = this.props.parentCallbackSetDate;

        Meteor.call('events.getDates', userId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                self.setState({
                    dates: result,
                    complete: true,
                });
                parentCallbackSetDate(result);
            }
        });

    }

    render() {
        if(this.state.complete === true ) {
            $('#calendar').fullCalendar({
                header : {
                    left : "prev",
                    center : "title",
                    right : "next"
                },
                dayRender: this.handleSpecificDayIcon.bind(this),
                dayClick: this.handleDayClick.bind(this)
            });
        }

        return (
            <div id="calendar"></div>
        )
    }
}

export default Calendar;

