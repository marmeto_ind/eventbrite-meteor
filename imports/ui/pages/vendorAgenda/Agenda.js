import React from 'react';
import './agenda.scss';
import Calender from './Calender';
import Header from '../header-username/header';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/vendorTopBar/TopBar';
import VendorLeftSideBar from "../vendorLeftSideBar/vendorLeftSideBar";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import ShowEvent from './ShowEvent';
import UpcomingEvents from './UpcomingEvents';
import {Meteor} from "meteor/meteor";
import {browserHistory} from "react-router";
import RegionSelect from '../../components/body/RegionSelect';
import { toast, ToastContainer } from 'react-toastify';

class Agenda extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            dates: [],
            guestNumber: '',
            events: []
        }
        this._callbackFromRegionSelect = this._callbackFromRegionSelect.bind(this);
    }

    componentWillMount() {
        const date = new Date().toString().substring(0, 15);
        this.setState({date: date});
    }

    parentCallbackSetDate(dates) {
        this.setState({dates: dates})
    }

    parentCallback(date) {
        this.setState({date: date});
    }

    handleInputChange(event) {
        const value = event.target.value;
        if(value < 999999) {
            this.setState({guestNumber: value})
        }
    }

    _callbackFromRegionSelect(data) {
        this.setState({region: data.label});
    }

    errorNotify = (message) => {
        toast.error(message, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    _handleEventCreate(event){
        const user = Meteor.userId();
        const eventName = $('#eventName').val();
        const region = this.state.region;
        const date = this.state.date.toString();
        const eventType = this.state.eventType;
        const numberOfGuest = $('#numOfGuest').val();

        if(!user) {
            browserHistory.push("/login");
        } else if (!region) {
            this.errorNotify("Region is not selected");
        } else if (!numberOfGuest) {
            this.errorNotify("Number of guest cannot be empty");
        } else if(date.length <= 4) {
            this.errorNotify("Date can not be empty");
        } else {
            console.log("event insert by seller block");
            console.log({user, eventName, region, date, eventType, numberOfGuest})
            Meteor.call('events.insertBySeller', eventName, region, 
                eventType, date, numberOfGuest, user, function(error, result) {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log({result})
                        location.reload();
                    }
                });
        }
    }

    componentDidMount(){
        const self = this;
        const userId = Meteor.userId();

        Meteor.call('events.getUpcomingEventsOfSeller', userId, function (err, result) {
            if(err) {
                self.setState({events: []});
            } else {
                if(result) {
                    self.setState({events: result});
                } else {
                    self.setState({events: []});
                }
            }
        })
    }

    _selectDate(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;
        if(day !== currentDate) {
            this.setState({date: day});
        }
    }

    render() {

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>
                    <div className="clearfix"></div>
                    <div className="clearfix"></div>
                    <div className="body_container_wrapper chat-body_wrapper">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 ">
                            <div className={"vendorMyserviceTitleBlock"}>
                                <div className={"vendorMyServiceTtext vendorMyServiceTextDiv"}>
                                    MY SERVICE
                                </div>
                                <div className={"vendorMyServiceHr"}>
                                    <img className="svg-img-style" src="/img/2pt_line.svg"/>
                                </div>
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse"
                                        data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <VendorLeftSideBar/>
                        </div>
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 vendor_agenda_wrapper">
                            <div className={"row"}>
                                <div className={"col-md-12 display_flex agenda-padding"}>
                                    <div className={"sectionblock1 calender-block"}>
                                        <div className={" leftPading"}>
                                            <div className={"vendorAgendaHrTitleBlock"}>
                                                <div className={"vendorAgenndaText vendorAgenndaTextDiv"}>
                                                    Agenda
                                                </div>
                                                <div className={"vendorAgendaHr"}>
                                                    <img className="svg-img-style" src="/img/2pt_line.svg"/>
                                                </div>
                                            </div>
                                            <div className="clearfix"></div>
                                            <div className={"agenda-calender "}>
                                                <Calender parentCallback={this.parentCallback.bind(this)}
                                                          parentCallbackSetDate = {this.parentCallbackSetDate.bind(this)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className={"sectionblock2 vendor-right-list-padding right_padding vendor_agenda_book"}>
                                        <div className={"vendorAgendaDateHrTitleBlock"}>
                                            <div className={"vendorAgendaDateText vendorAgendaDateTextDiv"}>
                                                {"BOOK"}
                                            </div>
                                            <div className={"vendorAgendaDateHr"}>
                                                <img className={"svg-img-style"} src={"/img/2pt_line.svg"}/>
                                            </div>
                                        </div>
                                        <div className="clearfix"></div>
                                        <ul>
                                            <li className="list-group-item list-right-padding-style">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img width-height-25px"} src="/img/svg_img/event.svg" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <form>
                                                            <input id={"eventName"} className="dashboard-search placeholder-fix" 
                                                            type="text" placeholder="COUPLE IN LOVE" name="event_name" />
                                                        </form>
                                                        <p className={"vendor-stats-p"}>NAME</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="list-group-item list-right-padding-style">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img width-height-25px"} src="/img/svg_img/time_icon.svg" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <form>
                                                            <p>KL 11:32PM</p>
                                                        </form>
                                                        <p className={"vendor-stats-p"}>time</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="list-group-item list-right-padding-style">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img width-height-25px"} src="/img/new_img/gray_calendar.svg" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <DayPickerInput
                                                            onDayChange={day=> this._selectDate(day)}
                                                            value={this.state.date}
                                                            dayPickerProps={{ disabledDays: {before: new Date()} }}
                                                        />
                                                        <p className={"vendor-stats-p"}>DATE</p>
                                                    </div>
                                                </div>
                                            </li>
                                            {/* <li className="list-group-item list-right-padding-style ">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img"} src="/img/region_topBar.png" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <RegionSelect callbackFromParent={this._callbackFromRegionSelect}/>
                                                        <p className={"vendor-stats-p"}>REGION</p>
                                                    </div>
                                                </div>
                                            </li> */}
                                            <li className="list-group-item list-right-padding-style">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img width-height-25px"} src="/img/tabBarImg/guestNum.svg" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <input
                                                            id={"numOfGuest"}
                                                            className="dashboard-guest guest-placeholder-fix"
                                                            type="number"
                                                            placeholder="HOW MANY?"
                                                            name="num_of_guest"
                                                            value={this.state.guestNumber}
                                                            onChange={this.handleInputChange.bind(this)}
                                                        />
                                                        <p className={"vendor-stats-p"}>GUEST</p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="list-group-item list-right-padding-style">
                                                <div className={"vendorStatsList"}>
                                                    <div className={"vendorStatsFloatLeft"}>
                                                        <span>
                                                            <img className={"list-img width-height-25px"} src="/img/notes_tabBar.svg" alt="" />
                                                        </span>
                                                    </div>
                                                    <div className={"vendorStatsTextCenter"}>
                                                        <form>
                                                            <input id={"eventName"} className="dashboard-search placeholder-fix" 
                                                            type="text" placeholder="COUPLE THAT WANT THE TITLE EXTRA" name="event_name" />
                                                        </form>
                                                        <p className={"vendor-stats-p"}>Notes</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div className="new-event-button_wrapper relativeposition">
                                            <div className="relative-div news_service_button  home-login-button-div">
                                                <div className="text-style" onClick={this._handleEventCreate.bind(this)}>
                                                    <img className="service_back_logo-home-first-img" src={ "/img/newService/TICK.svg"}/>
                                                    <div className={ "back_top"}>SAVE</div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div className="clearfix"></div>
                                        <div className={"vendorUpcomingHrTitleBlock vendorUpcomingWrapper"}>
                                            <div className={"vendorUpcomingText vendorUpcomingTextDiv"}>
                                                Upcoming
                                            </div>
                                            <div className="clearfix"></div>
                                            <ShowEvent events={this.state.events}/>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default Agenda;