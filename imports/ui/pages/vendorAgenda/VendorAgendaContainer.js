import React from 'react';
import VendorAgenda from './Agenda';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class VendorAgendaContainer extends React.Component {

    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "vendor") {
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <VendorAgenda/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(VendorAgendaContainer);

