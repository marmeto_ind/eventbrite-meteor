import React from 'react';
import Header from '../../pages/homeHeader/homeHeader';
import Body from '../../components/body/BodyPart'
import Footer from '../../components/footer/Footer';
import './home.css';
import {Meteor} from 'meteor/meteor';
import MobileHomeFirst from '../../mobile/homeFirst/HomeFirst';
import {browserHistory} from 'react-router';


class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: '',
            sellers: '',
        }
    }

    componentWillMount(){
        if(Meteor.isCordova) {
            console.log("mobile");
            browserHistory.push('/mobile/home/first');
        }
        const self = this;
        Meteor.call('getAllCategories', function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({categories: result});
            }
        });

        Meteor.call('getAllSellers', function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({sellers: result});
            }
        })
    }

    render() {
        if(!this.state.categories) {
            return (
                <div></div>
            )
        } else if(!this.state.sellers) {
            return (
                <div></div>
            )
        } else {
            return (
                <div>
                    <Header/>
                    <Body categories={this.state.categories} sellers={this.state.sellers} />
                    <Footer/>
                </div>
            )
        }
    }
}

export default HomePage;
