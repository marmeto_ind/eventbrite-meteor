import React from 'react';
import Header from '../../pages/homeHeader/homeHeader';
import Body from '../../components/body/BodyPart'
import Footer from '../../components/footer/Footer';
import './home.css';
import './allpopup.scss';
import {Meteor} from 'meteor/meteor';
import MobileHomeFirst from '../../mobile/homeFirst/HomeFirst';
import {browserHistory} from 'react-router';


class allpopupdesign extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: '',
            sellers: '',
        }
    }

    componentWillMount(){
        if(Meteor.isCordova) {
            console.log("mobile");
            browserHistory.push('/mobile/home/first');
        }
        const self = this;
        Meteor.call('getAllCategories', function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({categories: result});
            }
        });

        Meteor.call('getAllSellers', function (err, result) {
            if(err) {
                console.error(err)
            } else {
                self.setState({sellers: result});
            }
        })
    }

    render() {
        if(!this.state.categories) {
            return (
                <div></div>
            )
        } else if(!this.state.sellers) {
            return (
                <div></div>
            )
        } else {
            return (
                <div>
                    <div className="popup_wrapper">
                        {/* alt chat popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>Alternative</h2>
                                        <div className="radio_button_wrapper">
                                            <div className="remember_div">
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="on" />
                                                    <span>Archive</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Block</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Delete</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Report</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* alt edit popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>Alternative</h2>
                                        <div className="radio_button_wrapper">
                                            <div className="remember_div padding_25px0">
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="on" />
                                                    <span>edit</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Delete</span>
                                                </label>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* alt Language popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>Language</h2>
                                        <div className="radio_button_wrapper">
                                            <div className="remember_div padding_top_inial">
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="on" />
                                                    <span>English</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Sevenska</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Nederland</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Hindi</span>
                                                </label>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* caution popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>Caution</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p>The picture you have chosen exceeds the limits.</p>
                                                <p>Please choose another picture</p>
                                            </div>
                                            <button>ok</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* change password popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>change password</h2>
                                        <div className="text_content_wrapper password_text">
                                            <div className="text_container">
                                                <div className="password password_new">
                                                    <input type="password" id="userPassword" className="type-password" maxlength="32" placeholder="***********" />
                                                    <p>Current Password</p>
                                                    <img src="/img/view.svg" alt="show password" className="show_password" />
                                                </div>
                                                <div className="password password_new">
                                                    <input type="password" id="userPassword" className="type-password" maxlength="32" placeholder="***********" />
                                                    <p>New Password</p>
                                                    <img src="/img/view.svg" alt="show password" className="show_password" />
                                                </div>
                                                <div className="password password_new">
                                                    <input type="password" id="userPassword" className="type-password" maxlength="32" placeholder="***********" />
                                                    <p>Repeat New Password</p>
                                                    <img src="/img/view.svg" alt="show password" className="show_password" />
                                                </div>
                                            </div>
                                            <button>change</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}


                        {/* Delete Account popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item delete_wrapper">
                                        <h2>Delete Account</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p>Are you sure you want to delete your account</p>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button">Yes</button>
                                                <button>No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* Delete Event popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item delete_wrapper">
                                        <h2>Delete Event</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p>Are you sure you want to delete your account</p>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button">Yes</button>
                                                <button>No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* Forgot Password popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item delete_wrapper">
                                        <h2>Forgot Password</h2>
                                        <div className="text_content_wrapper padding-top-bottom-0">
                                            <div className="text_container forgot_pasword">
                                                <p className="valid_username">Please enter a vaild username</p>
                                                <input type="text" placeholder="Enter Mail"  id="userPassword" className="typePasswordForgot" maxlength="32"/>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button">back</button>
                                                <button>reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* Region popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item">
                                        <h2>Region</h2>
                                        <div className="radio_button_wrapper">
                                            <div className="remember_div padding_top_inial">
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="on" />
                                                    <span>Amsterdam</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Berlin</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Mumbai</span>
                                                </label>
                                                <label className="radio">
                                                    <input type="radio" name="radio" value="off" />
                                                    <span>Stockholm</span>
                                                </label>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* signout Event popup design */}
                        {/* <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item sign_out">
                                        <h2>Sign out</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p>Are you sure you want to sign out your account?</p>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button">Yes</button>
                                                <button>No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* signout Event popup design */}
                        <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item terms_div">
                                        <h2>Terms</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p className="terms_text">ALorem Ipsum is simply dummy text of the printing and typesetting 
                                                    industry. Lorem Ipsum has been the industry's standard dummy 
                                                    text ever since the 1500s, when an unknown printer took a 
                                                    galley of type and scrambled it to make a type specimen book. 
                                                    It has survived not only five centuries,</p>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button">Cancel</button>
                                                <button>Accept</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            )
        }
    }
}

export default allpopupdesign;
