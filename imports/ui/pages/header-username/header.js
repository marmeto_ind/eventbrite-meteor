import React from 'react';
import  '../header-username/header.scss';
import {browserHistory} from 'react-router';

class Header extends React.Component {

    logOutpopUpFunction() {
        var logOutpopUp = document.getElementById("mylogOutpopUp");
        logOutpopUp.classList.toggle("show");
    };

    handleLogout(e) {
        e.preventDefault();
        Meteor.logout();
        browserHistory.push('/login');
        window.location.reload();        
    }
    
    componentDidMount() {
        $('body').click(function(){
            var logOutpopUp = document.getElementById("mylogOutpopUp");
            logOutpopUp.classList.remove("show");
        });
    }

    render() {
        let user = Meteor.user();
        let homeUrl = Meteor.userId()? "/home": "/"

        return(
            <header className="home-header">
                <div className={" header-container"}>
                    <div className={"header-div"}>
                        <div  id={"mobile_home_logo"} className={"pull-left col-md-8"}>
                            <div className={"row"}>
                                <div>
                                    <a href={homeUrl} className={"floatleft"} >
                                        <img className="header-logo" src="/img/white-logo.svg" alt="" />
                                    </a>
                                    <h1 className="header-text">WENDELY</h1>
                                </div>
                            </div>
                        </div>
                        <div className={"pull-right col-md-4 no-padding"}>
                            <nav className="home-header-nav home-header-nav-new">
                                <ul className={"floatright"}>
                                    <li className="username">
                                        <span className={"username-dot"}>.</span>
                                        <span className="username-link">
                                            {user && user.profile && user.profile.name}
                                        </span>
                                    </li>
                                    <li className="home-header-nav-item">
                                        <a href={""} title="User settings" className="homeheaderNavlink no-padding">
                                            <img  src="/img/user-black.png" alt="" />
                                            {/*/!*UserLogOutPopup*!/*/}
                                            {/*<div className="logOutpopUptext" id="mylogOutpopUp">*/}
                                                {/*<ul className="list-group  home-logInOutList">*/}
                                                    {/*<li className="list-group-item d-flex justify-content-between align-items-center"*/}
                                                    {/*onClick={this.handleLogout.bind(this)}*/}
                                                    {/*>*/}
                                                       {/*logOut*/}
                                                        {/*<span className="">*/}
                                                            {/*<img className={"logoutImage"} src={"/img/logout.png"} alt={"logIn"}/>*/}
                                                        {/*</span>*/}
                                                    {/*</li>*/}
                                                {/*</ul>*/}
                                            {/*</div>*/}
                                        </a>
                                    </li>

                                    {/*<li className="home-header-nav-item">*/}
                                        {/*<a href="#" className="homeheaderNavlink"></a>*/}
                                        {/*<img src="/img/home_page_settings.png" alt="" />*/}
                                    {/*</li>*/}
                                    <li className="home-header-nav-item">
                                        <a onClick={this.logOutpopUpFunction} title="Account setting" className="homeheaderNavlink logOutpopUp no-padding">
                                            <img className={"home-header-settingsimg"}  src="/img/settings-black.png" alt="" />

                                            {/*UserLogOutPopup*/}
                                            <div className="logOutpopUptext" id="mylogOutpopUp">
                                                <ul className="list-group  home-logInOutList">
                                                    <li className="list-group-item d-flex justify-content-between align-items-center"
                                                        onClick={this.handleLogout.bind(this)}
                                                    >
                                                        logOut
                                                        <span className="">
                                                            <img className={"logoutImage"} src={"/img/logout.png"} alt={"logIn"}/>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;