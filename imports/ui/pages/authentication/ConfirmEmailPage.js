import React from 'react';
import ConfirmEmail from "../../components/signupLogin/confirmEmail/ConfirmEmail";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

class ConfirmEmailPage extends React.Component {
    render() {
        return (
            <div className={"confirm-email-page"}>
                <Header/>
                <ConfirmEmail/>
                <Footer/>
            </div>
        )
    }
}

export default ConfirmEmailPage;