import React from 'react';
import VendorSignupComponent from '../../components/signupLogin/vendor/Signup';

class VendorSignup extends React.Component {
    render() {
        return (
            <div className={"seller-signup"}>
                <VendorSignupComponent/>
            </div>
        )
    }
}

export default VendorSignup;
