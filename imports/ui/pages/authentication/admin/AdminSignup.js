import React from 'react';
import AdminSignup from '../../../components/signupLogin/admin/Signup';

class AdminSignupPage extends React.Component {
    render() {
        return (
            <div>
                <AdminSignup/>
            </div>
        )
    }
}

export default AdminSignupPage;
