import React from 'react';
import RegionEditAdd from './regionEditAdd';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class RegionEditAddContainer extends React.Component {
    render() {
        const pathName = this.props.location.pathname;
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "admin") {
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <RegionEditAdd pathname={pathName} />
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(RegionEditAddContainer);

