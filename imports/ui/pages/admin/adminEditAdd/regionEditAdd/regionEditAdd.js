import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './regionEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Regions} from "../../../../../api/regions";
import { browserHistory } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import ShowEachRegion from './ShowEachRegion';
import ReactDOM from 'react-dom';

class RegionEditAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRegionId: '',
            newRegionImageUrl: '',
            newRegionImageUploadProgress: 0,
            updateRegionImageUrl: '',
            updateRegionImageUploadProgress: 0,
        }
    }

    parentCallback(regionId) {
        this.setState({selectedRegionId: regionId});
    }

    renderRegions(regions) {
        return regions.map((region, index) => (
            <ShowEachRegion key={index} region={region} parentCallback={this.parentCallback.bind(this)}/>
        ))
    }

    handleDelete (e) {
        e.preventDefault();
        const regionId = this.state.selectedRegionId;
        Meteor.call('regions.remove', regionId);
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    // Upload main image and store the url to state variable
    handleNewRegionImageUpload() {
        console.log("new category image upload");
        const mainPictureFile = $("#NewRegionPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newRegionImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newRegionImageUploadProgress: uploader.progress() * 100 });
            }
        });
    }

    handleNewRegionSave (e) {
        e.preventDefault();
        const regionName = ReactDOM.findDOMNode(this.refs.newRegionName).value.trim();
        // console.log(categoryName);
        if(!regionName) {
            this.errorNotify("Region name is empty");
        // } else if (!this.state.newRegionImageUrl) {
        //     this.errorNotify("Region image is not uploaded");
        } else {
            Meteor.call('regions.insert', regionName);
            // this.setState({newRegionImageUrl: ''});
            this.successNotify("Region saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleUpdateRegionImageUpload() {
        console.log("update region image upload");
        const mainPictureFile = $("#UpdateRegionPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({updateRegionImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ updateRegionImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    handleUpdateRegionSave (e) {
        e.preventDefault();
        const regionName = ReactDOM.findDOMNode(this.refs.UpdateRegionName).value.trim();
        if(!regionName) {
            this.errorNotify("Region name is empty");
        // } else if (!this.state.updateRegionImageUrl) {
        //     this.errorNotify("image is not uploaded");
        } else {
            Meteor.call('regions.update', this.state.selectedRegionId, regionName);
            this.successNotify("Region saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    render() {
        const regions = this.props.regions;
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}

                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                            {/*Region*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-4 region-hr"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                       Region
                                    </div>
                                    {/* <div className={"adminEditSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderRegions(regions)}
                                </ul>

                                {/*Edit PopUp box*/}

                                <div className="bg-overlay" id="edit-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                        <h2 className={"editAddText"}>Edit Region</h2>
                                        </div>
                                        <form action="">
                                            <div  className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>

                                                <input type="text" id="optin-name" ref={"UpdateRegionName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>

                                                {/*<div id={"picture-upload1"} className="picture-upload">*/}
                                                    {/*{*/}
                                                        {/*this.state.updateRegionImageUrl*/}
                                                        {/*?*/}
                                                        {/*<div className="add-more-pic new-region-picture">*/}
                                                            {/*<img*/}
                                                                {/*className={"region-image"}*/}
                                                                {/*src={this.state.updateRegionImageUrl}*/}
                                                                {/*style={{width: 150, height: 150}}*/}
                                                                {/*alt="Region Image" />*/}
                                                        {/*</div>*/}
                                                        {/*:*/}
                                                        {/*<div className="add-more-pic">*/}
                                                            {/*/!*<div className="add-file" htmlFor={"vendorsellerMainPicture"}>*!/*/}
                                                            {/*/!*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*!/*/}
                                                            {/*/!*</div>*!/*/}
                                                            {/*<div className={"admin-img-show"}>*/}
                                                                {/*<img className={"upload-image"} src={"/img/add-guest.jpg"} />*/}
                                                            {/*</div>*/}
                                                            {/*<label htmlFor="UpdateRegionPicture">*/}
                                                                {/*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*/}
                                                            {/*</label>*/}
                                                            {/*<hr />*/}

                                                            {/*<input*/}
                                                                {/*className="form-control-file"*/}
                                                                {/*id="UpdateRegionPicture" type="file"*/}
                                                                {/*onChange={this.handleUpdateRegionImageUpload.bind(this)}*/}
                                                            {/*/>*/}
                                                            {/*<h4>Upload Image</h4>*/}
                                                            {/*<p>(Max 100 Mb)</p>*/}
                                                            {/*<Progress completed={this.state.updateRegionImageUploadProgress} />*/}
                                                        {/*</div>*/}
                                                    {/*}*/}

                                                {/*</div>*/}

                                                {/*<label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>*/}
                                                    {/*/!*Attribute Image*!/*/}
                                                {/*</h1></label>*/}
                                                {/*<div id={"picture-upload1"} className="picture-upload">*/}
                                                    {/*{*/}
                                                        {/*this.state.updateRegionImageUrl*/}
                                                        {/*?*/}
                                                        {/*<div className="add-more-pic">*/}
                                                            {/*<img*/}
                                                                {/*className={"region-image"}*/}
                                                                {/*src={this.state.updateRegionImageUrl}*/}
                                                                {/*style={{width: 150, height: 150}}*/}
                                                                {/*alt="Region Image" />*/}
                                                        {/*</div>*/}
                                                        {/*:*/}
                                                        {/*<div className="add-more-pic">*/}
                                                            {/*/!*<div className="add-file" htmlFor={"vendorsellerMainPicture"}>*!/*/}
                                                            {/*/!*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*!/*/}
                                                            {/*/!*</div>*!/*/}
                                                            {/*<label htmlFor="AttributePicture">*/}
                                                                {/*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*/}
                                                            {/*</label>*/}
                                                            {/*<hr />*/}

                                                            {/*<input*/}
                                                                {/*className="form-control-file"*/}
                                                                {/*id="UpdateRegionPicture" type="file"*/}
                                                                {/*onChange={this.handleUpdateRegionImageUpload.bind(this)}*/}
                                                            {/*/>*/}
                                                            {/*<h4>Upload Image</h4>*/}
                                                            {/*<p>(Max 100 Mb)</p>*/}
                                                            {/*<Progress completed={this.state.updateRegionImageUploadProgress} />*/}
                                                        {/*</div>*/}
                                                    {/*}*/}

                                                {/*</div>*/}

                                            </div>
                                            <div id={"submitButton"} className={"save-changes-class"}>
                                                <input id={"regionSave"} type="submit" value="Save" onClick={this.handleUpdateRegionSave.bind(this)} />
                                            </div>

                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}



                                {/* Add PopUp box*/}

                                <div className="bg-overlay" id="add-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                            <h2 className={"editAddText"}>Add Region</h2>
                                        </div>

                                        {/*<h2 className={"editAddText"}>Add Region</h2>*/}

                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>
                                                <input type="text" id="new-region-name" ref={"newRegionName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>
                                             {/*<label className={"popUpfirstLabel"} htmlFor={"optin-name"}>*/}
                                                    {/*<h1>*/}
                                                        {/*/!*Attribute Image*!/*/}
                                                    {/*</h1>*/}
                                                {/*</label>*/}

                                                {/*<div id={"picture-upload1"}  className="picture-upload">*/}
                                                    {/*{*/}
                                                        {/*this.state.newRegionImageUrl*/}
                                                        {/*?*/}
                                                        {/*<div className="add-more-pic">*/}
                                                            {/*<img*/}
                                                                {/*className={"region-image"}*/}
                                                                {/*src={this.state.newRegionImageUrl}*/}
                                                                {/*style={{width: 150, height: 150}}*/}
                                                                {/*alt="Region Image" />*/}
                                                        {/*</div>*/}
                                                        {/*:*/}
                                                        {/*<div className="add-more-pic" onChange={this.handleNewRegionImageUpload.bind(this)}>*/}
                                                            {/*/!*<div className="add-file" htmlFor={"vendorsellerMainPicture"}>*!/*/}
                                                            {/*/!*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*!/*/}
                                                            {/*/!*</div>*!/*/}
                                                            {/*<label htmlFor="AttributePicture">*/}
                                                                {/*<img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />*/}
                                                            {/*</label>*/}
                                                            {/*<hr />*/}

                                                            {/*<input className="form-control-file" id="NewRegionPicture" type="file" />*/}
                                                            {/*<h4>Upload Image</h4>*/}
                                                            {/*<p>(Max 100 Mb)</p>*/}
                                                            {/*<Progress completed={this.state.newRegionImageUploadProgress} />*/}
                                                        {/*</div>*/}
                                                    {/*}*/}
                                                {/*</div>*/}

                                            </div>
                                            <div id={"submitButton"} className={"save-changes-class"}>
                                                <input style={{width:'55%'}} type="submit" value="Save"
                                                       onClick={this.handleNewRegionSave.bind(this)} />
                                            </div>



                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}

                                {/*Delete PopUP box */}
                                <div className="bg-overlay" id="delete-popup-option">
                                    <div className="subscribe-optin">
                                        <h2 className={"editAddText"}>Are you sure want to delete?</h2>

                                        <form id={"deleteForm"} action="">
                                            <div className={"confirm-width"}>
                                            <div id={"submitButton "} className={"yesBox"}>
                                                <input id={"yes-width"} type="submit" value="Yes" onClick={this.handleDelete.bind(this)} />
                                            </div>
                                            <div id={"submitButton"}  className={" CancelBox"}>
                                                <input type="submit" value="Cancel" onClick={this.handleDeleteCancel.bind(this)} />
                                            </div>
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}


                            </div>

                            

                        </div>
                        <div  className={"status_right_div admin_edit_block_pos"}>
                            <div className={"adminEditSectionHrTitleBlock"}>
                                <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                    &nbsp;
                                </div>
                            </div>
                            <div className="admin-edit-new-event-button">
                                <a href="#add-popup-option">
                                    <button className="new-event-button-left" onClick={this._handleNewEvent}>                                           
                                    <img src="/img/svg_img/new_button.svg" />
                                        <p>New Region</p>
                                    </button>
                                </a>
                            </div>
                        </div>
                            <ToastContainer/>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('regions.all');
    const regions = Regions.find().fetch();
    return {
        regions: regions,
    }
})(RegionEditAdd);