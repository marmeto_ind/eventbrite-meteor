import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class BannerSelect extends React.Component {
    state = {
        selectedOption: '',
    };

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        this.props.callbackFromParent(selectedOption);
    };

    _createDropDownOption = (bannerOptions) => {
        let dropDownOption = [];
        bannerOptions.forEach(function (data) {
            dropDownOption.push({value: data._id, label: data.name});
        });
        return dropDownOption;
    };

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        let bannerTypes = [{_id: 1, name: "Main Banner"}, {_id: 2, name: "Secondary Banner"}]
        const options = this._createDropDownOption(bannerTypes);

        return (
            <Select name="banner" value={value} onChange={this.handleChange}
                options={options} placeholder={"select banner"}
            />
        )
    }
}

export default BannerSelect;
