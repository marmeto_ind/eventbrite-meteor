import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './attributeEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
// import {SellerFacilities} from "../../../../../api/sellerFacilities";
import ShowEachFacility from './ShowEachFacility';
import { browserHistory } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
// import Progress from 'react-progressbar';
import ReactDOM from 'react-dom';
import axios from 'axios';
import RegionDropDown from './regionDropdown';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    renderSellerFacilities(){
        const seller = this.props.seller;
        const facilities = seller.profile.facilities;
        return facilities.map((facility, index) => (
            <img src={facility.image} key={index} />
        ))
    }

    render() {
        const seller = this.props.seller;
        const bkgndimg = seller.profile.mainImage;
        const backgroundImage = {
            backgroundImage: 'url(' + bkgndimg + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            width: "100%",
            cursor: "pointer",
            height: "190px"
        };

        return (
            <li>
                <div className="container_div border_white" style={backgroundImage}>
                    <div className="main_content">
                        <div className="content_wrapper">
                            <h2>
                                <img src="/img/star.png" />
                                <img src="/img/star.png" />
                                <img src="/img/star.png" />
                                <img src="/img/star.png" />
                                <img src="/img/star.png" />
                            </h2>
                            <p className="serviceClass">{seller.name}</p>
                            <p className="regionClass">{seller.profile.region}</p>
                            <p className="facility_wrapper">
                                {this.renderSellerFacilities()}
                            </p>
                        </div>
                    </div>
                    <div className="tag_wrapper">
                        <ul>
                            <li>
                                {seller.profile.hotTag? <img src="/img/tags_icon/TAGS_HOT.png" alt="tag" />: ''}
                                {seller.profile.goodDealTag? <img src="/img/tags_icon/TAGS_GOOD_DEAL.png" alt="tag" />: ''}
                                {seller.profile.newTag? <img src="/img/tags_icon/TAGS_NEW.png" alt="tag" />: ''} 
                            </li>
                        </ul>
                    </div>
                    <div className="right_div">
                        <div className="adminEditView">
                            <span><img className="list-img" src="/img/svg_img/greyEye.png" alt="" /></span>
                        </div>
                        <div className={"adminArrowView"}>
                                <span>
                                    <img className={"arrow_img"} src="/img/new_img/grey_arrow.svg" alt="" />
                                </span>
                        </div>
                        <div className="adminEditItem">
                            <a href="#edit-popup-option">
                                <span><img className="cross-img" src="/img/svg_img/pencil_svg.svg" alt="" /></span>
                            </a>
                        </div>
                        <div className="adminEditDelete">
                            <a href="#delete-popup-option">
                                <span><img className="cross-img" src="/img/svg_img/red_cross.svg" alt="" /></span>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

class ShowEachCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSeller: false
        }
    }

    toggleShowSeller() {
        this.setState({showSeller: !this.state.showSeller})
    }

    renderCategorySellers() {
        let sellers = this.props.category_seller.sellers;
        return sellers.map((seller, index) => <ShowEachSeller seller={seller} key={index} />)
    }

    render(){
        let category_seller = this.props.category_seller;
        let imageUrlRight = this.state.toggleState ?  '/img/svg_img/cross_svg.svg' : '/img/wendy_grey_tick.png' ;

        return (
            <div>
                <div className="content_wrapper">
                    <div className="show_rating_container show_rating_container_second" onClick={this.toggleShowSeller.bind(this)}>
                        <div className="show_rating_wrapper cursor_calss padding_10">
                            <div className="left_image">
                                <img src={category_seller.image} alt="" />
                            </div>
                            <div className="center_div">
                                <div className="best_seller_wrapper">
                                    <div className="content">
                                        <p className="heading_text line-height-37">{category_seller.name}({category_seller.sellers.length})</p>
                                    </div>
                                </div>
                            </div>
                            <div className="right_image">
                                <img src={imageUrlRight} className="right_img_icon" />
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.showSeller?
                    <ul className="list-group list-group-flush edit_list_all_block_padding">
                        {this.renderCategorySellers()}
                    </ul>
                    :""
                }
            </div>
        )
    }
}

class adminServiceEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFacilityId: '',
            newFacilityImageUrl: '',
            newFacilityImageUploadProgress: 0,
            updateFacilityImageUrl: '',
            updateFacilityImageUploadProgress: 0,
            toggleState: false,
            category_sellers: [],
            toggleState: true
        }
        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    componentDidMount() {
        axios.get('/api/get_sellers_regions')
            .then((res) => {
                const category_sellers = res.data;
                this.setState({category_sellers: category_sellers});
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleToggleClick(){
        this.setState({toggleState: !this.state.toggleState});
    }

    parentCallback(data) {
        this.setState({selectedFacilityId: data})
    }

    renderFacilities(facilities) {
        return facilities.map((facility, index) => (
            <ShowEachFacility facility={facility} key={index} parentCallback={this.parentCallback.bind(this)} />
        ))
    }

    handleDelete (e) {
        e.preventDefault();
        const facilityId = this.state.selectedFacilityId;
        Meteor.call('seller_facilities.remove', facilityId);
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    // Upload facility image and save the url to the database
    handleNewFacilityImageUpload() {
        const image = $("#NewFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newFacilityImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newFacilityImageUploadProgress: uploader.progress() * 100 });
            }
        });
    }

    handleNewFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.NewFacilityName).value.trim();
        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.newFacilityImageUrl) {
            this.errorNotify("Facility image is not uploaded");
        } else {
            Meteor.call('seller_facilities.insert', facilityName, this.state.newFacilityImageUrl);
            this.successNotify("Facility saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleUpdateFacilityImageUpload() {
        console.log("update facility image upload");
        const image = $("#UpdateFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({updateFacilityImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({
                    updateFacilityImageUploadProgress: uploader.progress() * 100
                });
            }
        })
    }

    handleUpdateFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.UpdateFacilityName).value.trim();
        const self = this;

        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.updateFacilityImageUrl) {
            // this.errorNotify("image is not uploaded");

            Meteor.call('seller_facilities.updateOnlyName',
                this.state.selectedFacilityId,
                facilityName, function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility Updated Successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                });
        } else {
            Meteor.call('seller_facilities.update',
                this.state.selectedFacilityId,
                facilityName,
                this.state.updateFacilityImageUrl,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        }
    }

    renderCategories(){
        return this.state.category_sellers.map((category_seller, index) => {
            return <ShowEachCategory key={index} category_seller={category_seller} />;
        })
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container admin_service_wrapper status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                      Services
                                    </div>
                                    {this.renderCategories()}
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        
                        <div className={"status_right_div admin_edit_block_pos"}>
                            <div className={"adminEditSectionHrTitleBlock"}>
                                <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                    &nbsp;
                                </div>
                            </div>
                            
                            <div className="clearfix" />
                            <div className="service_wrap">
                                <div className="content_wrapper">
                                    <div className="show_rating_container show_rating_container_second">
                                        <div className="show_rating_wrapper padding_10">
                                            <div className="left_image"><img src="/img/location.svg" alt="icon" /></div>
                                            <div className="center_div">
                                                <div className="best_seller_wrapper">
                                                    <div className="content">
                                                        <RegionDropDown callbackFromParent={this._callbackFromRegionSelect}/>
                                                        <p className="paragraph_text">Region</p>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix" />
                            <div className="admin-edit-new-event-button">
                                <a href="#add-popup-option"> <button className="new-event-button-left">
                                <img src="/img/svg_img/new_button.svg" />
                                    <p>New Service</p>
                                </button> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    return {}
})(adminServiceEdit);