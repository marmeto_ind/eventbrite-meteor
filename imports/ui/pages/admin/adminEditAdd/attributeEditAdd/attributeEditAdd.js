import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './attributeEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {SellerFacilities} from "../../../../../api/sellerFacilities";
import ShowEachFacility from './ShowEachFacility';
import { browserHistory } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import ReactDOM from 'react-dom';

class AttributeEditAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFacilityId: '',
            newFacilityImageUrl: '',
            newFacilityImageUploadProgress: 0,
            updateFacilityImageUrl: '',
            updateFacilityImageUploadProgress: 0,
        }
    }

    parentCallback(data) {
        this.setState({selectedFacilityId: data})
    }

    renderFacilities(facilities) {
        return facilities.map((facility, index) => (
            <ShowEachFacility facility={facility} key={index} parentCallback={this.parentCallback.bind(this)} />
        ))
    }

    handleDelete (e) {
        e.preventDefault();
        const facilityId = this.state.selectedFacilityId;
        Meteor.call('seller_facilities.remove', facilityId);
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    // Upload facility image and save the url to the database
    handleNewFacilityImageUpload() {
        const image = $("#NewFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newFacilityImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newFacilityImageUploadProgress: uploader.progress() * 100 });
            }
        });
    }

    handleNewFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.NewFacilityName).value.trim();
        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.newFacilityImageUrl) {
            this.errorNotify("Facility image is not uploaded");
        } else {
            Meteor.call('seller_facilities.insert', facilityName, this.state.newFacilityImageUrl);
            this.successNotify("Facility saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleUpdateFacilityImageUpload() {
        console.log("update facility image upload");
        const image = $("#UpdateFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({updateFacilityImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({
                    updateFacilityImageUploadProgress: uploader.progress() * 100
                });
            }
        })
    }

    handleUpdateFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.UpdateFacilityName).value.trim();
        const self = this;

        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.updateFacilityImageUrl) {

            Meteor.call('seller_facilities.updateOnlyName',
                this.state.selectedFacilityId,
                facilityName, function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility Updated Successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                });
        } else {
            Meteor.call('seller_facilities.update',
                this.state.selectedFacilityId,
                facilityName,
                this.state.updateFacilityImageUrl,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        }
    }

    render() {

        const facilities = this.props.facilities;
        // console.log(this.state.newFacilityImageUploadProgress);

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}

                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                              {/*Facility*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-4 region-hr"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                      Facility
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderFacilities(facilities)}
                                </ul>

                                {/* Edit PopUp box*/}

                                <div className="bg-overlay" id="edit-popup-option">
                                    <div className="subscribe-optin">
                                        <h2 className={"editAddText"}>Edit Facility</h2>
                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>
                                            <input type="text" id="optin-name" ref={"UpdateFacilityName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>


                                                <div id={"picture-upload1"} className="picture-upload">
                                                    <div className="add-more-pic" onChange={this.handleUpdateFacilityImageUpload.bind(this)}>
                                                        <div className={"admin-img-show"}>
                                                            <img
                                                                className={"upload-image"}
                                                                src={this.state.updateFacilityImageUrl?
                                                                    this.state.updateFacilityImageUrl:
                                                                    "/img/logo_new.png"}
                                                            />
                                                        </div>
                                                        <label htmlFor="UpdateFacilityPicture">
                                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                                        </label>
                                                        <hr />

                                                        <input className="form-control-file" id="UpdateFacilityPicture" type="file" />
                                                        <h4>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.updateFacilityImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id={"submitButton"}  className={"edit-save-change"}>
                                                <input type="submit"
                                                       value="Save"
                                                       onClick={this.handleUpdateFacilitySave.bind(this)}
                                                />
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/* Add PopUp box*/}

                                <div className="bg-overlay" id="add-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                        <h2 className={"editAddText"}>Add Facility</h2>
                                        </div>

                                        {/*<h2 className={"editAddText"}>Add Facility</h2>*/}

                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>
                                                <input type="text" id="optin-name" ref={"NewFacilityName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>


                                                <div id={"picture-upload1"} className="picture-upload">
                                                    <div className="add-more-pic" onChange={this.handleNewFacilityImageUpload.bind(this)}>

                                                        <div className={"admin-img-show"}>
                                                            <img
                                                                className={"upload-image"}
                                                                src={this.state.newFacilityImageUrl?
                                                                    this.state.newFacilityImageUrl:
                                                                    "/img/logo_new.png"}
                                                            />
                                                        </div>
                                                        <label htmlFor="NewFacilityPicture">
                                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                                        </label>
                                                        <hr />

                                                        <input className="form-control-file" id="NewFacilityPicture" type="file" />
                                                        <h4>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.newFacilityImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id={"submitButton"}  className={"edit-save-change"}>
                                                <input id={"attributeSave"} type="submit" value="Save" onClick={this.handleNewFacilitySave.bind(this)} />
                                            </div>

                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}

                                {/*Delete PopUP box */}
                                <div className="bg-overlay" id="delete-popup-option">
                                    <div className="subscribe-optin">
                                        <h2 className={"editAddText"}>Are you sure want to delete?</h2>

                                        <form id={"deleteForm"} action="">
                                            <div className={"confirm-width"}>
                                                <div id={"submitButton "} className={"yesBox"}>
                                                    <input id={"yes-width"} type="submit" value="Yes" onClick={this.handleDelete.bind(this)} />
                                                </div>
                                                <div id={"submitButton"}  className={" CancelBox"}>
                                                    <input type="submit" value="Cancel" onClick={this.handleDeleteCancel.bind(this)} />
                                                </div>
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}

                            </div>

                            

                        </div>
                        <div  className={"status_right_div admin_edit_block_pos"}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                      &nbsp;
                                    </div>
                                </div>
                            <div className="admin-edit-new-event-button">
                                <a href="#add-popup-option"> <button className="new-event-button-left" onClick={this._handleNewEvent}>
                                <img src="/img/svg_img/new_button.svg" />
                                    <p>New Facility</p>
                                </button> </a>
                            </div>
                        </div>
                        <ToastContainer/>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('seller_facilities.all');
    const facilities = SellerFacilities.find().fetch();
    return {
        facilities: facilities
    }
})(AttributeEditAdd);