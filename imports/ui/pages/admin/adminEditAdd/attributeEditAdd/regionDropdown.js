import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { withTracker } from 'meteor/react-meteor-data';
import {Regions} from "../../../../../api/regions";

class RegionSelect extends React.Component {
    state = {
        selectedOption: '',
    };

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        this.props.callbackFromParent(selectedOption);
    };

    _createDropDownOption = (inputRegions) => {
        let dropDownOption = [];
        inputRegions.forEach(function (data) {
            dropDownOption.push({value: data._id, label: data.name});
        });
        return dropDownOption;
    };

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        const options = this._createDropDownOption(this.props.regions);

        return (
            <Select name="region" value={value} onChange={this.handleChange}
                options={options} placeholder={"select region"}
            />
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('regions');
    return {
        regions: Regions.find({}).fetch(),
    }
})(RegionSelect);

