import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './attributeEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Banners} from "../../../../../api/banners";
import ShowEachFacility from './ShowEachFacility';
import { browserHistory } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import ReactDOM from 'react-dom';
import RegionDropDown from './regionDropdown';
import BannerDropDown from './bannerDropdown';
import SellerDropDown from './sellerDropdown';

class EachBanner extends React.Component {

    handleEdit(e) {
        const bannerId = this.props.banner._id;
        this.props.parentCallback(bannerId);
    }

    handleDelete(e) {
        const bannerId = this.props.banner._id;
        this.props.parentCallback(bannerId);
    }

    render() {
        const banner = this.props.banner;
        const bkgndimg = banner.banner_image;
        const backgroundImage = {
            backgroundImage: 'url("' + bkgndimg + '")',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            width: "100%",
            cursor: "pointer",
            height: "190px"
        };

        return (
            <li>
                <div className="container_div" style={backgroundImage}>
                    <div className="main_content">
                        <div className="content_wrapper">
                            <h2>{banner.banner_name}</h2>
                            <p className="serviceClass">{banner.seller}</p>
                            <p className="regionClass">{banner.region}</p>
                        </div>                        
                    </div>                    
                    <div className="tag_wrapper">
                        <ul>
                            <li>
                                <img src="/img/tag_image_mike.png" />
                            </li>
                        </ul>
                    </div>
                    <div className="right_div">
                        <div className="adminEditView">
                            <span><img className="list-img" src="/img/new_img/admin_eyeLine.svg" alt="" /></span>
                        </div>
                        <div className="adminEditItem">
                            <a href="#edit-popup-option" onClick={this.handleEdit.bind(this)}>
                                <span><img className="arrow_img" src="/img/new_img/white_arrow.svg" alt="" /></span>
                            </a>
                        </div>
                        <div className="adminEditDelete" onClick={this.handleDelete.bind(this)}>
                            <a href="#delete-popup-option">
                                <span><img className="cross-img" src="/img/svg_img/red_cross.svg" alt="" /></span>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

class BannerEditAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFacilityId: '',
            newFacilityImageUrl: '',
            newFacilityImageUploadProgress: 0,
            updateFacilityImageUrl: '',
            bannerUploadProgress: 0,
            region: '',
            banner: '',
            seller: '',
            bannerImageUrl: '',
            selectedBannerId: ''
        }
        this._callbackFromBannerSelect = this._callbackFromBannerSelect.bind(this);
        this._callbackFromRegionSelect = this._callbackFromRegionSelect.bind(this);
        this._callbackFromSellerSelect = this._callbackFromSellerSelect.bind(this);
        this.handleAddBannerImageUpload = this.handleAddBannerImageUpload.bind(this);
        this.bannerUpdateClick = this.bannerUpdateClick.bind(this);
    }

    handleDelete (e) {
        e.preventDefault();
        console.log("handle delete click")
        const bannerId = this.state.selectedBannerId;
        Meteor.call('banners.remove', bannerId);
        // const route = browserHistory.getCurrentLocation().pathname;
        // browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        // const route = browserHistory.getCurrentLocation().pathname;
        // browserHistory.push(route);
        window.location.hash = "#"
        // window.location.reload();
    }

    // Upload facility image and save the url to the database
    handleNewFacilityImageUpload() {
        const image = $("#NewFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newFacilityImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newFacilityImageUploadProgress: uploader.progress() * 100 });
            }
        });
    }

    handleNewFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.NewFacilityName).value.trim();
        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.newFacilityImageUrl) {
            this.errorNotify("Facility image is not uploaded");
        } else {
            Meteor.call('seller_facilities.insert', facilityName, this.state.newFacilityImageUrl);
            this.successNotify("Facility saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        console.log(string)
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        console.log(string)
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleAddBannerImageUpload() {
        console.log("new banner upload");
        const image = $("#UpdateFacilityPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;

        console.log({image});

        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                console.log(downloadUrl)
                that.setState({bannerImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({
                    bannerUploadProgress: uploader.progress() * 100
                });
            }
        })
    }

    handleUpdateFacilitySave (e) {
        e.preventDefault();
        const facilityName = ReactDOM.findDOMNode(this.refs.UpdateFacilityName).value.trim();
        const self = this;

        if(!facilityName) {
            this.errorNotify("Facility name is empty");
        } else if (!this.state.updateFacilityImageUrl) {
            // this.errorNotify("image is not uploaded");

            Meteor.call('seller_facilities.updateOnlyName',
                this.state.selectedFacilityId,
                facilityName, function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility Updated Successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                });
        } else {
            Meteor.call('seller_facilities.update',
                this.state.selectedFacilityId,
                facilityName,
                this.state.updateFacilityImageUrl,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Facility saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        }
    }

    _callbackFromRegionSelect(data) {
        this.setState({region: data.label});
    }

    _callbackFromSellerSelect(data) {
        this.setState({seller: data.label});
    }

    _callbackFromBannerSelect(data) {
        this.setState({banner: data.label});
    }

    bannerAddClick (e) {
        e.preventDefault();

        let banner_name = ReactDOM.findDOMNode(this.refs.bannerName).value.trim();
        let banner_type = this.state.banner;
        let banner_image = this.state.bannerImageUrl;
        let region = this.state.region;
        let seller = this.state.seller;        

        if (!banner_image) {
            this.errorNotify("banner image is not uploaded");
            return;
        }

        if (!banner_type) {
            this.errorNotify("banner type is not selected");
            return;
        }

        if (!banner_name) {
            this.errorNotify("banner name is not provided");
            return;
        }

        if (!region) {
            this.errorNotify("region is not selected");
            return;
        }

        if (!seller) {
            this.errorNotify("seller is not selected");
            return;
        }

        Meteor.call('banners.insert', {banner_name, banner_type, banner_image, region, seller}, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                console.log(result)
                window.location.reload;
            }
        });

    }

    bannerUpdateClick (e) {
        e.preventDefault();

        let banner_name = ReactDOM.findDOMNode(this.refs.bannerEditName).value.trim();
        let banner_type = this.state.banner;
        let banner_image = this.state.bannerImageUrl;
        let region = this.state.region;
        let seller = this.state.seller;
        let bannerId = this.state.selectedBannerId;

        if (!banner_image) {
            this.errorNotify("banner image is not uploaded");
            return;
        }

        if (!banner_type) {
            this.errorNotify("banner type is not selected");
            return;
        }

        if (!banner_name) {
            this.errorNotify("banner name is not provided");
            return;
        }

        if (!region) {
            this.errorNotify("region is not selected");
            return;
        }

        if (!seller) {
            this.errorNotify("seller is not selected");
            return;
        }

        Meteor.call('banners.update', {banner_name, banner_type, banner_image, region, seller, bannerId}, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                console.log(result)
                // window.location.reload;
            }
        });
    }

    parentCallback(bannerId) {
        this.setState({selectedBannerId: bannerId});
    }

    renderSecondaryBanners() {
        return this.props.banners.map((banner, index) => {
            if (banner.banner_type == "Secondary Banner") {
                return (<EachBanner key={index} banner={banner} parentCallback={this.parentCallback.bind(this)}/>)
            }
        })
    }

    handleBannerClick(banner) {
        console.log("banner click");
        console.log(banner);
    }

    renderMainBanners() {
        return this.props.banners.map((banner, index) => {
            if (banner.banner_type == "Main Banner") {
                return (<EachBanner key={index} banner={banner} parentCallback={this.parentCallback.bind(this)}/>)
            }
        })
    }

    render() {

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container admin_service_wrapper status_div_wrapper banner_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                      Main Banner
                                    </div>
                                    
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderMainBanners()}
                                </ul>
                            </div>
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                      Secondary Banner
                                    </div>                                    
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderSecondaryBanners()}
                                </ul>
                            </div>
                        </div>
                        
                        <div  className={"status_right_div admin_edit_block_pos"}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                      &nbsp;
                                    </div>
                                    {/* <div className={"adminEditSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                </div>
                                
                                <div className="clearfix" />
                                <div className="content_wrapper">

                                    <div className="show_rating_container show_rating_container_second">
                                        <div className="show_rating_wrapper padding_10">
                                            <div className="left_image"><img src="/img/location.svg" alt="icon" /></div>
                                            <div className="center_div">
                                                <div className="best_seller_wrapper">
                                                    <div className="content">
                                                        <RegionDropDown callbackFromParent={this._callbackFromRegionSelect}/>
                                                        <p className="paragraph_text">Region</p>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div className="show_rating_container show_rating_container_second">
                                        <div className="show_rating_wrapper padding_10">
                                            <div className="left_image"><img src="/img/location.svg" alt="icon" /></div>
                                            <div className="center_div">
                                                <div className="best_seller_wrapper">
                                                    <div className="content">
                                                        <BannerDropDown callbackFromParent={this._callbackFromRegionSelect}/>
                                                        <p className="paragraph_text">Banner</p>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>

                                </div>
                                <div className="clearfix" />
                                <div className="admin-edit-new-event-button">
                                    <a href="#add-popup-option"> 
                                        <button className="new-event-button-left" onClick={this._handleNewEvent}>
                                        <img src="/img/svg_img/new_button.svg" />
                                            <p>New Banner</p>
                                        </button> 
                                    </a>
                                </div>
                            </div>
                    </div>
                </div>

                {/* start Edit PopUp box*/}
                <div className="bg-overlay banner_pop_wrapper" id="edit-popup-option">
                    <div className="subscribe-optin">
                        <h2 className={"editAddText"}>Edit Banner</h2>
                        <form action="">
                            <div className={"edit-input-box"}>
                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                    {/*Attribute Name*/}
                                </h1></label>
                            <input type="text" id="optin-name" ref={"bannerEditName"} placeholder="Enter Name" />
                            </div>

                            <div className="dropdown_wrapper_popup"><SellerDropDown callbackFromParent={this._callbackFromSellerSelect}/></div>

                            <div className="dropdown_wrapper_popup"><RegionDropDown callbackFromParent={this._callbackFromRegionSelect}/></div>

                            <div className="dropdown_wrapper_popup"><BannerDropDown callbackFromParent={this._callbackFromBannerSelect}/></div>

                            <div className={"second-image-div"}>

                                <div id={"picture-upload1"} className="picture-upload">
                                    <div className="add-more-pic" onChange={this.handleAddBannerImageUpload}>
                                        <div className={"admin-img-show"}>
                                            <img
                                                className={"upload-image"}
                                                src={this.state.bannerImageUrl?
                                                    this.state.bannerImageUrl:
                                                    "/img/logo_new.png"}
                                            />
                                        </div>
                                        <label htmlFor="UpdateFacilityPicture">
                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                        </label>
                                        <hr />

                                        <input className="form-control-file" id="UpdateFacilityPicture" type="file" />
                                        <h4>Upload Image</h4>
                                        <p>(Max 100 Mb)</p>
                                        <Progress completed={this.state.bannerUploadProgress} />
                                    </div>
                                </div>
                            </div>
                            <div id={"submitButton"}  className={"edit-save-change"}>
                                <input type="submit"
                                        value="Save"
                                        onClick={this.bannerUpdateClick.bind(this)}
                                />
                            </div>
                        </form>
                        <a href="#" className="optin-close">&times;</a>
                    </div>
                </div>
                {/* end Edit PopUp box*/}


                {/* start add PopUp box*/}
                <div className="bg-overlay" id="add-popup-option">
                    <div className="subscribe-optin">
                        <h2 className={"editAddText"}>Add Banner</h2>
                        <form action="">
                            <div className={"edit-input-box"}>
                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                    {/*Attribute Name*/}
                                </h1></label>
                                <input type="text" id="optin-name" ref={"bannerName"} placeholder="Enter Name" />
                            </div>

                            <div><SellerDropDown callbackFromParent={this._callbackFromSellerSelect}/></div>

                            <div><RegionDropDown callbackFromParent={this._callbackFromRegionSelect}/></div>

                            <div><BannerDropDown callbackFromParent={this._callbackFromBannerSelect}/></div>

                            <div className={"second-image-div"}>
                                <div id={"picture-upload1"} className="picture-upload">
                                    <div className="add-more-pic" onChange={this.handleAddBannerImageUpload.bind(this)}>
                                        <div className={"admin-img-show"}>
                                            <img
                                                className={"upload-image"}
                                                src={this.state.bannerImageUrl?
                                                    this.state.bannerImageUrl:
                                                    "/img/logo_new.png"}
                                            />
                                        </div>
                                        <label htmlFor="UpdateFacilityPicture">
                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                        </label>
                                        <hr />

                                        <input className="form-control-file" id="UpdateFacilityPicture" type="file" />
                                        <h4>Upload Image</h4>
                                        <p>(Max 100 Mb)</p>
                                        <Progress completed={this.state.bannerUploadProgress} />
                                    </div>
                                </div>
                            </div>
                            <div id={"submitButton"}  className={"edit-save-change"}>
                                <input type="submit"
                                        value="Save"
                                        onClick={this.bannerAddClick.bind(this)}
                                />
                            </div>
                        </form>
                        <a href="#" className="optin-close">&times;</a>
                    </div>
                </div>
                {/* end add PopUp box*/}

                {/*Delete PopUP box */}
                <div className="bg-overlay" id="delete-popup-option">
                    <div className="subscribe-optin">
                        <h2 className={"editAddText"}>Are you sure want to delete?</h2>

                        <form id={"deleteForm"} action="">
                            <div className={"confirm-width"}>
                                <div id={"submitButton "} className={"yesBox"}>
                                    <input id={"yes-width"} type="submit" value="Yes" onClick={this.handleDelete.bind(this)} />
                                </div>
                                <div id={"submitButton"}  className={" CancelBox"}>
                                    <input type="submit" value="Cancel" onClick={this.handleDeleteCancel.bind(this)} />
                                </div>
                            </div>
                        </form>
                        <a href="#" className="optin-close">&times;</a>
                    </div>
                </div>

                {/*//*/}

                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('banners');
    const banners = Banners.find().fetch();
    return { banners }
})(BannerEditAdd);

