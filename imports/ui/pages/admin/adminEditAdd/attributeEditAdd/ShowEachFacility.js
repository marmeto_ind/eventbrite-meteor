import React from 'react';

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false
        }
    }

    editFacility() {
        const facilityId = this.props.facility._id;
        this.props.parentCallback(facilityId);
    }

    deleteFacility() {
        const facilityId = this.props.facility._id;
        this.props.parentCallback(facilityId);
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    render() {
        const facility = this.props.facility;

        return (
            <li className="list-group-item list-right-padding-style">
                {/* <div className={"adminEditList"}>
                    <div className={"adminEditFloatLeft"}>
                        <span>
                            <img className={"list-img"} src={facility.image} alt="" width={20} height={20} />
                        </span>
                    </div>
                    <div className={"adminEditTextCenter"}>
                        <p className={"editbox-textStyle"}>{facility.name}</p>
                    </div>
                    <div className={"adminEditView"}>
                    <span><img className={"list-img"} src="/img/svg_img/eye.svg" alt="" /></span>
                    </div>
                    <div className={"adminEditItem"}>
                        <a href="#edit-popup-option" onClick={this.editFacility.bind(this)}>
                            <span><img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" /></span>
                        </a>
                    </div>
                    <div className={"adminEditDelete"}>
                        <a href="#delete-popup-option" onClick={this.deleteFacility.bind(this)}>
                            <span><img className={"cross-img"} src="/img/svg_img/cross_svg.svg" alt="" /></span>
                        </a>
                    </div>
                </div> */}

                <div className={"adminEditListDesktop"}>
                    <div className="left_div">
                        <img src={facility.image} />
                        <div className={"adminEditTextCenterDesktop"}>
                            <p className={"editbox-textStyleDesktop"}>{this.state.showPassword? facility.name: "**********"}</p>
                        </div>
                    </div>
                    <div className="right_div">
                        <div className={"adminEditView"}>
                            <span>
                                <img className={"list-img"} 
                                src={!this.state.showPassword ? "/img/svg_img/greyEye.png" : "/img/svg_img/eye_edit.png"} 
                                onClick={this.toggleShowPassword.bind(this)}
                                alt="" />
                            </span>
                        </div>
                        <div className={"adminArrowView"}>
                                 <span>
                                    <img className={"arrow_img"} src="/img/new_img/grey_arrow.svg" alt="" />
                                </span>
                        </div>
                        <div className={"adminEditItem"}>
                                <a href="#edit-popup-option" onClick={this.editFacility.bind(this)}>
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" />
                                </span>
                            </a>
                        </div>
                        <div className={"adminEditDelete"}>
                                <a href="#delete-popup-option" onClick={this.deleteFacility.bind(this)}>    
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/red_cross.svg" alt="" />
                                </span>
                            </a>
                        </div>
                    </div>


                </div>


            </li>
        )
    }
}

export default ShowEachFacility;