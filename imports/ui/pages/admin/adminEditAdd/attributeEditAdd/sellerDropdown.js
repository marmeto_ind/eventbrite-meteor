import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../../api/sellers";

class SellerSelect extends React.Component {
    state = {
        selectedOption: '',
    };

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        this.props.callbackFromParent(selectedOption);
    };

    _createDropDownOption = (inputSellers) => {
        let dropDownOption = [];
        inputSellers.forEach(function (data) {
            dropDownOption.push({value: data._id, label: data.name});
        });
        return dropDownOption;
    };

    render() {
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;
        const options = this._createDropDownOption(this.props.sellers);

        return (
            <Select name="seller" value={value} onChange={this.handleChange}
                options={options} placeholder={"select seller"}
            />
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers');
    return {
        sellers: Sellers.find({}).fetch(),
    }
})(SellerSelect);

