import React from 'react';

class ShowEachEventType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false
        }
    }

    handleEdit() {
        this.props.parentCallback(this.props.eventType._id);
    }

    handleDelete() {
        this.props.parentCallback(this.props.eventType._id);
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    render() {
        const eventType = this.props.eventType;
        
        return (
            <li className="list-group-item list-right-padding-style">
                <div className={"adminEditList"}>
                    <div className={"adminEditFloatLeft"}>
                        <span>
                            <img className={"list-img"} src={eventType.image} alt="" width={20} height={20} />
                        </span>
                    </div>
                    <div className={"adminEditTextCenter"}>
                        <p className={"editbox-textStyle"}>
                            {this.state.showPassword? eventType.name: "**********"}
                        </p>
                    </div>
                    <div className={"admin__icon_container"}>
                        <div className={"adminEditView"}>
                        <span>
                            <img className={"list-img"} 
                            src={!this.state.showPassword ? "/img/svg_img/greyEye.png" : "/img/svg_img/eye_edit.png"} 
                            onClick={this.toggleShowPassword.bind(this)}
                            alt="" />
                        </span>
                        </div>
                        <div className={"adminArrowView"}>
                                <span>
                                    <img className={"arrow_img"} src="/img/new_img/grey_arrow.svg" alt="" />
                                </span>
                        </div>
                        <div className={"adminEditItem"}>
                            <a href="#edit-popup-option" onClick={this.handleEdit.bind(this)}>
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" />
                                </span>
                            </a>
                        </div>
                        <div className={"adminEditDelete"} onClick={this.handleDelete.bind(this)}>
                            <a href="#delete-popup-option">
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/red_cross.svg" alt="" />
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}


export default ShowEachEventType;