import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './eventEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {EventType} from "../../../../../api/eventType";
import ShowEachEventType from './ShowEachEventType';
import { browserHistory } from 'react-router';
import Progress from 'react-progressbar';
import { ToastContainer, toast } from 'react-toastify';
import ReactDOM from 'react-dom';

class EventEditAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventTypeId: '',
            newEventTypeImageUrl: '',
            newEventTypeImageUploadProgress: 0,
            updateEventTypeImageUrl: '',
            updateEventTypeImageUploadProgress: 0,
        }
    }

    parentCallback(data) {
        this.setState({eventTypeId: data});
    }

    renderEventTypes(eventTypes) {
        return eventTypes.map((eventType, index) => (
            <ShowEachEventType eventType={eventType} key={index} parentCallback={this.parentCallback.bind(this)}/>
        ))
    }

    handleDelete (e) {
        e.preventDefault();
        const eventTypeId = this.state.eventTypeId;
        console.log(eventTypeId);
        Meteor.call('event_types.remove', eventTypeId);
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    // Upload facility image and save the url to the database
    handleNewEventTypeImageUpload() {
        console.log("new event type image upload");
        const image = $("#NewEventTypePicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newEventTypeImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newEventTypeImageUploadProgress: uploader.progress() * 100 });
            }
        });
    }

    handleNewEventTypeSave (e) {
        e.preventDefault();
        const eventTypeName = ReactDOM.findDOMNode(this.refs.NewEventType).value.trim();
        // console.log(facilityName);
        if(!eventTypeName) {
            this.errorNotify("Event Type name is empty");
        } else if (!this.state.newEventTypeImageUrl) {
            this.errorNotify("Event Type image is not uploaded");
        } else {
            Meteor.call('event_types.insert', eventTypeName, this.state.newEventTypeImageUrl);
            // this.setState({newRegionImageUrl: ''});
            this.successNotify("Event Type saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleUpdateEventTypeImageUpload() {
        console.log("update event type image upload");
        const image = $("#UpdateEventTypePicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(image, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({updateEventTypeImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({
                    updateEventTypeImageUploadProgress: uploader.progress() * 100
                });
            }
        })
    }

    handleUpdateEventTypeSave (e) {
        e.preventDefault();
        const self = this;

        const eventTypeName = ReactDOM.findDOMNode(this.refs.UpdateEventTypeName).value.trim();
        if(!eventTypeName) {
            this.errorNotify("Event Type name is empty");
        } else if (!this.state.updateEventTypeImageUrl) {
            Meteor.call(
                'event_types.updateOnlyName',
                this.state.eventTypeId,
                eventTypeName,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        self.successNotify("Region saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        } else {
            Meteor.call('event_types.update', this.state.eventTypeId, eventTypeName, this.state.updateEventTypeImageUrl);

            this.successNotify("Region saved successfully");
            const route = browserHistory.getCurrentLocation().pathname;
            browserHistory.push(route);
            window.location.reload();
        }
    }

    render() {
        // console.log(this.state.eventTypeId);
        const eventTypes = this.props.eventTypes;

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}

                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                               {/*Event*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-4 region-hr"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                       Event Types
                                    </div>
                                    {/* <div className={"adminEditSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderEventTypes(eventTypes)}
                                </ul>
                                {/*PopUp box*/}

                                <div className="bg-overlay" id="edit-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                            <h2 className={"editAddText"}>Edit Event</h2>
                                        </div>

                                        {/*<h2 className={"editAddText"}>Edit Event</h2>*/}

                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>
                                                <input type="text" id="optin-name" ref={"UpdateEventTypeName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>

                                                <div id={"picture-upload1"} className="picture-upload">
                                                    <div className="add-more-pic"
                                                         onChange={this.handleUpdateEventTypeImageUpload.bind(this)}>

                                                        <div className={"admin-img-show"}>
                                                            <img
                                                                className={"upload-image"}
                                                                src={this.state.updateEventTypeImageUrl?
                                                                    this.state.updateEventTypeImageUrl:
                                                                    "/img/logo_new.png"}
                                                            />
                                                        </div>
                                                        <label htmlFor="UpdateEventTypePicture">
                                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                                        </label>
                                                        <hr />

                                                        <input className="form-control-file" id="UpdateEventTypePicture" type="file" />
                                                        <h4>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.updateEventTypeImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id={"submitButton"}  className={"edit-save-change"}>
                                                <input type="submit" value="Save"
                                                       onClick={this.handleUpdateEventTypeSave.bind(this)} />
                                            </div>

                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                <div className="bg-overlay" id="add-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                        <h2 className={"editAddText"}>Add Event</h2>
                                        </div>

                                        {/*<h2 className={"editAddText"}>Add Event</h2>*/}

                                        <form action="">
                                            <div  className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}>
                                                    <h1>
                                                        {/*Attribute Name*/}
                                                    </h1>
                                                </label>
                                                <input type="text" id="optin-name" ref={"NewEventType"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>


                                                <div id={"picture-upload1"} className="picture-upload">

                                                    <div className="add-more-pic" onChange={this.handleNewEventTypeImageUpload.bind(this)}>

                                                        <div className={"admin-img-show"}>
                                                            <img className={"upload-image"}
                                                                 src={this.state.newEventTypeImageUrl?
                                                                     this.state.newEventTypeImageUrl:
                                                                     "/img/logo_new.png"}
                                                            />
                                                        </div>
                                                        <label htmlFor="NewEventTypePicture">
                                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                                        </label>
                                                        <hr />

                                                        <input className="form-control-file" id="NewEventTypePicture" type="file" />
                                                        <h4>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.newEventTypeImageUploadProgress} />
                                                    </div>

                                                </div>
                                            </div>
                                            <div id={"submitButton"} className={"edit-save-change"}>
                                                <input id={"eventSave"} type="submit" value="Save"
                                                       onClick={this.handleNewEventTypeSave.bind(this)} />
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}

                                {/*Delete PopUP box */}
                                <div className="bg-overlay" id="delete-popup-option">
                                    <div className="subscribe-optin">
                                        <h2 className={"editAddText"}>Are you sure want to delete?</h2>

                                        <form id={"deleteForm"} action="">
                                            <div className={"confirm-width"}>
                                                <div id={"submitButton "} className={"yesBox"}>
                                                    <input id={"yes-width"} type="submit" value="Yes"
                                                           onClick={this.handleDelete.bind(this)} />
                                                </div>
                                                <div id={"submitButton"}  className={" CancelBox"}>
                                                    <input type="submit" value="Cancel"
                                                           onClick={this.handleDeleteCancel.bind(this)} />
                                                </div>
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}


                            </div>

                        </div>
                       
                        <div  className={"status_right_div admin_edit_block_pos"}>
                            <div className={"adminEditSectionHrTitleBlock"}>
                                <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                    &nbsp;
                                </div>
                            </div>
                            <div className="admin-edit-new-event-button">
                                <a href="#add-popup-option">
                                    <button className="new-event-button-left" onClick={this._handleNewEvent}>                                           
                                    <img src="/img/svg_img/new_button.svg" />
                                        <p>New Event Type</p>
                                    </button>
                                </a>
                            </div>
                        </div>
                        <ToastContainer/>
                       
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('event_types.all');
    const eventTypes = EventType.find().fetch();
    return {
        eventTypes: eventTypes,
    }
})(EventEditAdd);