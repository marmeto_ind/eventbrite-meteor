import React from 'react';
import Header from "../../header-username/header";
import AdminLeftEditBar from "../adminLeftEditBar";
import AdminTopBar from "../adminTopbar/adminTopbar";
import Footer from "../../../components/footer/Footer";
import './adminEditAdd.scss';

class AdminEditAdd extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    <div className={"row vendor-search-division"}>
                        <div className={"col-md-12"}>
                            <div className={"col-md-4"}>

                                <div className={"col-md-1 pagename-text edit-text-right-align"}>
                                    Edit
                                </div>
                                <div className={"col-md-3"}>
                                    <hr className={"admin-edit-hr"} />
                                </div>
                            </div>

                            <div className={"col-md-1 pagename-text"}>
                              region
                            </div>
                            <div className={"col-md-4 region-hr"}>
                                <hr />
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>
                    <div className="body_container chat-body">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            <div className={"col-md-7 no-padding"}>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item list-right-padding-style">
                                        <div className={"adminEditList"}>
                                            <div className={"adminEditFloatLeft"}>
                                                <span><img className={"list-img"} src="/img/svg_img/location_svg.svg" alt="" /></span>
                                            </div>
                                            <div className={"adminEditTextCenter"}>
                                                <p className={"editbox-textStyle"}>category</p>
                                            </div>
                                            <div className={"adminEditView"}>
                                                <span><img className={"list-img"} src="/img/svg_img/greyEye.png" alt="" /></span>
                                            </div>
                                            <div className={"adminEditItem"}>
                                                <span><img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" /></span>
                                            </div>
                                            <div className={"adminEditDelete"}>
                                                <span><img className={"cross-img"} src="/img/svg_img/cross_svg.svg" alt="" /></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item list-right-padding-style">
                                        <div className={"adminEditList"}>
                                            <div className={"adminEditFloatLeft"}>
                                                <span><img className={"list-img"} src="/img/svg_img/location_svg.svg" alt="" /></span>
                                            </div>
                                            <div className={"adminEditTextCenter"}>
                                                <p className={"editbox-textStyle"}>region</p>
                                            </div>
                                            <div className={"adminEditView"}>
                                                <span><img className={"list-img"} src="/img/svg_img/greyEye.png" alt="" /></span>
                                            </div>
                                            <div className={"adminEditItem"}>
                                                <span><img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" /></span>
                                            </div>
                                            <div className={"adminEditDelete"}>
                                                <span><img className={"cross-img"} src="/img/svg_img/cross_svg.svg" alt="" /></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>


                            </div>

                            <div  className={"col-md-5"}>
                                <div className="admin-edit-new-event-button">
                                    <button className="new-event-button-left" onClick={this._handleNewEvent}>
                                        New Event
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default AdminEditAdd;