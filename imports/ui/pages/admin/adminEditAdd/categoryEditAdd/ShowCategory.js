import React from 'react';


class ShowCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false
        }
    }

    handleEdit() {
        const category = this.props.category;
        const parentCallback = this.props.parentCallback(category._id);
    }

    handleDelete() {
        const category = this.props.category;
        const parentCallback = this.props.parentCallback(category._id);
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    render() {
        const category = this.props.category;
        
        return (
            <li className="list-group-item list-right-padding-style">
                <div className={"adminEditListDesktop"}>
                    <div className="left_div">
                        <img src={category.image} />
                        <div className={"adminEditTextCenterDesktop"}>
                            <p className={"editbox-textStyleDesktop"}>{this.state.showPassword? category.name: "**********"}</p>
                        </div>
                    </div>
                    <div className="right_div">
                        <div className={"adminEditView"}>
                            <span>
                                <img className={"list-img"} 
                                src={!this.state.showPassword ? "/img/svg_img/greyEye.png" : "/img/svg_img/eye_edit.png"} 
                                onClick={this.toggleShowPassword.bind(this)}
                                alt="" />
                            </span>
                        </div>
                        <div className={"adminArrowView"}>
                                <span>
                                    <img className={"arrow_img"} src="/img/new_img/grey_arrow.svg" alt="" />
                                </span>
                        </div>
                        <div className={"adminEditItem"}>
                            <a href="#edit-popup-option" onClick={this.handleEdit.bind(this)}>
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/pencil_svg.svg" alt="" />
                                </span>
                            </a>
                        </div>
                        <div className={"adminEditDelete"}>
                            <a href="#delete-popup-option" onClick={this.handleDelete.bind(this)}>
                                <span>
                                    <img className={"cross-img"} src="/img/svg_img/red_cross.svg" alt="" />
                                </span>
                            </a>
                        </div>
                    </div>


                </div>
            </li>
        )
    }
}

export default ShowCategory;