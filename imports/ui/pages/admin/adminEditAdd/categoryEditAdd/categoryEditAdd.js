import React from 'react';
import Header from "../../../header-username/header";
import AdminLeftEditBar from "../../adminLeftEditBar";
import AdminTopBar from "../../adminTopbar/adminTopbar";
import Footer from "../../../../components/footer/Footer";
import './categoryEditAdd.scss';
import '../adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../../../api/categories";
import {Meteor} from 'meteor/meteor';
import ShowCategory from './ShowCategory';
import { browserHistory } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import ReactDOM from 'react-dom';

class CategoryEditAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCategoryId: '',
            newCategoryImageUrl: '',
            updateCategoryImageUrl: '',
            newCategoryImageUploadProgress: 0,
            updateCategoryImageUploadProgress: 0,
        }
    }

    parentCallback(categoryId) {
        this.setState({selectedCategoryId: categoryId})
    }

    renderCategories(categories) {
        return categories.map((category, index) => (
            <ShowCategory parentCallback={this.parentCallback.bind(this)} category={category} key={index} />
        ))
    }

    handleDelete (e) {
        e.preventDefault();
        const categoryId = this.state.selectedCategoryId;
        Meteor.call('categories.remove', categoryId);
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleDeleteCancel (e) {
        e.preventDefault();
        const route = browserHistory.getCurrentLocation().pathname;
        browserHistory.push(route);
        window.location.reload();
    }

    handleNewCategorySave (e) {
        e.preventDefault();
        const categoryName = ReactDOM.findDOMNode(this.refs.newCategoryName).value.trim();
        // console.log(categoryName);
        if(!categoryName) {
            this.errorNotify("category name is empty");
        } else if (!this.state.newCategoryImageUrl) {
            this.errorNotify("image is not uploaded");
        } else {
            Meteor.call('categories.insert', categoryName, this.state.newCategoryImageUrl);
            // this.setState({newCategoryImageUrl: ''});
            this.successNotify("Category saved successfully");
            // const route = this.props.route.path;
            const route = browserHistory.getCurrentLocation().pathname;
            // console.log(route);
            browserHistory.push(route);
            window.location.reload();
        }
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    // Upload main image and store the url to state variable
    handleNewCategoryImageUpload() {
        // console.log("new category image upload");
        const mainPictureFile = $("#NewCategoryPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({newCategoryImageUrl: downloadUrl});
                that.setState({newCategoryImageUploadProgress: 0});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ newCategoryImageUploadProgress: uploader.progress() * 100 });
            }
        })

    }

    // Upload main image and store the url to state variable
    handleUpdateCategoryImageUpload() {
        // console.log("new category image upload");
        const mainPictureFile = $("#UpdateCategoryPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.errorNotify("error while uploading");
            } else {
                that.setState({updateCategoryImageUrl: downloadUrl});
                that.successNotify("Image successfully uploaded");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ updateCategoryImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    handleUpdateCategorySave (e) {
        e.preventDefault();
        const self = this;
        const categoryName = ReactDOM.findDOMNode(this.refs.updateCategoryName).value.trim();
        if(!categoryName) {
            this.errorNotify("category name is empty");
        } else if (!this.state.updateCategoryImageUrl) {
            // this.errorNotify("image is not uploaded");
            Meteor.call(
                'categories.updateOnlyName',
                this.state.selectedCategoryId,
                categoryName,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        // Reload the page after update
                        self.successNotify("Category saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        } else {
            Meteor.call(
                'categories.update',
                this.state.selectedCategoryId,
                categoryName,
                this.state.updateCategoryImageUrl,
                function (err, result) {
                    if(err) {
                        console.log(err);
                    } else {
                        // Reload the page after update
                        self.successNotify("Category saved successfully");
                        const route = browserHistory.getCurrentLocation().pathname;
                        browserHistory.push(route);
                        window.location.reload();
                    }
                }
            );
        }
    }

    render() {
        const categories = this.props.categories;

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}

                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                               {/*Category*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-4 region-hr"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div admin_edit_block_pos">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                       Categorys
                                    </div>
                                    {/* <div className={"adminEditSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                </div>
                                <div className="clearfix"></div>
                                <ul className="list-group list-group-flush edit_list_all_block_padding">
                                    {this.renderCategories(categories)}
                                </ul>

                                <div className="bg-overlay" id="edit-popup-option">
                                    <div className="subscribe-optin">

                                        <div className={"popup-edit-style"}>
                                            <h2 className={"editAddText"}>Edit Category</h2>
                                        </div>

                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"optin-name"}><h1>
                                                    {/*Attribute Name*/}
                                                </h1></label>
                                                <input type="text" id="new-category-name" ref={"updateCategoryName"} placeholder="Enter Name" />
                                            </div>
                                            <div className={"second-image-div"}>

                                                <div id={"picture-upload1"} className="picture-upload">
                                                    <div className="add-more-pic" onChange={this.handleUpdateCategoryImageUpload.bind(this)}>
                                                        <div className={"admin-img-show"}>
                                                                <img
                                                                    className={"upload-image"}
                                                                    src={this.state.updateCategoryImageUrl?
                                                                        this.state.updateCategoryImageUrl:
                                                                        "/img/logo_new.png"}
                                                                />
                                                            </div>
                                                        <label htmlFor="UpdateCategoryPicture">
                                                            <img  id="hi" className={"camera-img"} src="/img/photo-camera.png" alt="" />
                                                        </label>

                                                        <hr />

                                                        <input className="form-control-file" id="UpdateCategoryPicture" type="file" />
                                                        <h4 className={"upload-image-text"}>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.updateCategoryImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id={"submitButton"}  className={"edit-save-change"}>
                                                <input type="submit" value="Save" onClick={this.handleUpdateCategorySave.bind(this)} />
                                            </div>

                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/* Add PopUp box*/}

                                <div className="bg-overlay" id="add-popup-option">
                                    <div className="subscribe-optin">
                                        <div className={"popup-edit-style"}>
                                        <h2 className={"editAddText"}>Add Category</h2>
                                        </div>
                                        <form action="">
                                            <div className={"edit-input-box"}>
                                                <label className={"popUpfirstLabel"} htmlFor={"new-category-name"}>
                                                    <h1>
                                                        {/*Attribute Name*/}
                                                    </h1>
                                                </label>
                                                <input type="text" id="new-category-name" ref={"newCategoryName"} placeholder="Enter Category" />
                                            </div>
                                            <div className={"second-image-div"}>

                                                <div id={"picture-upload1"} className="picture-upload">
                                                    <div className="add-more-pic"
                                                         onChange={this.handleNewCategoryImageUpload.bind(this)}>
                                                        <div className={"admin-img-show"}>
                                                            <img className={"upload-image"}
                                                                 src={this.state.newCategoryImageUrl
                                                                     ?
                                                                     this.state.newCategoryImageUrl
                                                                     :
                                                                     "/img/logo_new.png"}
                                                            />
                                                        </div>

                                                        <label htmlFor="NewCategoryPicture">
                                                            <img
                                                                className={"camera-img"}
                                                                src="/img/photo-camera.png"
                                                                alt="" />
                                                        </label>
                                                        <hr />

                                                        <input
                                                            className="form-control-file"
                                                            id="NewCategoryPicture"
                                                            type="file" />

                                                        <h4>Upload Image</h4>
                                                        <p>(Max 100 Mb)</p>
                                                        <Progress completed={this.state.newCategoryImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id={"submitButton"} className={"edit-save-change"}>
                                                <input id={"categorySave"} type="submit" value="Save" onClick={this.handleNewCategorySave.bind(this)} />
                                            </div>



                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}

                                {/*Delete PopUP box */}
                                <div className="bg-overlay" id="delete-popup-option">
                                    <div className="subscribe-optin">
                                        <h2 className={"editAddText"}>Are you sure want to delete?</h2>

                                        <form id={"deleteForm"} action="">
                                            <div className={"confirm-width"}>
                                                <div id={"submitButton "} className={"yesBox"}>
                                                    <input id={"yes-width"} type="submit" value="Yes" onClick={this.handleDelete.bind(this)} />
                                                </div>
                                                <div id={"submitButton"}  className={" CancelBox"}>
                                                    <input type="submit" value="Cancel" onClick={this.handleDeleteCancel.bind(this)} />
                                                </div>
                                            </div>
                                        </form>
                                        <a href="#" className="optin-close">&times;</a>
                                    </div>
                                </div>

                                {/*//*/}


                            </div>
                            
                        </div>
                        <div  className={"status_right_div admin_edit_block_pos"}>
                            <div className={"adminEditSectionHrTitleBlock"}>
                                <div className={"adminEditSectionText adminEditSectionTextDiv no_before"}>
                                    &nbsp;
                                </div>
                            </div>
                            <div className="admin-edit-new-event-button">
                                <a href="#add-popup-option">
                                    <button className="new-event-button-left" onClick={this._handleNewEvent}>
                                    <img src="/img/svg_img/new_button.svg" />
                                        <p>New Category</p>
                                    </button>
                                </a>
                            </div>
                        </div>
                        <ToastContainer/>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('categories.all');
    const categories = Categories.find().fetch();
    return {
        categories: categories,
    }
})(CategoryEditAdd);