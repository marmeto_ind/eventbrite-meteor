import React from 'react';
import CategoryEditAdd from './categoryEditAdd';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class CategoryEditAddContainer extends React.Component {
    render() {
        const pathName = this.props.location.pathname;
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "admin") {
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <CategoryEditAdd pathname={pathName}/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(CategoryEditAddContainer);

