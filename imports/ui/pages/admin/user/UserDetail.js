import React from 'react';
import Header from '../../../components/header/Header';
import Footer from '../../../components/footer/Footer';
import UserDetail from '../../../components/user/admin/UserDetail';

class UserDetailPage extends React.Component {
    render() {
        // console.log(this.props.params.id)

        const userId = this.props.params.id;

        return (
            <div>
                <Header/>
                <UserDetail userId={userId}/>
                <Footer/>
            </div>
        )
    }
}

export default UserDetailPage;