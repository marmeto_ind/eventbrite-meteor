import React from 'react';
import UsersListPage from '../../../components/user/admin/UsersList';
import Header from '../../../components/header/Header';
import Footer from '../../../components/footer/Footer';

class UserListPage extends React.Component {
    render() {
        return (
            <div className={"seller-list-page"}>
                <Header/>
                <UsersListPage/>
                <Footer/>
            </div>
        )
    }
}

export default UserListPage;