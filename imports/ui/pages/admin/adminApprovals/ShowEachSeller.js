import React from 'react';
import {browserHistory} from 'react-router';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
    }

    handleOnSellerClick=(e)=>{
        e.preventDefault();
        const seller = this.props.seller;
        const sellerId = seller._id;
        browserHistory.push('/admin/approval/details/' + sellerId);
    };

    render() {
        const seller = this.props.seller;

        return (
            <div className="third-div">
                <ul className="list-group vendorstats-listBackground reviews-background approval_wrapper_section" onClick={this.handleOnSellerClick.bind(this)}>
                    <div className="left_div">
                        <img src="/img/cake.png" alt="" /><span>{seller.createdAt.toString().substring(0, 10)}</span>
                    </div>
                    <div className="center_div">
                        <h2>{seller.name}</h2>
                        <p className="address">{seller.profile.address}</p>
                        <p className="image_details">{seller.profile.moreImages.length + 1} PICTURES
                        </p>
                    </div>
                    <div className="right_div"><img src="/img/newService/TICK.svg" /></div>
                </ul>
            </div>
        )
    }
}

export default ShowEachSeller;