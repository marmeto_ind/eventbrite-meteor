import React from 'react';
import Header from "../../header-username/header";
import AdminLeftEditBar from "../adminLeftEditBar";
import AdminTopBar from "../adminTopbar/adminTopbar";
import Footer from "../../../components/footer/Footer";
import './adminApprovals.scss';
import '../adminEditAdd/adminEditAdd.scss';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import ShowEachSeller from './ShowEachSeller';

class AdminApprovals extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    renderSellersForApproval() {
        const sellers = this.props.sellers;
        return sellers.map((seller, index) => (
            <ShowEachSeller key={index} seller={seller} />
        ))
    }

    render() {
        return(
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}

                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                              {/*approvals*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-7"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 adminEditTitleBlockPadding">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 adminApprovalHrTitleBlockPadding">
                            <div className={"adminApprovalHrTitleBlock"}>
                                <div className={"adminApprovalText adminApprovalTextDiv"}>
                                    approvals
                                </div>
                                {/* <div className={"adminApprovalHr"}>
                                    <img className={"vendor_home_line_svg"} src={"/img/line.svg"} />
                                </div> */}
                            </div>
                            <div className="clearfix"></div>
                            <div className={"col-md-12 no-padding admin_approval_list_block_position  admin-approvals"}>
                                {this.renderSellersForApproval()}
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({"isApprove": false}).fetch();
    return {
        sellers: sellers,
    }
})(AdminApprovals);