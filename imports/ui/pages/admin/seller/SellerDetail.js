
import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerDetail from '../../../components/seller/admin/SellerDetail';

class SellerDetailPage extends React.Component {
    render() {
        const sellerId = this.props.params.id;
        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><SellerDetail sellerId={sellerId}/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellerDetailPage;