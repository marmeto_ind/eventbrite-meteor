// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import SellerUpdate from '../../../components/seller/admin/SellerUpdate';
//
// class SellerUpdatePage extends React.Component {
//     render() {
//
//         const sellerId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <SellerUpdate sellerId={sellerId} />
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default SellerUpdatePage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerUpdate from '../../../components/seller/admin/SellerUpdate';

class SellerUpdatePage extends React.Component {
    render() {
        const sellerId = this.props.params.id;
        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><SellerUpdate sellerId={sellerId} /></div>
            </div>
        </div>
        </div>
        )
    }
}

export default SellerUpdatePage;