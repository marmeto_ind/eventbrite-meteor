import React from 'react';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';
import {Messages} from "../../../../api/messages";
import {MessageView} from "../../../../api/messageView";
import {emojify} from 'react-emojione';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerName: '',
            numOfMessage: '',
            userName: ''
        }
    }

    componentDidMount() {
        $(document).ready(function () {
            $(".adminlistMenu li").click(function () {
                $(".adminlistMenu li").removeClass("selected-list-active");
                $(this).addClass("selected-list-active");
            });
        });

        const self = this;
        const {user,seller} = this.props;
        
        if (seller) {
            const sellerId = seller._id;
            const sellerName = seller.name;
            this.setState({sellerName: sellerName});
            const userId = Meteor.userId();

            Meteor.call('messages.getCountBySellerId', userId, sellerId, function (err, result) {
                if (err) {
                    console.error(err);
                } else {
                    if (result > 0) {
                        self.setState({numOfMessage: result})
                    }
                }
            })
        } else if (user) {
            this.setState({userName: user.userName});
        }        
    }

    handleSellerClick(e) {
        e.preventDefault();
        
        const {seller, user} = this.props;
        if (seller) {
            const sellerId = seller._id;
            this.props.parentCallback({sellerId});

            const user = Meteor.user();
            const userId = Meteor.userId();
            const sellerName = this.props.seller.name;
            Meteor.call('chat_with.getRecordBySellerIdUserId', sellerId, userId, function (err, result) {
                if (err) {
                    console.error(err);
                } else {
                    if (!result) {
                        const userName = user.profile.name;
                        Meteor.call('chat_with.insert', userId, sellerId, sellerName, userName, function (err, result) {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log(result);
                            }
                        })
                    }
                }
            })
        } else if (user) {
            const userId = user.user;
            this.props.parentCallback({userId});
        }        
    }

    showMessage(message) {
        if (this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    render() {

        return (
            <li className="list-group-item list-right-padding-style btn li-background"
                onClick={this.handleSellerClick.bind(this)}
            >
                <div className={"admin-main-width"}>
                    <div className={"admin-list-left-icon"}>
                        <span><img className={"list-img"} src="/img/user-black.png" alt=""/></span>
                    </div>

                    <div className={"admin-name capital-text"}>
                        <p>{this.state.sellerName || this.state.userName}</p>
                        <p className={"venue-p-margin"}>{this.showMessage(this.props.latestMessage || "")}</p>
                    </div>
                    {
                        this.props.newMsgCount ?
                            <div className="right_div_image">
                                <span>{this.props.newMsgCount}</span>
                            </div>
                            :
                            ''
                    }
                    {/* <div className="right_div_image">
                        <span>{this.props.newMsgCount}</span>
                    </div>
                    <div className={"admin-list-right-icon"}>
                        <span className={"admin-seller-no-msg"}>{this.props.newMsgCount}</span>
                    </div> */}
                </div>
            </li>
        )
    }
}

export default withTracker((data) => {
    const {seller, user} = data;
    if (seller) {
        const sellerId = seller._id;
        const userId = Meteor.userId();
        Meteor.subscribe('messages.all');

        const latestMessageCollection = Messages.findOne({
            "user": userId,
            "seller": sellerId,
            "sender": "seller"
        }, {sort: {createdAt: -1}});

        let latestMessage = "";
        if (latestMessageCollection) {
            latestMessage = latestMessageCollection.message;
        }

        Meteor.subscribe('message_view.all');

        let newMsgCount = 0;

        let msgView = MessageView.findOne({"user": userId, "seller": sellerId});

        let lastView = '';
        if (msgView) {
            lastView = msgView.updatedAt;
        }

        // Now check the number of message created after the time
        if (lastView) {
            newMsgCount = Messages.find({
                "user": userId,
                "seller": sellerId,
                "sender": "seller",
                "createdAt": {$gt: lastView}
            }).count()
        } else {
            newMsgCount = Messages.find({"user": userId, "seller": sellerId, "sender": "seller"}).count()
        }

        return {
            latestMessage,
            newMsgCount
        }
    } else if (user) {
        // console.log({user});
        return {}
    }
})(ShowEachSeller)
