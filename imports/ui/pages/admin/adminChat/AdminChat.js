import React from 'react';
import Header from '../../header-username/header';
import Footer from '../../../components/footer/Footer';
import AdminTopBar from "../adminTopbar/adminTopbar";
import "./adminChat.scss";
import AdminLeftEditBar from "../adminLeftEditBar";
import ChatSection from './ChatSection';
import ChatWithList from './ChatWithList';

class AdminChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedSeller: '',
            selectedUser: ''
        };
    }
    
    parentCallback({sellerId, userId}) {
        if (sellerId) {
            this.setState({selectedSeller: sellerId})
        } else if (userId) {
            this.setState({selectedUser: userId})
        }        
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <AdminTopBar/>
                    <div className="clearfix"></div>
                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 no-padding">
                            <div className={"adminEditTitleBlock padding20"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <div className={"admin_chat_edit_block"}>
                            <AdminLeftEditBar />
                            </div>
                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 no-padding">
                            <div className={"adminApprovalHrTitleBlock padding20"}>
                                <div className={"adminApprovalText adminChatTextDiv"}>
                                   CHAT
                                </div>
                                {/* <div className={"adminChatHr"}>
                                    <img  src={"/img/line.svg"} />
                                </div> */}
                            </div>
                            <div className={"col-md-12 display_flex leftPadding no-padding"}>
                                <ChatSection sellerId={this.state.selectedSeller} userId={this.state.selectedUser} />
                                <ChatWithList parentCallback={this.parentCallback.bind(this)} />
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
            )
    }
}

export default AdminChat;