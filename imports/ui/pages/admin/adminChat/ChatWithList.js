import React from 'react';
import {Meteor} from 'meteor/meteor';
import ShowEachSeller from './ShowEachSeller';

class ChatWithList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellers: [],
            users: []
        };
    }

    componentDidMount() {
        const self = this;
        const adminId = Meteor.userId();
        Meteor.call('seller.getAllSellers', function (err, result) {
            if (err) {
                console.error(err);
            } else {
                if (!result) {
                    self.setState({sellers: []})
                } else {
                    self.setState({sellers: result})
                }
            }
        });
        Meteor.call('chat_with.getUserChatListWithAdmin', adminId, function (error, result) {
            if (error) {
                console.log(error)
            } else {
                self.setState({users: result})
            }
        })
    }

    renderUsers() {
        const parentCallback = this.props.parentCallback;
        return this.state.users.map((user, index) => (
            <ShowEachSeller user={user} key={index} parentCallback={parentCallback}/>
        ))
    }

    renderEachSeller() {
        const parentCallback = this.props.parentCallback;
        return this.state.sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index} parentCallback={parentCallback}/>
        ))
    }

    render() {
        return (
            <div id={"chatListName"} className={"sectionblock2 admin_chat_right_block_padding"}>
                <ul className="list-group list-group-flush adminlistMenu">
                    {this.renderUsers()}
                    {this.renderEachSeller()}
                </ul>
            </div>
        )
    }
}

export default ChatWithList;