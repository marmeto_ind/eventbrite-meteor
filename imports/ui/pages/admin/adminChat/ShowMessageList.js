import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Messages} from "../../../../api/messages";
import ShowEachMessage from './ShowEachMessage';

class ShowMessageList extends React.Component {

    componentDidMount() {
        let {user, sellerId, userId} = this.props;

        if (sellerId) {
            Meteor.call('message_view.upsert', userId, sellerId, "admin", function (err, result) {
                if (err) {
                    console.log("error occured")
                } else {
                    console.log(result)
                }
            })
        }        
    }

    renderMessages() {
        return this.props.messages.map((message, index) => (
            <ShowEachMessage message={message} key={index}/>
        ))
    }

    render() {
        
        return (
            <ul className="list-group list-group-flush">
                {this.renderMessages()}
            </ul>
        )
    }
}

export default withTracker((data) => {
    const {sellerId, user} = data;
    const userId = Meteor.userId();
    
    Meteor.subscribe('messages.all');

    if (sellerId) {
        const messages = Messages.find({"user": userId, "seller": sellerId}).fetch();
        return {
            messages,
            userId,
            sellerId
        }
    } else if (user) {
        const messages = Messages.find({"admin": userId, "user": user}).fetch();
        return {
            messages,
            userId,
            user
        }
    }    
})(ShowMessageList);
