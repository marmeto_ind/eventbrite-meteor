import React from 'react';
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';

class SendMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openEmoji: false,
        };

    }

    _handleMessageSend(event) {
        event.preventDefault();
        const message = $("#messageText").val();
        const userId = Meteor.userId();
        const {sellerId, user} = this.props;
        const sender = 'admin';

        if(message) {
            if (sellerId) {
                Meteor.call('messages.insert', message, userId, sellerId, sender, function (err, result) {
                    if(err) {
                        console.error(err);
                    } else {
                        // console.log(result);
                    }
                });
                $("#messageText").val("");
            } else if (user) {
                Meteor.call('messages.adminInsert', message, user, userId, sender, function (err, result) {
                    if (err) {
                        console.error(err);
                    }
                });
                $("#messageText").val("");
            }            
        }
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;
        console.log(emojiCode);
        $("#messageText").val(emojiCode);
    }

    _handleOpenEmoji() {
        console.log("open emoji");
        this.setState({openEmoji: !this.state.openEmoji});
    }

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#admin_attachFile")[0].files[0];

        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                console.log(uploader.progress());
                // this.setState({ logoImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    render() {
        const sellerId = this.props.sellerId;

        return (
            <div>
                {this.state.openEmoji === true ? <div className={"web_admin_emoji_block"}>
                        <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)} />
                    </div>:
                    ''
                }
            <div className={"adminchatType-box"}>
                <ul className="list-group list-group-flush">
                    <li className="">
                        <div className={"adminChatBlock_wrapper"}>
                            <div className={"adminChatBlock"}>
                                <div className={"block2"}>
                                    <label htmlFor="admin_attachFile" >
                                    <span>
                                        <img
                                            className={"list-img_admin"}
                                            src="/img/clip_chat.svg"
                                            alt=""
                                        />
                                    </span>
                                    </label>
                                    <div onChange={this._handleFileUpload.bind(this)}>
                                        <input
                                            type="file"
                                            className="form-control-file"
                                            id="admin_attachFile"
                                        />
                                    </div>
                                </div>
                                <div className={"block1"}>
                                    <span>
                                        <img className={"list-img_admin"}
                                             src="/img/emoji_chat.svg"
                                             alt=""
                                             onClick={this._handleOpenEmoji.bind(this)}
                                        />
                                    </span>
                                </div>
                                
                                <div className={"block3"}>
                                    <input id={"messageText"}
                                           className="type-search"
                                           type="text"
                                           placeholder="WRITE A MESSAGE"
                                           name="message"
                                    />
                                </div>
                                <div className={"block4"}>
                                    <span className={"btn"} onClick={this._handleMessageSend.bind(this)}>
                                        <img className={"list-img_admin chat-send"}  src="/img/send_image.svg" alt="" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        )
    }
}

export default SendMessage;