import React from 'react';
import SendMessage from './SendMessage';
import ShowMessageList from './ShowMessageList';

class ChatSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {sellerId, userId} = this.props;
        if(!sellerId && !userId) {
            return (
                <div className={"sectionblock1 no-padding"}>
                    <div className={"no-user-selected-style"}>No Vendor Selected</div>
                </div>
            )
        } if (userId) {
            return (
                <div className={"sectionBlock1_wrapper no-padding"}>
                    <div className="admin_chat_wrapper">
                        <div className={"adminchat-message-list"}>
                            <ShowMessageList user={userId} />
                        </div>
                        <SendMessage user={userId} />
                    </div>
                </div>
            )
        }
        return (
            <div className={"sectionBlock1_wrapper no-padding"}>
                <div className="admin_chat_wrapper">
                    <div className={"adminchat-message-list"}>
                        <ShowMessageList sellerId={sellerId} />
                    </div>
                    <SendMessage sellerId={sellerId} />
                </div>
            </div>
        )
    }
}

export default ChatSection;



