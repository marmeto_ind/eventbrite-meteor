import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import './adminTopbar.scss';

class AdminTopBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isApproved: false,
            usernameEdit: false,
            unseenMessage: ''
        }
    }

    componentWillMount() {
        let userId = Meteor.userId();
        console.log(userId);
        let viewer = "admin";
        let self = this;
        Meteor.call('user.getAllUnseenMessageCountByAdmin', userId, viewer, function (error, result) {
            if (error) {
                console.log(error)
            } else {
                self.setState({unseenMessage: result})
            }
        })
    }

    componentDidMount() {
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length - 1];
        const admin_approval_part = url_parts[url_parts.length - 2];

        if (admin_approval_part === 'details') {
            $(".approve_block").addClass("admin_approval_div_displayBlock");
            $(".approve_block_img").addClass("admin_approval_div_displayBlock");
        } else {
            $(".approve_block").addClass("admin_approval_div_displayNone");
            $(".approve_block_img").addClass("admin_approval_div_displayNone");
        }
        if (url_last_part === 'chat') {
            $("#admin-chat-img").attr('src', "/img/new_img/chatIcon.svg");
        } else if (url_last_part === 'approvals') {
            $("#admin-tick-img").attr('src', "/img/mychoice-tick.png");
        } else if (url_last_part === 'stats') {
            $("#admin-red-tool").attr('src', "/img/new_img/black_graph.svg");
        }

        if (this.props.seller) {
            const isApprove = this.props.seller.isApprove;
            if (isApprove === true) {
                this.setState({isApproved: true});
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seller) {
            const isApprove = nextProps.seller.isApprove;
            if (isApprove === true) {
                this.setState({isApproved: true});
            }
        }
    }

    handleApproveSeller(e) {
        e.preventDefault();
        const sellerId = this.props.sellerId;
        const result = Meteor.call('seller.approve', sellerId);
        if (!result) {
            console.log("approved successfully");
        } else {
            console.log("some error occurred");
        }
    }

    // handleUserNameBlur handle click on some other place
    handleUserNameBlur() {
        const updateUserName = $("#updateUserName").val();
        this.setState({usernameEdit: false});
        let userProfile = Meteor.user().profile;
        
        if (updateUserName) {
            userProfile['name'] = updateUserName;
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    profile: userProfile,
                }
            });
        }
    }

    // handle name edit
    handleUserNameEdit(event) {
        this.setState({usernameEdit: true});
    }

    render() {
        const user = Meteor.user();

        return (
            <div className="col-lg-12 col-xs-12 bottom-menu-cntnr">
                <div className="bottom-menu ">

                    <div className="bottom-menu-container">
                        <div className="bottom-menu-container">
                            <div className={"row"}>
                                <div className={"col-md-12"}>
                                    <div className={"col-md-4"}>
                                        <div className="fix-bar-option col1-style  common-event-name-div">
                                            {/*administrator...*/}
                                            {
                                                this.state.usernameEdit ?
                                                    <input className="mobile_userSettingPhoneEdit_input" type="text"
                                                           onBlur={this.handleUserNameBlur.bind(this)}
                                                           placeholder={user.profile.name}
                                                           id="updateUserName" autoFocus/> :
                                                    <p className={"settings-h2 padding-right-30px"}
                                                       onClick={this.handleUserNameEdit.bind(this)}>
                                                        {this.props.userName}
                                                    </p>
                                            }
                                        </div>
                                    </div>
                                    <div className={"col-md-5 admin_topbar_approval "}>
                                        {/* <div className={"approve_block"}>
                                            Approve?
                                        </div> */}
                                    </div>
                                    <div className={"col-md-1 admin_topbar_approval_img"}>
                                        {/* <div className={"approve_block_img"}
                                             onClick={this.handleApproveSeller.bind(this)}>
                                            <img
                                                className={"approval-tick"}
                                                src={this.state.isApproved === true
                                                    ? "/img/show-tick.png"
                                                    : "/img/wendy_grey_tick.png"
                                                }
                                                alt=""/>
                                        </div> */}
                                    </div>
                                    <div id={"mobile_userRight_navBar"} className={"col-md-2  col-xs-5"}>
                                        <div className="row">
                                            <div className={"col-md-4 col-xs-4 relativeposition"}>
                                                <a href="/admin/chat">
                                                    <img id={"admin-chat-img"} className="create-add"
                                                         src="/img/chat-notifi.png"
                                                         alt=""/>
                                                    <div className="admin_chat_count_icon_style">
                                                        <span>{this.props.unseenMessage}</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className={"col-md-4 col-xs-4"}>
                                                <a href="/admin/approvals">
                                                    <img id={"admin-tick-img"} className="create-add"
                                                         src="/img/wendy_grey_tick.png" alt=""/>
                                                </a>
                                            </div>
                                            <div className={"col-md-4  col-xs-4"}>
                                                <a href="/admin/stats">
                                                    <img id={"admin-red-tool"} className="create-add"
                                                         src="/img/svg_img/Graph.svg"
                                                         alt=""/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        )
    }
}

// export default AdminTopBar;
export default withTracker((data) => {
    let unseenMessage = '';
    Meteor.subscribe('getUnseenMessage', Meteor.userId(), function (count) {
        unseenMessage = count;
    });
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    return {
        unseenMessage: unseenMessage,
        userName: userName
    }
})(AdminTopBar)

