import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {SellerViews} from "../../../../api/sellerViews";

class ViewStatistics extends React.Component {
    render() {
        const totalNumOfView = this.props.totalNumOfView;
        const numOfViewInLastWeek = this.props.numOfViewInLastWeek;
        const numOfViewInMonth = this.props.numOfViewInMonth;

        return (
            <div className={"fourth-div views-background"}>
                <ul className="list-group vendorstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/svg_img/greyEye.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfViewInMonth} V/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfViewInLastWeek} VIEWERS LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfView} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(()=> {
    Meteor.subscribe('seller_views.all');

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfView = SellerViews.find().count();
    const numOfViewInLastWeek = SellerViews.find({"createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    const numOfViewInMonth = SellerViews.find({"createdAt": {"$gt": new Date(oneMonthAgo)}}).count();

    return {
        totalNumOfView: totalNumOfView,
        numOfViewInLastWeek: numOfViewInLastWeek,
        numOfViewInMonth: numOfViewInMonth,
    }
})(ViewStatistics);