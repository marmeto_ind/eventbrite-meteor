import React from 'react';

class ShowEachActiveChat extends React.Component {
    render() {
        const record = this.props.record;

        return (
            <li className="no-padding ">
                {/* <div className={"vendorStatsList"}>
                    <div className={"vendorStatsFloatLeft"}>
                        <span>
                            <img className={"list-img"} src="/img/greyStatue.png" alt="" />
                        </span>
                    </div>
                    <div className={"vendorStatsTextCenter"}>
                        <p>{record.userName} - {record.sellerName}</p>
                        <p className={"vendor-stats-p"}>Couple</p>
                        <p className="header_text">Toni and Alexandra - Panorama</p>
                        <p className="message">Hi, How are you?</p>
                    </div>
                </div> */}
                <div className="content_wrapper">
                    <div className="show_rating_container show_rating_container_second">
                        <div className="show_rating_wrapper">
                            <div className="left_image"><img src="/img/new_img/password.svg" alt="icon" /></div>
                            <div className="center_div">
                                <div className="best_seller_wrapper">
                                    <p className="header_text text-limit">Toni and Alexandra - Panorama</p>
                                    <p className="message text-limit">Hi, How are you?</p>
                                </div>
                            </div>
                            <div className="right_image">
                                {/* <img src="/img/view.svg" alt="icon" /> */}
                                <span>3</span>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachActiveChat;