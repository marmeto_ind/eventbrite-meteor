import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';

class UserStatistics extends React.Component {
    render() {
        const totalNumOfUser = this.props.totalNumOfUser;
        const numOfNewUserInLastWeek = this.props.numOfNewUserInLastWeek;
        const numOfUserInMonth = this.props.numOfUserInMonth;

        return (
            <div className={"fourth-div new-users-background"}>
                <ul className="list-group vendorstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/agenda-user.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfUserInMonth} U/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfNewUserInLastWeek} NEW USERS LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfUser} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(()=> {
    Meteor.subscribe('sellers.all');

    Meteor.call('users.getTotalCount', function (err, result) {
        if(err) {
            console.error(err);
        } else {
            Session.set('totalNumOfUser', result);
        }
    });

    Meteor.call('users.getUsersCountInLastWeek', function (err, result) {
        if(err) {
            console.error(err);
        } else {
            Session.set('numOfUserInLastWeek', result);
        }
    });

    Meteor.call('users.getUsersCountInMonth', function (err, result) {
        if(err) {
            console.error(err);
        } else {
            Session.set('numOfUserInMonth', result);
        }
    });

    return {
        totalNumOfUser: Session.get('totalNumOfUser') ,
        numOfNewUserInLastWeek: Session.get('numOfUserInLastWeek'),
        numOfUserInMonth: Session.get('numOfUserInMonth'),
    }
})(UserStatistics);