import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {ChatWith} from "../../../../api/chatWith";
import {Meteor} from 'meteor/meteor';
import ShowEachActiveChat from './ShowEachActiveChat';

class ShowActiveChats extends React.Component {
    renderActiveChats() {
        return this.props.chatRecords.map((record, index) => (
            <ShowEachActiveChat key={index} record={record} />
        ))
    }

    render() {
        return (
            <ul className="list-group list-group-flush">
                {
                    (this.props.chatRecords.length > 0)? this.renderActiveChats():
                    <div className={"no-event-booked-style"}>NO RECORD FOUND</div>
                }
            </ul>
        )
    }
}

export default withTracker(()=>{
    Meteor.subscribe('chat_with.all');
    const chatRecords = ChatWith.find({}, {sort: {createdAt: -1}, limit: 3}).fetch();
    return {
        chatRecords: chatRecords,
    }
})(ShowActiveChats);