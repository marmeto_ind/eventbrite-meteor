import React from 'react';

class ShowEachApproval extends React.Component {
    render() {
        const seller = this.props.seller;

        return (
            <div className={"third-div"}>
                <ul className="list-group vendorstats-listBackground reviews-background approval_wrapper_section">
                    {/* <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/cake.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {seller.name}
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {seller.profile.address}
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {seller.profile.moreImages.length + 1} Pictures
                    </li> */}
                    {/*<li className="list-group-item text-list-div-style all-div-text4">*/}
                        {/*(430)*/}
                    {/*</li>*/}
                    <div className="left_div">
                        <img className="" src="/img/cake.png" alt="" />
                        <span>11:42</span>
                    </div>
                    <div className="center_div">
                        <h2>{seller.name}</h2>
                        <p className="address">{seller.profile.address}</p>
                        <p className="image_details">{seller.profile.moreImages.length + 1} Pictures</p>
                    </div>
                    <div className="right_div">
                        <img className="service_back_logo-home-first-img" src="/img/newService/TICK.svg" />
                    </div>
                </ul>
            </div>
        )
    }
}

export default ShowEachApproval;