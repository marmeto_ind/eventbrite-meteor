import React from 'react';
import ShowEachApproval from './ShowEachApproval';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../api/sellers";
import {Meteor} from 'meteor/meteor';

class ShowApprovals extends React.Component {
    renderApprovals() {
        return this.props.sellers.map((seller, index)=>(
            <ShowEachApproval key={index} seller={seller} />
        ))
    }

    render() {
        return (
            <div>
                {this.renderApprovals()}
            </div>
        )
    }
}

export default withTracker(()=>{
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({"isApprove" : true}, {sort: {createdAt: -1}, limit: 3}).fetch();
    // console.log(sellers);

    return {
        sellers: sellers,
    }
})(ShowApprovals);