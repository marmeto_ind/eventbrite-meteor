import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Sellers} from "../../../../api/sellers";

class ServiceStatistics extends React.Component {
    render() {
        const totalNumOfService = this.props.totalNumOfSeller;
        const numOfServiceInLastWeek = this.props.numOfServiceInLastWeek;
        const numOfServiceInMonth = this.props.numOfServiceInMonth;

        return (
            <div className={"third-div new-service-background"}>
                <ul className="list-group vendorstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/agenda-shake-hand.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfServiceInMonth} S/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfServiceInLastWeek} NEW SERVICES LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfService} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (430)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(()=> {
    Meteor.subscribe('sellers.all');

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfSeller = Sellers.find().count();
    const numOfServiceInLastWeek = Sellers.find({"createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    const numOfServiceInMonth = Sellers.find({"createdAt": {"$gt": new Date(oneMonthAgo)}}).count();

    return {
        totalNumOfSeller: totalNumOfSeller,
        numOfServiceInLastWeek: numOfServiceInLastWeek,
        numOfServiceInMonth: numOfServiceInMonth,
    }
})(ServiceStatistics);
