import React from 'react';
import ShowEachService from './ShowEachService';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../../api/sellers";
import {Meteor} from 'meteor/meteor';

class ShowNewServices extends React.Component {
    renderServices() {
        return this.props.sellers.map((seller, index)=> (
            <ShowEachService key={index} seller={seller} />
        ))
    }

    render() {
        return (
            <ul className="list-group list-group-flush">
                {this.renderServices()}
            </ul>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({}, {sort: {createdAt: -1}, limit: 3}).fetch();
    // console.log(sellers);
    return {
        sellers: sellers,
    }
})(ShowNewServices);