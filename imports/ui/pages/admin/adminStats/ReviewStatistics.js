import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Review} from "../../../../api/review";

class ReviewStatistics extends React.Component {
    render() {
        const totalNumOfReview = this.props.totalNumOfReview;
        const numOfReviewInLastWeek = this.props.numOfReviewInLastWeek;
        const numOfReviewInMonth = this.props.numOfReviewInMonth;

        return (
            <div className={"fourth-div"}>
                <ul className="list-group reviews-background vendorstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/agenda-star.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfReviewInMonth} R/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfReviewInLastWeek} REVIEWS LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfReview} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(()=> {
    Meteor.subscribe('reviews.all');

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfReview = Review.find().count();
    const numOfReviewInLastWeek = Review.find({"createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    const numOfReviewInMonth = Review.find({"createdAt": {"$gt": new Date(oneMonthAgo)}}).count();

    return {
        totalNumOfReview: totalNumOfReview,
        numOfReviewInLastWeek: numOfReviewInLastWeek,
        numOfReviewInMonth: numOfReviewInMonth,
    }
})(ReviewStatistics);