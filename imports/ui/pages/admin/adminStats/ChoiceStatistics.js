import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Choices} from "../../../../api/choice";

class ChoiceStatistics extends React.Component {
    render() {
        const totalNumOfChoice = this.props.totalNumOfChoice;
        const numOfChoiceInLastWeek = this.props.numOfChoiceInLastWeek;
        const numOfChoiceInMonth = this.props.numOfChoiceInMonth;

        return (
            <div className={"fourth-div choices-background"}>
                <ul className="list-group vendorstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/new_img/vendor_eye.svg" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfChoiceInMonth} C/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfChoiceInLastWeek} CHOICES LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfChoice} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(()=> {
    Meteor.subscribe('choices.all');

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfChoice = Choices.find().count();
    const numOfChoiceInLastWeek = Choices.find({"createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    const numOfChoiceInMonth = Choices.find({"createdAt": {"$gt": new Date(oneMonthAgo)}}).count();

    return {
        totalNumOfChoice: totalNumOfChoice,
        numOfChoiceInLastWeek: numOfChoiceInLastWeek,
        numOfChoiceInMonth: numOfChoiceInMonth,
    }
})(ChoiceStatistics);