import React from 'react';
import Header from "../../header-username/header";
import AdminLeftEditBar from "../adminLeftEditBar";
import AdminTopBar from "../adminTopbar/adminTopbar";
import Footer from "../../../components/footer/Footer";
import './adminStats.scss';
import '../adminEditAdd/adminEditAdd.scss';
import ChatStatistics from './ChatStatistics';
import ServiceStatistics from './ServiceStatistics';
import UserStatistics from './UserStatistics';
import ViewStatistics from './ViewStatistics';
import ChoiceStatistics from './ChoiceStatistics';
import ReviewStatistics from './ReviewStatistics';
import ShowNewServices from './ShowNewServices';
import ShowApprovals from './ShowApprovals';
import ShowActiveChats from './ShowActiveChats';

class AdminStats extends React.Component
{
    render()
    {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container status_div_wrapper">
                    <AdminTopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container chat-body_wrapper satus">
                        <div className="status_left_div">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="status_center_div">
                            <div className={""}>
                                <div className={"adminEditSectionHrTitleBlock"}>
                                    <div className={"adminEditSectionText adminEditSectionTextDiv"}>
                                        Statistics
                                    </div>
                                    {/* <div className={"adminEditSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                </div>
                                <div className="clearfix"></div>
                                <ChatStatistics/>
                                <ServiceStatistics/>
                                <UserStatistics/>
                                <ViewStatistics/>
                                <ChoiceStatistics/>
                                <ReviewStatistics/>
                            </div>
                        <div className="clearfix"></div>
                    </div>

                        <div  className={"status_right_div"}>
                                <div className={"adminStatsSectionHrTitleBlock"}>
                                    <div className={"adminStatsSectionText_wrapper"}>
                                        Active chats
                                    </div>
                                    {/* <div className={"adminStatsSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                    <div className="clearfix"></div>
                                    <ShowActiveChats/>
                                </div>
                                

                                <div className={"adminStatsSectionHrTitleBlock adminstatsBlockPadding"}>
                                    <div className={"adminStatsSectionText_wrapper"}>
                                       New users
                                    </div>
                                    {/* <div className={"adminStatsSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                    <div className="clearfix"></div>

                                    <ShowNewServices/>
                                    
                                </div>

                                <div className={"adminStatsSectionHrTitleBlock adminstatsBlockPadding"}>
                                    <div className={"adminStatsSectionText_wrapper"}>
                                       New Services
                                    </div>
                                    {/* <div className={"adminStatsSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                    <div className="clearfix"></div>

                                    <ShowNewServices/>
                                    
                                </div>
                                

                                <div className={"adminApprovalsSectionHrTitleBlock adminstatsBlockPadding"}>
                                    <div className={"adminApprovalsSectionText_wrapper"}>
                                       Approvals
                                    </div>
                                    {/* <div className={"adminApprovalsSectionHr"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div> */}
                                    <div className="clearfix"></div>

                                    <ShowApprovals/>
                                </div>
                                

                            </div>
                        </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default AdminStats;