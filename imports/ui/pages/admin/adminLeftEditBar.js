import React from 'react';
import '../admin/adminLeftEditBar.scss';
import { browserHistory } from 'react-router';

class AdminLeftEditBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedEditTab: ""
        }
    }

    componentDidMount(){
        // let {pathname} = window.location;
        // this.setState({selectedEditTab: pathname});
    }

    _categoryOnClick = (e) => {
        e.preventDefault();
        browserHistory.push('/admin/categoryEditAdd')
    };

    __serviceOnClick = (e) =>{
        e.preventDefault();
        browserHistory.push('/admin/adminServiceEdit')
    };

    _regionOnClick = (e) => {
        e.preventDefault();
        browserHistory.push('/admin/regionEditAdd')
    };

    _attributeOnClick = (e) => {
        e.preventDefault();
        browserHistory.push('/admin/facilityEditAdd')
    };

    _eventOnClick = (e) => {
        e.preventDefault();
        browserHistory.push('/admin/eventEditAdd')
    };

    _bannerOnClick = (e) => {
        e.preventDefault();
        browserHistory.push('/admin/bannerEditAdd')
    };

    render() {
        let {pathname} = window.location;
        let eventActiveClass =  pathname === "/admin/eventEditAdd"? "active": "";
        let categoryActiveClass =  pathname === "/admin/categoryEditAdd"? "active": "";
        let regionActiveClass =  pathname === "/admin/regionEditAdd"? "active": "";
        let serviceActiveClass =  pathname === "/admin/adminServiceEdit"? "active": "";
        let facilityActiveClass =  pathname === "/admin/facilityEditAdd"? "active": "";
        let bannerActiveClass =  pathname === "/admin/bannerEditAdd"? "active": "";
        
        return (
          <div className={"edit-padding"}>
              <ul className="list-group list-group-flush">
              <li className={"list-group-item list-right-padding-style " + eventActiveClass} onClick={this._eventOnClick}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span>
                                  <img className={"list-img"} src="/img/svg_img/add_event.svg" alt="" />
                                </span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>EVENT TYPE</p>
                          </div>
                      </div>
                  </li>
                  <li className={"list-group-item list-right-padding-style " + categoryActiveClass} onClick={this._categoryOnClick}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span><img className={"list-img"} src="/img/svg_img/category_svg.svg" alt="" /></span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>category</p>
                          </div>
                      </div>
                  </li>               
                  <li className={"list-group-item list-right-padding-style " + serviceActiveClass} onClick={this.__serviceOnClick}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span><img className={"list-img"} src="/img/new_img/white_vendor.svg" alt="" /></span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>Service</p>
                          </div>
                      </div>
                  </li>
                  <li className={"list-group-item list-right-padding-style " + regionActiveClass} onClick={this._regionOnClick}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span><img className={"list-img"} src="/img/svg_img/location_svg.svg" alt="" /></span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>region</p>
                          </div>
                      </div>
                  </li>
                  <li className={"list-group-item list-right-padding-style " + facilityActiveClass} onClick={this._attributeOnClick}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span><img className={"list-img"} src="/img/svg_img/facility_svg.svg" alt="" /></span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>facility</p>
                          </div>
                      </div>
                  </li>                  
                  <li className={"list-group-item list-right-padding-style " + bannerActiveClass} onClick={this._bannerOnClick.bind(this)}>
                      <div className={"adminList"}>
                          <div className={"adminFloatLeft"}>
                              <span><img className={"list-img"} src="/img/new_img/white_banner.svg" alt="" /></span>
                          </div>
                          <div className={"adminTextCenter"}>
                              <p>BANNERS</p>
                          </div>
                      </div>
                  </li>
              </ul>
          </div>
        )
    }
}

export default AdminLeftEditBar;