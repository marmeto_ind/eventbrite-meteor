import React from 'react';
import AdminEdit from './AdminEdit';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class AdminEditContainer extends React.Component {
    render() {
        if(this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>)
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "admin") {
                    browserHistory.push('/login');
                    return (<div></div>)
                } else {
                    return (
                        <AdminEdit/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(AdminEditContainer);

