import React from 'react';
import Header from '../../header-username/header';
import Footer from '../../../components/footer/Footer';
import AdminTopBar from "../adminTopbar/adminTopbar";
import "./adminEdit.scss";
import AdminLeftEditBar from "../adminLeftEditBar";

class AdminEdit extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <AdminTopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container_wrapper chat-body_wrapper">
                        <div className="col-lg-4 col-sm-12 col-md-4 col-xs-12 adminEditTitleBlockPadding">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>

                            <div className="clearfix"></div>

                            <AdminLeftEditBar />

                        </div>

                        <div className="col-lg-8 col-sm-12 col-md-8col-xs-12 admin-edit-new-event-button_padding">
                            <div>
                                <p className={"welcome-msg"}>  Welcome</p> <br/>
                                <p className={"welcome-msg"}>Admin </p> <br/>
                                <p className={"welcome-msg"}> to</p> <br/>
                                <p className={"welcome-msg"}> wendely</p>
                            </div>

                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default AdminEdit;