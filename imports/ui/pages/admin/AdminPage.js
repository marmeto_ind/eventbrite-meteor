import React from 'react';
import Header from '../../components/admin/Header';
import Footer from '../../components/footer/Footer';
import LeftSidebar from '../../components/admin/LeftSidebar'
import Dashboard from '../../components/admin/Dashboard';


class AdminPage extends React.Component {
    render() {
        return (

            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><Dashboard/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminPage;