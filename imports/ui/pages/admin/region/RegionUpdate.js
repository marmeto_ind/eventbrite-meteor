import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import RegionDetail from '../../../components/region/admin/RegionDetail';
import RegionUpdate from '../../../components/region/admin/RegionUpdate';

class RegionUpdatePage extends React.Component {
    render() {
        const regionId = this.props.params.id;
        return (

            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><RegionUpdate regionId={regionId}/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RegionUpdatePage;