// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import RegionCreate from '../../../components/region/admin/RegionCreate';
//
// class RegionAddPage extends React.Component {
//     render() {
//         return (
//             <div>
//                 <Header/>
//                 <RegionCreate/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default RegionAddPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import RegionCreate from '../../../components/region/admin/RegionCreate';

class RegionAddPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><RegionCreate/></div>
            </div>
        </div>
        </div>
        )
    }
}

export default RegionAddPage;