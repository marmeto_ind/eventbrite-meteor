// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import RegionDetail from '../../../components/region/admin/RegionDetail';
//
// class RegionDetailPage extends React.Component {
//     render() {
//         // console.log(this.props.params.id)
//
//         const regionId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <RegionDetail regionId={regionId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default RegionDetailPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import RegionDetail from '../../../components/region/admin/RegionDetail';

class RegionDetailPage extends React.Component {
    render() {
        const regionId = this.props.params.id;

        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><RegionDetail regionId={regionId}/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RegionDetailPage;