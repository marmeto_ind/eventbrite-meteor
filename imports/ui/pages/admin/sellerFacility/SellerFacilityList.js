// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import SellerFacilityList from '../../../components/sellerFacility/admin/SellerFacilityList';
//
// class SellerFacilityListPage extends React.Component {
//     render() {
//         return (
//             <div className={"seller-list-page"}>
//                 <Header/>
//                 <SellerFacilityList/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default SellerFacilityListPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerFacilityList from '../../../components/sellerFacility/admin/SellerFacilityList';

class SellerFacilityListPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><SellerFacilityList/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellerFacilityListPage;