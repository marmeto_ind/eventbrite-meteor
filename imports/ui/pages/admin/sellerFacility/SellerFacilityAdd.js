import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerFacilityCreate from '../../../components/sellerFacility/admin/SellerFacilityCreate';

class SellerFacilityAddPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><SellerFacilityCreate/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellerFacilityAddPage;