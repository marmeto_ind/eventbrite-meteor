// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import SellerFacilityUpdate from '../../../components/sellerFacility/admin/SellerFacilityUpdate'
//
// class SellerFacilityUpdatePage extends React.Component {
//     render() {
//         const sellerFacilityId = this.props.params.id;
//         console.log(sellerFacilityId);
//
//         return (
//             <div>
//                 <Header/>
//                 <SellerFacilityUpdate sellerFacilityId={sellerFacilityId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default SellerFacilityUpdatePage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerFacilityUpdate from '../../../components/sellerFacility/admin/SellerFacilityUpdate';

class SellerFacilityUpdatePage extends React.Component {
    render() {

        const sellerFacilityId = this.props.params.id;

        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><SellerFacilityUpdate sellerFacilityId={sellerFacilityId}/></div>
            </div>
        </div>
        </div>
        )
    }
}

export default SellerFacilityUpdatePage;