// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import SellerFacilityDetail from '../../../components/sellerFacility/admin/SellerFacilityDetail';
//
// class SellerFacilityDetailPage extends React.Component {
//     render() {
//         // console.log(this.props.params.id)
//
//         const sellerFacilityId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <SellerFacilityDetail sellerFacilityId={sellerFacilityId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default SellerFacilityDetailPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerFacilityDetail from '../../../components/sellerFacility/admin/SellerFacilityDetail';

class SellerFacilityDetailPage extends React.Component {
    render() {

        const sellerFacilityId = this.props.params.id;

        return (

            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><SellerFacilityDetail sellerFacilityId={sellerFacilityId}/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellerFacilityDetailPage;
