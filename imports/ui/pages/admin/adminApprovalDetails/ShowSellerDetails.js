import React from 'react';
import ShowEachImage from './ShowEachImage';
import ShowEachFacility from './ShowEachFacility';
import {Sellers} from "../../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import ReactPlayer from 'react-player';
import { withTracker } from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';
import StarRatingComponent from 'react-star-rating-component';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class ShowUnApprovedSellers extends React.Component {
    constructor(props) {
        super(props);
        this.state= {}
        this.handleNewSellerClick = this.handleNewSellerClick.bind(this);
    }

    handleNewSellerClick() {
        const seller = this.props.seller;
        const sellerId = seller._id;
        browserHistory.push('/admin/approval/details/'+sellerId);
        this.props.parentCallback({sellerId});
    }

    render() {
        const seller = this.props.seller;
        const selectedSellerId = this.props.selectedSellerId;
        const sellerId = seller && seller._id;
        let divClass = sellerId == selectedSellerId? "show_rating_wrapper padding_10 left_div_30 active": "show_rating_wrapper padding_10 left_div_30";
        
        let day = seller.createdAt.toLocaleString('en-US', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
          }).split(',')[0];
        
        return (
        <div className="show_rating_container show_rating_container_second" onClick={this.handleNewSellerClick}>
            <div className={divClass}>
                <div className="left_image">
                    <img src={seller.profile.mainImage} alt="icon" />
                </div>
                <div className="center_div">
                    <div className="best_seller_wrapper">                                        
                        <div className="content">
                            <p className="heading_text">{seller.name}</p>
                            <p className="paragraph_text">{seller.profile.address}</p>
                        </div>
                    </div>
                </div>
                <div className="right_image">
                    <p>{day}</p>
                    <p>{seller.createdAt.toString().substring(0, 10)}</p>
                </div>
            </div>
        </div>
    )
    }
}

class ShowSellerDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            selectedSellerId: '',
            hotTag: false,
            newTag: false,
            goodDealTag: false,
            rating: 1
        }
        this.handleToggleGoodDealTag = this.handleToggleGoodDealTag.bind(this);
        this.handleToggleNewTag = this.handleToggleNewTag.bind(this);
        this.handleToggleHotTag = this.handleToggleHotTag.bind(this);
        this.handleApproveSeller = this.handleApproveSeller.bind(this);
    }

    renderOtherImages(images) {
        if (!images) {
            return
        }
        return images.map((image, index)=> (
            <ShowEachImage key={index} image={image}/>
        ));
    }

    renderFacilities(facilities) {
        return facilities.map((facility, index)=>(
            <ShowEachFacility key={index} facility={facility} />
        ));
    }

    handleToggleHotTag() {
        this.setState({hotTag: !this.state.hotTag});
    }

    handleToggleNewTag() {
        this.setState({newTag: !this.state.newTag});
    }

    handleToggleGoodDealTag() {
        this.setState({goodDealTag: !this.state.goodDealTag});
    }

    rednderUnApprovedSellers(){
        let selectedSellerId = this.state.selectedSellerId || this.props.seller._id;
        return this.props.sellers.map((seller, index) => <ShowUnApprovedSellers 
                parentCallback={this.props.parentCallback} 
                key={index} selectedSellerId={selectedSellerId} seller={seller}/>)
    }

    isFacilityProvided(facliityName) {
        let facilities = this.props.seller.profile.facilities;
        for (let index = 0; index < facilities.length; index++) {
            if (!facliityName) {
                continue
            }
            if (!facilities[index].name) {
                continue
            }
            if (facliityName.toLowerCase() == facilities[index].name.toLowerCase()) {
                return "active"
            }            
        }
        return ""
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
    }

    handleApproveSeller(e) {
        e.preventDefault();
        const seller = this.props.seller;
        const sellerId = seller._id;
        const {hotTag, newTag, goodDealTag, rating} = this.state;
        
        Meteor.call('seller.approveWithTag', {hotTag, newTag, goodDealTag, rating, sellerId}, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                console.log(result)
                browserHistory.push("/admin/approvals");
            }
        });
    }

    render() {
        const seller = this.props.seller;
        const { rating } = this.state;
        
        return (
            <div className={"col-md-12 col-xs-12 no-padding adminApprovalDetailsList"}>
                <div className="col-md-6 no-padding col-lg-6">
                    <div className="content_wrapper">
                        <div className="show_rating_container show_rating_container_second">
                            <div className="show_rating_wrapper padding_10 left_div_30">
                                <div className="left_image">
                                    <img src="/img/new_img/service.svg" alt="icon"   />
                                </div>
                                <div className="center_div">
                                    <div className="best_seller_wrapper">                                        
                                        <div className="content">
                                            <p className="heading_text font-smooth-unset">{seller.name}</p>
                                            <p className="paragraph_text font-smooth-unset">Service name</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="right_image">
                                    {/* <p>Saturday</p>
                                    <p>11:32</p> */}
                                </div>
                            </div>
                        </div>
                        
                        <div className="show_rating_container show_rating_container_second">
                            <div className="show_rating_wrapper padding_10 left_div_30">
                                <div className="left_image">
                                    <img src="/img/new_img/guest_list.svg" alt="icon"   />
                                </div>
                                <div className="center_div">
                                    <div className="best_seller_wrapper">
                                        <div className="content">
                                            <p className="heading_text font-smooth-unset">{seller.category.name}</p>
                                            <p className="paragraph_text font-smooth-unset">category</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="right_image">
                                    {/* <p>Saturday</p>
                                    <p>11:32</p> */}
                                </div>
                            </div>
                        </div>

                    </div>
                    <ul className="list-group">
                        {/*<li className="list-group-item admin-approved-div">*/}
                            {/*<div>*/}
                                {/*Approve?*/}
                            {/*</div>*/}
                            {/*<div className={"approval-tick-div btn"} onClick={this.handleApproveSeller.bind(this)}>*/}
                                {/*<img className={"approval-tick"} src="/img/12.png" alt="" />*/}
                            {/*</div>*/}
                        {/*</li>*/}
                        <li className="list-group-item admin-approvals-first-div no-padding">
                            
                            <Carousel>
                                <div id={"dashboard_homeImg"} className="body_portion">
                                    {/* <img src={"/img/body_bac_k.jpg"} className="slider_image_sell" /> */}
                                    <img className="approvals-img slider_image_sell" src={seller.profile.mainImage} alt="" />
                                </div>
                            </Carousel>
                        </li>
                        
                        <li className="list-group-item admin-approvals-first-div no-padding border_20_img">
                            <img className="approvals-img" src={seller.profile.logoImage} alt="" />
                            {/*<video src={seller.profile.video} width="100%"></video>*/}
                            {/* <ReactPlayer
                                url={seller.profile.video}
                                controls={true}
                                width='100%'
                                height='100%'
                            /> */}
                        </li>

                        {seller && seller.profile && seller.profile.moreImages && seller.profile.moreImages.length > 0 && 
                        <li className="list-group-item admin-approvals-first-div no-padding">                            
                            <Carousel>
                                {this.renderOtherImages(seller.profile.moreImages)}
                                <div id={"dashboard_homeImg"} className="body_portion">
                                    <img className="approvals-img slider_image_sell" src={seller.profile.mainImage} alt="" />
                                </div>
                            </Carousel>
                        </li>}

                        {/* <li className="list-group-item admin-approvals-first-div no-padding">
                            <div className={"admin-slideShowScroll"}>
                            <div id="images admin-slideShowScrollDiv">
                                {this.renderOtherImages(seller.profile.moreImages)}
                            </div>
                                <div className="admin-logo-img-div">
                                    <img className={"admin-logo-img"} src={seller.profile.logoImage} alt="" />
                                </div>
                            </div>
                        </li> */}

                        {/* <li className="list-group-item admin-approvals-first-div no-padding background_none">
                            <img className="approvals-img" src="/img/body_bac_k.jpg" alt="" />
                        </li> */}

                        <li className="list-group-item admin-approvals-first-div no-padding">
                            {/* <img className="approvals-img" src="/img/body_bac_k.jpg" alt="" /> */}
                            {/*<video src={seller.profile.video} width="100%"></video>*/}
                            <ReactPlayer
                                url={seller.profile.video}
                                controls={true}
                                width='100%'
                                height='100%'
                            />
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <div className="heading-text">
                                        <h2>STANDARD PACK</h2>
                                    </div>
                                    {/* <h4>Baptzr/Main Courses/Dessert</h4> */}
                                    <p className="first_paragraph"> {seller.profile.standardPack}</p>
                                    {/* <p className="second_paragraph"> {seller.profile.standardPack}</p> */}
                                </div>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <div className="heading-text">
                                        <h2 className={"vendor-genorous-pack"}>Generous PACK</h2>
                                    </div>
                                    {/* <h4>Baptzr/Main Courses/Dessert</h4> */}
                                    <p className="first_paragraph">{seller.profile.generousPack}</p>
                                    {/* <p className="second_paragraph"> {seller.profile.generousPack}</p> */}
                                </div>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <div className="heading-text">
                                        <h2  className={"vendor-exclusive-pack"}>Exclusive PACK</h2>
                                    </div>
                                    {/* <h4>Baptzr/Main Courses/Dessert</h4> */}
                                    <p className="first_paragraph">{seller.profile.exclusivePack}</p>
                                    {/* <p className="second_paragraph"> Welcome drink / chocolate</p> */}
                                    
                                </div>
                            </div>
                        </li>

                        <li className="list-group-item  admin-approvals-list">
                            <div className={"vendor-check-div"}>
                                {/*<input className={"floatLeft"} type="checkbox" id="vendor-service-check" />*/}
                                {/* <div className="vendor-check-box-item">
                                    {this.renderFacilities(seller.profile.facilities)}
                                </div> */}
                                <div className="facility-dv new_service_position">
                                    <ul>
                                        <li className={this.isFacilityProvided("parking")}><img src="/img/newService/PARKING.png" /><br /><span>Parking</span></li>
                                        <li className={this.isFacilityProvided("smoking")}><img src="/img/newService/SMOKING.png" /><br /><span>Smoking</span></li>
                                        <li className={this.isFacilityProvided("elevator")}><img src="/img/newService/ELEVATOR.png" /><br /><span>Elevator</span></li>
                                        <li className={this.isFacilityProvided("wifi")}><img src="/img/newService/WIFI.png" /><br /><span>Wifi</span></li>
                                        <li className={this.isFacilityProvided("bar")}><img src="/img/newService/BAR.png" /><br /><span>Bar</span></li>
                                        <li className={this.isFacilityProvided("kitchen")}><img src="/img/newService/KITCHEN.png" /><br /><span>Kitchen</span></li>
                                        <li className={this.isFacilityProvided("stage")}><img src="/img/newService/STAGE.png" /><br /><span>Stage</span></li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-area">
                                <h2 className={""}>
                                    {seller.profile.area}
                                </h2>
                                <p>AREA (M2)</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-celing_height">
                                <h2 className={""}>
                                    {seller.profile.ceilingHeight}
                                </h2>
                                <p>CEILING HEIGHT (M)</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-guest">
                                <h2 className={""}>
                                    {seller.profile.maxPeople}
                                </h2>
                                <p>MAXIMUM PEOPLE</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-wedding">
                                <h2 className={""}>
                                    {seller.profile.eventType.name}
                                </h2>
                                <p>TYPES OF EVENTS VENUE IS SUITED FOR</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-location">
                                <h2 className={""}>
                                    {seller.profile.country}
                                </h2>
                                <p>COUNTRY</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-location">
                                <h2 className={""}>
                                    {seller.profile.region}
                                </h2>
                                <p>REGION</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-category">
                                <h2 className={""}>
                                    {seller.category.name}
                                </h2>
                                <p>CATEGORY</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-location">
                                <h2 className={""}>
                                    {seller.profile.address}
                                </h2>
                                <p>ADDRESS</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-web">
                                <h2 className={""}>
                                    {seller.profile.website}
                                </h2>
                                <p>WEBSITE</p>
                            </div>
                        </li>

                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className="vendor-SideBarContctDetails admin-mail">
                                <h2 className={""}>
                                    {seller.profile.email}
                                </h2>
                                <p>Mail</p>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar admin-approvals-list dyanamically_center">
                            <div className=" vendor-SideBarContctDetails admin-phone">
                                <h2 className={""}>
                                    {seller.profile.phone}
                                </h2>
                                <p>PhoneNumber</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="col-md-6 col-lg-6 padding-right-0">
                    <div className="content_wrapper">
                        <div className="relative-div news_service_button home-login-button-div" 
                                onClick={this.handleApproveSeller}>
                            <div className="text-style">
                                <img className="service_back_logo-home-first-img" src="/img/newService/TICK.svg" />
                                <div className="back_top">Approve</div>
                            </div>
                        </div>
                        <div className="show_rating_container">
                            <div className="show_rating_wrapper">
                                <div className="left_image">
                                    <img src="/img/new_img/password.svg" alt="icon" />
                                </div>
                                <div className="center_div">
                                    <div className="best_seller_wrapper">
                                        {
                                            this.state.goodDealTag?
                                            <img src="/img/tags_icon/TAGS_GOOD_DEAL.png" onClick={this.handleToggleGoodDealTag} className="margin-top-28" />
                                            :<img src="/img/tags_icon/TAGS_GOOD_DEAL_UNSELECT.svg" onClick={this.handleToggleGoodDealTag} className="margin-top-28" />
                                        }
                                        
                                        {
                                            this.state.hotTag?
                                            <img src="/img/tags_icon/TAGS_HOT.png" onClick={this.handleToggleHotTag} className="margin-top-28" />
                                            :<img src="/img/tags_icon/TAGS_HOT_UNSELECT.svg" onClick={this.handleToggleHotTag} className="margin-top-28" />
                                        }
                                        
                                        {
                                            this.state.newTag?
                                            <img src="/img/tags_icon/TAGS_NEW.png" onClick={this.handleToggleNewTag} className="margin-top-28" />
                                            :<img src="/img/tags_icon/TAGS_NEW_UNSELECT.svg" onClick={this.handleToggleNewTag} className="margin-top-28" />
                                        }
                                    </div>
                                </div>
                                <div className="right_image">
                                    <img src="/img/svg_img/greyEye.png" alt="icon" />
                                </div>
                            </div>
                        </div>
                        <div className="show_rating_container show_rating_container_second">
                            <div className="show_rating_wrapper">
                                <div className="left_image">
                                    <img src="/img/new_img/password.svg" alt="icon"   />
                                </div>
                                <div className="center_div">
                                    <div className="best_seller_wrapper">
                                        {/* <img src="/img/star.png" />
                                        <img src="/img/star.png" />
                                        <img src="/img/star.png" />
                                        <img src="/img/star.png" /> */}
                                        <StarRatingComponent
                                            name="rate1"
                                            starCount={5}
                                            value={rating}
                                            onStarClick={this.onStarClick.bind(this)}
                                        />
                                    </div>
                                </div>
                                <div className="right_image">
                                    <img src="/img/svg_img/greyEye.png" alt="icon" />
                                </div>
                            </div>
                        </div>
                        <div className="edit_admin_wrapper">
                            <div className="adminEditTitleBlock no-padding">
                                <div className="adminEditText adminEditTextDiv">approvals</div>
                            </div>                            
                        </div>
                        
                        {this.rednderUnApprovedSellers()}

                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({"isApprove": false}).fetch();
    return {
        sellers: sellers,
    }
})(ShowSellerDetails);
