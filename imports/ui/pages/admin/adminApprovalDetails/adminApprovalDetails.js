import React from 'react';
import Header from "../../header-username/header";
import AdminLeftEditBar from "../adminLeftEditBar";
import AdminTopBar from "../adminTopbar/adminTopbar";
import Footer from "../../../components/footer/Footer";
import './adminApprovalDetails.scss';
import '../adminApprovals/adminApprovals.scss';
import ShowSellerDetails from './ShowSellerDetails';

class AdminApprovalDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            seller: '',
            sellerId: ''
        }
        this.parentCallback = this.parentCallback.bind(this);
    }

    componentDidMount() {
        const self = this;
        const sellerId = this.props.sellerId;
        if (sellerId) {
            Meteor.call('seller.findOne', sellerId, function (err, result) {
                if(err) {
                    console.log(err);
                } else {
                    self.setState({seller: result});
                }
            })    
        }
    }

    parentCallback(options) {
        let sellerId = options.sellerId;
        let self = this;
        if (sellerId != this.state.sellerId) {
            Meteor.call('seller.findOne', sellerId, function (err, result) {
                if(err) {
                    console.log(err);
                } else {
                    self.setState({seller: result});
                }
            })
        }
    }

    render() {
        const seller = this.state.seller;
        const sellerId = this.props.sellerId;

        if(!seller) {
            return (
                <div></div>
            )
        }
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <AdminTopBar sellerId={sellerId} seller={seller}/>
                    <div className="clearfix"></div>
                    {/*<div className={"row vendor-search-division"}>*/}
                        {/*<div className={"col-md-12"}>*/}
                            {/*<div className={"col-md-4"}>*/}
                                {/*<div className={"col-md-1 pagename-text edit-text-right-align"}>*/}
                                    {/*Edit*/}
                                {/*</div>*/}
                                {/*<div className={"col-md-3"}>*/}
                                    {/*<hr className={"admin-edit-hr"} />*/}
                                {/*</div>*/}
                            {/*</div>*/}

                            {/*<div className={"col-md-1 pagename-text"}>*/}
                                {/*approvals*/}
                            {/*</div>*/}
                            {/*<div className={"col-md-7"}>*/}
                                {/*<hr />*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}

                    <div className="clearfix"></div>
                    <div className="body_container">
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 adminEditTitleBlockPadding">
                            <div className={"adminEditTitleBlock"}>
                                <div className={"adminEditText adminEditTextDiv"}>
                                    EDIT
                                </div>
                                {/* <div className={"adminEditHr"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <div className="responsive_btn_v">
                                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>
                            <div className="clearfix"></div>
                            <AdminLeftEditBar />
                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12  adminApprovalDetaisPage adminApprovalHrTitleBlockPadding">
                            <div className={"adminApprovalHrTitleBlock"}>
                                <div className={"adminApprovalText adminApprovalTextDiv"}>
                                    approvals
                                </div>
                                {/* <div className={"adminApprovalHr admin_approval_details_hr col-xs-9 col-md-6"}>
                                    <img className={"svg-img-style"} src={"/img/line.png"} />
                                </div> */}
                            </div>
                            <ShowSellerDetails parentCallback={this.parentCallback} seller={seller}/>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default AdminApprovalDetails;

