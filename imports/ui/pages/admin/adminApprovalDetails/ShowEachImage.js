import React from 'react';

class ShowEachImage extends React.Component {
    render() {
        return (
            <div id={"dashboard_homeImg"} className="body_portion">
            <img src={this.props.image} className={"approvals-img slider_image_sell"} alt={"Images"} />
            </div>            
        )
    }
}

export default ShowEachImage;