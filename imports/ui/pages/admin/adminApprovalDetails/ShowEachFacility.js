import React from 'react';

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage: '',
        }
    }

    componentDidMount() {
        const facility = this.props.facility;
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    self.setState({facilityImage: result.image});
                }
            }
        });
    }

    render () {
        // console.log(this.props);
        const facility = this.props.facility;
        const facilityImage = this.state.facilityImage;
        const FacilitymainImage = facilityImage? facilityImage: "/img/parking.png";

        const FacilitydivStyle = {
            backgroundImage: 'url(' + FacilitymainImage + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain',
            backgroundPosition: '50% 0%',
        };

        return (
            <div className="vendor-service-first-div">
                <div style={FacilitydivStyle}>
                    {facility.name}
                </div>
            </div>
        )
    }
}

export default ShowEachFacility;