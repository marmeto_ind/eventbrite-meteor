// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import EventCreate from '../../../components/event/admin/EventCreate';
//
// class EventAddPage extends React.Component {
//     render() {
//         return (
//             <div>
//                 <Header/>
//                 <EventCreate/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventAddPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventCreate from '../../../components/event/admin/EventCreate';

class EventAddPage extends React.Component {
    render() {
        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><EventCreate/></div>
            </div>
        </div>
        </div>
        )
    }
}

export default EventAddPage;