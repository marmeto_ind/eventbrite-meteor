// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import EventUpdate from '../../../components/event/admin/EventUpdate'
//
// class EventUpdatePage extends React.Component {
//     render() {
//         const eventId = this.props.params.id;
//         // console.log(regionId);
//
//         return (
//             <div>
//                 <Header/>
//                 <EventUpdate eventId={eventId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventUpdatePage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventList from '../../../components/event/admin/EventList';
import EventUpdate from '../../../components/event/admin/EventUpdate';

class EventUpdatePage extends React.Component {
    render() {
        const eventId = this.props.params.id;
        return (
            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
            <div className={"admin-body"}>
                <div className={"row "}>
                    <div className={"col-md-3"}><LeftSidebar /></div>
                    <div className={"col-md-9"}><EventUpdate eventId={eventId}/></div>
                </div>
            </div>
            </div>
        )
    }
}

export default EventUpdatePage;