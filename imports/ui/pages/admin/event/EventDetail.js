// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import EventDetail from '../../../components/event/admin/EventDetail';
//
// class EventDetailPage extends React.Component {
//     render() {
//         // console.log(this.props.params.id)
//
//         const eventId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <EventDetail eventId={eventId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventDetailPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventDetail from '../../../components/event/admin/EventDetail';

class EventDetailPage extends React.Component {
    render() {
        const eventId = this.props.params.id;

        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
            <div className={"admin-body"}>
                <div className={"row "}>
                    <div className={"col-md-3"}><LeftSidebar /></div>
                    <div className={"col-md-9"}><EventDetail eventId={eventId}/></div>
                </div>
            </div>
            </div>
        )
    }
}

export default EventDetailPage;