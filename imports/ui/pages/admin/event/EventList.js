// import React from 'react';
// import EventList from '../../../components/event/admin/EventList';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
//
// class SellerListPage extends React.Component {
//     render() {
//         return (
//             <div className={"seller-list-page"}>
//                 <Header/>
//                 <EventList/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default SellerListPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventList from '../../../components/event/admin/EventList';

class EventListPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
            <div className={"admin-body"}>
                <div className={"row "}>
                    <div className={"col-md-3"}><LeftSidebar /></div>
                    <div className={"col-md-9"}><EventList/></div>
                </div>
            </div>
            </div>
        )
    }
}

export default EventListPage;