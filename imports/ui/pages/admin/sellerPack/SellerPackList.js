import React from 'react';
import Header from '../../../components/header/Header';
import Footer from '../../../components/footer/Footer';
import SellerPackList from '../../../components/sellerPack/admin/SellerPackList'

class SellerListPage extends React.Component {
    render() {
        return (
            <div className={"seller-list-page"}>
                <Header/>
                <SellerPackList/>
                <Footer/>
            </div>
        )
    }
}

export default SellerListPage;