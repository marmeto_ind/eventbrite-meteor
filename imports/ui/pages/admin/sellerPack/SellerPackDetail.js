import React from 'react';
import Header from '../../../components/header/Header';
import Footer from '../../../components/footer/Footer';
import SellerFacilityDetail from '../../../components/sellerFacility/admin/SellerFacilityDetail';

class SellerFacilityDetailPage extends React.Component {
    render() {
        // console.log(this.props.params.id)

        const sellerFacilityId = this.props.params.id;

        return (
            <div>
                <Header/>
                <SellerFacilityDetail sellerFacilityId={sellerFacilityId}/>
                <Footer/>
            </div>
        )
    }
}

export default SellerFacilityDetailPage;
