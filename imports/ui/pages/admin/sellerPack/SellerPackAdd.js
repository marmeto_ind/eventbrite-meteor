// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import RegionCreate from '../../../components/region/admin/RegionCreate';
//
// class RegionAddPage extends React.Component {
//     render() {
//         return (
//             <div>
//                 <Header/>
//                 <RegionCreate/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default RegionAddPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import SellerFacilityCreate from '../../../components/sellerFacility/admin/SellerFacilityCreate';

class SellerFacilityAddPage extends React.Component {
    render() {
        return (
            <div>
                <LeftSidebar />
                <div className={"admin-body"}>
                    <Header/>
                    <SellerFacilityCreate/>
                </div>
            </div>
        )
    }
}

export default SellerFacilityAddPage;