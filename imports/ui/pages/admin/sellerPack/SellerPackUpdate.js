import React from 'react';
import Header from '../../../components/header/Header';
import Footer from '../../../components/footer/Footer';
import RegionUpdate from '../../../components/region/admin/RegionUpdate'

class RegionUpdatePage extends React.Component {
    render() {
        const regionId = this.props.params.id;
        // console.log(regionId);

        return (
            <div>
                <Header/>
                <RegionUpdate regionId={regionId}/>
                <Footer/>
            </div>
        )
    }
}

export default RegionUpdatePage;