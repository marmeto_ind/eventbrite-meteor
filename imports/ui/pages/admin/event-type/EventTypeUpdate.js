// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import EventTypeUpdate from '../../../components/eventType/admin/EventTypeUpdate';
//
// class EventTypeUpdatePage extends React.Component {
//     render() {
//
//         const eventTypeId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <EventTypeUpdate eventTypeId={eventTypeId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventTypeUpdatePage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventTypeUpdate from '../../../components/eventType/admin/EventTypeUpdate';

class EventTypeUpdatePage extends React.Component {
    render() {
        const eventTypeId = this.props.params.id;
        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><EventTypeUpdate eventTypeId={eventTypeId}/></div>
            </div>
        </div>
        </div>
        )
    }
}

export default EventTypeUpdatePage;