// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import EventTypeCreate from '../../../components/eventType/admin/EventTypeCreate';
//
// class EventTypeAddPage extends React.Component {
//     render() {
//         return (
//             <div>
//                 <Header/>
//                 <EventTypeCreate/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventTypeAddPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventTypeCreate from '../../../components/eventType/admin/EventTypeCreate';

class EventTypeAddPage extends React.Component {
    render() {
        return (

            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
        <div className={"admin-body"}>
            <div className={"row "}>
                <div className={"col-md-3"}><LeftSidebar /></div>
                <div className={"col-md-9"}><EventTypeCreate/></div>
            </div>
        </div>
        </div>
        )
    }
}

export default EventTypeAddPage;