// import React from 'react';
// import EventTypeList from '../../../components/eventType/admin/EventTypeList';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
//
// class EventTypeListPage extends React.Component {
//     render() {
//         return (
//             <div className={"seller-list-page"}>
//                 <Header/>
//                 <EventTypeList/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default EventTypeListPage;

import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import EventTypeList from '../../../components/eventType/admin/EventTypeList';

class EventTypeListPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><EventTypeList/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default EventTypeListPage;