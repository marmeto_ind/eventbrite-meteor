import React from 'react';
import CategoryList from '../../../components/category/admin/CategoryList';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar'

class CategoryListPage extends React.Component {
    render() {
        return (

            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><CategoryList/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CategoryListPage;