// import React from 'react';
// import Header from '../../../components/header/Header';
// import Footer from '../../../components/footer/Footer';
// import CategoryUpdate from '../../../components/category/admin/CategoryUpdate';
//
// class CategoryUpdatePage extends React.Component {
//     render() {
//         // console.log(this.props.params.id)
//
//         const categoryId = this.props.params.id;
//
//         return (
//             <div>
//                 <Header/>
//                 <CategoryUpdate categoryId={categoryId}/>
//                 <Footer/>
//             </div>
//         )
//     }
// }
//
// export default CategoryUpdatePage;


import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar';
import CategoryUpdate from '../../../components/category/admin/CategoryUpdate';

class CategoryUpdatePage extends React.Component {
    render() {
        const categoryId = this.props.params.id;

        return (
            <div className={"admin-page"}>
            <div className={"admin-header"}><Header/></div>
            <div className={"admin-body"}>
                <div className={"row "}>
                    <div className={"col-md-3"}><LeftSidebar /></div>
                    <div className={"col-md-9"}><CategoryUpdate categoryId={categoryId}/></div>
                </div>
            </div>
            </div>
        )
    }
}

export default CategoryUpdatePage;