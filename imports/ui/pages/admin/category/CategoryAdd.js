import React from 'react';
import Header from '../../../components/admin/Header';
import LeftSidebar from '../../../components/admin/LeftSidebar'
import CategoryCreate from '../../../components/category/admin/CategoryCreate';

class CategoryAddPage extends React.Component {
    render() {
        return (
            <div className={"admin-page"}>
                <div className={"admin-header"}><Header/></div>
                <div className={"admin-body"}>
                    <div className={"row "}>
                        <div className={"col-md-3"}><LeftSidebar /></div>
                        <div className={"col-md-9"}><CategoryCreate/></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CategoryAddPage;