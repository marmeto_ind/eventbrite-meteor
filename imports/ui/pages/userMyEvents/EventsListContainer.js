import React from 'react';
import ReactPaginate from 'react-paginate';
import EventsList from './EventsList';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';

class EventsListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: '',
        }
    }

    handlePageClick(data) {
        console.log(data);
        this.setState({currentPage: data.selected})
    }

    render() {
        const numOfPage = this.props.numOfPage;
        const currentPage = this.state.currentPage;

        return (
            <div  className={"sectionblock1"}>
                <div className={"userTitleUpcomingEventBlock"}>
                    <div className={"userTitleUpcomingEventBlocktextHeading"}>
                        UpComing
                    </div>
                    {/* <div className={"userTitleUpcomingEventBlockImg"}>
                        <img className={"svg-img-style"} src={"/img/line.png"} />
                    </div> */}
                </div>
                {/*<EventsList curentPage={currentPage} />*/}
                <EventsList />
                {/*{numOfPage>0*/}
                    {/*?*/}
                    {/*<ReactPaginate*/}
                        {/*previousLabel={"prev"}*/}
                        {/*nextLabel={"next"}*/}
                        {/*breakLabel={<a href="">...</a>}*/}
                        {/*breakClassName={"break-me"}*/}
                        {/*pageCount={numOfPage}*/}
                        {/*marginPagesDisplayed={2}*/}
                        {/*pageRangeDisplayed={4}*/}
                        {/*onPageChange={this.handlePageClick.bind(this)}*/}
                        {/*containerClassName={"pagination"}*/}
                        {/*subContainerClassName={"pages pagination"}*/}
                        {/*activeClassName={"active"}*/}
                    {/*/>*/}
                    {/*:*/}
                    {/*<div></div>*/}
                {/*}*/}
            </div>
        )
    }
}

// export default EventsListContainer;

export default withTracker(() => {
    Meteor.subscribe('events.all');
    const userId = Meteor.userId();
    const numOfEvent = Events.find({"user" : userId}).count();
    const numOfPage = Math.floor((numOfEvent -1) / 4 + 1);
    
    return {
        numOfPage: numOfPage,
    }
})(EventsListContainer);

