import React from 'react';
import ShowEachEvent from './ShowEachEvent';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Events} from "../../../api/events";
import ShowEachPastEvent from './ShowEachPastEvent';

class EventsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            upcomingEvents : [],
            pastEvents: [],
        }
    }

    componentWillReceiveProps(nextProps) {
        const userEvents = nextProps.userEvents;
        const self = this;

        if(userEvents) {
            userEvents.map((event) => {
                const eventDate = new Date(event.date);
                const today = new Date();

                if(today.getTime() < (eventDate.getTime() + 24 * 60 * 60 * 1000)) {
                    let upcomingEvents = self.state.upcomingEvents;
                    upcomingEvents.push(event);
                    self.setState(event)
                } else  {
                    let pastEvents = self.state.pastEvents;
                    pastEvents.push(event);
                    self.setState({pastEvents: pastEvents});
                }
            })

        }
    }

    renderUpcomingUserEvents = (events) => {
        return events.map((event, index) => (
            <ShowEachEvent key={index} event={event}/>
        ))
    };

    renderPastUserEvents = (events) => {
        return events.map((event, index) => (
            <ShowEachPastEvent key={index} event={event} />
        ))
    };

    render() {
        const userEvents = this.props.userEvents;

        return (
            <div className="padding_top_9">
                {this.state.upcomingEvents.length === 0? <div className={"no-upcomingEvents-style "}>No Upcoming Events</div>: ''}

                {this.renderUpcomingUserEvents(this.state.upcomingEvents)}

                {/*<div className={"col-md-1 upcomingtext upcoming-position"}>*/}
                    {/*Past*/}
                {/*</div>*/}
                {/*<div className={"col-md-3 upcoming-hr"}>*/}
                    {/*/!*<hr className={"past_hr "} />*!/*/}
                    {/*<img className={"home_line_svg"} src={"/img/line.svg"} />*/}
                {/*</div>*/}
                <div className={"userTitlePastEventBlock"}>
                    <div className={"userTitlePastEventBlocktext"}>
                        Past
                    </div>
                    <div className={"userTitlePastEventBlockImg"}>
                        <img className={"svg-img-style"} src={"/img/line.png"} />
                    </div>
                </div>

                {this.state.pastEvents.length === 0? <div className={"no-pastEvent-booked-style"}>No Past Events</div>: ''}

                {this.renderPastUserEvents(this.state.pastEvents)}
            </div>

        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('events');
    const userId = Meteor.userId();
    const userEvents = Events.find({"user" : userId}, {sort: { createdAt: -1 }}).fetch();

    return {
        userEvents: userEvents,
    };
})(EventsList);
