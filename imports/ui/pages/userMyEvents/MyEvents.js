import React from 'react';
import Header from '../header-username/header';
import './my-events.scss';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/userTopBar/TopBar';
import LeftSideBar from '../../components/userLeftSideBar/LeftSideBar';
import ShowEachEvent from './ShowEachEvent';
import { browserHistory } from 'react-router';
import EventListContainer from './EventsListContainer';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import RegionSelect from '../../components/body/RegionSelect';

class MyEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
            guestNumber: ''
        };
        this._callbackFromRegionSelect = this._callbackFromRegionSelect.bind(this)
    }

    _handleSearchKeyChange = (event) => {
        let searchKey = event.target.value;
        this.setState({searchKey: searchKey});
    };

    renderUserEvents = (events) => {
        return events.map((event, index) => (
            <ShowEachEvent key={index} event={event}/>
        ))
    };

    _handleNewEvent = () => {
        browserHistory.push("/home");
    };

    handleInputChange(event) {
        const value = event.target.value;
        if(value < 999999) {
            this.setState({guestNumber: event.target.value})
        }
    }

    parentCallBack(option) {
        let {searchKey } = option;
        this.setState({searchKey})
    }

    _handleEventCreate(event){
        const user = Meteor.userId();
        const eventName = $('#eventName').val();
        const region = this.state.region;
        const date = this.state.date.toString();
        const eventType = this.state.eventType;
        const numberOfGuest = $('#numOfGuest').val();

        console.log({user, eventName, region, date, eventType, numberOfGuest})
        
        if(!user) {
            browserHistory.push("/login");
        } else if (!eventName) {
            this.errorNotify("Event Name cannot be empty");
        } else if (!region) {
            this.errorNotify("Region is not selected");
        } else if (!numberOfGuest) {
            this.errorNotify("Number of guest cannot be empty");
        } else if(date.length <= 4) {
            this.errorNotify("Date can not be empty");
        } else {
            Meteor.call('events.insert', eventName, region, eventType, date, numberOfGuest, user, function (error, result) {
                if(error) {
                    console.log(error)
                } else {
                    console.log(result)
                    location.reload();
                }
            });
            
        }
    }

    _selectDate(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;
        if(day !== currentDate) {
            this.setState({date: day});
        }
    }

    _callbackFromRegionSelect(data) {
        this.setState({region: data.label});
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>
                    <div className="clearfix"></div>

                    <div className="clearfix"></div>
                    <div className="body_container">
                        <LeftSideBar 
                            searchKey={this.state.searchKey}
                            parentCallBack={this.parentCallBack.bind(this)}
                        />
                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12 userMyEventsBlockPosition">
                            <div className={"row"}>
                                <div className={"col-md-12 display_flex leftPading userMyEvent_position"}>
                                <EventListContainer/>
                                <div className={"sectionblock2"}>
                                    <div className={"userTitleUpcomingEventBlock"}>
                                        <div className={"userTitleUpcomingEventBlocktextHeading"}>
                                            CREATE
                                        </div>
                                    </div>
                                    <ul className="padding_top_9">
                                        <li className="list-group-item list-right-padding-style user-addEventBlk">
                                            <div className={"vendorStatsList"}>
                                                <div className={"vendorStatsFloatLeft"}>
                                                    <span>
                                                        <img className={"list-img"} src="/img/notes.svg" alt="" />
                                                    </span>
                                                </div>
                                                <div className={"vendorStatsTextCenter"}>
                                                    <form>
                                                        <input id={"eventName"} className="dashboard-search placeholder-fix  dashboard-search-input-box" 
                                                        type="text" placeholder="COUPLE IN LOVE" name="event_name" />
                                                    </form>
                                                    <p className={"vendor-stats-p"}>NAME</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="list-group-item list-right-padding-style user-addEventBlk">
                                            <div className={"vendorStatsList"}>
                                                <div className={"vendorStatsFloatLeft"}>
                                                    <span>
                                                        <img className={"list-img"} src="/img/date_topBar.png" alt="" />
                                                    </span>
                                                </div>
                                                <div className={"vendorStatsTextCenter"}>
                                                    <DayPickerInput
                                                        onDayChange={day=> this._selectDate(day)}
                                                        value={this.state.date}
                                                        dayPickerProps={{ disabledDays: {before: new Date()} }}
                                                    />
                                                    <p className={"vendor-stats-p"}>DATE</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="list-group-item list-right-padding-style user-addEventBlk">
                                            <div className={"vendorStatsList"}>
                                                <div className={"vendorStatsFloatLeft"}>
                                                    <span>
                                                        <img className={"list-img"} src="/img/region_topBar.png" alt="" />
                                                    </span>
                                                </div>
                                                <div className={"vendorStatsTextCenter"}>
                                                    <RegionSelect callbackFromParent={this._callbackFromRegionSelect}/>
                                                    <p className={"vendor-stats-p"}>REGION</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="list-group-item list-right-padding-style user-addEventBlk">
                                            <div className={"vendorStatsList"}>
                                                <div className={"vendorStatsFloatLeft"}>
                                                    <span>
                                                        <img className={"list-img"} src="/img/guset_topBar.png" alt="" />
                                                    </span>
                                                </div>
                                                <div className={"vendorStatsTextCenter"}>
                                                    <input
                                                        id={"numOfGuest"}
                                                        className="dashboard-guest guest-placeholder-fix"
                                                        type="number"
                                                        placeholder="HOW MANY?"
                                                        name="num_of_guest"
                                                        value={this.state.guestNumber}
                                                        onChange={this.handleInputChange.bind(this)}
                                                    />
                                                    <p className={"vendor-stats-p"}>GUEST</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="new-event-button_wrapper relativeposition">
                                        <div className="relative-div news_service_button  home-login-button-div">
                                            <div className="text-style" onClick={this._handleEventCreate.bind(this)}>
                                                <img className="service_back_logo-home-first-img" src="/img/newService/TICK.svg" />
                                                <div className="back_top">CREATE</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default MyEvents;