import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import ReviewsComponent from './reviewsComponent';

class ShowEachEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerName: '',
            eventTypeImage: '',
        }
    }

    componentDidMount() {
        const self = this;
        const event = this.props.event;
        const eventId = event._id;
        Meteor.call('events.getSellerName', eventId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                self.setState({sellerName: result});
            }
        });

        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    self.setState({eventTypeImage: result});
                }
            }
        })
    }

    render() {
        const event = this.props.event;

        const divMargin = {
            "margin": "10px"
        };

        return (
            <div className={"second-div "}>
                <ul className="list-group upcoming-list-background upcoming-event-list adminApprovalDetailsList">

                    <li className="list-group-item vendor-leftSideBar admin-approvals-list margin_bottom_0">
                        <div className={"vendorleftSide-text-div"}>
                            <div className="left_image">
                                <img src="/img/date_topBar.png" />
                            </div>
                            <div>
                                <div className="heading-text">
                                <h2 className={"vendor-genorous-pack"}>{event? event.name: 'Event Name'}</h2>
                                
                            </div>
                            <h4>{event? event.date.substr(0, 15): 'Event Date'}</h4>
                            <p className="first_paragraph">{this.state.sellerName} 
                            {/* Panoramaevent Center */}
                            </p>
                            <p className="second_paragraph">({event? event.numOfGuest: "Number of Guest"})</p>
                        </div>
                        </div>
                    </li> 
                    {/* <li className="list-group-item div-left-icon  ">
                        <img
                            className="create-add all-div-icon"
                            src={this.state.eventTypeImage? this.state.eventTypeImage: "/img/statue .png"}
                            alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style  event-img">
                        {event? event.name: 'Event Name'}
                    </li>
                    <li className="list-group-item text-list-div-style event-list1 ">
                        <hr className={"hr-width"}/>
                    </li>
                    <li className="list-group-item text-list-div-style  event-list2 ">
                        {event? event.date.substr(0, 15): 'Event Date'}
                    </li>
                    <li className="list-group-item text-list-div-style  event-list3">
                        {this.state.sellerName}
                    </li>
                    <li className="list-group-item text-list-div-style  event-list4">
                        ({event? event.numOfGuest: "Number of Guest"})
                    </li> */}                    
                </ul>
            </div>
        )
    }
}

export default ShowEachEvent;