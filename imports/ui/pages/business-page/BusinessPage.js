import React from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import DashBord from '../../components/body/Business';

class BusinessPage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <DashBord />
                <Footer/>
            </div>
        )
    }
}

export default BusinessPage;