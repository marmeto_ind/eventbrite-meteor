import React from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import UserServiceBody from '../../components/body/UserServiceBody';

class UserService extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <UserServiceBody />
                <Footer/>
            </div>
        )
    }
}

export default UserService;