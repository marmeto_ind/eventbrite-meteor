import React from 'react';
import  './homeHeader.scss';
import '../homePage/allpopup.scss';
import {browserHistory} from 'react-router';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDeletePopup: false,
        };
        this.handleLogout = this.handleLogout.bind(this);
        this.handle_contact_us = this.handle_contact_us.bind(this);
        this.handle_invite_friends = this.handle_invite_friends.bind(this);
        this.handleDeleteAccountClick = this.handleDeleteAccountClick.bind(this);
        this.handleContactUsSubmitClick = this.handleContactUsSubmitClick.bind(this);
        this.handleInviteFriendSubmitClick = this.handleInviteFriendSubmitClick.bind(this);
    }

    // logOutpopUpFunction() {
    //     var settingspopUp = document.getElementById("UserSettingpopUp");
    //     settingspopUp.classList.remove("show");
    //     var logOutpopUp = document.getElementById("mylogOutpopUp");
    //     logOutpopUp.classList.toggle("show");
    // };

    settingspopUpFunction() {
        // var logOutpopUp = document.getElementById("mylogOutpopUp");
        // logOutpopUp.classList.remove("show");
        var settingspopUp = document.getElementById("UserSettingpopUp");
        settingspopUp.classList.toggle("show");
    };

    handleLogout(e) {
        e.preventDefault();
        Meteor.logout();
        browserHistory.push('/login');
        window.location.reload();
    }
    
    componentDidMount() {
        $('body').click(function(){
            var settingspopUp = document.getElementById("UserSettingpopUp");
            settingspopUp.classList.remove("show");
            // var logOutpopUp = document.getElementById("mylogOutpopUp");
            // logOutpopUp.classList.remove("show");
        });
    }

    handle_invite_friends(e) {
        $("#invite_friends_create_popup").show();
        $('body').css('overflow', 'hidden');
    
        $("#invite_friends_create_popup_ok").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
        $(".deleteMeetingClose").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
    };

    handle_contact_us(e) {
        // $("#contact_us_create_popup").show();
        // $('body').css('overflow', 'hidden');

        // $("#contact_us_create_popup_ok").click(function (e) {
        //     $("#contact_us_create_popup").hide();
        //     e.preventDefault();
        //     $('body').css('overflow', 'auto');
        // });
        // $(".deleteMeetingClose").click(function (e) {
        //     $("#contact_us_create_popup").hide();
        //     e.preventDefault();
        //     $('body').css('overflow', 'auto');
        // });
        browserHistory.push("/user/chat");
        // window.location.reload();
    };

    handleDeleteAccountClick() {
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    handleDeleteYes(){
        this.removeUser();
        this.setState({showDeletePopup: !this.state.showDeletePopup})
        window.location.reload();
    }
    
    handleDeleteNo(){
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }


    handleInviteFriendSubmitClick() {
        let invite_mail = $("#invite_mail").val();
        let user = Meteor.user();
        let userName = user && user.profile.name;
        let email_msg = `${userName} invited you to www.wendely.com`;
        
        Meteor.call(
            'sendEmail',
            invite_mail,
            'nipor87@hotmail.com',
            'Invite to www.wendely.com',
            email_msg
        );
    }

    handleContactUsSubmitClick() {
        let query = $("#contact_us").val();
        let user = Meteor.user();
        let email_msg = `email: ${ user && user.emails[0] && user.emails[0].address}, query: ${query}`;
        let customer_email = user && user.emails[0] && user.emails[0].address;
        Meteor.call(
            'sendEmail',
            'nipor87@hotmail.com',
            customer_email,
            'Customer Query',
            email_msg
        );
    }

    render() {
        let user = Meteor.user();

        return(
            <header className="home-header">
                <div className={" header-container"}>
                    <div className={"header-div"}>
                        <div  id={"mobile_home_logo"} className={"pull-left col-md-8"}>
                            <div className={"row"}>

                                <div>
                                    <a href="/home" className={"floatleft"} >
                                        <img className="header-logo" src="/img/logo_new.png" alt="" />
                                    </a>
                                    <h1 className="header-text">WENDELY</h1>
                                </div>
                            </div>
                        </div>

                        <div className={"pull-right col-md-4 no-padding"}>
                            <nav className="home-header-nav home-header-nav-new">
                                <ul className={"floatright"}>
                                    <li className="username">
                                        <span className={"username-dot"}>.</span>
                                        <span className="username-link">
                                            {user.profile.name}
                                        </span>
                                    </li>
                                    <li className="home-header-nav-item">
                                        <a className="homeheaderNavlink"><img src="/img/user_pic.svg" alt="" /></a>
                                            {/*/!*UserLogOutPopup*!/*/}
                                            {/*<div className="logOutpopUptext" id="mylogOutpopUp">*/}
                                                {/*<ul className="list-group  home-logInOutList">*/}
                                                    {/*<li className="list-group-item d-flex justify-content-between align-items-center"*/}
                                                    {/*onClick={this.handleLogout.bind(this)}*/}
                                                    {/*>*/}
                                                       {/*logOut*/}
                                                        {/*<span className="">*/}
                                                            {/*<img className={"logoutImage"} src={"/img/white-logout.png"} alt={"logIn"}/>*/}
                                                        {/*</span>*/}
                                                    {/*</li>*/}
                                                {/*</ul>*/}
                                            {/*</div>*/}

                                    </li>

                                    <li className="home-header-nav-item">
                                        <a onClick={this.settingspopUpFunction} title="Account setting" href="#" className="homeheaderNavlink settings-img setting-hrefBox UserSettingpopUp">
                                            <img className={"home-header-settingsimg"}  src="/img/alternative.svg" alt="" />

                                            {/*UserSettingsPopup*/}
                                            <div className="UserSettingspopUptext UserSettingspopUptext_update" id="UserSettingpopUp">
                                                {/* <ul className="list-group  home-logInOutList">
                                                    <li className="list-group-item d-flex justify-content-between align-items-center settings-list_popUp"
                                                        >
                                                        <a className="settings-text" href={"/user/chat"}>
                                                       My Chat
                                                    </a>
                                                        <span className="settingstextdiv1">
                                                            <img className={"logoutImage"} src={"/img/chat-notification.png"} alt={"logIn"}/>
                                                        </span>
                                                    </li>

                                                    <li className="list-group-item d-flex justify-content-between align-items-center settings-list_popUp"
                                                        >
                                                        <a className="settings-text" href={"/user/my-events"}>
                                                        My Events
                                                        </a>
                                                        <span className="settingstextdiv2">
                                                            <img className={"logoutImage"} src={"/img/notepad.png"} alt={"logIn"}/>
                                                        </span>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-between align-items-center settings-list_popUp"
                                                       >
                                                        <a className="settings-text" href={"/user/guest-list"}>
                                                        My GuestList
                                                        </a>
                                                        <span className="settingstextdiv3">
                                                            <img className={"logoutImage"} src={"/img/pencil-edit-button.png"} alt={"logIn"}/>
                                                        </span>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-between align-items-center settings-list_popUp"
                                                        onClick={this.handleLogout.bind(this)}
                                                    >
                                                        <a className="settings-text" href="">
                                                        LOG OUT
                                                        </a>
                                                        <span className="settingstextdiv4">
                                                            <img className={"logoutImage"} src={"/img/logout.png"} alt={"logIn"}/>
                                                        </span>
                                                    </li>
                                                </ul> */}
                                                <ul className="list-group  home-logInOutList">
                                                    <li className="list-group-item d-flex service_name justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image"><img src="/img/service_name_icn.svg" /></div>
                                                        <div className="content">
                                                            <p className="heading_text">{user && user.profile.name}</p>
                                                            <p className="paragraph_text">name</p>
                                                        </div>
                                                        <div className="right_image">&nbsp;</div>
                                                    </li>
                                                    <li className="list-group-item d-flex service_email justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image"><img src="/img/mail_sub_menu.svg" /></div>
                                                        <div className="content">
                                                            <p className="heading_text">{user && user.emails[0] && user.emails[0].address}</p>
                                                            <p className="paragraph_text">mail</p>
                                                        </div>
                                                        <div className="right_image"><img src="/img/new_img/eye_line.svg" /></div>
                                                    </li>
                                                    <li className="list-group-item d-flex service_contact_no justify-content-between align-items-center settings-list_popUp">
                                                    <div className="left_image"><img src="/img/phone_icon.svg" /></div>
                                                        <div className="content">
                                                            <p className="heading_text">{user.profile.phoneNumber}</p>
                                                            <p className="paragraph_text">phone no</p>
                                                        </div>
                                                        <div className="right_image"><img src="/img/new_img/eye_line.svg" /></div>
                                                    </li>
                                                    <li className="list-group-item d-flex service_language justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image">
                                                            <img src="/img/language_sub_menu.svg" />
                                                        </div>
                                                        <div className="content">
                                                            <p className="heading_text">English</p>
                                                            <p className="paragraph_text">Language</p>
                                                        </div>
                                                        <div className="right_image">&nbsp;</div>
                                                    </li>

                                                    <li className="list-group-item d-flex service_password justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image"><img src="/img/PASSWORD.svg" /></div>
                                                        <div className="content">
                                                            <input type="password" className="submenu_passshow" placeholder="********" />
                                                            <p className="paragraph_text">password</p>
                                                            <img src="/img/new_img/eye_line.svg" className="eye_icon" />
                                                        </div>
                                                        <div className="right_image"><img src="/img/CHANGE_PASSWORD.svg" /></div>
                                                    </li>

                                                    <li className="list-group-item d-flex service_friends justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image"><img src="/img/INVITE.svg" /></div>
                                                        <div className="content">
                                                            <button onClick={this.handle_invite_friends}>Invite friends</button>
                                                        </div>
                                                        <div className="right_image">&nbsp;</div>
                                                    </li>

                                                    <li className="list-group-item d-flex service_contact_us justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image"><img src="/img/SUPPORT.svg" /></div>
                                                        <div className="content">
                                                            <button onClick={this.handle_contact_us}>Contact us</button>
                                                        </div>
                                                        <div className="right_image">&nbsp;</div>
                                                    </li>

                                                    <li className="list-group-item d-flex delete_account justify-content-between align-items-center settings-list_popUp">                                                        
                                                        <div className="left_image"><img src="/img/Delete_account.svg" /></div>
                                                        <div className="content">
                                                            <button onClick={this.handleDeleteAccountClick}>delete account</button>
                                                        </div>
                                                        <div className="right_image">&nbsp;</div>
                                                    </li>

                                                    <li className="list-group-item d-flex service_logout justify-content-between align-items-center settings-list_popUp">
                                                        <div className="left_image">&nbsp;</div>
                                                        <div className="content">
                                                            <button onClick={this.handleLogout.bind(this)}>sign out</button>
                                                        </div>
                                                        <div className="right_image"><img src="/img/logout_icon.svg" /></div>
                                                    </li>

                                                </ul>
                                                
                                                {/* Start invite friends pop up section */}
                                                <div id={"invite_friends_create_popup"}>
                                                    <div className={"inner_div"}>
                                                        <span className="deleteMeetingClose">&times;</span>
                                                        <p className={"invite"}>Invite friends</p>
                                                        <div className="invite_email">
                                                            <input type={"email"} id={"invite_mail"} placeholder="ENTER EMAIL"/>
                                                            <p className={"subtext_P"}>Mail</p>
                                                        </div>
                                                        <button id={"invite_friends_create_popup_ok"} onClick={this.handleInviteFriendSubmitClick}>Invite</button>
                                                    </div>
                                                </div>
                                                {/* End invite friends pop up section */}


                                                {/* Start contact us pop up section */}
                                                <div id={"contact_us_create_popup"}>
                                                    <div className={"inner_div"}>
                                                        <span className="deleteMeetingClose">&times;</span>
                                                        <p className={"invite"}>Contact Us</p>
                                                        <div className="invite_email">
                                                            <textarea name="contact_us" id="contact_us" cols="30" rows="5"></textarea>
                                                            {/* <p className={"subtext_P"}>Enter Query</p> */}
                                                        </div>
                                                        <button id={"contact_us_create_popup_ok"} onClick={this.handleContactUsSubmitClick}>Submit</button>
                                                    </div>
                                                </div>
                                                {/* End contact us pop up section */}

                                                {
                                                    this.state.showDeletePopup?
                                                    <div className="delete_account_wrapper">                         
                                                        <div className="popup_wrapper">
                                                            <div className="alt_chat_wrapper">
                                                                <div className="popup_content">
                                                                    <div className="popup_content_conatiner">
                                                                        <div className="popup_item delete_wrapper">
                                                                            <h2>Delete Account</h2>
                                                                            <div className="text_content_wrapper">
                                                                                <div className="text_container">
                                                                                    <p>Are you sure you want to delete your account</p>
                                                                                </div>
                                                                                <div className="button_wrapper">
                                                                                    <button className="first_button" onClick={this.handleDeleteYes.bind(this)}>Yes</button>
                                                                                    <button onClick={this.handleDeleteNo.bind(this)}>No</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    : ''
                                                }

                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    
                        

                    </div>
                </div>
            </header>
        )
    }
}

export default Header;