import React from 'react';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import DashboardBody from '../../components/body/DashboardBody';

class LoginGuest extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <DashboardBody/>
                <Footer/>
            </div>
        )
    }
}

export default LoginGuest;
