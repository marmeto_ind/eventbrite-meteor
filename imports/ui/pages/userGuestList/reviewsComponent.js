import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import {Meteor} from 'meteor/meteor';

class ReviewsComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            rating: 0
        };
    }

    componentDidMount() {
        const self = this;
        const eventId = this.props.eventId;
        
        Meteor.call('choices.getDataById', eventId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                if(result) {
                    const userId = result.user;
                    const sellerId = result.seller;

                    self.setState({userId: userId});
                    self.setState({sellerId: sellerId});
                    self.setState({eventId: eventId});
                }
            }
        });

        Meteor.call('reviews.getDataById', eventId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                if(result) {
                    const review = result.review;
                    self.setState({rating: review});
                }
            }
        })

    }

    onStarClick(nextValue, prevValue, name) {
        const review = nextValue;
        this.setState({rating: review});
        const userId = this.state.userId;
        const sellerId = this.state.sellerId;
        const eventId = this.state.eventId;

        // console.log(sellerId);

        if(sellerId) {
            Meteor.call('reviews.insert', userId, sellerId, eventId, review, function (err, result) {
                if(err) {
                    console.error(err);
                } else {
                    console.log(result);
                }
            })
        } else {
            alert("No Seller Selected for the event");
        }
    }

    render() {
        const { rating } = this.state;
        // console.log(this.state);



        return (
            <div>
                <StarRatingComponent
                    name="rate1"
                    starCount={5}
                    value={rating}
                    onStarClick={this.onStarClick.bind(this)}
                />
            </div>
        );
    }
}


export default ReviewsComponent;