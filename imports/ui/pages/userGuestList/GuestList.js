import React from 'react';
import Header from '../header-username/header';
import './guest-list.scss';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/userTopBar/TopBar';
import GuestListSection from './GuestListSection';
import LeftSideBar from '../../components/userLeftSideBar/LeftSideBar';
import TableList from './TableList';
import EventListContainer from './EventsListContainer';

class GuestList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
            selectedTable: '',
            selectedEvent: ''
        };
        this.parentCallback = this.parentCallback.bind(this);
        this.parentEvtCallback = this.parentEvtCallback.bind(this);
    }

    _handleSearchKeyChange(event) {
        let searchKey = event.target.value;
        this.setState({ searchKey: searchKey });
    }

    parentCallback(tableId) {
        this.setState({ selectedTable: tableId });
    }

    parentEvtCallback(eventId) {
        //console.log("dsdsd");
        this.setState({ selectedEvent: eventId });
        //console.log("Selected Event ", this.props.selectedEvent);
    }

    // parentCallBack(option) {
    //     let { searchKey } = option;
    //     this.setState({ searchKey })
    // }

    render() {
        return (
            <div>
                <Header />
                <div className="body-cntnr main-container GuestList_wrapper">
                    <TopBar />
                    <div className="clearfix"></div>
                    <div className="clearfix"></div>
                    <div className="body_container ">
                        {/* <div  className={"user_guestlist_leftSideBar"}>
                            <LeftSideBar 
                                searchKey={this.state.searchKey}
                                parentCallBack={this.parentCallBack.bind(this)}
                            />
                        </div> */}
                        <div className="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                            <EventListContainer parentEvtCallback={this.parentEvtCallback} />
                        </div>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                            <div className="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                                <div className="GuestListSectionHeading">
                                    tables
                                    </div>
                            </div>
                            <div className="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                                <div className="GuestListSectionHeading">
                                    Guest List
                                    </div>
                            </div>
                            {/* <div className="col-md-8 display_flex padding leftPading">
                                <div className="sectionblock1">
                                    
                                </div>
                                <div className="sectionblock2">
                                    
                                </div>
                            </div> */}
                            <div className="clearfix" />
                            <div className={"row margin-0"}>
                                <div className={"col-md-12 display_flex padding leftPading "}>
                                    <div id={"chatListName"} className={"sectionblock1 tableListBox usertableListBlock"}>
                                        <TableList parentCallback={this.parentCallback} eventId={this.state.selectedEvent} />
                                    </div>
                                    <GuestListSection tableId={this.state.selectedTable} />
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default GuestList;