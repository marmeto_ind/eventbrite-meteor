import React from 'react';
import ShowList from './ShowList';

class GuestListSection extends React.Component {
    constructor(props) {
        super(props);
        this._handleKeyPress = this._handleKeyPress.bind(this)
    }

    _handleAddGuest(event) {
        const name = $("#guestName").val();
        const tableId = this.props.tableId;
        const user = Meteor.userId()? Meteor.userId(): '';

        if (name) {
            if(!user) {
                console.log('User is not signed up');
            } else if (!tableId) {
                alert("no table selected");
            } else {
                Meteor.call('guests.insert', name, user, tableId);
                $("#guestName").val("");
            }
        }
    }

    _handleKeyPress(event) {
        if (event.key === 'Enter') {
          this._handleAddGuest();
        }
    }

    render() {
        const tableId = this.props.tableId;
        return (
            <div className={"sectionblock1_new guest-list-background box-padding no"}>
                {/*<div className={"userTitleGuestBlock "}>*/}
                    {/*<div className={"userTitleGuesttext"}>*/}
                        {/*GUESTLIST*/}
                    {/*</div>*/}
                    {/*<div className={"userTitleGuestlineImg"}>*/}
                        {/*<img className={"svg-img-style"} src={"/img/line.png"} />*/}
                    {/*</div>*/}
                {/*</div>*/}


                <ShowList tableId={tableId}/>

                <div className={"col-lg-12  search-section userGuestListTypeBox"}>
                    <div className={"width_90_center list-div-left-background list-type-box guest-type-box "}>
                        <div className={"user_guestList_block"}>
                            <div className={"block1"}>
                                <span><img className={"list-img"}  src="/img/new_img/guest_table.svg" alt="" /></span>
                            </div>
                            <div className={"block2  guest-list-search-box"}>

                                <input
                                    id="guestName"
                                    className="type-search"
                                    type="text"
                                    placeholder="WRITE A Table"
                                    name="guest"
                                    onKeyPress={this._handleKeyPress.bind(this)} />

                            </div>
                            <div className={"block3"}>
                                <span className={"btn"} onClick={this._handleAddGuest.bind(this)}>
                                    <img className={"list-img user_guestListaddutton"}  src="/img/new_img/table_plus.svg" alt="" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default GuestListSection;