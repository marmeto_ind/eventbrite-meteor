import React from 'react';
import List from './List';
import {Guests} from "../../../api/guests";
import { withTracker } from 'meteor/react-meteor-data';


class ShowList extends React.Component {
    renderList() {
        return this.props.guests.map((guest, index) => (
            <List key={index} guest={guest}/>
        ));
    }

    render() {
        return (
            <div className={"userGuestList"}>
                <ul className="ticketView_new">
                    {this.renderList()}
                </ul>
            </div>
        )
    }
}

// export default ShowList;
export default withTracker((data) => {
    Meteor.subscribe('guests');
    const tableId = data.tableId;
    // console.log(tableId);
    const user = Meteor.userId();
    return {
        guests: Guests.find({"user" : user, "tableId" : tableId}, {sort: {createdAt: -1}}).fetch(),
    }
})(ShowList);