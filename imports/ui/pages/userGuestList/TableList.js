import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { GuestTable } from "../../../api/guestTable";
import ShowEachTable from './ShowEachTable';

class TableList extends React.Component {
    constructor(props) {
        super(props);
        this._handleKeyPress = this._handleKeyPress.bind(this);
    }

    addTable(e) {
        // e.preventDefault();
        const tableName = $("#tableName").val();
        const eventId = this.props.eventId;
        console.log("table Name is ", tableName);

        if (tableName) {
            Meteor.call('guests_table.insert', eventId, tableName, Meteor.userId(), function (err, result) {
                if (err) {
                    console.error(err)
                } else {
                    console.log(result);
                }
            });
        }

        $("#tableName").val('');
    }


    renderTables() {
        const parentCallback = this.props.parentCallback;
        return this.props.tables.map((table, index) => (
            <ShowEachTable key={index} table={table} parentCallback={parentCallback} />
        ))
    }

    _handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.addTable();
        }
    }

    returnAdd(eventId) {
        if (eventId) {
            return (<li className="list-group-item list-right-padding-style_wrap">
                <div className={"seller-main-width"}>
                    <div className={"seller-list-left-icon"}>
                        <span><img className={"list-img"} src="/img/guest1.jpg" alt="" /></span>
                    </div>
                    <div className={"capital-text"}>
                        <input
                            id="tableName"
                            className="type-search_wrap"
                            type="text"
                            placeholder="ADD TABLE"
                            name="guest"
                            onKeyPress={this._handleKeyPress} />
                    </div>
                    <div className="right_side_wrapper">
                        <div className={""} onClick={this.addTable.bind(this)}>
                            <span><img className={"list-img user_guestListaddutton"} src="/img/new_img/table_plus.svg" alt="" /></span>
                        </div>
                    </div>
                </div>
            </li>
            )
        } else {
            return(<p className="no_event_error">Please select a event to view table member</p>)
        }
    }

    render() {
        const eventId = this.props.eventId;
        return (
            <ul className="list-group list-group-flush">
                {this.renderTables()}
                {this.returnAdd(eventId)}
            </ul>
        )
    }
}

export default withTracker((data) => {
    const eventId = data.eventId;
    const userId = Meteor.userId();
    Meteor.subscribe('guests_table.all');
    if (eventId) {
        const tables = GuestTable.find({ $and: [{ "user": userId }, { "eventId": eventId }] }).fetch();
        return {
            tables: tables,
        }
    } else {
        return {
            tables: [],
        }
    }
})(TableList);

//export default TableList;