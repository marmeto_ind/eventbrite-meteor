import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Guests } from "../../../api/guests";


class ShowEachTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: '',
            showWhiteIcon: false,
        }
    }


    componentDidMount() {
        const self = this;
        $(document).ready(function () {
            $(".tablelistMenu").click(function (e) {
                if (e.target.id !== "guest_cancel") {
                    // self.setState({selectedValue:false});
                    $(".tablelistMenu").removeClass("selected-list-active");
                    $(this).addClass("selected-list-active");
                    self.setState({ selected: '' });
                    // self.setState({selectedValue:true});
                }

                // $("#guest_cancel").attr('src',"/img/new_img/white_cancel.png");
            });
        });
    }

    _handleDeleteTable(e) {
        e.preventDefault();
        const table = this.props.table;
        Meteor.call('guests_table.remove', table._id);
    }

    _handleTableSelect(e) {
        e.preventDefault();
        if (e.target.id !== "guest_cancel") {
            this.setState({ selected: true });
            const table = this.props.table;
            this.props.parentCallback(table._id);
        }
    }

    render() {
        const table = this.props.table;
        // console.log(this.state.selected);

        return (
            <li className="list-group-item list-right-padding-style btn li-background tablelistMenu " onClick={this._handleTableSelect.bind(this)}>
                <div className={"seller-main-width"}>
                    <div className={"seller-list-left-icon"}>
                        <span>
                            <img className={"list-img"}
                                src={this.state.selected === true ? "/img/new_img/guest_table.svg" : "/img/new_img/guest_table_dark.svg"}
                                alt="" />
                        </span>
                    </div>

                    <div className={"seller-name capital-text"}>
                        <p>{table.name}</p>
                        {/*<p className={"venue-p-margin"}>Hi Panorama</p>*/}

                    </div>
                    <div className="right_side_wrapper">
                        <div className={"seller-list-right-icon"}>
                            <span className={""}> {this.props.numOfUser} </span>
                        </div>
                        <div className={"tableDeleteBox"}>
                            <span className="pull-right" onClick={this._handleDeleteTable.bind(this)}>
                                <img
                                    id={"guest_cancel"}
                                    className=""
                                    src={this.state.selected === true ? "/img/new_img/table_cross.svg" : "/img/new_img/table_cross.svg"}
                                    alt=""
                                />
                            </span>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

export default withTracker((data) => {
    const tableId = data.table._id;
    const userId = Meteor.userId();
    Meteor.subscribe('guests_table.all');
    const numOfUser = Guests.find({ "user": userId, "tableId": tableId }).count();
    return {
        numOfUser: numOfUser
    }
})(ShowEachTable);