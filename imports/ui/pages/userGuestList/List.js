import React from 'react'

class List extends React.Component {
    _handleListDelete(e) {
        e.preventDefault();
        const guest = this.props.guest;
        Meteor.call('guests.remove', guest._id);
    }

    render() {
        const guest = this.props.guest;
        return (
            <li className="list-group-item guest-list-list-style">
                {/*<div className={"show-list"}>*/}
                    {/*<span className="pull-left"><img className=" vendor-style" src="/img/guest3.jpg" alt="" /></span>*/}
                    {/*<div className={"list-center-div div-text-style"}>*/}
                        {/*<span className={"block"}>{guest.name}</span>*/}
                        {/*/!*<span>Venue</span>*!/*/}
                    {/*</div>*/}
                    {/*<span className="pull-right" onClick={this._handleListDelete.bind(this)}>*/}
                        {/*<img className="create-add" src="/img/share.png" alt="" />*/}
                    {/*</span>*/}
                {/*</div>*/}



                <div className={"guestListItemWidth"}>
                    <div className={"iconBox"}>
                        <span><img  src="/img/guest3.jpg" alt="" /></span>
                    </div>
                    <div  className={"listName"}>
                        <span className={"block guestlist-span "}>{guest.name}</span>
                    </div>
                    <div  className={"iconBox"}>
                          <span  onClick={this._handleListDelete.bind(this)}>
                        <img className="create-add" src="/img/guest-cancel.png" alt="" />
                    </span>
                    </div>
                </div>

            </li>
        )
    }
}

export default List;