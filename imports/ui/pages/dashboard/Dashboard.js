import React from 'react';
import Header from '../../components/header/Header';
import Body from '../../components/body/BodyPart'
import Footer from '../../components/footer/Footer';
import './Dashboard.css';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import axios from 'axios';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            sellers: [],
        }
    }

    componentWillMount() {
        if (Meteor.isCordova) {
            browserHistory.push('/mobile/home/first');
        }

        if (Meteor.Device.isPhone()) {
            browserHistory.push('/mobile/home/first');
        }
    }

    componentDidMount() {
        axios.get('/api/sellers')
            .then((res) => {
                const sellers = res.data;
                this.setState({ sellers });
              })
            .catch((error) => {
                console.log(error);
            });

        axios.get('/api/categories')
            .then((res) => {
                const categories = res.data;
                this.setState({ categories });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <Header/>
                <Body categories={this.state.categories} sellers={this.state.sellers}/>
                <Footer/>
            </div>
        )
    }
}

export default Dashboard;
