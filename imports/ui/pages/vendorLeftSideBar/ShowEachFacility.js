import React from 'react';

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage: '',
        }
    }

    componentDidMount() {
        const facility = this.props.facility;
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    self.setState({facilityImage: result.image});
                }
            }
        });
    }

    render() {
        const facility = this.props.facility;

        const facilityImage = this.state.facilityImage;
        const vendorFacilitymainImage = facilityImage? facilityImage: "/img/parking.png";

        // const vendorFacilitymainImage = "/img/parking.png";

        const vendorFacilitydivStyle = {
            backgroundImage: 'url(' + vendorFacilitymainImage + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '20px 20px',
            backgroundPosition: '50% 50%',
        };

        return (
            <div className="vendor-service-first-div service-name">
                <div style={vendorFacilitydivStyle}>
                {facility.name}
                </div>
            </div>
        )
    }
}

export default ShowEachFacility;