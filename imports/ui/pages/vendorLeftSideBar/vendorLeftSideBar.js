import React from 'react';
import './vendorLeftSideBar.scss';
import ShowEachFacility from './ShowEachFacility';
import ReactPlayer from 'react-player';

class ShowEachOtherImage extends React.Component {
    render() {
        return (
            <div className={"vendor-more-pictures-scroll-div"}>
                <img className="vendor-main-picture-size" src={this.props.image}/>
            </div>
            )
    }
}

class VendorLeftSideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seller: '',
        };
    }

    componentDidMount() {
        $(document).ready(function(){
            $(".service-name").click(function () {
                $(".service-name").removeClass("service-active-class");
                $(this).addClass("service-active-class");
            });
        });

        const userId = Meteor.userId();
        const self = this;
        Meteor.call('seller.findOneByUserId', userId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                self.setState({seller: result});
            }
        })
    }
    _handleNewEvent(){

    }

    renderOtherImages(images) {
        return images.map((image, index)=>(
            <ShowEachOtherImage image={image} key={index} />
        ))
    }

    renderSellerFacilities(facilities) {
        return facilities.map((facility, index)=>(
            <ShowEachFacility key={index} facility={facility} />
        ))
    }

    isFacilityProvided(facliityName) {
        let facilities = this.state.seller.profile.facilities;
        for (let index = 0; index < facilities.length; index++) {
            if (!facliityName) {
                continue
            }
            if (!facilities[index].name) {
                continue
            }
            if (facliityName.toLowerCase() == facilities[index].name.toLowerCase()) {
                return "active"
            }
        }
        return ""
    }

    render() {
        const seller = this.state.seller;

        if(seller){
            return (
                <div>
                    <ul className="list-group chat_leftbar_wrapper adminApprovalDetailsList vendor_li_wrap">
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list ">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-name">
                                <h2 className={""}>
                                    {seller.name}
                                </h2>
                                <p>SERVICE NAME</p>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-category">
                                <h2 className={""}>
                                    {seller.category.name}
                                </h2>
                                <p>CATEGORY</p>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className="picture-upload">
                                <div className="add-more-pic">
                                    <div>
                                        <img className="vendor-main-picture-size"
                                             src={seller.profile.mainImage}
                                        />
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className="picture-upload">
                                <div className="add-more-pic">
                                    <div>
                                        <img className="vendor-main-picture-size"
                                             src={seller.profile.logoImage}
                                        />
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className="picture-upload">
                                <div className="add-more-pic vendor-more-pictures-scroll">
                                    {this.renderOtherImages(seller.profile.moreImages)}
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className="picture-upload">
                                <div className="add-more-pic">
                                    <div className="vendor-video-div">
                                        <ReactPlayer
                                            url={seller.profile.video}
                                            controls={true}
                                            width='100%'
                                            height='100%'
                                            style={{borderRadius:'4px'}}
                                        />
                                    </div>
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list img_wrapper front_image">
                            <img src={seller.profile.mainImage} className="fixed_image" />
                            <div className="caption_text">
                                <div className="caption_text_content">
                                    <img src="/img/camera_white.svg" />
                                    <p>Front image</p>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list img_wrapper logo_image">
                            <img src={seller.profile.logoImage} className="fixed_image" />
                            <div className="caption_text">
                                <div className="caption_text_content">
                                    <img src="/img/camera_white.svg" />
                                    <p>Logo image</p>
                                </div>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list img_wrapper more_image">
                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Sk%C3%A4rmklipp.JPG" className="fixed_image" />
                            <div className="caption_text">
                                <div className="caption_text_content">
                                    <img src="/img/photo-camera.png" />
                                    <hr />
                                    <p>More Picture</p>
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list padding-0">
                            <div className="more_image_wrapper">
                                <div className="more_image_content_box">
                                    {this.renderOtherImages(seller.profile.moreImages)}
                                </div>
                                <div className="more_image_gallery">
                                    <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Screenshot from 2019-01-31 17-04-58.png" />
                                    <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Screenshot from 2019-01-31 17-04-58.png" />
                                    <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Screenshot from 2019-01-31 17-04-58.png" />
                                    <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Screenshot from 2019-01-31 17-04-58.png" />
                                    <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Screenshot from 2019-01-31 17-04-58.png" />
                                </div>
                                <div className="more_image_text">
                                    <div className="text_content">
                                        <img src="/img/camera_white.svg" />
                                        <p>More pictures</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list img_wrapper video_wrapper">
                            <img src="https://wendely-images.s3-us-east-2.amazonaws.com/Sk%C3%A4rmklipp.JPG" className="fixed_image" />
                            <div className="caption_text">
                                <div className="caption_text_content">
                                    <img src="/img/photo-camera.png" />
                                    <hr />
                                    <p>Video</p>
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list no-padding overflowHidden">
                            <div className="">
                                <div className="add-more-pic">
                                    <div className="vendor-video-div">
                                        <ReactPlayer
                                            url={seller.profile.video}
                                            controls={true}
                                            width='100%'
                                            height='200px'
                                            style={{borderRadius:'4px'}}
                                        />
                                    </div>
                                </div>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <p className="">
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    </p>
                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                    <div className="heading-text">
                                        <h2>STANDARD PACK</h2>
                                        <hr/>
                                    </div>
                                    <h4>Baptzr/Main Courses/Dessert</h4>
                                    <b>Welcome drink / chocolate</b>
                                    <p>{seller.profile.standardPack}</p>
                                </div>
                            </div>
                        </li> */}


                        <li className="list-group-item vendor-leftSideBar admin-approvals-list vendor-serviceLeft-list">
                            <div className={"vendorleftSide-text-div"}>
                                <p className="">
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                </p>
                                <div>
                                    <div className="heading-text">
                                    <h2 className={"vendor-genorous-pack"}>STANDARD PACK</h2>
                                </div>
                                {/* <h4>Baptzr/Main Courses/Dessert</h4>
                                <p className="first_paragraph"> Welcome drink / chocolate</p> */}
                                <p className="second_paragraph">{seller.profile.generousPack}</p>
                                </div>
                            </div>
                        </li>

                        {/* <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <p className="">
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    </p>
                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                    <div className="heading-text">
                                        <h2 className={"vendor-genorous-pack"}>Generous PACK</h2>
                                        <hr/>
                                    </div>
                                    <h4>Baptzr/Main Courses/Dessert</h4>
                                    <b>Welcome drink / chocolate</b>
                                    <p>{seller.profile.generousPack}</p>
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item vendor-leftSideBar admin-approvals-list vendor-serviceLeft-list">
                            <div className={"vendorleftSide-text-div"}>
                                <p className="">
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                </p>
                                <div>
                                    <div className="heading-text">
                                    <h2 className={"vendor-genorous-pack"}>Generous PACK</h2>
                                </div>
                                {/* <h4>Baptzr/Main Courses/Dessert</h4> */}
                                {/* <p className="first_paragraph"> Welcome drink / chocolate</p> */}
                                <p className="second_paragraph"> {seller.profile.generousPack}</p>
                                </div>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-leftSideBar vendor-service-firstlastdiv">
                            <div className={"vendorleftSide-text-div"}>
                                <div>
                                    <p className="">
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    </p>
                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                    <div className="heading-text">
                                        <h2  className={"vendor-exclusive-pack"}>Exclusive PACK</h2>
                                        <hr/>
                                    </div>
                                    <h4>Baptzr/Main Courses/Dessert</h4>
                                    <b>Welcome drink / chocolate</b>
                                    <p>{seller.profile.exclusivePack}</p>
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item vendor-leftSideBar admin-approvals-list vendor-serviceLeft-list">
                            <div className={"vendorleftSide-text-div"}>
                                <p className="">
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                    <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                </p>
                                <div>
                                    <div className="heading-text">
                                    <h2 className={"vendor-exclusive-pack"}>Exclusive PACK</h2>
                                </div>
                                {/* <h4>Baptzr/Main Courses/Dessert</h4>
                                <p className="first_paragraph"> Welcome drink / chocolate</p> */}
                                <p className="second_paragraph"> {seller.profile.generousPack}</p>
                                </div>
                            </div>
                        </li>
                        {/* <li className="list-group-item vendor-serviceLeft-list">
                            <div className={"service-left-padding"}>
                                <div className="vendor-check-box-item">
                                    {this.renderSellerFacilities(seller.profile.facilities)}
                                </div>
                            </div>
                        </li> */}
                        <li className="list-group-item admin-approvals-list vendor-serviceLeft-list">
                            <div className={"vendor-check-div"}>
                            {/*<input className={"floatLeft"} type="checkbox" id="vendor-service-check" />*/}
                            {/* <div className="vendor-check-box-item">
                            {this.renderFacilities(seller.profile.facilities)}
                            </div> */}
                                <div className="facility-dv new_service_position">
                                    <ul>
                                        <li className={this.isFacilityProvided("parking")}><img src="/img/newService/PARKING.png" /><br /><span>Parking</span></li>
                                        <li className={this.isFacilityProvided("smoking")}><img src="/img/newService/SMOKING.png" /><br /><span>Smoking</span></li>
                                        <li className={this.isFacilityProvided("elevator")}><img src="/img/newService/ELEVATOR.png" /><br /><span>Elevator</span></li>
                                        <li className={this.isFacilityProvided("wifi")}><img src="/img/newService/WIFI.png" /><br /><span>Wifi</span></li>
                                        <li className={this.isFacilityProvided("bar")}><img src="/img/newService/BAR.png" /><br /><span>Bar</span></li>
                                        <li className={this.isFacilityProvided("kitchen")}><img src="/img/newService/KITCHEN.png" /><br /><span>Kitchen</span></li>
                                        <li className={this.isFacilityProvided("stage")}><img src="/img/newService/STAGE.png" /><br /><span>Stage</span></li>

                                        {/* <li className="active"><img src="/img/newService/PARKING.png" /><br /><span>Parking</span></li>
                                        <li><img src="/img/newService/SMOKING.png" /><br /><span>Smoking</span></li>
                                        <li><img src="/img/newService/ELEVATOR.png" /><br /><span>Elevator</span></li>
                                        <li><img src="/img/newService/WIFI.png" /><br /><span>Wifi</span></li>
                                        <li><img src="/img/newService/BAR.png" /><br /><span>Bar</span></li>
                                        <li><img src="/img/newService/KITCHEN.png" /><br /><span>Kitchen</span></li>
                                        <li><img src="/img/newService/STAGE.png" /><br /><span>Stage</span></li> */}
                                    </ul>
                                </div>
                            </div>

                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-area">
                                {/* <h2 className={""}>
                                    {seller.profile.area}
                                </h2>
                                <p>AREA (M2)</p> */}

                                <div className="area facility_div display_inherit">
                                    <div className="facility_div_content">
                                        <div className="left_div">&nbsp;</div>
                                        <div className="right_div"><input type="text" id="area" className="right_div_class" value={seller.profile.area} />
                                            <p className="facility_name">Area</p><span className="power_text">M<sup>2</sup></span></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-celingHeight">
                                {/* <h2 className={""}>
                                    {seller.profile.ceilingHeight}
                                </h2>
                                <p>CEILING HEIGHT (M)</p> */}
                                <div className="area facility_div display_inherit">
                                    <div className="facility_div_content">
                                        <div className="left_div">&nbsp;</div>
                                        <div className="right_div"><input type="text" id="area" className="right_div_class" value={seller.profile.ceilingHeight} />
                                            <p className="facility_name">CEILING HEIGHT</p><span className="power_text">M</span></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-maxPeople">
                                {/* <h2 className={""}>
                                    {seller.profile.maxPeople}
                                </h2>
                                <p>MAXIMUM PEOPLE</p> */}
                                <div className="area facility_div display_inherit">
                                    <div className="facility_div_content">
                                        <div className="left_div">&nbsp;</div>
                                        <div className="right_div"><input type="text" id="area" className="right_div_class" value={seller.profile.maxPeople} />
                                            <p className="facility_name">MAXIMUM PEOPLE</p></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-eventType">
                                {/* <h2 className={""}>
                                    {seller.profile.eventType.name}
                                </h2>
                                <p>TYPES OF EVENT VENUE IS SUITED FOR</p> */}

                                <div className="area facility_div display_inherit">
                                    <div className="facility_div_content">
                                        <div className="left_div">&nbsp;</div>
                                        <div className="right_div"><input type="text" id="area" className="right_div_class" value={seller.profile.eventType.name} />
                                            <p className="facility_name">TYPES OF EVENT VENUE IS SUITED FOR</p></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-location">
                                <h2 className={""}>
                                    {seller.profile.country}
                                </h2>
                                <p>COUNTRY</p>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-location">
                                <h2 className={""}>
                                    {seller.profile.region}
                                </h2>
                                <p>REGION</p>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-location">
                                <h2 className={""}>
                                    {seller.profile.address}
                                </h2>
                                <p>ADDRESS</p>
                            </div>
                        </li>
                        <li className="list-group-item vendor-leftSideBar vendor-serviceLeft-list">
                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-website">
                                <h2 className={""}>
                                    {seller.profile.website? seller.profile.website: "No Website Available"}
                                </h2>
                                <p>WEBSITE</p>
                            </div>
                        </li>
                        <li className="marginleft_10 listStyle_type">
                            <div className="new-event-button_wrapper">
                                <div onClick={this._handleNewEvent} className="relative-div news_service_button  home-login-button-div">
                                    <div className="text-style">
                                        <img className="service_back_logo-home-first-img" src={ "/img/newService/TICK.svg"}/>
                                        <div className={ "back_top"}>BOOK</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            )
        } else {
            return (
                <div>
                    <div className={"vendor-no-left-lisBar"}>
                        <p className={"vendor-no-left-lisBar"} >Seller Not Registered</p>
                        <a className={"vendor-no-left-lisBar text-decoration-underline "} href="/vendor/add-service">
                            Please Register Here
                        </a>
                    </div>
                </div>
            )
        }
    }
}

export default VendorLeftSideBar;