import React from 'react';
import UserChoice from './UserChoice';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class UserChoiceContainer extends React.Component {

    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    // Meteor.logout();
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <UserChoice/>
                    )
                }
            }
        }
    }
}

// export default UserChatContainer;

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    // console.log(logginIn);
    return {
        logginIn: logginIn,
    }
})(UserChoiceContainer);

