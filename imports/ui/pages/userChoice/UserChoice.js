import React from 'react';
import Header from '../header-username/header';
import './user-choice.scss';
import Footer from '../../components/footer/Footer';
import TopBar from '../../components/userTopBar/TopBar';
import LeftSideBar from '../../components/userLeftSideBar/LeftSideBar';

class UserChoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: '',
        };
    }

    _handleSearchKeyChange(event){
        let searchKey = event.target.value;
        this.setState({searchKey: searchKey});
    }

    render() {

        const choiceDiv = {
            backgroundColor: 'black',
            marginBottom: '20px',
            // backgroundImage: 'url(' + choiceImage + ')',
        };

        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>

                    <div className="clearfix"></div>
                    <div className={"row vendor-search-division"}>
                        <div className={"col-md-12"}>
                            <div className={"col-md-4 vendor-search-box"}>

                                <div className="form-group has-feedback">

                                    <input type="text"
                                           className="search-vendor-control"
                                           placeholder="SEARCH VENDOR"
                                           onChange={this._handleSearchKeyChange.bind(this)}
                                    />
                                    <span className="form-control-feedback">
                                       <img className="vendor-search-logo" src="/img/vendor-search.png" alt="" />
                                    </span>
                                </div>
                            </div>

                            <div className={"col-md-2 pagename-text"}>
                                MY CHOICE
                            </div>
                            <div className={"col-md-6"}>
                                <hr />
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>
                    <div className="body_container chat-body">
                        <LeftSideBar searchKey={this.state.searchKey}/>

                        <div className="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            <div style={choiceDiv}  className="top-body-container">
                                <div className="clearfix"></div>
                                <div className="vale_container">
                                    <div className="mychoice_data">
                                        <div className="value-main-content">
                                            <div className={"shadow-box-main-div"}>
                                                <div className={"greyBackground"}>
                                                </div>

                                                <div className={"shadow-box-div"}>
                                            <div className="circle-panorama">
                                                <div className="img_crcl_b">
                                                    <img className={"choice-circle-img"} src='/img/body_bac_k.jpg' alt={"Thumbnail Images"} />
                                                </div>
                                            </div>

                                            <div className="circle-star-margin">
                                                <div className="rating">
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span >
                                                    <span>
                                                    <img className="star-logo" src="/img/10.png" alt="" />
                                                </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/11.png" alt="" />
                                                        {/*comment*/}
                                                    </span>
                                                </div>
                                            </div>

                                            <h3 className={"choice-text-style"}>Assyriska Eventcenter</h3>
                                            <p className={"choice-text-style"}>Sodertalje</p>
                                            <div className="icon_div">
                                               <span>
                                                        <img className="icon-img" src="/img/white-chat.png" alt="" />
                                               </span>
                                                <span>
                                                    <img className="icon-img" src="/img/2.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="icon-img" src="/img/3.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="icon-img" src="/img/4.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="icon-img" src="/img/5.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="icon-img" src="/img/6.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="icon-img" src="/img/7.png" alt="" />
                                                </span>
                                            </div>
                                                </div>
                                            </div>
                                            <div className="div-float">
                                                <p className={"soldout first-soldout"}>Sold Out</p>
                                                <p className={"best-deal best-deal-margin first-best-deal"}>Best Deal</p>
                                                <p className={"hot first-hot"}>Hot</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <div className="top-body-container  relativeposition">
                                <div className="place-header">
                                    <h4 className={"venues-div"}>Venues</h4>
                                    <hr className="hr" />
                                </div>
                                <div className="clearfix"></div>
                                <div  style={choiceDiv} className="vale_container">
                                    <div className="mychoice_data  my-data">
                                        <div className="value-main-content">
                                            <div className={"shadow-box-main-div"}>
                                                <div className={"greyBackground"}>
                                                </div>

                                                <div className={"shadow-box-div"}>
                                            <div className="circle-panorama">
                                                <div className="img_crcl_b">
                                                    <img className={"choice-circle-img"} src='/img/body_bac_k.jpg' alt={"Thumbnail Images"} />
                                                </div>
                                            </div>
                                            <div className="circle-star-margin">
                                                <div className="rating">
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span >
                                                    <span>
                                                    <img className="star-logo" src="/img/10.png" alt="" />
                                                </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/11.png" alt="" />
                                                    </span>
                                                </div>
                                            </div>

                                            <h3 className={"choice-text-style"}>Panorama Eventcenter</h3>
                                            <p className={"choice-text-style"}>Sodertalje</p>
                                            <div className="icon_div">
                                                <span>
                                                        <img className="icon-img" src="/img/white-chat.png" alt="" />
                                               </span >
                                                <span>
                                                        <img className="icon-img" src="/img/2.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/3.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/4.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/5.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/6.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/7.png" alt="" />
                                                    </span >

                                            </div>
                                                </div>
                                            </div>
                                            <div className="div-float second-choice-div">
                                                <p className={"second-best-deal best-deal"}>Best Deal</p>
                                                <p className={"second-hot hot"}>Hot</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"></div>

                            <div   className="top-body-container  relativeposition">
                                <br/>
                                <div className="value_image">
                                    <img src="/img/body_bac_k.jpg" alt="images" />
                                </div>
                                <br/>
                                <div className="clearfix"></div>
                                <div style={choiceDiv} className="vale_container">
                                    <div className="mychoice_data">
                                        <div className="value-main-content">
                                            <div className={"shadow-box-main-div"}>
                                                <div className={"greyBackground"}>
                                                </div>

                                                <div className={"shadow-box-div"}>
                                            <div className="star-block">
                                                <div className="rating">
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span >
                                                    <span>
                                                    <img className="star-logo" src="/img/10.png" alt="" />
                                                </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/10.png" alt="" />
                                                    </span>
                                                    <span>
                                                        <img className="star-logo" src="/img/11.png" alt="" />
                                                    </span>
                                                </div>
                                            </div>

                                            <h3 className={"choice-text-style"}>Panorama Eventcenter</h3>
                                            <p className={"choice-text-style"}>Sodertalje</p>
                                            <div className="div-float-top third-choice-top-div">
                                                <p className={"top-hot top-all-div hot"}>Hot</p>
                                                <p className={"top-soldout top-all-div soldout"}>Sold out</p>
                                                <p className={"top-best-deal top-all-div best-deal"}>Best Deal</p>
                                            </div>
                                            <div className="icon_div">
                                                <span>
                                                        <img className="icon-img" src="/img/white-chat.png" alt="" />
                                               </span >
                                                <span>
                                                        <img className="icon-img" src="/img/2.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/3.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/4.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/5.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/6.png" alt="" />
                                                    </span >
                                                <span>
                                                        <img className="icon-img" src="/img/7.png" alt="" />
                                                    </span >

                                            </div>
                                                </div>
                                            </div>
                                            <div className="div-float third-choice-div">
                                                <p className={"third-best-deal best-deal"}>Best Deal</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default UserChoice;