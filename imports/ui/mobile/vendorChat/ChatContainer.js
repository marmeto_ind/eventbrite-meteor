import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import Chat from './Chat';

class ChatContainer extends React.Component {
    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/vendor/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "vendor") {
                    browserHistory.push("/mobile/vendor/login");
                    return (<div></div>);
                } else {
                    return (
                        <Chat userId={this.props.userId} sellerId={this.props.sellerId} />
                    )
                }
            }
        }
    }
}


export default withTracker((data) => {
    const sellerId = data.params.sellerId;
    const userId = data.params.userId;

    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
        sellerId: sellerId,
        userId: userId,
    }
})(ChatContainer);

