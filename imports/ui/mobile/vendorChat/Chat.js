import React from 'react';
import './chat.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import ShowMessages from './ShowMessages';
import {Meteor} from 'meteor/meteor';
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';
import {emojify} from 'react-emojione';

class MobileVendorChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            userId: '',
        };
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;

        const existingValue = $("#messageText").val();
        if (existingValue) {
            $("#messageText").val(existingValue + " " + emojiCode);
        } else {
            $("#messageText").val(emojiCode);
        }
    }

    _handleOpenEmoji() {
        this.setState({openEmoji: !this.state.openEmoji});
    }

    componentDidMount() {
        const self = this;
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;

        this.setState({userId: userId});
        Meteor.call('users.getUserName', userId, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                self.setState({userName: result.profile.name})
            }
        });

        // Save user entry in the room
        Meteor.call('message_view.upsert', userId, sellerId, "seller", function (err, result) {
            if (err) {
                console.log("error occured")
            } else {
                console.log(result)
            }
        })
    }

    handleClick(event) {
        if (event.target.id === "messageText" || event.target.id === "mobile_emoji_img") {
            // $('#tabBarDiv').hide();
            // $("#mobileVendorChatBox").css("bottom", "-20px");
        } else if (event.target.value !== "") {
            // $('#tabBarDiv').hide();
            // $("#mobileVendorChatBox").css("bottom", "-20px");
        } else {
            // $('#tabBarDiv').show();
            // $("#mobileVendorChatBox").css("bottom", "35px");
            // $('.mobile_user_emoji_block').hide();
        }
    }

    handleSendMessage = () => {
        const message = $("#messageText").val();
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;

        if (message) {
            Meteor.call('messages.insert', message, userId, sellerId, 'seller', function (err, result) {
                if (err) {
                    console.error(err);
                } else {
                    $("#messageText").val("");
                }
            })
        }
    };

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#mobileVendorAttach")[0].files[0];

        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                console.log(uploader.progress());
            }
        })
    }

    render() {
        const element = $(".list-group-item:last-child").offset();
        if (element) {
            $('html, body').animate({
                scrollTop: ($(".list-group-item:last-child").offset().top)
            }, 50);
        }

        const userId = this.props.userId;
        const sellerId = this.props.sellerId;

        return (
            <div onClick={this.handleClick.bind(this)}>
                <div className={"mobile-chat-conv-div-header vendor_chat_wrapper"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>{this.state.userName}</p>
                    </div>
                    <div>
                        <p className={"hanna-text"}><span
                            className={"mobile_vendor_dot"}>.</span>{Meteor.user() ? Meteor.user().profile.name : ''}
                        </p>
                    </div>
                </div>

                <div className={"mobileVendorChatBlock"}>
                    <div>
                        <ShowMessages userId={userId} sellerId={sellerId}/>
                    </div>
                    {this.state.openEmoji === true ? <div className={"mobile_user_emoji_block"}>
                            <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)}/>
                        </div> :
                        ''
                    }

                    <div id={"mobileVendorChatBox"} className={"vendor_box_wrapper"}>
                        <div className="vendor_box_container">
                            <div className={"block1"}>
                                <span>
                                    <img className={"list-img width-15"}
                                        src="/img/svg_img/phone_icon.svg"
                                        id="mobile_emoji_img"
                                        alt=""
                                        
                                    />
                                </span>
                            </div>
                            <div className={"block2"} onChange={this._handleFileUpload.bind(this)}>
                                <label htmlFor={"mobileVendorAttach"}>
                                <span>
                                    <img
                                        className={"list-img"}
                                        src="/img/clip_chat.svg"
                                        alt=""
                                    />
                                </span>
                                </label>
                                <input className="form-control-file" id="mobileVendorAttach" type="file"/>
                            </div>
                            <div className={"block3"}>
                                <input id={"messageText"}
                                    className="type-search_wrap"
                                    type="text"
                                    placeholder="WRITE A MESSAGE"
                                    name="message"
                                />
                            </div>
                            <div className={"block4"}>
                            <span className={"btn"}>
                                <img id={"vendorSendMessage"}
                                    className={"list-img"}
                                    src="/img/emoji_chat.svg"
                                    alt=""
                                    onClick={this._handleOpenEmoji.bind(this)}
                                />
                            </span>
                            </div>
                            <div className={"block4"}>
                            <span className={"btn"}>
                                <img id={"vendorSendMessage"}
                                    className={"list-img"}
                                    src="/img/send_image.svg"
                                    alt=""
                                    onClick={this.handleSendMessage.bind(this)}
                                />
                            </span>
                        </div>
                        </div>
                    </div>
                </div>

                <VendorBottomTabBar/>
            </div>
        )
    }
}

export default MobileVendorChat;