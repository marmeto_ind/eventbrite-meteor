import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import ShowEachMessage from './ShowEachMessage';

class ShowMessages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderMessages() {
        return this.props.messages.map((message, index) => (
            <ShowEachMessage message={message} key={index} />
        ))
    }

    render() {
        return (
            <div className={"float-chat-conv-div"}>
                <ul className="list-group chats-conv-List">
                    {this.renderMessages()}
                </ul>
            </div>
        )
    }
}

export default withTracker((data) => {
    const userId = data.userId;
    const sellerId = data.sellerId;
    Meteor.subscribe('messages.all');
    const messages = Messages.find({"user" : userId, "seller" : sellerId}).fetch();
    return {
        messages: messages,
    };
})(ShowMessages);