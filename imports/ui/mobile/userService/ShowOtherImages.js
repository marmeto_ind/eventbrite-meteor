import React from 'react';
import ShowEachImage from './ShowEachImage';

class ShowOtherImages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderOtherImages(images) {
        return images.map((image, index) => (
            <ShowEachImage image={image} key={index}/>
        ))
    }

    render() {
        const images = this.props.images;
        // console.log(images);

        return (
            <div className="mobile-vendor-slideShowScroll">
                {this.renderOtherImages(images)}

                {/*<div className={"mobile-vendor-slideShowScrollDiv"}>*/}
                    {/*<img className={"mobile-gallery-img"} src={"/img/venu.jpg"} alt="" />*/}
                {/*</div>*/}
                {/*<div className={"mobile-vendor-slideShowScrollDiv"}>*/}
                    {/*<img className={"mobile-gallery-img"} src={"/img/venu.jpg"} alt="" />*/}
                {/*</div>*/}
                {/*<div className={"mobile-vendor-slideShowScrollDiv"}>*/}
                    {/*<img className={"mobile-gallery-img"} src={"/img/venu.jpg"} alt="" />*/}
                {/*</div>*/}
                {/*<div className={"mobile-vendor-slideShowScrollDiv"}>*/}
                    {/*<img className={"mobile-gallery-img"} src={"/img/venu.jpg"} alt="" />*/}
                {/*</div>*/}
            </div>
        )
    }
}

export default ShowOtherImages;