import React from 'react';
import './service.scss';
import { withTracker } from 'meteor/react-meteor-data';
import { Sellers } from "../../../api/sellers";
import { Meteor } from 'meteor/meteor';
import ShowSellerRating from './ShowSellerRating';
import ReactPlayer from 'react-player';
import ShowEachFacility from './ShowEachFacility';
import { browserHistory } from 'react-router';
import { Choices } from "../../../api/choice";
import BottomTabBar from "../userBottomBar/BottomTabBar";
import { Carousel } from 'react-responsive-carousel';
import ShowOtherImages from './ShowOtherImages';
import { ToastContainer, toast } from 'react-toastify';

class Service extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: '',
            isSelected: '',
            sellerLogoImage: ''
        };
    }

    renderSellerFacilities() {
        const seller = this.props.seller;
        return seller.profile.facilities.map((facility, index) => (
            <ShowEachFacility facility={facility} key={index} />
        ))
    }

    _handleAddChoice(e) {
        const userId = Meteor.userId();
        let username = Meteor.user().username;
        if (username === "guest") {
            browserHistory.push("/mobile/login");
        } else {
            const seller = this.props.seller;
            const sellerId = this.props.sellerId;
            const eventId = this.props.eventId;
            const categoryId = seller.category._id;
            if (eventId === "search_page") {
                $("#create_popup").show();
                $("#create_popup_ok").click(function () {
                    $("#create_popup").hide();
                });
            } else {
                this.successNotify("Saving Changes");
                console.log("userId", userId, sellerId, categoryId, eventId)
                Meteor.call('choices.insertOrRemove', userId, sellerId, categoryId, eventId, function (err, result) {
                    if (err) {
                        console.log(err);
                        toast.error("Can't save.", {
                            position: toast.POSITION.TOP_CENTER
                        });
                    } else {
                        console.log(result);
                        // toast.success("Saved.", {
                        //     position: toast.POSITION.TOP_CENTER
                        // });
                        //browserHistory.push("/mobile/user/category/"+ categoryId + '/' + eventId)
                    }
                }
                );
            }
        }
    }

    _handleChatButtonClick(e) {
        let username = Meteor.user().username;
        if (username === "guest") {
            browserHistory.push("/mobile/login");
        } else {
            const userId = Meteor.userId();
            const sellerId = this.props.sellerId;
            const sellerName = this.props.seller.name;
            const userName = Meteor.user().profile.name;
            Meteor.call('chat_with.insert', userId, sellerId, sellerName, userName);
            browserHistory.push('/mobile/user/chats');
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.seller !== prevProps.seller) {
            const seller = this.props.seller;
            this.setState({ sellerLogoImage: seller.profile.logoImage });
        }
    }

    componentDidMount() {
        const userId = Meteor.userId();
        const sellerId = this.props.sellerId;
        if (userId) {
            Meteor.call("seller_views.insert", userId, sellerId);
        }
    }

    handleImageError() {
        this.setState({ sellerLogoImage: 'https://via.placeholder.com/150' })
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.TOP_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.TOP_CENTER
        });
    };


    render() {
        const seller = this.props.seller;
        const margin = {
            margin: '0'
        };
        const video_div = {
            margin: '0 auto',
            width: '100%',
            height: '100%',
            minHeight: '100%',
            borderRadius: '4px',
            'box-shadow': '1px 2px 12px 0px #22222C'
        };

        if (!seller) {
            return <div className={"mobile-vendor-details"}></div>;
        }

        return (
            <div className={"mobile-vendor-details"}>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"mobile-vendor-name"}>{seller.name}</p>
                        <h1 className={"mobile-admin-name"}></h1>
                    </div>
                </div>
                <div className={"mobile-user-service-block"}>
                    <div className="mobile-business-profile-conainer no-padding">
                        <div className="mobile-mydash_bannner mobile-seller-mainImage-div">
                            <div className="">
                                <div className="banner deatls_carouel">
                                    <Carousel
                                        showArrows={true}
                                        showIndicators={false}
                                        autoPlay={true}
                                        stopOnHover={true}
                                        interval={5000}
                                        transitionTime={350} >
                                        {
                                            seller.profile.moreImages.map((url, index) => (
                                                <div className={"height_100 carousel_img"} key={index}>
                                                    <img height={"100%"} src={url} alt={""} />
                                                </div>
                                            ))
                                        }
                                        <div style={video_div}>
                                            <video
                                                src={seller.profile.video}
                                                preload="auto"
                                                controls
                                                controlsList={"nodownload"}
                                                style={{ width: '100%', height: '100%', borderRadius: '4px' }}
                                            />
                                        </div>
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        <div className="col-sm-12 col-md-12 col-xs-12 body_portion_ev res-no-padding">
                            <div className="mobile-event-right-side-element">
                                <div className="upper-section mobile-upper-section">
                                    <div className="mobile-button_evet">
                                        <button className="button_evet_left" onClick={this._handleAddChoice.bind(this)}>
                                            Choose
                                            <span className="mobile_user_service_circle_m">
                                                <img className="mychoice-star-logo"
                                                    src={this.props.choice ? "/img/tick_golden.svg" : ''} alt="" />
                                            </span>
                                        </button>
                                        <span className={"mobile_chat_now_img"}><img
                                            src={"/img/tabBarImg/roundChatIcon.svg"} /></span>
                                        <button
                                            className="button_evet_right"
                                            onClick={this._handleChatButtonClick.bind(this)}
                                        >
                                            Chat
                                        </button>
                                    </div>
                                    <div>
                                        <div className="mobile-logo-absolute">
                                            <img className={"mobile-circle-image"}
                                                src={this.state.sellerLogoImage} alt={"logo"}
                                                onError={this.handleImageError.bind(this)}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="black_container big-block no-padding relativeposition">
                                    <div className="check_box_item mobile-full-width-design">
                                        <div className="mobile-box-container">
                                            <ShowSellerRating seller={seller} />
                                            <div className="heading-text">
                                                <h3 style={margin} className={"h3class"}>{seller.name}</h3>
                                                <p className="mobile_eventLine"><img
                                                    src="/img/mobile_searcBox_line.png" /></p>
                                            </div>
                                            <h4 className={"pack-text"}>{seller.profile.address}</h4>
                                            <b className={"pack-small-text"}>{seller.profile.region}</b>
                                            <p className={"pack-small-text"}>{seller.profile.country}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="black_container big-block user-service-box margin_zero checkbox-div">
                                    <div className="mobile-userServiceDivBlock">
                                        <div className="mobile_facilities_wrapper">
                                            <ul>
                                                {this.renderSellerFacilities()}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container big-block no-padding">
                                    <div className="check_box_item mobile-full-width-design">
                                        <div className="mobile-box-container mobile-box-container1 .mobile-same-icon">
                                            <p className="star-print">
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                            </p>
                                            <div className="">
                                                <h2 className={"standard-menu"}>STANDARD MENU</h2>
                                                <p className="mobile_eventLine"><img
                                                    src="/img/mobile_searcBox_line.png" /></p>
                                            </div>
                                            <h4 className={"pack-text"}>{seller.profile.standardPack}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container big-block no-padding">
                                    <div className="check_box_item mobile-full-width-design">
                                        <div className="mobile-box-container mobile-box-container1 .mobile-same-icon">
                                            <p className="star-print">
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                            </p>
                                            <h2 className={"generous-pack"}>GENEROUS MENU</h2>
                                            <p className="mobile_eventLine"><img src="/img/mobile_searcBox_line.png" />
                                            </p>
                                            <h4 style={margin} className={"pack-text"}>
                                                {seller.profile.generousPack}
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container big-block no-padding">
                                    <div className="check_box_item mobile-full-width-design">
                                        <div className="mobile-box-container mobile-box-container1 .mobile-same-icon">
                                            <p className="star-print">
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                                <span>
                                                    <img className="service-star-logo" src="/img/whie-star.png" alt="" />
                                                </span>
                                            </p>
                                            <h2 className={"exclusive-text"}>EXCLUSIVE MENU</h2>
                                            <p className="mobile_eventLine"><img src="/img/mobile_searcBox_line.png" />
                                            </p>
                                            <h4 style={margin} className={"pack-text"}>
                                                {seller.profile.exclusivePack}
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container_text no-padding relativePosition">
                                    <div className="header_text">
                                        <h3>More Info</h3>
                                    </div>
                                </div>
                                <div className="black_container no-padding relativePosition">
                                    <div
                                        className="check_box_item mobile_last_service_block mobile-service-email-width-design">
                                        <div className="mobile-box-container email">
                                            <h2 className={"service-bottom-text"}>
                                                {seller.profile.email}
                                            </h2>
                                            <p className={"service-name"}>Mail</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container no-padding relativePosition">
                                    <div
                                        className="check_box_item mobile_last_service_block mobile-service-call-width-design">
                                        <div className="mobile-box-container contact">
                                            <h2 className={"service-bottom-text"}>
                                                {seller.profile.phone}
                                            </h2>
                                            <p className={"service-name"}>Phone</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="black_container no-padding relativePosition">
                                    <div
                                        className="check_box_item mobile_last_service_block mobile-service-internet-width-design">
                                        <div className="mobile-box-container website">
                                            <h2 className={"service-bottom-text"}>
                                                {seller.profile.website}
                                            </h2>
                                            <p className={"service-name"}>Website</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <ToastContainer />
                <div id={"create_popup"}>
                    <div className={"inner_div"}>
                        <p>Please create an event</p>
                        <button id={"create_popup_ok"}>OK</button>
                    </div>
                </div>
                <BottomTabBar />
            </div>
        )
    }
}

export default withTracker((data) => {
    const userId = Meteor.userId();
    let sellerId = '';
    let eventId = '';

    if (userId) {
        sellerId = data.sellerId;
        eventId = data.eventId;
    } else {
        sellerId = data.params.sellerId;
        eventId = data.params.eventId;
    }

    Meteor.subscribe('sellers.all');
    Meteor.subscribe('choices.all');

    let categoryId = '';
    let choice = '';

    const seller = Sellers.findOne({ "_id": sellerId });

    if (seller) {
        categoryId = seller.category._id;
    }

    choice = Choices.findOne({
        "user": userId,
        "seller": sellerId,
        "categoryId": categoryId,
        "eventId": eventId
    });

    return {
        seller: seller,
        choice: choice,
    }
})(Service);
