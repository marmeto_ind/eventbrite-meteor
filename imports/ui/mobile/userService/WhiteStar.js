import React from 'react';

class WhiteStar extends React.Component {
    render() {
        return (
            <img className="mobile-star-logo" src="/img/star.png" alt="" />
        )
    }
}

export default WhiteStar;