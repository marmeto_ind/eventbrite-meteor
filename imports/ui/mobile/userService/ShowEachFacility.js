import React from 'react';

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage: '',
        }
    }

    componentDidMount() {
        const facility = this.props.facility;
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    self.setState({facilityImage: result.image});
                }
            }
        });
    }

    render() {
        const facility = this.props.facility;
        // const mobileFacilitymainImage ="/img/parking.png";
        const mobileFacilitymainImage =this.state.facilityImage;

        const mobileFacilitydivStyle = {
            backgroundImage: 'url(' + mobileFacilitymainImage + ')',
            // backgroundRepeat: 'no-repeat',
            // backgroundSize: '15px 15px',
            // backgroundPosition: '45% 25%',
            // textOverflow: 'ellipsis',
            // overflow: 'hidden',
            // whiteSpace:'nowrap'
        };
        
        return (
            // <div className="freeparking div_same mobile-userServiceDiv">
            //     <div className="facility_div" style={mobileFacilitydivStyle}> <span className="facilityClass">{facility.name}</span></div>
            // </div>
            
            <li className="active">
                <img src={mobileFacilitydivStyle}/><br />
                <span>{facility.name}</span>
            </li>
        )
    }
}

export default ShowEachFacility;