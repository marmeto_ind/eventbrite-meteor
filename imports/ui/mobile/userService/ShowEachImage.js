import React from 'react';

class ShowEachImage extends React.Component {
    render() {
        const image = this.props.image;
        // console.log(image);

        return (
            <div className={"mobile-vendor-slideShowScrollDiv"}>
                <img className={"mobile-gallery-img"} src={image} alt="" />
            </div>
        )
    }
}

export default ShowEachImage;