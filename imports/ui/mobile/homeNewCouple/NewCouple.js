import React from 'react';
import './new-couple.scss';
import {browserHistory} from 'react-router';
import {Meteor} from 'meteor/meteor';

class NewCouple extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            showRepeatPassword: false
        }
    }

    componentDidMount() {
        const self = this;
        // Validation before submit
        
        $(document).ready(function () {
            $(".exist-text").hide()
            //$(".wrong-password").hide();
            $(".mandatory_text").hide();
            validateEmail = function (elementValue) {
                let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return emailPattern.test(elementValue);
            };
            
            $('#userEmail').change(function () {
                let value = $(this).val();
                let valid = validateEmail(value);
                if (!valid) {
                    $(".mobile-wrong-details").show();
                    $(".exist-text").hide();
                    $(".mobile-input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".mobile-password_input_field_empty").hide();
                    $(".mobile-username-field").hide();
                    $(".mobile-username_empty-field").hide();
                    $(".mobile-useremail_empty-field").hide();
                } else {
                    $(".mobile-wrong-details").hide();
                    $(".exist-text").hide();
                    $(".mobile-input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".mobile-password_input_field_empty").hide();
                    $(".mobile-username-field").hide();
                    $(".mobile-username_empty-field").hide();
                    $(".mobile-useremail_empty-field").hide();
                }
            });

            $('#userPassword').change(function () {
                let password = $("#userPassword").val();
                if (password.length < 4) {
                    $(".mobile-password_input_field_empty").show();
                    $(".wrong-password").hide();
                    $(".mobile-wrong-details").hide();
                    $(".exist-text").hide();
                    $(".mobile-input_field_empty").hide();
                    $(".mobile-username-field").hide();
                    $(".mobile-username_empty-field").hide();
                    $(".mobile-useremail_empty-field").hide();
                } else {
                    $(".mobile-password_input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".mobile-wrong-details").hide();
                    $(".exist-text").hide();
                    $(".mobile-input_field_empty").hide();
                    $(".mobile-username-field").hide();
                    $(".mobile-username_empty-field").hide();
                    $(".mobile-useremail_empty-field").hide();
                }
            });

        });
    }

    validateEmail(elementValue) {
        let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };

    _handleSignUp(e) {
        e.preventDefault();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();
        let repeat_password = $("#repeatPassword").val();
        let name = $("#userName").val();
        let phoneNumber = $("#userNumber").val();
        let email_Value_check = this.validateEmail(email);

        if (password !== repeat_password) {
            console.log({msg: "password not same as repeat password"});

            $(".mobile-password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").hide();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").show();
        } else if (email.length === 0 && password.length === 0) {
            console.log({msg: "email length and password length is zero"});

            $(".mobile-password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").show();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").hide();
            console.log(password.length);
        } else if (name.length === 0 && email.length === 0) {
            console.log({msg: "name and email length is zero"});

            $(".mobile-password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").hide();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").show();
            console.log(password.length);
        } else if (name.length === 0 && password.length === 0) {
            console.log({msg: "name and password length is zero"});

            $(".mobile-password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").hide();
            $(".mobile-username-field").show();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").hide();
            console.log(password.length);
        } else if (name.length === 0) {
            console.log({msg: "name length is zero"});

            $(".mobile-password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").hide();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").show();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").hide();
        } else if (!email_Value_check) {
            console.log({msg: "email value check is not true"});

            $(".mobile-wrong-details").show();
            $(".mobile-input_field_empty").hide();
            $(".exist-text").hide();
            $(".wrong-password").hide();
            $(".mobile-password_input_field_empty").hide();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").hide();
            console.log("Wrong Email Pattern");
        } else if (password.length < 4) {
            console.log({msg: "password length is less than 4"});

            $(".mobile-password_input_field_empty").show();
            $(".wrong-password").hide();
            $(".mobile-wrong-details").hide();
            $(".exist-text").hide();
            $(".mobile-input_field_empty").hide();
            $(".mobile-username-field").hide();
            $(".mobile-username_empty-field").hide();
            $(".mobile-useremail_empty-field").hide();
            $(".mobile-password-not-match-field").hide();
            console.log(password.length);
        } else {

            // Create Account if validated
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    phoneNumber: phoneNumber,
                    role: "user",
                }
            }, (err) => {
                if (err) {
                    console.log({err})
                    if (err.reason === "Password may not be empty") {
                        $(".wrong-password").show();
                        $(".exist-text").hide();
                        $(".mobile-wrong-details").hide();
                        $(".mobile-input_field_empty").hide();
                        $(".mobile-password_input_field_empty").hide();
                        $(".mobile-username-field").hide();
                    } else if (err.reason === "Email already exists.") {
                        $(".exist-text").show();
                        $(".wrong-password").hide();
                        $(".mobile-wrong-details").hide();
                        $(".mobile-input_field_empty").hide();
                        $(".mobile-password_input_field_empty").hide();
                        $(".mobile-username-field").hide();
                    }
                } else {
                    browserHistory.push('/mobile/user/chats');
                }
            });
        }
    }

    backToHome(e) {
        browserHistory.push('/mobile/home/first');
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    toggleShowRepeatPassword() {
        this.setState({showRepeatPassword: !this.state.showRepeatPassword})
    }

    render() {
        return (
            <div className="mobile_new_couple_signup">
                {/* <div className="new-couple-mobile-container">
                    <div className="new-couple-mobile-div-wrapper">
                        <div className="new-couple-mobile-login-info">
                            <div className="relative-div home-login-button-div  new_login_logo">
                                <div className="text-style">
                                    <img className="logo-home-first-img" src={"/img/homeImg/Asset 2.svg"}/>
                                </div>
                                <div className={"absolute-div-style"}>New User</div>
                            </div>

                            <h3 className={"mobile_user_exist_text"}>Email Already Exists</h3>
                            <h3 className="mobile-wrong-password">Please enter your password correctly</h3>
                            <h3 className="mobile-wrong-details">Please enter valid email address</h3>
                            <h3 className="mobile-input_field_empty">Please enter email and password</h3>
                            <h3 className="mobile-password_input_field_empty">You must provide at least 4 characters</h3>
                            <h3 className="mobile-username-field">Please Enter Username and password</h3>
                            <h3 className="mobile-username_empty-field">Please Enter Username</h3>
                            <h3 className="mobile-useremail_empty-field">Please Enter username and email</h3>
                            <h3 className="mobile-password-not-match-field">Passwords does not match</h3>
                            <div className="new-couple-login-form">
                                <form>
                                    <div className="username">
                                        <input type="text" id="userName" className="type-username"
                                               placeholder="ENTER USERNAME"/>
                                        <p>username</p>
                                    </div>
                                    <div className="email">
                                        <input type="email" id="userEmail" className="type-email"
                                               placeholder="ENTER EMAIL"/>
                                        <p>Mail</p>
                                    </div>
                                    <div className="password">
                                        <input type={!this.state.showPassword? "password": "text"} id="userPassword" className="type-password"
                                               placeholder="********"/>
                                        <p>password</p>
                                        <img src={!this.state.showPassword ? "/img/view.svg" : "/img/hide.svg"}
                                             className={"mobile_user_settings_signOut_text_logo"}
                                             onClick={this.toggleShowPassword.bind(this)}/>
                                    </div>
                                    <div className="password">
                                        <input type={!this.state.showRepeatPassword? "password": "text"} id="repeatPassword" className="type-password"
                                               placeholder="********"/>
                                        <p>Repeat Password</p>
                                        <img src={!this.state.showRepeatPassword ? "/img/view.svg" : "/img/hide.svg"}
                                             className={"mobile_user_settings_signOut_text_logo"}
                                             onClick={this.toggleShowRepeatPassword.bind(this)}/>
                                    </div>
                                    <div className="keepMeLoggedIn">
                                        <label>
                                            <input type="checkbox" name="remember"/>
                                            <span>Keep me logged in</span>
                                        </label>
                                    </div>
                                    <div className="relative-div start-plan-div">
                                        <button className="text-style home-login-btn"
                                                onClick={this._handleSignUp.bind(this)}>
                                            <img className="logo-home-first-img login_img_position"
                                                 src={"/img/svg_img/plusSvg.svg"}/>
                                            <div className={"absolute-div-style"}>Create</div>
                                        </button>
                                    </div>
                                    <div style={{paddingTop: '10px'}} className="relative-div home-login-button-div">
                                        <div className="text-style relative-div" onClick={this.backToHome.bind(this)}>
                                            <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"}/>
                                            <div className={"absolute-div-style back_top"}>Back</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> */}
                <div className="user-signup-wrapper">
                    <div className="user-signup-container new_couple_block">
                        <div className="user-signup-wrapper-content padding-top-0">
                            <div className="user-signup-login-info">
                                <div className="login-header">
                                    <span className="login-header-icon">
                                        <img className="logo-login-img"
                                            src={"/img/newService/new_user_icon.svg"}/></span>
                                    <h4 id={"text1"} className={"user-signup-login-text"}>New User</h4>
                                </div>

                                <h3 className="wrong-password">Please enter your password correctly</h3>
                                <h3 className="exist-text">Email Already Exists</h3>
                                <h3 className="wrong-details">Please enter valid email address</h3>
                                <h3 className="input_field_empty">Please enter email and password</h3>
                                <h3 className="password_input_field_empty">You must provide atleast 4
                                    characters</h3>
                                <h3 className="username_field">Please Enter Username</h3>
                                <div className="login-form user-login-form-style">
                                    <h5 className="mandatory_text">Please enter username and password</h5>
                                    <form>
                                        <div className="username-div">
                                            <input id="userName" type="text" className="type-username-sign"
                                                    placeholder="Enter Username"/>
                                            <p>username</p>
                                        </div>
                                        <div className="email">
                                            <input id="userEmail" type="text" className="type-email-sign"
                                                    placeholder="Email"/>
                                            <p>Mail</p>
                                        </div>
                                        <div className="password password_new">
                                            <input id="userPassword" type={!this.state.showPassword? "password": "text"}
                                                    className="type-password" maxLength={"32"}
                                                    placeholder="********" />
                                            <p>password</p>
                                            <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"} 
                                                alt="show password" 
                                                onClick={this.toggleShowPassword.bind(this)}
                                                className="show_password" />
                                        </div>
                                        
                                        <div className="password password_reenter">
                                            <input id="repeatPassword" type={!this.state.showRepeatPassword? "password": "text"}
                                                    className="type-password" maxLength={"32"}
                                                    placeholder="********" />
                                            <p>Repeat password</p>
                                            <img src={!this.state.showRepeatPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"} 
                                                alt="show password" 
                                                onClick={this.toggleShowRepeatPassword.bind(this)}
                                                className="show_password" />
                                        </div>
                                        
                                        <div className="remember_div">
                                            <label className="radio">
                                                <input name="radio" type="radio" />
                                                <span>Keep Me Logged In</span>
                                            </label>
                                        </div>

                                        <div className="button_wrapper">
                                            <button className="create_button_signup" onClick={this._handleSignUp.bind(this)}>
                                                <img className="logo-home-first-img" src="/img/tick.svg" />
                                                <div className="absolute_div_style_mobile">Create</div>
                                            </button>
                                            <button className="bck_button_signup" onClick={this.backToHome.bind(this)}>
                                                <img className="logo-home-first-img" src="/img/svg_img/backSvg.svg" />
                                                <div className="absolute_div_style_mobile">Back</div>
                                            </button>
                                        </div>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NewCouple;