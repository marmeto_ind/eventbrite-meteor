import React from 'react';
import './search-vendor.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Categories} from "../../../api/categories";
import {Regions} from "../../../api/regions";
import ShowEachCategory from '../userMyEvent/ShowEachSearchCategory';
import ShowEachRegion from './ShowEachRegion';
import ShowVendors from './ShowVendors';

class SearchVendor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryId:'',
            regionId: '',
            searchKey: '',
        };
    }

    renderCategories() {
        const categories = this.props.categories;
        return categories.map((category, index) => (
            <ShowEachCategory category={category} key={index}/>
        ))
    }

    // renderRegions() {
    //     const regions = this.props.regions;
    //     return regions.map((region, index) => (
    //         <ShowEachRegion region={region} key={index} />
    //     ))
    // }

    handleSearchInputChange(e) {
        console.log(e.target.value);
        this.setState({searchKey: e.target.value});
    }

    render() {
        const choiceDiv = {
            backgroundColor: 'black',
            marginBottom: '20px',
        };

        if(Meteor.userId()) {
            $("#searchPage_userName").show();
        }
        else if (!Meteor.userId()) {
            $("#searchPage_userName").hide();
        }

        return (
            <div>
                <div  className={"height_100"}>
                    {/*Header Bar*/}
                    <div className={"mobile-chat-conv-div-header"}>

                        <div className={"chat-conv-div"}>
                            <p className={"chattext"}>SEARCH</p>
                            <p id="searchPage_userName" className={"hanna-text"}>
                                {this.props.userName? <span className={"mobile_user_dot"}>.</span>: <span></span>}
                                {this.props.userName}
                            </p>
                        </div>

                    </div>

                    {/*Body part */}
                    <div className={"relativeposition"}>
                        <form>
                            <div className={"mobile-search-div"}>
                                <ul className={"list-group li-box"}>
                                    <li className={"list-group-item paddingBottom"}>
                                        <div className={"row"}>
                                            <input type={"text"}
                                                   className={"mobile-user-search"}
                                                   placeholder={"Search Vendor"}
                                                   onChange={this.handleSearchInputChange.bind(this)}
                                            />
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div className={"mobile-search-icon-div mobile_user_search_vendor_block_img"}>
                                <img className={"mobile-search-img"} src={"/img/vendor-search.png"} alt={"search"} />
                                {/*<hr className={"mobile_search_belowLine"} />*/}
                                {/*<p className={"mobile_searchLinePMargin"}> <img className={"mobile_searchLine"} src={"/img/mobile_search_line.png"} /></p>*/}
                            </div>
                        </form>
                    </div>

                    {/*<div className={"mobile-search-results-div mobile-search-results-vendor--div"}>*/}
                        {/*<div className="mobile-search-top-body-container  relativeposition">*/}
                            {/*<ShowVendors*/}
                                {/*categoryId={this.state.categoryId}*/}
                                {/*regionId={this.state.regionId}*/}
                                {/*searchKey={this.state.searchKey}*/}
                            {/*/>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    <div className={"mobile-user-my-events-search-results-div"}>
                        <ul className={"list-group"}>
                            {this.renderCategories()}
                        </ul>
                    </div>


                    <BottomTabBar/>

                </div>
            </div>
        )
    }
}

// export default SearchVendor;

export default withTracker(() => {
    Meteor.subscribe('categories.all');
    Meteor.subscribe('regions.all');
    const categories = Categories.find({}).fetch();
    const regions = Regions.find({}).fetch();
    const user = Meteor.user();
    let userName = '';
    if(user) {
        userName = user.profile.name;
    } else {
        userName = '';
    }
    return {
        categories: categories,
        regions: regions,
        userName: userName,
    }
})(SearchVendor);