import React, {Component} from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import ShowEachVendor from './ShowEachVendor';
import {Regions} from "../../../api/regions";

const choiceDiv = {
    // backgroundColor: 'black',
    marginBottom: '20px',
};

class ShowVendors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderSellers() {
        const sellers = this.props.sellers;

        return sellers.map((seller, index) => (
            <ShowEachVendor seller={seller} key={index} />
        ))
    }

    render() {
        const sellers = this.props.sellers;

        return (
            <div>
                {this.renderSellers()}
            </div>
        )
    }
}

export default withTracker((data) => {
    const categoryId = data.categoryId;
    const regionId = data.regionId;
    const searchKey = data.searchKey;

    let query = {};

    let sellers = [];

    Meteor.subscribe('regions.all');
    let regionName = '';

    if(regionId) {
        const region = Regions.findOne({"_id" : regionId});
        if(region) {
            regionName = region.name;
        }
    }


    if(categoryId) {
        query["category._id"] = categoryId
    }
    if(regionName) {
        // now region is not associated with any seller
        // query["profile.address"] = regionName
    }
    if(searchKey) {
        query["name"] = {$regex : searchKey, '$options' : 'i'}
    }

    Meteor.subscribe('sellers.all');

    sellers = Sellers.find(query).fetch();

    return {
        sellers: sellers,
    }
})(ShowVendors);