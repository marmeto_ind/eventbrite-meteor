import React, {Component} from 'react';

class ShowEachCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const category = this.props.category;
        return (
            <option value={category._id}>{category.name}</option>
        )
    }
}

export default ShowEachCategory;