import React from 'react';
import './search-vendor.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Categories} from "../../../api/categories";
import {Regions} from "../../../api/regions";
import ShowEachCategory from './ShowEachCategory';
import ShowEachRegion from './ShowEachRegion';
import ShowVendors from './ShowVendors';

class SearchVendor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryId:'',
            regionId: '',
            searchKey: '',
        };
    }

    handleCategoryChange() {
        const categoryId = $("#category-select").val();
        this.setState({categoryId: categoryId});
    }

    handleRegionChange() {
        // const options = document.getElementById('region-select').options;
        const regionId = $("#region-select").val();
        this.setState({regionId: regionId});
    }

    renderCategories() {
        const categories = this.props.categories;
        return categories.map((category, index) => (
            <ShowEachCategory category={category} key={index}/>
        ))
    }

    renderRegions() {
        const regions = this.props.regions;
        return regions.map((region, index) => (
            <ShowEachRegion region={region} key={index} />
        ))
    }

    handleSearchInputChange(e) {
        // console.log(e.target.value);
        this.setState({searchKey: e.target.value});
    }

    render() {
        const choiceDiv = {
            backgroundColor: 'black',
            marginBottom: '20px',
        };

        if(Meteor.userId()) {
            $("#searchPage_userName").show();
        }
        else if (!Meteor.userId()) {
            $("#searchPage_userName").hide();
            }

            // console.log(this.props.categoryId);
        const categoryId = this.props.categoryId;

        return (
            <div>
                <div  className={"height_100"}>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>

                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>VENDOR LIST</p>
                        <p id="searchPage_userName" className={"hanna-text"}>
                            {this.props.userName? <span className={"mobile_user_dot"}>.</span>: <span></span>}
                            {this.props.userName}
                        </p>
                    </div>

                </div>

                {/*Body part */}
                <div className={"relativeposition"}>
                <form>
                <div className={"mobile-search-div"}>
                </div>

                  
                </form>
                </div>

                <div className={"mobile-search-results-div mobile-search-results-vendor--div"}>
                    <div className="mobile-search-top-body-container  relativeposition">
                        <ShowVendors
                            categoryId={categoryId}
                            regionId={this.state.regionId}
                            searchKey={this.state.searchKey}
                        />
                    </div>
                </div>


                <BottomTabBar/>

            </div>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('categories.all');
    Meteor.subscribe('regions.all');
    const categories = Categories.find({}).fetch();
    const regions = Regions.find({}).fetch();
    const user = Meteor.user();
    let userName = '';
    if(user) {
        userName = user.profile.name;
    } else {
        userName = '';
    }
    return {
        categories: categories,
        regions: regions,
        userName: userName,
    }
})(SearchVendor);