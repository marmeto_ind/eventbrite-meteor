import React, {Component} from 'react';

class ShowEachRegion extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const region = this.props.region;
        return (
            <option value={region._id}>{region.name}</option>
        )
    }
}

export default ShowEachRegion;
