import React from 'react';
import GoldenStar from './GolderStar';
import WhiteStar from './WhiteStar';
import ShowFacilityIcons from './ShowFacilityIcons';
import {browserHistory} from 'react-router';

class ShowEachVendor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: '',
        };
    }

    componentDidMount() {
        const self = this;
        const seller = this.props.seller;
        Meteor.call('reviews.getSellerRating', seller._id, function (err, result) {
            if(err) {
                console.error(err)
            } else {
                // console.log(result);
                self.setState({review: result});
            }
        })
    }

    renderSellerFacilities(facilities) {
        return facilities.map((facility, index)=>(
            <ShowFacilityIcons facility={facility} key={index} />
        ))
    }

    renderRating(){
        const review = this.state.review;
        const extra = 5 - review;
        var reviewComponent = [];

        for (var i=0; i < review; i++) {
            reviewComponent.push(<GoldenStar key={i} />);
        }
        for (var j=0; j < extra; j++) {
            reviewComponent.push(<WhiteStar key={i + j} />);
        }
        return <div className="rating">{reviewComponent}</div>;
    }

    handleSellerClick() {
        const seller = this.props.seller;
        const sellerId = seller._id;
        // 1234 is dummy event id against which we need to check in seller page
        if(Meteor.userId()) {
            browserHistory.push("/mobile/user/service/" + sellerId + "/search_page");
        } else {
            browserHistory.push("/mobile/user/service/" + sellerId + "/search_page/guest");
        }
    }

    render() {
        const seller = this.props.seller;
        const mobile_vendor_backGroundImg = seller.profile.mainImage;
        const choiceDiv = {
            backgroundImage: 'url(' + mobile_vendor_backGroundImg + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            marginBottom: '10px',
            minHeight: '167px',
            height:'167px',
        };


        return (
            <div>
                <div  style={choiceDiv} className="vale_container">
                   

                    <div className="mychoice_data  my-data" onClick={this.handleSellerClick.bind(this)}>
                        <div className="value-main-content">
                            <div className={"shadow-box-main-div"}>
                                <div className={"mobile_grey_background"}>
                                </div>
                                <div className={"shadow-box-div "}>
                                <div>
                                    <img src={seller.profile.logoImage} className={"mobile_vendor_logo"} />
                                </div>
                                                <div className="circle-star-margin">
                                        <div className="rating">
                                            {this.renderRating()}
                                            {/*<span><img className="star-logo" src="/img/10.png" alt="" />*/}
                                            {/*</span><span><img className="star-logo" src="/img/10.png" alt="" />*/}
                                            {/*</span><span><img className="star-logo" src="/img/10.png" alt="" />*/}
                                            {/*</span><span><img className="star-logo" src="/img/11.png" alt="" />*/}
                                            {/*</span><span><img className="star-logo" src="/img/11.png" alt="" /></span>*/}
                                        </div>
                                    </div>
                                    <img src={"/img/mobile_searcBox_line.png"} />

                                    <h3 className={"choice-text-style"}>{seller.name}</h3>
                                    <p className={"choice-text-style"}>{seller.profile.address}</p>
                                    <div className="icon_div">

                                        {this.renderSellerFacilities(seller.profile.facilities)}

                                    </div>
                                </div>
                            </div>
                            {/* <div className="div-float second-choice-div">
                                <p className={"second-best-deal best-deal"}>Best Deal</p>
                                <p className={"second-hot hot"}>Hot</p>
                            </div> */}
                            {/*<div className={"mobile_category_seller_tick"}>*/}
                                {/*<img*/}
                                    {/*src={"/img/show-tick.png"}*/}
                                {/*/>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowEachVendor;