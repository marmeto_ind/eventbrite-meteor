import React from 'react';
import './new-users.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import ShowUsers from './ShowUsers';

class NewUsers extends React.Component {
    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-service-div"}>
                        <p className={"chattext"}>New Users</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: {this.props.adminName}</p>
                    </div>
                </div>

                <ShowUsers/>

                <AdminTabBar/>
            </div>
        )
    }
}

// export default NewUsers;

export default withTracker(() => {
    const adminName = Meteor.user()? Meteor.user().profile.name: '';
    return {
        adminName: adminName,
    }
})(NewUsers);