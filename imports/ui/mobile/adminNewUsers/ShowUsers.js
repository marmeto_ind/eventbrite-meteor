import React from 'react';
import {Meteor} from 'meteor/meteor';
import ShowEachUser from './ShowEachUser';

class ShowUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
        };
    }

    componentDidMount() {
        // console.log("show users");
        const self = this;
        Meteor.call('users.getAllUsers', function (err, result) {
            if(err) {
                console.log(err);
            } else {
                // console.log(result);
                self.setState({users: result});
            }
        })
    }

    renderUsers() {
        return this.state.users.map((user, index) => (
            <ShowEachUser user={user} key={index}/>
        ))
    }

    render() {
        // console.log(this.state.users);

        return (
            <div className={"mobile-adminNewUsers-block"}>
                <ul>
                    {this.renderUsers()}
                    {/*<li className={"list-group-item mobile-admin-chat-list"}>*/}
                        {/*<div className={"admin-chatbox"}>*/}
                            {/*<div className={"admin-chatImg"}>*/}
                                {/*<img src={"/img/user-login.png"} alt={"Img"} />*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatBody"}>*/}
                                {/*<p className={"firstPclass"}>Josef Maria</p>*/}
                                {/*<p className={"secondPclass"}>StockHolem</p>*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatCount"}>*/}
                                {/*<p>Thursday</p>*/}
                                {/*<p>11:45</p>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</li>*/}
                    {/*<li className={"list-group-item mobile-admin-chat-list"}>*/}
                        {/*<div className={"admin-chatbox"}>*/}
                            {/*<div className={"admin-chatImg"}>*/}
                                {/*<img src={"/img/user-login.png"} alt={"Img"} />*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatBody"}>*/}
                                {/*<p className={"firstPclass"}>Tony Alexandara</p>*/}
                                {/*<p className={"secondPclass"}>StockHolem</p>*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatCount"}>*/}
                                {/*<p>Thursday</p>*/}
                                {/*<p>11:45</p>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</li>*/}
                    {/*<li className={"list-group-item mobile-admin-chat-list"}>*/}
                        {/*<div className={"admin-chatbox"}>*/}
                            {/*<div className={"admin-chatImg"}>*/}
                                {/*<img src={"/img/user-login.png"} alt={"Img"} />*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatBody"}>*/}
                                {/*<p className={"firstPclass"}>Freddi Lissa</p>*/}
                                {/*<p className={"secondPclass"}>StockHolem</p>*/}
                            {/*</div>*/}
                            {/*<div className={"admin-chatCount"}>*/}
                                {/*<p>Thursday</p>*/}
                                {/*<p>11:45</p>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</li>*/}
                </ul>

            </div>
        )
    }
}

export default ShowUsers;