import React from 'react';

class ShowEachUser extends React.Component {
    render() {
        const user = this.props.user;

        const createdAt = user.createdAt.toString();
        const day = user.createdAt.getDay();

        // console.log(createdAt);


        let weekday = new Array(7);
        weekday[0] =  "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";

        return (
            <li className={"list-group-item mobile-admin-chat-list"}>
                <div className={"admin-chatbox"}>
                    <div className={"admin-chatImg"}>
                        <img src={"/img/user-login.png"} alt={"Img"} />
                    </div>
                    <div className={"admin-chatBody"}>
                        <p className={"firstPclass"}>{user.profile.name}</p>
                        <p className={"secondPclass"}>StockHolem</p>
                    </div>
                    <div className={"admin-chatCount"}>
                        <p>{weekday[day]}</p>
                        <p>{createdAt.substring(16, 21)}</p>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachUser;