import React from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {Session} from 'meteor/session';
import './vendor_login_signup.scss';


class MobileVendorLogin extends React.Component {
    _handleLogin(e) {
        e.preventDefault();
        let email = $('#vendorEmail').val();
        let password = $('#vendorPassword').val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                console.log(user.profile.role === "vendor");
                if(user.profile.role === "vendor") {
                    console.log("successfully loggedIn");
                    const user = Meteor.user();
                    const userId = user._id;
                    const userProfile = user.profile.role;
                    Session.set({
                        userId: userId,
                        userProfile: userProfile,
                    });
                    browserHistory.push('/mobile/vendor/chats');
                } else {
                    Meteor.logout(function (err) {
                        throw err;
                    });
                    console.log("Not authorized");
                }
            } else {
                alert("incorrect credential");
                throw err;
            }
        })
    }


    render() {
        return (
            <div className={"mobile_vendor_login_signup"}>
            <div className="home-login-mobile-wrapper">
                <div className="home-login-mobile-container">
                    <div className="home-login-mobile-div-wrapper">
                        <div className="home-login-mobile-login-info">
                            <div className="relative-div">
                                <img className="home-login-img" src={"/img/login-01.png"} />
                                <div className={"home-login-absolute-div-style"}><span>Login</span></div>
                            </div>
                            <div className="home-login-form">
                                <form>
                                    {/*<div className="username">*/}
                                    {/*<input type="text" className="type-username " placeholder="USERNAME" />*/}
                                    {/*<p>username</p>*/}
                                    {/*</div>*/}
                                    <div className="email">
                                        <input type="text" id="vendorEmail" className="type-email " placeholder="EMAIL" />
                                        <p>Mail</p>
                                    </div>
                                    <div className="password">
                                        <input type="password" id="vendorPassword" className="type-password" placeholder="********" />
                                        <p>password</p>
                                    </div>

                                    <div className="relative-div home-login-button-div">
                                        {/*<input type="submit" className="text-style" value="New Couple" />*/}
                                        <button onClick={this._handleLogin.bind(this)} className="text-style home-login-btn">
                                            <img className="logo-home-first-img" src={"/img/login-01.png"} />
                                        </button>
                                        <div className={"absolute-div-style"}>Log In</div>
                                    </div>

                                    <div className="relative-div home-welcome-img-div">
                                        {/*<input type="submit" className="text-style" value="New Service" />*/}
                                        <div><img  src={"/img/leaf.png"} /></div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

export default MobileVendorLogin;