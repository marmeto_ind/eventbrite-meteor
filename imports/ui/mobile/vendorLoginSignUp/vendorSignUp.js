import React from 'react';
import  {Accounts} from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import {Session} from 'meteor/session';
import './vendor_login_signup.scss';


class MobileVendorSignup extends React.Component {

    _handleSignUp(e) {
        e.preventDefault();
        let email = $("#vendorEmail").val();
        let password = $("#vendorPassword").val();
        let name = $("#vendorName").val();
        let phoneNumber = $("#vendorPhone").val();

        Accounts.createUser({
            email,
            password,
            profile: {
                name: name,
                phoneNumber: phoneNumber,
                role: "vendor",
            } }, (err) => {
            if(err) {
                throw err;
            } else {
                console.log('vendor signup succesfull');
                const user = Meteor.user();
                const userId = user._id;
                const userProfile = user.profile.role;
                Session.set({
                    userId: userId,
                    userProfile: userProfile,
                });
                browserHistory.push('/mobile/vendor/chats');
            }
        });
    }


    render() {
        return (
            <div className={"mobile_vendor_login_signup"}>
            <div className={"mobile_vendor_login"}>
                <div className="home-login-mobile-wrapper">
                    <div className="home-login-mobile-container">
                        <div className="home-login-mobile-div-wrapper">
                            <div className="home-login-mobile-login-info">
                                <div className="relative-div">
                                    <img className="home-login-img" src={"/img/logo_tick.png"} />
                                    <div className={"home-login-absolute-div-style"}><span>Sign up</span></div>
                                </div>
                                <div className="home-login-form">
                                    <form>
                                        {/*<div className="username">*/}
                                        {/*<input type="text" className="type-username " placeholder="USERNAME" />*/}
                                        {/*<p>username</p>*/}
                                        {/*</div>*/}
                                        <div className="username mobile_vendor_user_signUp ">
                                            <input type="text" id="vendorName" className="type-name" placeholder="Username" />
                                            <p>Username</p>
                                        </div>
                                        <div className="password">
                                            <input type="password" id="vendorPassword" className="type-password" placeholder="********" />
                                            <p>password</p>
                                        </div>
                                        <div className="email">
                                            <input type="text" id="vendorEmail" className="type-email " placeholder="EMAIL" />
                                            <p>Mail</p>
                                        </div>
                                        <div className="phone posrel">
                                            <input type="password" id="vendorPhone" className="type-phone" placeholder="PhoneNumber" />
                                            <p>PhoneNumber</p>
                                        </div>

                                        <div className="relative-div home-login-button-div mobile_vendor_signUpButton">
                                            {/*<input type="submit" className="text-style" value="New Couple" />*/}
                                            <button  className="text-style home-login-btn"  onClick={this._handleSignUp.bind(this)}>
                                                <img className="logo-home-first-img" src={"/img/logo_tick.png"} />
                                                <div className={"absolute-div-style"}>SignUp</div>
                                            </button>
                                        </div>

                                        {/*<div className="relative-div home-welcome-div">*/}
                                            {/*/!*<input type="submit" className="text-style" value="New Service" />*!/*/}
                                            {/*<div><img className="logo-home-first-img" src={"/img/leaves.png"} /></div>*/}
                                        {/*</div>*/}
                                        <div className="relative-div relativeposition">
                                            {/*<input type="submit" className="text-style" value="New Service" />*/}
                                            <div className={"new-couple-welcome-img-div"}><img  src={"/img/leaf.png"} /></div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

export default MobileVendorSignup;