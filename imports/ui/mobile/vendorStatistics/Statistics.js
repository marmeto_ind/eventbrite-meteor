import React from 'react';
import './statistics.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import VendorChoiceStats from './VendorChoiceStats';
import VendorViewStats from './VendorViewStats';
import VendorReviewStats from './VendorReviewStats';

class Statistics extends React.Component {
    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>Statistics</p>
                        <p className={"hanna-text"}><span className={"mobile_vendor_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>

                    </div>
                </div>

                <VendorBottomTabBar/>

                <div className={"mobileVendorStatsBlock"}>
                    {/*<div className={"second-div choices-background "}>*/}
                        {/*<ul className="list-group vendor-stats-list">*/}
                            {/*<li className="list-group-item div-left-icon">*/}
                                {/*<img className="create-add all-div-icon " src="/img/show-tick.png" alt="" />*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text1 mstats-margin">*/}
                             {/*8 C/M*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                                {/*<hr className={"hr-width"}/>*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                                {/*2 CHOICES LAST WEEK*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                                {/*1567 IN TOTAL SINCE START*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text4">*/}
                                {/*(1)*/}
                            {/*</li>*/}
                        {/*</ul>*/}
                    {/*</div>*/}
                    <VendorChoiceStats/>

                    {/*<div className={"third-div views-background"}>*/}
                        {/*<ul className="list-group vendor-stats-list">*/}
                            {/*<li className="list-group-item div-left-icon">*/}
                                {/*<img className="create-add all-div-icon " src="/img/agenda-view.png" alt="" />*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text1 mstats-margin">*/}
                                {/*2 V/M*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                                {/*<hr className={"hr-width"}/>*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                                {/*5 VIEW LAST WEEK*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                                {/*345 IN TOTAL SINCE START*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text4">*/}
                                {/*(430)*/}
                            {/*</li>*/}
                        {/*</ul>*/}
                    {/*</div>*/}
                    <VendorViewStats/>

                    {/*<div className={"fourth-div"}>*/}
                        {/*<ul className="list-group vendorstats-listBackground background_White">*/}
                            {/*<li className="list-group-item div-left-icon">*/}
                                {/*<img className="create-add all-div-icon " src="/img/agenda-star.png" alt="" />*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text1">*/}
                                {/*5 R/M*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                                {/*<hr className={"hr-width"}/>*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                                {/*12 REVIEW LAST WEEK*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                               {/*123 IN TOTAL SINCE START*/}
                            {/*</li>*/}
                            {/*<li className="list-group-item text-list-div-style all-div-text4">*/}
                                {/*(1)*/}
                            {/*</li>*/}
                        {/*</ul>*/}
                    {/*</div>*/}

                    <VendorReviewStats/>
                </div>
            </div>
        )
    }
}

export default Statistics;