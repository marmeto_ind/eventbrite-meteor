import React from 'react';

class VendorReview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const self = this;
        const userId = Meteor.userId();
        Meteor.call('reviews.getReviewStats', userId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                if(result) {
                    self.setState({
                        totalNumOfView: result.totalNumOfView,
                        numOfViewInLastWeek: result.numOfViewInLastWeek,
                        numOfViewInMonth: result.numOfViewInMonth,
                    })
                }
            }
        });
    }

    render() {
        return (
            <div className={"fourth-div"}>
                <ul className="list-group vendorstats-listBackground background_White">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/agenda-star.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {this.state.numOfViewInMonth} R/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                    <p className="mobile_admin_line"><img src="/img/mobile_searcBox_line.png" /></p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {this.state.numOfViewInLastWeek} REVIEW LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {this.state.numOfViewInLastWeek} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default VendorReview;