import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Choices} from "../../../api/choice";

class VendorChoice extends React.Component {
    render() {
        const totalNumOfChoice = this.props.totalNumOfChoice;
        const numOfChoiceInLastWeek = this.props.numOfChoiceInLastWeek;
        const numOfChoiceInMonth = this.props.numOfChoiceInMonth;

        // console.log(totalNumOfChoice);
        // console.log(numOfChoiceInLastWeek);
        // console.log(numOfChoiceInMonth);

        return (
            <div className={"second-div choices-background "}>
                <ul className="list-group vendor-stats-list">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/show-tick.png" alt="" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1 moile_vendor_stats_margin_top">
                        {numOfChoiceInMonth} C/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                    <p className="mobile_admin_line"><img src="/img/mobile_searcBox_line.png" /></p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 moile_vendor_stats_margin_top ">
                        {numOfChoiceInLastWeek} CHOICES LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfChoice} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4 moile_vendor_stats_margin_top">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

// export default VendorChoice;

export default withTracker(()=> {
    Meteor.subscribe('choices.all');
    Meteor.subscribe('sellers.all');
    const userId = Meteor.userId();
    // console.log(userId);
    const seller = Sellers.findOne({"user": userId});

    var sellerId = '';
    if(seller) {
        sellerId = seller._id;
        // console.log(sellerId);
    }

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfChoice = Choices.find({"seller": sellerId}).count();
    const numOfChoiceInLastWeek = Choices.find({"seller": sellerId, "createdAt": {"$gt": new Date(oneWeekAgo)}}).count();
    const numOfChoiceInMonth = Choices.find({"seller": sellerId, "createdAt": {"$gt": new Date(oneMonthAgo)}}).count();

    return {
        totalNumOfChoice: totalNumOfChoice,
        numOfChoiceInLastWeek: numOfChoiceInLastWeek,
        numOfChoiceInMonth: numOfChoiceInMonth,
    }
})(VendorChoice);