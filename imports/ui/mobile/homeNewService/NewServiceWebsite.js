import React from 'react';
import './new-service-website.scss';
import {ToastContainer, toast} from 'react-toastify';
import Header from '../../components/header/Header';
import Progress from 'react-progressbar';
import CategoryDropDown from './CategoryDropDown';
import EventTypeDropDown from './EventTypeDropDown';
import ShowSellerFacilities from './ShowSellerFacilities';
import {Accounts} from 'meteor/accounts-base';
import {browserHistory} from 'react-router';
import ReactPlayer from 'react-player';
import {Meteor} from 'meteor/meteor';
import Footer from '../../components/footer/Footer';
import '../../pages/homePage/allpopup.scss';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import axios from 'axios';

class NewService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainPictureUrl: '',
            logoUrl: '',
            morePicture: [],
            videoUrl: '',
            mainImageUploadProgress: 0,
            logoImageUploadProgress: 0,
            videoUploadProgress: 0,
            morePictureUploadProgress: 0,
            showNewPassword: false,
            showRepeatNewPassword: false,
            isCategoryVenue: false,
            country: '',
            region: '',
            facilities: [],
            eventTypes: [],
            termsToggle:false,
            showTermCondPopup: false,
            acceptTermsCondition: false,
            radioChecked: false
        };
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleMainImageUpload = this._handleMainImageUpload.bind(this);
        this._handleMorePictureUpload = this._handleMorePictureUpload.bind(this);
        this._handleVideoUpload = this._handleVideoUpload.bind(this);
        this._handleLogoUpload = this._handleLogoUpload.bind(this);
        this.popupFuction = this.popupFuction.bind(this);
        this.handleTermsConditionClick = this.handleTermsConditionClick.bind(this);
        this.handleTermsConditionYesClick = this.handleTermsConditionYesClick.bind(this);
        this.handleTermsConditionNoClick = this.handleTermsConditionNoClick.bind(this);
        this.toggleRadioChecked = this.toggleRadioChecked.bind(this);
    }

    componentDidMount() {
        axios.get('/api/facilities')
            .then((res) => {
                const facilities = res.data;
                this.setState({ facilities });
            })
            .catch(function (error) {
                console.log(error);
            });

        axios.get('/api/event-types')
            .then((res) => {
                const eventTypes = res.data;
                this.setState({ eventTypes });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    popupFuction(){}

    handleTermsConditionClick(event){
        event.preventDefault();
        this.setState({showTermCondPopup: !this.state.showTermCondPopup})
    }

    handleTermsConditionYesClick(event){
        event.preventDefault();
        this.setState({acceptTermsCondition: true})
        this.setState({showTermCondPopup: !this.state.showTermCondPopup})
        this.setState({radioChecked: true})
    }

    handleTermsConditionNoClick(event){
        event.preventDefault();
        this.setState({showTermCondPopup: !this.state.showTermCondPopup})
        this.setState({radioChecked: false})
    }
    
    selectCountry (val) {
        this.setState({ country: val });
    }

    selectRegion (val) {
        this.setState({ region: val });
    }

    _handleMainImageUpload() {
        const mainPictureFile = $("#vendorMainPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify(error.message);
            } else {
                that.setState({mainPictureUrl: downloadUrl});
                that.uploadSuccessNotify("Main Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({mainImageUploadProgress: uploader.progress() * 100});
            }
        })
    }

    // backToHome(e) {
    //     browserHistory.push('/mobile/home/first');
    // }

    // Upload logo and store the url to state variable
    _handleLogoUpload() {
        const logo = $("#vendorLogo")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(logo, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                that.uploadErrorNotify(error.message);
                console.error(error);
            } else {
                that.setState({logoUrl: downloadUrl});
                that.uploadSuccessNotify("Logo Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({logoImageUploadProgress: uploader.progress() * 100});
            }
        })
    }

    // Upload video and store the url to state variable
    _handleVideoUpload() {
        const video = $("#vendorVideo")[0].files[0];
        const uploader = new Slingshot.Upload("myVideoUploads");
        let that = this;
        uploader.send(video, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify(error.message);
            } else {
                that.setState({videoUrl: downloadUrl});
                that.uploadSuccessNotify("Video");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({videoUploadProgress: uploader.progress() * 100});
            }
        })
    }

    // Upload more pictures and store the picture url to state variable
    _handleMorePictureUpload() {
        const morePictures = $("#mobileMorePictutres")[0].files;
        if (morePictures.length > 6) {
            this.errorNotify("you can upload max 6 other pictures")
        } else {
            const that = this;
            _.map(morePictures, function (file) {
                let uploader = new Slingshot.Upload("myImageUploads");
                uploader.send(file, function (err, downloadUrl) {
                    computation.stop();
                    if (err) {
                        that.setState({morePictureUploadProgress: 0});
                        that.uploadErrorNotify("Picture");
                        console.log(err)
                    } else {
                        const morePictureArray = that.state.morePicture;
                        that.setState({morePictureUploadProgress: 0});
                        morePictureArray.push(downloadUrl);
                        that.setState({morePicture: morePictureArray})
                        that.uploadSuccessNotify("Picture");
                    }
                });
                // Track progress
                let computation = Tracker.autorun(() => {
                    if (!isNaN(uploader.progress())) {
                        that.setState({morePictureUploadProgress: uploader.progress() * 100});
                    }
                })
            });
        }
    }

    uploadSuccessNotify = (string) => {
        toast.success("Successfully uploaded " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    uploadErrorNotify = (string) => {
        toast.error("Error while uploading " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    _handleSubmit(e) {
        e.preventDefault();

        // fetch all data from the form
        const password = $("#password").val();
        const repeatPassword = $("#repeatPassword").val();
        const name = $("#name").val();
        const address = $("#address").val();

        const { country, region } = this.state;

        const area = $("#area").val();
        const ceilingHeight = $("#ceilingHeight").val();
        const maxPeople = $("#maxPeople").val();

        const standardPack = $("#standardPack").val();
        const generousPack = $("#generousPack").val();
        const exclusivePack = $("#exclusivePack").val();

        const categoryId = $("#category").val();
        const categoryName = $("#category option:selected").text();

        const eventTypeId = $("#eventType").val();
        const eventTypeName = $("#eventType option:selected").text();

        const email = $("#vendorEmail").val();
        const vendorPhone = $("#vendorNumber").val();
        const websiteName = $("#vendorWeb").val();

        const category = {_id: categoryId, name: categoryName};
        const eventType = {_id: eventTypeId, name: eventTypeName};

        const mainImage = this.state.mainPictureUrl;
        const logoImage = this.state.logoUrl;
        const moreImages = this.state.morePicture;
        const sellerVideo = this.state.videoUrl;

        // Initialized seller facilities object
        const sellerFacilities = [];
        const selectedFacilities = $("input:checked");

        // get the id the the selected facilities and save it
        for (let index = 0; index < selectedFacilities.length; ++index) {
            let item = selectedFacilities[index];
            let itemId = item.id;

            let jquerySelector = "label[for='" + itemId.toString() + "']";
            let value = $(jquerySelector).text();
            let facility = {};

            facility["_id"] = item.id;
            facility["name"] = value;

            sellerFacilities.push(facility);
        }

        if (this.state.isCategoryVenue) {
            if (!area) {
                this.errorNotify("Area is empty");
            } else if (!ceilingHeight) {
                this.errorNotify("Ceiling height is empty");
            } else if (!maxPeople) {
                this.errorNotify("Max People field is empty");
            } else if (!eventType) {
                this.errorNotify("Event type is not selected");
            }
        }

        if (!name) {
            this.errorNotify("User name is empty");
        } else if (!password) {
            this.errorNotify("Password is empty");
        } else if (password !== repeatPassword) {
            this.errorNotify("Password and Repeat Password does not match");
        } else if (!category) {
            this.errorNotify("Category is empty");
        } else if (!mainImage) {
            this.errorNotify("Main image is not uploaded");
        } else if (!logoImage) {
            this.errorNotify("Logo image is not uploaded");
        } else if (!moreImages) {
            this.errorNotify("Other Images are not uploaded");
        } else if (!sellerVideo) {
            this.errorNotify("Video is not uploaded");
        } else if (!address) {
            this.errorNotify("Please fill address field");
        } else if (!region) {
            this.errorNotify("Please fill region field");
        } else if (!country) {
            this.errorNotify("Please fill country field");
        } else if (!standardPack) {
            this.errorNotify("Standard pack field is empty");
        } else if (!generousPack) {
            this.errorNotify("Generous pack field is empty");
        } else if (!exclusivePack) {
            this.errorNotify("Exclusive pack field is empty");
        } else if (!email) {
            this.errorNotify("Email field is empty");
        } else if (!vendorPhone) {
            this.errorNotify("Vendor phone is empty");
        } else if (!websiteName) {
            this.errorNotify("Website name is empty");
        } else {
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    phoneNumber: vendorPhone,
                    role: "vendor",
                }
            }, (err) => {
                if (err) {
                    throw err;
                } else {
                    const user = Meteor.userId();

                    const result = Meteor.call('sellers.insert',
                        user, name, category, {
                            mainImage: mainImage,
                            logoImage: logoImage,
                            moreImages: moreImages,
                            video: sellerVideo,
                            address: address,
                            region: region,
                            country: country,
                            website: websiteName,
                            email: email,
                            phone: vendorPhone,
                            standardPack: standardPack,
                            generousPack: generousPack,
                            exclusivePack: exclusivePack,
                            facilities: sellerFacilities,
                            area: area,
                            ceilingHeight: ceilingHeight,
                            maxPeople: maxPeople,
                            eventType: eventType
                        });
                    if (!result) {
                        this.successNotify("Seller Service Added");
                        browserHistory.push("/vendor/chat");
                    } else {
                        console.log(result);
                        this.errorNotify("Please try again");
                    }
                }
            })
        }
    }

    renderOtherImages() {
        return this.state.morePicture.map((imageUrl, index) => {
            return (
                <div className={"mobile-vendor-more-pictures-scroll-div"} key={index}>
                    <img src={imageUrl} alt="" width={100} className={"mobile-more-pictures-img"}/>
                </div>
            )
        });
    }

    toggleNewPassword() {
        this.setState({showNewPassword: !this.state.showNewPassword})
    }

    toggleRepeatNewPassword() {
        this.setState({showRepeatNewPassword: !this.state.showRepeatNewPassword})
    }

    handleCategoryClick(category) {
        if (category.name.toLowerCase() === "venue") {
            this.setState({isCategoryVenue: true});
        } else {
            this.setState({isCategoryVenue: false});
        }
    }

    renderSellerFacilities() {
        return this.state.facilities.map((facility, index) => (
            <li className="active" key={index}>
                <img src={facility.image} alt="" /><br />
                <span>{facility.name}</span>
            </li>
        ));
    }

    renderEventTypes(){
        return this.state.eventTypes.map((eventType, index) => (<option key={index}>{eventType.name}</option>))
    }

    toggleRadioChecked() {
        this.setState({radioChecked: !this.state.radioChecked})
    }
    
    render() {

        const img1 = this.state.mainPictureUrl;
        const img2 = this.state.logoUrl;
        const img3 = "/img/venu.jpg";
        const { country, region } = this.state;

        const MainBackStyle = {
            backgroundImage: 'url(' + img1 + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        const LogoBackStyle = {
            backgroundImage: 'url(' + img2 + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        const VideoBackStyle = {
            padding: "10px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        // console.log(this.state.termsToggle)
        const stateValue = this.state.termsToggle;

        return (
            <div className="page-login page_background">
                <Header />
                <div className="new-service-mobile-wrapper">
                    <div className="new-service-mobile-container">
                        <div className="new-service-mobile-div-wrapper">
                            <div className="new-service-mobile-login-info">
                                <div className="relative-div home-login-button-div new-service-text-div  new_login_logo">
                                    <div className="text-style">
                                        <img className="logo-home-first-img" src={ "/img/newService/NEW_SERVICE.svg"}/>
                                    </div>
                                    <div className={ "absolute-div-style"}>New Service</div>
                                </div>
                                <div className="relative-div home-login-button-div mobile_back_div">
                                    <p className="mandatory_fields"><img src="/img/svg_img/warning-sign.svg" /><span>Mandatory Fields</span></p>
                                </div>
                                <form className="new_services_form">
                                    <div className="new-service-login-form ">
                                        <div className={ "col-sm-4"}>
                                            <div className="pack_wrapper">
                                                <h2 className="heading_text">what's on your menu?</h2>
                                                <div className="pack_content">
                                                    <div className="activated-pack">
                                                        <div className="box-container same-icon">
                                                            <p className="star-print">
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                            </p>
                                                            <div style={{position: 'relative', right: '10px'}} className="heading-text act1">
                                                                <h2>STANDARD menu</h2>
                                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                                            </div>
                                                            <textarea id={ "standardPack"} className={ "mobile-textarea-box"}>
                                                                        </textarea>
                                                        </div>
                                                    </div>

                                                    <div className="activated-pack">
                                                        <div className="box-container same-icon">
                                                            <p className="star-print">
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                            </p>
                                                            <div style={{position: 'relative', right: '10px'}} className="heading-text act1">
                                                                <h2>Generous menu</h2>
                                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                                            </div>
                                                            <textarea id={ "generousPack"} className={ "mobile-textarea-box"}>
                                                                        </textarea>

                                                        </div>
                                                    </div>

                                                    <div className="activated-pack">
                                                        <div className="box-container same-icon">
                                                            <p className="star-print">
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                                <img className={ "vendor-star"} src={ "/img/star.png"} alt={ "star"}/>
                                                            </p>
                                                            <div style={{position: 'relative', right: '10px'}} className="heading-text act1">
                                                                <h2>Exclusive menu</h2>
                                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                                            </div>
                                                            <textarea id={ "exclusivePack"} className={ "mobile-textarea-box"}>
                                                                        </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="facility_wrapper">
                                                    <h2 className="heading_text"> facility</h2>
                                                    <div className="facility-dv  new_service_position">
                                                        <ul>
                                                            {this.renderSellerFacilities()}                            
                                                        </ul>
                                                    </div>
                                                    <div className="area facility_div display_inherit">
                                                        <div className="facility_div_content">
                                                            <div className="left_div">
                                                                &nbsp;
                                                            </div>
                                                            <div className="right_div">
                                                                <input type="text" id="area" className="right_div_class" />
                                                                <p className="facility_name">Area</p>
                                                                <span className="power_text">M<sup>2</sup></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="ceiling_height facility_div display_inherit">
                                                        <div className="facility_div_content">
                                                            <div className="left_div">
                                                                &nbsp;
                                                            </div>
                                                            <div className="right_div">
                                                                <input type="text" id="ceilingHeight" className="right_div_class" />
                                                                <p className="facility_name">Ceiling Height</p>
                                                                <span className="power_text">M</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="maximum_people facility_div display_inherit">
                                                        <div className="facility_div_content">
                                                            <div className="left_div">
                                                                &nbsp;
                                                            </div>
                                                            <div className="right_div">
                                                                <input type="text" id="maxPeople" className="right_div_class" />
                                                                <p className="facility_name">Maximum People</p>
                                                                <span className="power_text">PCS</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="even_type facility_div display_inherit">
                                                        <div className="facility_div_content">
                                                            <div className="left_div">
                                                                &nbsp;
                                                            </div>
                                                            <div className="right_div">
                                                                <select>
                                                                    { this.renderEventTypes() }
                                                                </select>
                                                                <p className="facility_name">Type of Events</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className={ "col-sm-4"}>
                                            <div className="picture_box_wrapper">
                                                <h2 className="heading_text">Pictures</h2>
                                                <div className="picture_box_content">
                                                    { this.state.mainPictureUrl ?
                                                    <div style={MainBackStyle} className="picture-upload">
                                                        <div className="not_vaidate">
                                                            <img src="/img/newService/EX_MARK.svg" />
                                                        </div>
                                                        <div className="add-more-pic">
                                                            <div>
                                                                <label className="add-file" htmlFor={ "vendorMainPicture"}>
                                                                    <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                        alt=""/>
                                                                </label>
                                                                <img src={ "/img/mobile_searcBox_line.png"} className="line_image"/>

                                                                <input className="form-control-file" id="vendorMainPicture" type="file" onChange={this._handleMainImageUpload.bind(this)} />
                                                                <h4>Front Picture</h4>
                                                                <p className={ "pClass"}>(Size X sizw)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div className="picture-upload logo_pic_wrapper">
                                                        <div className="not_vaidate">
                                                            <img src="/img/newService/EX_MARK.svg" />
                                                        </div>
                                                        <div className="add-more-pic add_more_pic_beforeUpload">
                                                            <div>
                                                                <label className="add-file" htmlFor={ "vendorMainPicture"}>
                                                                    <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                        alt=""/>
                                                                </label>
                                                                <img src={ "/img/mobile_searcBox_line.png"} className="line_image"/>
                                                                <input className="form-control-file" id="vendorMainPicture" type="file" onChange={this._handleMainImageUpload.bind(this)} />
                                                                <h4>Front Picture</h4>
                                                                <p className={ "pClass"}>(Size X size)</p>
                                                                <Progress completed={this.state.mainImageUploadProgress}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    } { this.state.logoUrl ?
                                                    <div style={LogoBackStyle} className="picture-upload">
                                                        <div className="not_vaidate">
                                                            <img src="/img/newService/EX_MARK.svg" />
                                                        </div>
                                                        <div className="add-more-pic">
                                                            <div onChange={this._handleLogoUpload.bind(this)}>
                                                                <label className="add-file" htmlFor={ "vendorLogo"}>
                                                                    <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                        alt=""/>
                                                                </label>
                                                                <img src={ "/img/mobile_searcBox_line.png"} className="line_image"/>

                                                                <input className="form-control-file" id="vendorLogo" type="file" />
                                                                <h4>Logo Picture</h4>
                                                                <p className={ "pClass"}>(Size X size)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div className="picture-upload background-circle-center">
                                                        <div className="not_vaidate">
                                                            <img src="/img/newService/EX_MARK.svg" />
                                                        </div>
                                                        <div className="add-more-pic add_more_pic_beforeUpload border_circle">
                                                            <div onChange={this._handleLogoUpload.bind(this)}>
                                                                <label className="add-file" htmlFor={ "vendorLogo"}>
                                                                <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                        alt=""/>
                                                                </label>
                                                                <img src={ "/img/mobile_searcBox_line.png"} className="line_image"/>
                                                                <input className="form-control-file" id="vendorLogo" type="file" />
                                                                <h4>Logo Picture</h4>
                                                                <p className={ "pClass"}>(Max 5 Mb)</p>
                                                                <Progress completed={this.state.logoImageUploadProgress}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    }
                                                    <div className="picture-upload more-pictures-relative-div">
                                                    <div className="not_vaidate">
                                                                {/* <img src="/img/newService/EX_MARK.svg" /> */}
                                                            </div>
                                                        <div className={ "more-pictures-absolute-div"}>
                                                            { this.state.morePicture.length === 0 ?
                                                            <div onChange={this._handleMorePictureUpload.bind(this)}>
                                                                <div className="add-more-pic padding_top10">
                                                                    <label className="add-file" htmlFor={ "mobileMorePictutres"}>
                                                                        <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                            alt=""/>
                                                                    </label>
                                                                    <img src={ "/img/mobile_searcBox_line.png"} className="line_image"/>
                                                                    <input className="form-control-file" id="mobileMorePictutres" type="file" multiple/>
                                                                    <h4 className={ "h4More"}>More Pictures</h4>
                                                                    
                                                                    <p className="morePicparagraph">(Max 25 Mb)</p>
                                                                </div>
                                                                <Progress completed={this.state.morePictureUploadProgress}/>
                                                            </div>
                                                            :
                                                            <div></div>
                                                            }
                                                        </div>
                                                        { this.state.morePicture.length === 0 ?
                                                        <div className={ "mobile-vendor-more-pictures-scroll"}>
                                                            <div className={ "mobile-vendor-more-pictures-scroll-div"}>
                                                                {/* <img src={ "/img/logo_new.png"} className={ "mobile-more-pictures-img"} /> */}
                                                            </div>
                                                            <div className={ "mobile-vendor-more-pictures-scroll-div"}>
                                                                {/* <img src={ "/img/logo_new.png"} className={ "mobile-more-pictures-img"} /> */}
                                                            </div>
                                                            <div className={ "mobile-vendor-more-pictures-scroll-div"}>
                                                                {/* <img src={ "/img/logo_new.png"} className={ "mobile-more-pictures-img"} /> */}
                                                            </div>
                                                            <div className={ "mobile-vendor-more-pictures-scroll-div"}>
                                                                {/* <img src={ "/img/logo_new.png"} className={ "mobile-more-pictures-img"} /> */}
                                                            </div>
                                                        </div>
                                                        : this.renderOtherImages() }
                                                    </div>
                                                    <div style={VideoBackStyle} className="picture-upload video-relative-div">
                                                        { this.state.videoUrl ?
                                                        <div>
                                                            <ReactPlayer className={ "mobile_vendor_video"} url={this.state.videoUrl} controls={true} width="60%" height={ "100%"} />
                                                        </div>
                                                        :
                                                        <div>
                                                            <div className="not_vaidate">
                                                                {/* <img src="/img/newService/EX_MARK.svg" /> */}
                                                            </div>
                                                            <div className="add-more-pic">
                                                            
                                                            <div onChange={this._handleVideoUpload.bind(this)}>
                                                                <label className="add-file" htmlFor={ "vendorVideo"}>
                                                                                        <img id="hi" className="white-add-img"
                                                                                            src="/img/new_img/video_svg.svg" alt=""/>
                                                                                    </label>
                                                                <img src={ "/img/mobile_searcBox_line.png"} className="line_image" />

                                                                <input className="form-control-file" id="vendorVideo" type="file" />
                                                                <h4>Video</h4>
                                                                <p className={ "pClass"}>(Max 25 Mb)</p>
                                                                <Progress completed={this.state.videoUploadProgress}/>
                                                            </div>

                                                        </div>
                                                        </div>
                                                        }
                                                    </div>

                                                </div>

                                                <div className="clearfix"></div>
                                                    <div className="button_wrapper">
                                                        <div className="remember_div new_service">
                                                            <label className="radio" id={!this.state.radioChecked? "no__check": ""}>
                                                                <input name="radio" type="radio" checked="false" onClick={this.toggleRadioChecked}/>
                                                                <span>
                                                                <p className="terms_condition">
                                                                ACCEPT THE <button onClick={this.handleTermsConditionClick.bind(this)}>TERMS OF USE</button>
                                                                </p>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        
                                                        <div onClick={this._handleSubmit.bind(this)} className="relative-div news_service_button  home-login-button-div">
                                                            <div className="text-style">
                                                                <img className="service_back_logo-home-first-img" src={ "/img/newService/TICK.svg"}/>
                                                                <div className={ "back_top"}>Add New Service</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div className={ "col-sm-4"}>
                                            <div className="info_wrapper">
                                                <div className="first_div">
                                                    <h2 className="heading_text"> Info</h2>
                                                    <div className="info_content">
                                                        <div className="username mobile_home_new_service_margin margin-top-0">
                                                            <input type="text" id={ "name"} className="type-username" placeholder="BUSINESS NAME" />
                                                            <p className="line_height">Business Name</p>
                                                            <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>
                                                        <div className="category_select">
                                                            <CategoryDropDown handleCategoryClick={this.handleCategoryClick.bind(this)}/>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div className="location_wrapper">
                                                    <h2 className="heading_text">Location</h2>
                                                    <div className="location_content">
                                                        <div className="address">
                                                            <CountryDropdown value={country} onChange={(val)=> this.selectCountry(val)} />
                                                                <p className="line_height">Country</p>
                                                                <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>

                                                        <div className="address">
                                                            <RegionDropdown country={country} value={region} onChange={(val)=> this.selectRegion(val)} />
                                                                <p className="line_height">Region</p>
                                                                <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>

                                                        <div className="address_one">
                                                            <input type="text" id="address" className="type-username" placeholder="ADDRESS" />
                                                            <p className="line_height">Address</p>
                                                            <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>

                                                    </div>
                                                    <div className="moreinfo_content">
                                                        <h2 className="heading_text">More info</h2>
                                                        <div className="mail">
                                                            <input type="text" id="vendorEmail" className="type-email" placeholder="EMAIL" />
                                                            <p className="line_height">Mail</p>
                                                            <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>

                                                        <div className="number">
                                                            <input type="number" id={ "vendorNumber"} className="type-email" placeholder="NUMBER" />
                                                            <p className="line_height">PHONENR</p>
                                                            <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>

                                                        <div className="website">
                                                            <input type="text" id={ "vendorWeb"} className="type-email" placeholder="WEBSITE" />
                                                            <p className="line_height">Website</p>
                                                            <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="password_content">
                                                    <h2 className="heading_text">Security</h2>
                                                    <div className="password">
                                                        <input type={!this.state.showNewPassword ? "password" : "text"} id={ "password"} className="type-password" placeholder="********" />
                                                        <p className="line_height">password</p>
                                                        <img src={!this.state.showNewPassword ? "/img/new_img/white_without_serv_line.svg" : "/img/new_img/white_serv_line.svg"} className={ "mobile_user_settings_signOut_text_logo"} onClick={this.toggleNewPassword.bind(this)}/>
                                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                    </div>

                                                    <div className="password">
                                                        <input type={!this.state.showRepeatNewPassword ? "password" : "text"} id={ "repeatPassword"} className="type-password" placeholder="********" />
                                                        <p className="line_height">repeat password</p>
                                                        <img src={!this.state.showRepeatNewPassword ? "/img/new_img/white_without_serv_line.svg" : "/img/new_img/white_serv_line.svg"} className={ "mobile_user_settings_signOut_text_logo"} onClick={this.toggleRepeatNewPassword.bind(this)}/>
                                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <ToastContainer />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="footer_loginpage">
                    <Footer />
                </div>

                {this.state.showTermCondPopup? 
                <div className="popup_wrapper">
                    <div className="alt_chat_wrapper">
                        <div className="popup_content">
                            <div className="popup_content_conatiner">
                                <div className="popup_item terms_div">
                                    <h2>Terms</h2>
                                    <div className="text_content_wrapper">
                                        <div className="text_container">
                                            <p className="terms_text">ALorem Ipsum is simply dummy text of the printing and typesetting 
                                                industry. Lorem Ipsum has been the industry's standard dummy 
                                                text ever since the 1500s, when an unknown printer took a 
                                                galley of type and scrambled it to make a type specimen book. 
                                                It has survived not only five centuries,</p>
                                        </div>
                                        <div className="button_wrapper">
                                            <button className="first_button" onClick={this.handleTermsConditionNoClick}>Cancel</button>
                                            <button onClick={this.handleTermsConditionYesClick}>Accept</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                :""}

            </div>
        )
    }
}

export default NewService;

