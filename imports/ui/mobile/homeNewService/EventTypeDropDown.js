import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {EventType} from "../../../api/eventType";

class EachEventType extends React.Component {
    render() {
        const eventType = this.props.eventType;
        return (
            <option value={eventType._id}>{eventType.name}</option>
        )
    }
}

class EventTypeDropDown extends React.Component {
    renderEventTypes() {
        return this.props.eventTypes.map((eventType) => (
            <EachEventType key={eventType._id} eventType={eventType}/>
        ));
    }

    render() {
        const style = {
            height: "100%",
        };
        return (
            <div className="category mobile-category">
                {/*<input type="text" className="type-username" placeholder="CATEGORY" />*/}
                {/*<p>category</p>*/}
                <select id={"eventType"} className={"mobile-venue-select mobile-category-style"}>
                    {this.renderEventTypes()}
                </select>
                <p className="line_height">TYPES OF EVENTS</p>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('event_types');
    const eventTypes = EventType.find({}).fetch();
    return {
        eventTypes,
    };
})(EventTypeDropDown);

