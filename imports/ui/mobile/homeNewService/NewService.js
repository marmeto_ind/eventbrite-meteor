import React from 'react';
import './new-service.scss';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import CategoryDropDown from './CategoryDropDown';
import EventTypeDropDown from './EventTypeDropDown';
import ShowSellerFacilities from './ShowSellerFacilities';
import { Accounts } from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import ReactPlayer from 'react-player';
import { Meteor } from 'meteor/meteor';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

class NewService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainPictureUrl: '',
            logoUrl: '',
            morePicture: [],
            videoUrl: '',
            mainImageUploadProgress: 0,
            logoImageUploadProgress: 0,
            videoUploadProgress: 0,
            morePictureUploadProgress: 0,
            showNewPassword: false,
            showRepeatNewPassword: false,
            isCategoryVenue: false,
            country: '',
            region: '',
            showTermCondPopup: false,
        };
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleMainImageUpload = this._handleMainImageUpload.bind(this);
        this._handleMorePictureUpload = this._handleMorePictureUpload.bind(this);
        this._handleVideoUpload = this._handleVideoUpload.bind(this);
        this._handleLogoUpload = this._handleLogoUpload.bind(this);
        this.handleTermsConditionClick = this.handleTermsConditionClick.bind(this);
        this.handleTermsConditionYesClick = this.handleTermsConditionYesClick.bind(this);
        this.handleTermsConditionNoClick = this.handleTermsConditionNoClick.bind(this);
    }

    handleTermsConditionClick(event) {
        event.preventDefault();
        this.setState({ showTermCondPopup: !this.state.showTermCondPopup })
    }

    handleTermsConditionYesClick(event) {
        event.preventDefault();
        this.setState({ acceptTermsCondition: true })
        this.setState({ showTermCondPopup: !this.state.showTermCondPopup })
        this.setState({ radioChecked: true })
    }

    handleTermsConditionNoClick(event) {
        event.preventDefault();
        this.setState({ showTermCondPopup: !this.state.showTermCondPopup })
        this.setState({ radioChecked: false })
    }


    selectCountry(val) {
        this.setState({ country: val });
    }

    selectRegion(val) {
        this.setState({ region: val });
    }

    _handleMainImageUpload() {
        const mainPictureFile = $("#vendorMainPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify(error.message);
            } else {
                that.setState({ mainPictureUrl: downloadUrl });
                that.uploadSuccessNotify("Main Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({ mainImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    backToHome(e) {
        browserHistory.push('/mobile/home/first');
    }

    // Upload logo and store the url to state variable
    _handleLogoUpload() {
        const logo = $("#vendorLogo")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(logo, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                that.uploadErrorNotify(error.message);
                console.error(error);
            } else {
                that.setState({ logoUrl: downloadUrl });
                that.uploadSuccessNotify("Logo Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({ logoImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    // Upload video and store the url to state variable
    _handleVideoUpload() {
        const video = $("#vendorVideo")[0].files[0];
        const uploader = new Slingshot.Upload("myVideoUploads");
        let that = this;
        uploader.send(video, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify(error.message);
            } else {
                that.setState({ videoUrl: downloadUrl });
                that.uploadSuccessNotify("Video");
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                this.setState({ videoUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    // Upload more pictures and store the picture url to state variable
    _handleMorePictureUpload() {
        const morePictures = $("#mobileMorePictutres")[0].files;
        if (morePictures.length > 6) {
            this.errorNotify("you can upload max 6 other pictures")
        } else {
            const that = this;
            _.map(morePictures, function (file) {
                let uploader = new Slingshot.Upload("myImageUploads");
                uploader.send(file, function (err, downloadUrl) {
                    computation.stop();
                    if (err) {
                        that.setState({ morePictureUploadProgress: 0 });
                        that.uploadErrorNotify("Picture");
                        console.log(err)
                    } else {
                        const morePictureArray = that.state.morePicture;
                        that.setState({ morePictureUploadProgress: 0 });
                        morePictureArray.push(downloadUrl);
                        that.setState({ morePicture: morePictureArray })
                        that.uploadSuccessNotify("Picture");
                    }
                });
                // Track progress
                let computation = Tracker.autorun(() => {
                    if (!isNaN(uploader.progress())) {
                        that.setState({ morePictureUploadProgress: uploader.progress() * 100 });
                    }
                })
            });
        }
    }

    uploadSuccessNotify = (string) => {
        toast.success("Successfully uploaded " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    uploadErrorNotify = (string) => {
        toast.error("Error while uploading " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    _handleSubmit(e) {
        e.preventDefault();

        // fetch all data from the form
        const password = $("#password").val();
        const repeatPassword = $("#repeatPassword").val();
        const name = $("#name").val();
        const address = $("#address").val();

        const { country, region } = this.state;

        const area = $("#area").val();
        const ceilingHeight = $("#ceilingHeight").val();
        const maxPeople = $("#maxPeople").val();

        const standardPack = $("#standardPack").val();
        const generousPack = $("#generousPack").val();
        const exclusivePack = $("#exclusivePack").val();

        const categoryId = $("#category").val();
        const categoryName = $("#category option:selected").text();

        const eventTypeId = $("#eventType").val();
        const eventTypeName = $("#eventType option:selected").text();

        const email = $("#vendorEmail").val();
        const vendorPhone = $("#vendorNumber").val();
        const websiteName = $("#vendorWeb").val();

        const category = { _id: categoryId, name: categoryName };
        const eventType = { _id: eventTypeId, name: eventTypeName };

        const mainImage = this.state.mainPictureUrl;
        const logoImage = this.state.logoUrl;
        const moreImages = this.state.morePicture;
        const sellerVideo = this.state.videoUrl;

        // Initialized seller facilities object
        const sellerFacilities = [];
        const selectedFacilities = $("input:checked");

        // get the id the the selected facilities and save it
        for (let index = 0; index < selectedFacilities.length; ++index) {
            let item = selectedFacilities[index];
            let itemId = item.id;

            let jquerySelector = "label[for='" + itemId.toString() + "']";
            let value = $(jquerySelector).text();
            let facility = {};

            facility["_id"] = item.id;
            facility["name"] = value;

            
            sellerFacilities.push(facility);
        }
        console.log(sellerFacilities);
        //return;

        if (this.state.isCategoryVenue) {
            if (!area) {
                this.errorNotify("Area is empty");
            } else if (!ceilingHeight) {
                this.errorNotify("Ceiling height is empty");
            } else if (!maxPeople) {
                this.errorNotify("Max People field is empty");
            } else if (!eventType) {
                this.errorNotify("Event type is not selected");
            }
        }

        if (!name) {
            this.errorNotify("User name is empty");
        } else if (!password) {
            this.errorNotify("Password is empty");
        } else if (password !== repeatPassword) {
            this.errorNotify("Password and Repeat Password does not match");
        } else if (!category) {
            this.errorNotify("Category is empty");
        } else if (!mainImage) {
            this.errorNotify("Main image is not uploaded");
        } else if (!logoImage) {
            this.errorNotify("Logo image is not uploaded");
        } else if (!moreImages) {
            this.errorNotify("Other Images are not uploaded");
        } else if (!sellerVideo) {
            this.errorNotify("Video is not uploaded");
        } else if (!address) {
            this.errorNotify("Please fill address field");
        } else if (!region) {
            this.errorNotify("Please fill region field");
        } else if (!country) {
            this.errorNotify("Please fill country field");
        } else if (!standardPack) {
            this.errorNotify("Standard pack field is empty");
        } else if (!generousPack) {
            this.errorNotify("Generous pack field is empty");
        } else if (!exclusivePack) {
            this.errorNotify("Exclusive pack field is empty");
        } else if (!email) {
            this.errorNotify("Email field is empty");
        } else if (!vendorPhone) {
            this.errorNotify("Vendor phone is empty");
        } else if (!websiteName) {
            this.errorNotify("Website name is empty");
        } else {
            Accounts.createUser({
                email,
                password,
                profile: {
                    name: name,
                    phoneNumber: vendorPhone,
                    role: "vendor",
                }
            }, (err) => {
                if (err) {
                    throw err;
                } else {
                    const user = Meteor.userId();

                    const result = Meteor.call('sellers.insert',
                        user, name, category, {
                            mainImage: mainImage,
                            logoImage: logoImage,
                            moreImages: moreImages,
                            video: sellerVideo,
                            address: address,
                            region: region,
                            country: country,
                            website: websiteName,
                            email: email,
                            phone: vendorPhone,
                            standardPack: standardPack,
                            generousPack: generousPack,
                            exclusivePack: exclusivePack,
                            facilities: sellerFacilities,
                            area: area,
                            ceilingHeight: ceilingHeight,
                            maxPeople: maxPeople,
                            eventType: eventType
                        });
                    if (!result) {
                        this.successNotify("Seller Service Added");
                        browserHistory.push("/mobile/vendor/chats");
                    } else {
                        console.log(result);
                        this.errorNotify("Please try again");
                    }
                }
            })
        }
    }

    renderOtherImages() {
        return this.state.morePicture.map((imageUrl, index) => {
            return (
                <div className={"mobile-vendor-more-pictures-scroll-div"} key={index}>
                    <img src={imageUrl} alt="" width={100} className={"mobile-more-pictures-img"} />
                </div>
            )
        });
    }

    toggleNewPassword() {
        this.setState({ showNewPassword: !this.state.showNewPassword })
    }

    toggleRepeatNewPassword() {
        this.setState({ showRepeatNewPassword: !this.state.showRepeatNewPassword })
    }

    handleCategoryClick(category) {
        if (category.name.toLowerCase() === "venue") {
            this.setState({ isCategoryVenue: true });
        } else {
            this.setState({ isCategoryVenue: false });
        }
    }

    render() {
        // const img1 = this.state.mainPictureUrl;
        // const img2 = this.state.logoUrl;
        const img3 = "/img/venu.jpg";

        // console.log({"mainPicture": this.state.mainPictureUrl});
        // console.log({"logoUrl": this.state.logoUrl});

        const { country, region } = this.state;
        // console.log("url('" + this.state.mainPictureUrl + "')");

        let MainBackStyle = {
            backgroundImage: "url('" + this.state.mainPictureUrl + "')",
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        let LogoBackStyle = {
            backgroundImage: "url('" + this.state.logoUrl + "')",
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        let VideoBackStyle = {
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow: "hidden",
            width: "100%",
            cursor: "pointer"
        };

        return (
            <div className="new-service-mobile-wrapper">
                <div className="new-service-mobile-container">
                    <div className="new-service-mobile-div-wrapper mobile_wrapper">
                        <div className="new-service-mobile-login-info">
                            {/*<div className="relative-div new-service-text-div">*/}
                            {/*<img className="home-login-img" src={"/img/handshake-01.png"} />*/}
                            {/*<div className={"new-service-login-div"}><span>New Service</span></div>*/}
                            {/*</div>*/}
                            <div className="relative-div home-login-button-div new-service-text-div  new_login_logo">
                                {/*<input type="submit" className="text-style" value="New Couple" />*/}
                                <div className="text-style">
                                    <img className="logo-home-first-img" src={"/img/new_service.svg"} />
                                </div>
                                <div className={"absolute-div-style"}>New Service</div>
                            </div>
                            {/* <div className="relative-div home-login-button-div mobile_back_div">
                                <div className="text-style" onClick={this.backToHome.bind(this)}>
                                    <img className="service_back_logo-home-first-img" src={"/img/svg_img/backSvg.svg"}/>
                                    <div className={"absolute-div-style back_top"}>Back</div>
                                </div>
                            </div> */}
                            <div className="new-service-login-form ">
                                <span className="mandatory_text">Mandatory Fields</span>
                                <form>
                                    <div className="username mobile_home_new_service_margin">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type="text" id={"name"} className="type-username"
                                            placeholder="BUSINESS NAME" />
                                        <p className="line_height">Business Name</p>
                                    </div>
                                    <CategoryDropDown handleCategoryClick={this.handleCategoryClick.bind(this)} />

                                    <span className="heading-title1">Pictures</span>
                                    {
                                        this.state.mainPictureUrl ?
                                            <div style={MainBackStyle} className="picture-upload">
                                                <img className="pic-mandate" src="/img/new_img/newservice_exclm.svg"></img>
                                                <div className="add-more-pic">
                                                    <div>
                                                        <label className="add-file" htmlFor={"vendorMainPicture"}>
                                                            <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                alt="" />
                                                        </label>
                                                        <img src={"/img/mobile_searcBox_line.png"} />

                                                        <input
                                                            className="form-control-file"
                                                            id="vendorMainPicture"
                                                            type="file"
                                                            onChange={this._handleMainImageUpload.bind(this)}
                                                        />
                                                        <h4>Main Picture</h4>
                                                        <p className={"pClass"}>(Size x Size)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            :
                                            <div className="picture-upload">
                                                <img className="pic-mandate" src="/img/new_img/newservice_exclm.svg"></img>
                                                <div className="add-more-pic add_more_pic_beforeUpload">
                                                    <div>
                                                        <label className="add-file" htmlFor={"vendorMainPicture"}>
                                                            <img id="hi" className="white-add-img" src="/img/new_img/camera_svg.svg"
                                                                alt="" />
                                                        </label>
                                                        <img src={"/img/mobile_searcBox_line.png"} />
                                                        <input
                                                            className="form-control-file"
                                                            id="vendorMainPicture"
                                                            type="file"
                                                            onChange={this._handleMainImageUpload.bind(this)}
                                                        />
                                                        <h4>Front Picture</h4>
                                                        <p className={"pClass"}>(Size X size)</p>
                                                        <Progress completed={this.state.mainImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                    }
                                    {
                                        this.state.logoUrl ?
                                            <div style={LogoBackStyle} className="picture-upload">
                                                {/*<div className={"mobile-white-camera-abs"}>*/}
                                                {/*<img id="hi" className="mobile_side_img" src="/img/photo-camera.png"*/}
                                                {/*alt=""/>*/}
                                                {/*</div>*/}
                                                <img className="pic-mandate" src="/img/new_img/newservice_exclm.svg"></img>
                                                <div className="add-more-pic">
                                                    <div onChange={this._handleLogoUpload.bind(this)}>
                                                        <label className="add-file" htmlFor={"vendorLogo"}>
                                                            <img id="hi" className="white-add-img"
                                                                src="/img/new_img/camera_svg.svg"
                                                                alt="" />
                                                        </label>
                                                        <img src={"/img/mobile_searcBox_line.png"} />

                                                        <input className="form-control-file" id="vendorLogo"
                                                            type="file" />
                                                        <h4>Logo Picture</h4>
                                                        <p className={"pClass"}>(Max 25 Mb)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            :
                                            <div className="picture-upload">
                                                {/*<div className={"mobile-white-camera-abs"}>*/}
                                                {/*<img id="hi" className="mobile_side_img" src="/img/photo-camera.png"*/}
                                                {/*alt=""/>*/}
                                                {/*</div>*/}
                                                <img className="pic-mandate" src="/img/new_img/newservice_exclm.svg"></img>
                                                <div className="add-more-pic add_more_pic_beforeUpload">
                                                    <div onChange={this._handleLogoUpload.bind(this)}>
                                                        <label className="add-file" htmlFor={"vendorLogo"}>
                                                            <img id="hi" className="white-add-img"
                                                                src="/img/new_img/camera_svg.svg"
                                                                alt="" />
                                                        </label>
                                                        <img src={"/img/mobile_searcBox_line.png"} />
                                                        <input className="form-control-file" id="vendorLogo"
                                                            type="file" />
                                                        <h4>Logo Picture</h4>
                                                        <p className={"pClass"}>(Max 25 Mb)</p>
                                                        <Progress completed={this.state.logoImageUploadProgress} />
                                                    </div>
                                                </div>
                                            </div>
                                    }
                                    <div className="picture-upload more-pictures-relative-div">
                                        <div className={"more-pictures-absolute-div"}>
                                            {/*<div className={"mobile-more-img-abs"}>*/}
                                            {/*<img id="hi" className="mobile_side_img" src="/img/photo-camera.png"*/}
                                            {/*alt=""/>*/}
                                            {/*</div>*/}
                                            {
                                                this.state.morePicture.length === 0
                                                    ?
                                                    <div onChange={this._handleMorePictureUpload.bind(this)}>
                                                        <div className="add-more-pic padding_top10">
                                                            <label className="add-file" htmlFor={"mobileMorePictutres"}>
                                                                <img id="hi" className="white-add-img"
                                                                    src="/img/new_img/camera_svg.svg" alt="" />
                                                            </label>
                                                            <img src={"/img/mobile_searcBox_line.png"} />
                                                            <input
                                                                className="form-control-file"
                                                                id="mobileMorePictutres"
                                                                type="file" multiple />
                                                            <h4 className={"h4More"}>More Pictures</h4>
                                                            <p className="position_absolute">(Max 5 Mb)</p>
                                                        </div>
                                                        <Progress completed={this.state.morePictureUploadProgress} />
                                                    </div>
                                                    :
                                                    <div></div>
                                            }
                                        </div>
                                        {
                                            this.state.morePicture.length === 0
                                                ?
                                                <div className={"mobile-vendor-more-pictures-scroll"}>
                                                    <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                        {/* <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} /> */}
                                                    </div>
                                                    <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                        {/* <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} /> */}
                                                    </div>
                                                    <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                        {/* <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} /> */}
                                                    </div>
                                                    <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                        {/* <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} /> */}
                                                    </div>
                                                </div>
                                                :
                                                this.renderOtherImages()
                                        }
                                    </div>
                                    <div style={VideoBackStyle} className="picture-upload video-relative-div">
                                        {/*<div className={"mobile-video-img-abs"}>*/}
                                        {/*<img id="hi" className="white-add-img" src="/img/play-button.png" alt="" />*/}
                                        {/*</div>*/}
                                        {
                                            this.state.videoUrl ?
                                                <div>
                                                    <ReactPlayer className={"mobile_vendor_video"}
                                                        url={this.state.videoUrl} controls={true}
                                                        width="60%" height={"100%"}
                                                    />
                                                </div>
                                                :

                                                <div className="add-more-pic">
                                                    <div onChange={this._handleVideoUpload.bind(this)}>
                                                        <label className="add-file" htmlFor={"vendorVideo"}>
                                                            <img id="hi" className="white-add-img"
                                                                src="/img/new_img/video_svg.svg" alt="" />
                                                        </label>
                                                        <img src={"/img/mobile_searcBox_line.png"} />

                                                        <input className="form-control-file" id="vendorVideo"
                                                            type="file" />
                                                        <h4>Video</h4>
                                                        <p className={"pClass"}>(Max 25 Mb)</p>
                                                        <Progress completed={this.state.videoUploadProgress} />
                                                    </div>

                                                </div>
                                        }
                                    </div>
                                    <span className="heading-title1">What's on your menu?</span>
                                    <div className="activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                            </p>
                                            <div style={{ position: 'relative' }}
                                                className="heading-text act1">
                                                <h2 className="standardPack">STANDARD menu</h2>
                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                            </div>
                                            <textarea id={"standardPack"} className={"mobile-textarea-box"}>
                                            </textarea>
                                            {/*<h4>Lorem ipsum Lorem ipsum Lorem ipsum </h4>*/}
                                            {/*<h4>Baptzr/Main Courses/Dessert</h4>*/}
                                            {/*<h4>Welcome drink / chocolate</h4>*/}
                                        </div>
                                    </div>

                                    <div className="activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                            </p>
                                            <div style={{ position: 'relative' }}
                                                className="heading-text act1">
                                                <h2 className="generousPack">Generous menu</h2>
                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                            </div>
                                            <textarea id={"generousPack"} className={"mobile-textarea-box"}>
                                            </textarea>

                                        </div>
                                    </div>

                                    <div className="activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                                <img className={"vendor-star"} src={"/img/newstar.svg"} alt={"star"} />
                                            </p>
                                            <div style={{ position: 'relative' }}
                                                className="heading-text act1">
                                                <h2 className="exclusivePack">Exclusive menu</h2>
                                                <img className="line_svg_position" src="/img/mobile_searcBox_line.png" />
                                            </div>
                                            <textarea id={"exclusivePack"} className={"mobile-textarea-box"}>
                                            </textarea>
                                        </div>
                                    </div>
                                    <span className="heading-title1">Facility</span>
                                    <div className="facility-dv new_service_position">
                                        {/*<div className="new_service_checkbox">*/}
                                        {/*<img src="/img/svg_img/checkbox_svg.svg" alt=""/>*/}
                                        {/*</div>*/}
                                        <ShowSellerFacilities />
                                    </div>
                                    <div className="area facility_div">
                                        <div className="facility_div_content">
                                            <div className="left_div width-40">
                                                &nbsp;
                                            </div>
                                            <div className="right_div width-60">
                                                <input type="text" className="right_div_class" />
                                                <p className="facility_name">Area</p>
                                                <span className="power_text">M<sup>2</sup></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="ceiling_height facility_div">
                                        <div className="facility_div_content">
                                            <div className="left_div width-40">
                                                &nbsp;
                                            </div>
                                            <div className="right_div width-60">
                                                <input type="text" className="right_div_class" />
                                                <p className="facility_name">Ceiling Height</p>
                                                <span className="power_text">M</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="maximum_people facility_div">
                                        <div className="facility_div_content">
                                            <div className="left_div width-40">
                                                &nbsp;
                                            </div>
                                            <div className="right_div width-60">
                                                <input type="text" className="right_div_class" />
                                                <p className="facility_name">Maximum People</p>
                                                <span className="power_text">PCS</span>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div className="event_type facility_div">
                                        <div className="facility_div_content">
                                            <div className="left_div">
                                                &nbsp;
                                            </div>
                                            <div className="right_div">
                                                <select>
                                                    <option>Wedding</option>
                                                    <option>Wedding</option>
                                                    <option>Wedding</option>
                                                </select>
                                                <p className="facility_name">Type of Events</p>
                                            </div>
                                        </div>
                                    </div> */}

                                    {this.state.isCategoryVenue ?
                                        <div>
                                            <div className="credintials">
                                                <input type="text" id={"area"} className="type-username"
                                                    placeholder="Area" />
                                                <p className="line_height">AREA</p>
                                            </div>

                                            < div className="credintials">
                                                < input type="text" id={"ceilingHeight"} className="type-username"
                                                    placeholder="Ceiling Height" />
                                                <p className="line_height">CEILING HEIGHT</p>
                                            </div>

                                            <div className="credintials">
                                                <input type="text" id={"maxPeople"} className="type-username"
                                                    placeholder="Maximum People" />
                                                <p className="line_height">MAXIMUM PEOPLE</p>
                                            </div>

                                            <EventTypeDropDown />
                                        </div>
                                        : ""}
                                    <span className="heading-title1">Location</span>
                                    <div className="address">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <CountryDropdown
                                            value={country}
                                            onChange={(val) => this.selectCountry(val)} />
                                        {/*<input type="text" id="country" className="type-username"*/}
                                        {/*placeholder="Country"/>*/}
                                        <p className="line_height">Country</p>
                                    </div>

                                    <div className="address">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        {/*<input type="text" id="region" className="type-username" placeholder="REGION"/>*/}
                                        <RegionDropdown
                                            country={country}
                                            value={region}
                                            onChange={(val) => this.selectRegion(val)} />
                                        <p className="line_height">Region</p>
                                    </div>

                                    <div className="address">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type="text" id="address" className="type-username"
                                            placeholder="ADDRESS" />
                                        <p className="line_height">Address</p>
                                    </div>
                                    <span className="heading-title1">More Info</span>
                                    <div className="mail">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type="text" id="vendorEmail" className="type-email" placeholder="EMAIL" />
                                        <p className="line_height">Mail</p>
                                    </div>

                                    <div className="number">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type="number" id={"vendorNumber"} className="type-email"
                                            placeholder="NUMBER" />
                                        <p className="line_height">PHONENR</p>
                                    </div>

                                    <div className="website">
                                        <input type="text" id={"vendorWeb"} className="type-email"
                                            placeholder="WEBSITE" />
                                        <p className="line_height">Website</p>
                                    </div>

                                    <span className="heading-title1">Security</span>

                                    <div className="password">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type={!this.state.showNewPassword ? "password" : "text"} id={"password"}
                                            className="type-password"
                                            placeholder="***********" />
                                        <p className="line_height">password</p>
                                        <img src={!this.state.showNewPassword ? "/img/svg_img/eye_edit.png" : "/img/svg_img/greyEye.png"}
                                            className={"mobile_user_settings_signOut_text_logo"}
                                            onClick={this.toggleNewPassword.bind(this)} />
                                    </div>

                                    <div className="password">
                                        <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                                        <input type={!this.state.showRepeatNewPassword ? "password" : "text"}
                                            id={"repeatPassword"} className="type-password"
                                            placeholder="***********" />
                                        <p className="line_height">repeat password</p>
                                        <img src={!this.state.showRepeatNewPassword ? "/img/svg_img/eye_edit.png" : "/img/svg_img/greyEye.png"}
                                            className={"mobile_user_settings_signOut_text_logo"}
                                            onClick={this.toggleRepeatNewPassword.bind(this)} />
                                    </div>

                                    <div className="relative-div  home-login-button-div">
                                        <div className="terms_condition">
                                            <span>
                                                ACCEPT THE <a href="#" onClick={this.handleTermsConditionClick.bind(this)}>TERMS OF USE</a>
                                                <input type="checkbox" className="term-check" ></input>
                                            </span>
                                        </div>
                                    </div>

                                    <div onClick={this._handleSubmit.bind(this)}
                                        className="relative-div mobile-service-add new-login-service-btn home-login-button-div">
                                        <div className="text-style">
                                            <img className="service_back_logo-home-first-img"
                                                src={"/img/tick.svg"} />
                                            <div className={"absolute-div-style line-height-32"}>Add new Service</div>
                                        </div>
                                    </div>

                                </form>
                                <ToastContainer />
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.showTermCondPopup ?
                    <div className="popup_wrapper">
                        <div className="alt_chat_wrapper">
                            <div className="popup_content">
                                <div className="popup_content_conatiner">
                                    <div className="popup_item terms_div">
                                        <h2>Terms</h2>
                                        <div className="text_content_wrapper">
                                            <div className="text_container">
                                                <p className="terms_text">ALorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry. Lorem Ipsum has been the industry's standard dummy
                                                    text ever since the 1500s, when an unknown printer took a
                                                    galley of type and scrambled it to make a type specimen book.
                                                It has survived not only five centuries,</p>
                                            </div>
                                            <div className="button_wrapper">
                                                <button className="first_button" onClick={this.handleTermsConditionNoClick}>Close</button>
                                                {/* <button onClick={this.handleTermsConditionYesClick}>Accept</button> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : ""}


            </div>
        )
    }
}

export default NewService;

