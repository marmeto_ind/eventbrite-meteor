import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Categories} from "../../../api/categories";
import _ from 'lodash';

class EachCategory extends React.Component {
    render() {
        const category = this.props.category;
        return (
            <option value={category._id}>{category.name}</option>
        )
    }
}

class CategoryDropDown extends React.Component {
    renderCategories() {
        return this.props.categories.map((category) => (
            <EachCategory handleCategoryClick={this.props.handleCategoryClick} key={category._id} category={category}/>
        ));
    }

    handleSelectValueChange() {
        const categoryId = $("#category").val();
        const categoryName = $("#category option:selected").text();
        this.props.handleCategoryClick({_id: categoryId, name: categoryName});
    }

    render() {
        return (
            <div className="category mobile-category">
                {/*<input type="text" className="type-username" placeholder="CATEGORY" />*/}
                {/*<p>category</p>*/}
                <img className="mandate-img" src="/img/new_img/newservice_exclm.svg"></img>
                <select onChange={this.handleSelectValueChange.bind(this)} id={"category"} className={"mobile-venue-select mobile-category-style"}>
                    {this.renderCategories()}
                </select>
                <p className="line_height">Category</p>
            </div>
        )
    }
}


export default withTracker(() => {
    Meteor.subscribe('categories');
    const categories = Categories.find({}).fetch();

    return {
        categories: categories,
    };
})(CategoryDropDown);

