import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../../api/sellerFacilities";

class EachSellerFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage:'',
            selected: {},
        }
        this.selectItem = this.selectItem.bind(this);
    }
 
    componentDidMount() {
        const facility = this.props.sellerFacility;
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err)
            } else {
                if(result) {
                    self.setState({facilityImage: result.image});
                }
            }
        });
    }

    selectItem(facilityId){
        var selected = this.state.selected;
        selected[facilityId] = !selected[facilityId];
        this.setState({selected: selected});
        console.log(facilityId);

      }
    // changeBackground = () => {
	// 	$('#serviceBackground').css('background','#ff8a6f');
	// };

    render() {
        const activeClass = this.state.activeClass;
        const sellerFacility = this.props.sellerFacility;
        const mobileVendorAddFacilitymainImage = this.state.facilityImage;
        const facilityId = sellerFacility._id;
        const mobileVendorAddFacilitydivStyle = {
            backgroundImage: 'url(' + mobileVendorAddFacilitymainImage + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '15px 15px',
            backgroundPosition: '45% 25%',
        };
        var checkedStatus = false;
        var className = this.state.selected[facilityId] ? ' active' : ' inactive';
        if (className.trim() == 'active'){
            checkedStatus = true;
        }
        var onClick = this.selectItem.bind(this, facilityId);
        return (
            <li  style={mobileVendorAddFacilitydivStyle}  className={'form-check SellerFacilityClass paddingBottom'+ className} onClick={onClick} >
                <input className="form-check-input facility-check-box mobite-checkbox hidden"
                        id={sellerFacility._id}  type="checkbox"  checked={checkedStatus} value={sellerFacility._id}  />
                <img className="facility-img" src={mobileVendorAddFacilitymainImage}></img>
                <label className="form-check-label" htmlFor={sellerFacility._id}>
                    {sellerFacility.name}
                </label>
            </li>
        )
    }
}

class SelectSellerFacility extends React.Component {
    renderSellerFacilities() {
        return this.props.sellerFacilities.map((sellerFacility) => (
            <EachSellerFacility key={sellerFacility._id} sellerFacility={sellerFacility} />
        ));
    }

    render() {
        return (
            <div className="box-container same-icon new_service_facility">
                <div className={"all-facility-dv mobile-facility-div facility-div-background"}>
                    {this.renderSellerFacilities()}
                </div>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('seller_facilities');
    const facilities= SellerFacilities.find({}).fetch();
    return {
        sellerFacilities: facilities,
    };
})(SelectSellerFacility);