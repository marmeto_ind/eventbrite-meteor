import React from 'react';
import './settings.scss';
import '../../pages/homePage/allpopup.scss'
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import {withTracker} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';
import { Accounts } from 'meteor/accounts-base'
import {Meteor} from "meteor/meteor";

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            usernameEdit: false,
            showEmail: false,
            showCurrentPassword: false,
            showNewPassword: false,
            showRepeatNewPassword: false,
            showDeletePopup: false,
            showLanguagePopup: false
        }
    }

    componentDidMount() {
        this.handleLanguageChange();
    }

    handleLogout(e) {
        e.preventDefault();
        Meteor.logout();
        browserHistory.push('/mobile/home/first');
    }

    // handleUserNameBlur handle click on some other place
    handleUserNameBlur() {
        const updateUserName = $("#updateUserName").val();
        this.setState({usernameEdit: false});
        let userProfile = Meteor.user().profile;

        if (updateUserName) {
            userProfile['name'] = updateUserName;
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    profile: userProfile,
                }
            });
        }
    }

    // handle name edit
    handleUserNameEdit(event) {
        this.setState({usernameEdit: true});
        $('#tabBarDiv').hide();
    }

    showAdminUserName(userName) {
        if (userName.length > 15) {
            return userName.substring(0, 12) + "...";
        } else {
            return userName
        }
    }

    toggleShowEmail() {
        this.setState({showEmail: !this.state.showEmail})
    }

    toggleCurrentPassword() {
        this.setState({showCurrentPassword: !this.state.showCurrentPassword})
    }

    toggleNewPassword() {
        this.setState({showNewPassword: !this.state.showNewPassword})
    }

    toggleRepeatNewPassword() {
        this.setState({showRepeatNewPassword: !this.state.showRepeatNewPassword})
    }

    changePassword(e) {
        $("#invite_friends_create_popup").show();
        $('body').css('overflow', 'hidden');

        $("#invite_friends_create_popup_ok").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
        $(".deleteMeetingClose").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
    };

    handleChangePasswordClick() {
        let oldPassword = $("#current_password").val();
        let newPassword = $("#new_password").val();
        let repeatNewPassword = $("#repeat_new_password").val();

        if (newPassword === repeatNewPassword) {
            Accounts.changePassword(oldPassword, newPassword, function (err, result) {
                if (err){
                    console.log(err);
                    alert(err.reason)
                } else {
                    alert("password changed successfully");
                }
            })
        } else {
            alert("new passwords does not match");
        }
    }

    removeUser(){
        let user_id = Meteor.userId();
        Meteor.call('users.remove', user_id, function (err, result) {
            if (err) {
                console.error(err)
            } else {
                // Send mail to the user regarding removing of account
                let user_type=  Meteor.user().profile.role;
                let email = Meteor.user().emails[0].address;

                let emailOptions = {
                    user_type: user_type,
                    event_type: "account_remove",
                    user_id: user_id,
                    email: email
                };

                Meteor.call('emails.send', emailOptions, function (err, result) {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log(result)
                    }
                });
            }
        });
    }

    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        console.log(document.cookie)
    }

    handleLanguageChange() {
        // for swiden language use this setcookie("googtrans", "/en/sv", 30, "/", "");
        // For dutch language use this setcookie("googtrans", "/en/nl", 30, "/", "");
        // For english use this setcookie("googtrans", "", -30, "/", "");
        this.setCookie("googtrans", "", 30);
    }

    handleDeleteAccountClick() {
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    handleDeleteYes(){
        this.removeUser();
        this.setState({showDeletePopup: !this.state.showDeletePopup})
        window.location.reload();
    }

    handleDeleteNo(){
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    render() {
        const user = Meteor.user();

        return (
            <div>
                <div className={"height_100"}>
                    <div className={"mobile-admin-approval-service-div-header"}>
                        <div className={"mobile-admin-service-div"}>
                            <p className={"chattext"}>Settings</p>
                            <p className={"hanna-text paddingBottom15"}><span
                                className={"mobile_admin_dot"}>.</span>Admin: {this.showAdminUserName(this.props.userName)}
                            </p>
                        </div>
                    </div>

                    <div>
                        {/*Body Part*/}
                        <div className={"vendor-mobile-settings-block"}>
                            <div className="mobile-settings-div set-Username">
                                {
                                    this.state.usernameEdit ?
                                        <input className="mobile_userSettingPhoneEdit_input" type="text"
                                               onBlur={this.handleUserNameBlur.bind(this)}
                                               placeholder={user.profile.name}
                                               id="updateUserName" autoFocus/> :
                                        <h2 className={"settings-h2"} onClick={this.handleUserNameEdit.bind(this)}>
                                            {this.props.userName}
                                        </h2>
                                }
                                <img src={"/img/svg_img/user.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Admin Name</p>
                            </div>

                            <div className="mobile-settings-div set-category">
                                <h2 className={"settings-h2"}>{this.state.showEmail ? this.props.email : "**********"}</h2>
                                <img src={"/img/new_img/at_mail.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Email</p>
                                <img src={!this.state.showEmail ? "/img/new_img/settings_eyeLine.svg" : "/img/new_img/settings_eye_line.svg"}
                                     className={"mobile_user_settings_signOut_text_logo"}
                                     onClick={this.toggleShowEmail.bind(this)}/>
                            </div>

                            <div className="mobile-settings-div set-category">
                                <h2 className={"settings-h2"}>{this.props.email}</h2>
                                <img src={"/img/new_img/language.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Language</p>
                            </div>

                            <div className="mobile-settings-div set-category">
                                <h2 className={"settings-h2"}>*************</h2>
                                <img src={"/img/new_img/password.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Password</p>
                                {/*<img src={"/img/tabBarImg/red_logout.svg"}*/}
                                {/*className={"mobile_user_settings_signOut_text_logo"}/>*/}
                                <img src={"/img/new_img/password_arrow.svg"}
                                     onClick={this.changePassword.bind(this)}
                                     className={"mobile_user_settings_signOut_text_logo"}
                                     style={{"right": '17px',"width":"15px","top":"17px"}}/>
                            </div>

                            <div className="mobile-settings-div2 set-category" onClick={this.handleDeleteAccountClick.bind(this)}>
                                <h2 className={"settings-h2"}>Delete Account</h2>
                                <img src={"/img/new_img/delete_account.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>

                            <div className="mobile-settings-div2 set-Signout settings-margin-bottom"
                                 onClick={this.handleLogout.bind(this)}>
                                <img src={"/img/tabBarImg/red_logout.svg"}
                                     className={"mobile_user_settings_signOut_text_logo"}/>
                                <h2 className={"settings-h2"}>Sign out</h2>
                            </div>

                            <div id={"invite_friends_create_popup"}>
                                <div className={"inner_div"}>
                                    <span className="deleteMeetingClose">&times;</span>
                                    <p className={"invite"}>Change Password</p>
                                    <div className="invite_email changePassword">
                                        <div>
                                            <input type={"password"} id={"current_password"}
                                                   placeholder="CURRENT PASSWORD"/>
                                            <img
                                                src={!this.state.showCurrentPassword ? "/img/new_img/settings_eye_line.svg" : "/img/new_img/settings_eyeLine.svg"}
                                                className={"mobile_user_settings_signOut_text_logo"}
                                                onClick={this.toggleCurrentPassword.bind(this)}/>
                                        </div>
                                    </div>
                                    <div className="invite_email changePassword">
                                        <div>
                                            <input type={"password"} id={"new_password"} placeholder="NEW PASSWORD"/>
                                            <img src={!this.state.showNewPassword ? "/img/new_img/settings_eye_line.svg" : "/img/new_img/settings_eyeLine.svg"}
                                                 className={"mobile_user_settings_signOut_text_logo"}
                                                 onClick={this.toggleNewPassword.bind(this)}/>
                                        </div>
                                    </div>
                                    <div className="invite_email changePassword">
                                    <div>
                                    <input type={"password"} id={"repeat_new_password"}
                                               placeholder="REPEAT NEW PASSWORD"/>
                                        <img src={!this.state.showRepeatNewPassword ? "/img/new_img/settings_eye_line.svg" : "/img/new_img/settings_eyeLine.svg"}
                                             className={"mobile_user_settings_signOut_text_logo"}
                                             onClick={this.toggleRepeatNewPassword.bind(this)}/>
                                    </div>
                                       
                                    </div>
                                    <button id={"invite_friends_create_popup_ok"}
                                            onClick={this.handleChangePasswordClick.bind(this)}>Change
                                    </button>
                                </div>
                            </div>
                        
                        {
                            this.state.showLanguagePopup?
                            <div className="popup_wrapper">
                                {/* alt Language popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item">
                                                <h2>Language</h2>
                                                <div className="radio_button_wrapper">
                                                    <div className="remember_div padding_top_inial">
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="on" />
                                                            <span>English</span>
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="off" />
                                                            <span>Sevenska</span>
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="off" />
                                                            <span>Nederland</span>
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="off" />
                                                            <span>Hindi</span>
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :""
                        }
                        
                        {
                            this.state.showDeletePopup ?
                            <div className="popup_wrapper">
                                {/* Delete Event popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item delete_wrapper">
                                                <h2>Delete Event</h2>
                                                <div className="text_content_wrapper">
                                                    <div className="text_container">
                                                        <p>Are you sure you want to delete your account</p>
                                                    </div>
                                                    <div className="button_wrapper">
                                                        <button className="first_button" onClick={this.handleDeleteYes.bind(this)}>Yes</button>
                                                        <button onClick={this.handleDeleteNo.bind(this)}>No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :""
                        }

                        </div>
                    </div>
                    <AdminTabBar/>
                </div>
            </div>
        )
    }
}

export default withTracker(() => {
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    const email = Meteor.user() ? Meteor.user().emails[0].address : '';
    return {
        userName: userName,
        email: email,
    }
})(Settings);