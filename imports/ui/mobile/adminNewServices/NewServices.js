import React from 'react';
import './new-services.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import {Sellers} from "../../../api/sellers";
import { withTracker } from 'meteor/react-meteor-data';
import ShowEachService from './ShowEachService';

class NewServices extends React.Component {
    renderSellers() {
        return this.props.sellers.map((seller, index) => (
            <ShowEachService seller={seller} key={index} />
        ))
    }

    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-service-div"}>
                        <p className={"chattext"}>New Services</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: Mati</p>
                    </div>
                </div>

                <div className={"mobile-adminNewServices-block"}>
                    <ul>
                        {this.renderSellers()}
                    </ul>

                </div>
                <AdminTabBar/>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({},{sort:{createdAt:-1},limit: 3}).fetch();

    return {
        sellers: sellers,
    }
})(NewServices);

