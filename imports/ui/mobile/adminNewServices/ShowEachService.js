import React from 'react';

class ShowEachService extends React.Component {
    render() {
        const seller = this.props.seller;
        console.log(seller);

        const createdAt = seller.createdAt.toString();
        const day = seller.createdAt.getDay();

        let weekday = new Array(7);
        weekday[0] =  "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";

        return (
            <li className={"list-group-item mobile-admin-chat-list"}>
                <div className={"admin-chatbox"}>
                    <div className={"admin-chatImg"}>
                        <img src={seller.profile.logoImage} alt={"Img"} />
                    </div>
                    <div className={"admin-chatBody"}>
                        <p className={"firstPclass"}>{seller.name}</p>
                        <p className={"secondPclass"}>{seller.profile.address}</p>
                    </div>
                    <div className={"admin-chatCount"}>
                        <p>{weekday[day]}</p>
                        <p>{createdAt.substring(16, 21)}</p>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachService;