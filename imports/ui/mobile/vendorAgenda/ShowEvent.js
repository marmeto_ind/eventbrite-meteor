import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class ShowEvent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            event: '',
        };
    }

    componentDidMount() {
        const date = this.props.selectedDate;
        const userId = Meteor.userId();
        Meteor.call('events.getBookedEvent', userId, date, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                // console.log(result);
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        const self = this;
        const date = nextProps.selectedDate;
        // console.log(date);

        const userId = Meteor.userId();
        Meteor.call('events.getBookedEvent', userId, date, function (err, result) {
            if(err) {
                self.setState({event: ""});
                console.error(err);
            } else {
                // console.log(result);
                if(result) {
                    self.setState({event: result});
                } else {
                    self.setState({event: ""});
                }
            }
        });
    }

    handleEventClick() {
        // console.log("show event click");
        browserHistory.push("/mobile/vendor/date/" + this.state.event._id);
    }

    render() {
        // console.log(this.props.selectedDate);

        if(this.state.event) {
            // console.log(this.state.event);
            const event = this.state.event;
            // console.log(event);
            return (
                <div className={"mobile-vendor-agenda-div"} onClick={this.handleEventClick.bind(this)}>
                    <ul className="list-group mobile-vendor-agenda-div-background mobile-cvendor-agenda-list">
                        <li className="list-group-item div-left-icon  ">
                            <img className="create-add all-div-icon mobile-agenda-notifi-div " src="/img/new_img/bellIcon.svg" alt="" />
                        </li>
                        <li className="list-group-item text-list-div-style  mobile-agenda-name">
                            {event.name}
                        </li>
                        <li className="list-group-item text-list-div-style event-list1 ">
                            {/* <hr className={"hr-width"}/> */}
                            <p class="mobile_agenda_event_line"><img src="/img/mobile_searcBox_line.png" /></p>
                        </li>
                        <li className="list-group-item text-list-div-style  event-list2 ">
                            {event.date}
                        </li>
                        <li className="list-group-item text-list-div-style  event-list3">
                            Exclusive pack
                        </li>
                        <li className="list-group-item text-list-div-style  event-list4">
                            {event.numOfGuest}
                        </li>
                    </ul>
                </div>
            )
        } else {
            return (
                <div className={"mobile-no-event-booked-div"}>
                    <ul className="list-group mobile-vendor-agenda-div-background mobile-cvendor-agenda-list">
                        <li className="list-group-item div-left-icon mobile_no_event_booked ">
                            <img className="create-add all-div-icon mobile_no_events_notifi" src="/img/new_img/bellIcon.svg" alt="" />
                            No Upcoming events
                        </li>
                        {/*<li className="list-group-item text-list-div-style  mobile-agenda-name">*/}
                            {/*Hanna & Montana*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style event-list1 ">*/}
                            {/*<hr className={"hr-width"}/>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style  event-list2 ">*/}
                            {/*SATurday kl 21:00*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style  event-list3">*/}
                            {/*Exclusive pack*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style  event-list4">*/}
                            {/*430*/}
                        {/*</li>*/}
                    </ul>
                </div>
            )
        }

    }
}

export default ShowEvent;

// export default withTracker((data) => {
//     const date = data.selectedDate;
//     return {
//
//     }
// })(ShowEvent);