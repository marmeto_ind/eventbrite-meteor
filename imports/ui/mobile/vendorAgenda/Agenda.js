import React from 'react';
import './agenda.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import MobileCalendar from "./MobileShowCalendar";
import ShowEvent from './ShowEvent';
import {Meteor} from "meteor/meteor";
import {browserHistory} from "react-router";

class Agenda extends React.Component {
    constructor(props) {
        super(props);

        const isoDate = new Date();
        const today = isoDate.toString().substring(0, 15);

        this.state = {
            selectedDate: today,
            dates: []
        };
    }

    parentCallbackSetDate(dates) {
        this.setState({dates: dates})
    }

    handleCalendarDayClick(date) {
        this.setState({selectedDate: date});

        if(!this.state.dates.includes(date)) {
            browserHistory.push("/mobile/vendor/add-booking?date="+date);
        }
    }

    render() {

        return (
            <div>
                <div className={"height_100"}>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>AGENDA</p>
                        <p className={"hanna-text"}><span className={"mobile_vendor_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>
                    </div>
                </div>

                <div className={"mobile-vendor-agenda-block"}>
                    <ShowEvent selectedDate={this.state.selectedDate}/>
                </div>

                <div  className={"mobile-vendor-agenda-calender"}>
                    <MobileCalendar
                        parentCallback={this.handleCalendarDayClick.bind(this)}
                        parentCallbackSetDate={this.parentCallbackSetDate.bind(this)}
                    />
                </div>

                <VendorBottomTabBar/>
            </div>
            </div>
        )
    }
}

export default Agenda;