import React from 'react';
import $ from 'jquery';
import 'jquery-ui';
import 'moment/min/moment.min';
import 'fullcalendar/dist/fullcalendar.css';
import 'fullcalendar/dist/fullcalendar';
import moment from "moment";
import { withTracker } from 'meteor/react-meteor-data';

class MobileCalendar extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            selectedDate: '',
            dates: [],
            complete: false
        }
    }

    handleDayClick(date, jsEvent, view) {
        const parentCallback = this.props.parentCallback;

        this.setState({selectedDate: date.toDate().toString().substring(0, 15)});

        parentCallback(this.state.selectedDate);
        // $(this).css('background-color', 'red');
    }

    handleSpecificDayIcon(date,cell) {
        _.each(this.state.dates, function (data) {
            const specificEventDate = moment(data).add(12, 'hours');
            if (date.isSame(specificEventDate, "day")) {
                cell.css("background-image", "url('/img/greyStatue.png')");
                cell.css("background-size", "35px 35px");
                cell.css("position", "relative");
                cell.css("background-repeat", "no-repeat");
                cell.css("background-position", "50% 50%");
            }
        })
    }

    componentDidMount(){
        const self = this;
        const userId = Meteor.userId();

        const parentCallbackSetDate = this.props.parentCallbackSetDate;

        Meteor.call('events.getDates', userId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                self.setState({
                    dates: result,
                    complete: true,
                });
                parentCallbackSetDate(result);
            }
        });
        // if($("#mobile_calendar").is(":visible")) {
        //     var dateLength = $('#mobile_calendar .fc-toolbar h2').text().length;
        //     if(dateLength <= 9) {
        //         console.log("Length is Less");
        //         $('#mobile_calendar .fc-center').css('width','34%');
        //     }
    
        //     else {
        //         console.log("Length is increased");
        //         $('#mobile_calendar .fc-center').css('width','60%');
        //     }
        // }

        // else {
        //     console.log("Not Visible");
        // }
     

    }

    render() {
        // console.log(this.state.dates);

        if(this.state.complete === true ) {
            $('#mobile_calendar').fullCalendar({
                header : {
                    left : "prev",
                    center : "title",
                    right : "next"
                },
                dayRender: this.handleSpecificDayIcon.bind(this),
                dayClick: this.handleDayClick.bind(this)
            });
            var dateLength = $('#mobile_calendar .fc-toolbar h2').text().length;
            if(dateLength <= 10) { 
                $('#mobile_calendar .fc-center').css('width','34%');
            }
    
            else {
                $('#mobile_calendar .fc-center').css('width','60%');
            }

            $('#mobile_calendar .fc-center').bind("DOMSubtreeModified",function(){
                var dateLength = $('#mobile_calendar .fc-toolbar h2').text().length;
                var dateText = $('#mobile_calendar .fc-toolbar h2').text();
                var dateTxtValue = dateText.split(" ")[0];

                if(dateTxtValue == 'September') {
                    $('.mobile-vendor-agenda-calender #mobile_calendar .fc-toolbar h2').css('font-size','24px');
                }

                else if(dateTxtValue == 'March') {
                    $('.mobile-vendor-agenda-calender #mobile_calendar .fc-toolbar h2').css('font-size','21px');
 
                }

                else {
                    $('.mobile-vendor-agenda-calender #mobile_calendar .fc-toolbar h2').css('font-size','25px');
                }
            if(dateLength <= 10) {
                $('#mobile_calendar .fc-center').css('width','34%');
            }
    
            else {
                $('#mobile_calendar .fc-center').css('width','60%');
            }
              });
              
        }

        return (
            <div id="mobile_calendar"></div>
        )
    }
}

export default MobileCalendar;

