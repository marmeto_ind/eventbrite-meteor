import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import Agenda from './Agenda';

class VendorAgendaContainer extends React.Component {
    componentWillMount() {
        // console.log(Meteor.user());
    }

    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/vendor/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "vendor") {
                    // Meteor.logout();
                    browserHistory.push("/mobile/vendor/login");
                    return (<div></div>);
                } else {
                    return (
                        <Agenda/>
                    )
                }
            }
        }
    }
}


export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(VendorAgendaContainer);

