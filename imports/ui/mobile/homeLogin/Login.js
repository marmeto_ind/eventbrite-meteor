import React from 'react';
import './login.scss';
import {browserHistory} from 'react-router';

class Login extends React.Component {

    _handleLogin(e) {
        e.preventDefault();

        let email = $("#userEmail").val();
        let password = $("#userPassword").val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                if(user.profile.role === "user") {
                    browserHistory.push("/mobile/user/chats");
                } else {
                    Meteor.logout();
                    alert("Incorrect Credential");
                }
            }
            else {
                console.error(err);
                alert("Incorrect Credential");
            }
        });
    }

    render() {
        return (
            <div className="home-login-mobile-wrapper">
                <div className="home-login-mobile-container">
                    <div className="home-login-mobile-div-wrapper">
                        <div className="home-login-mobile-login-info">
                            <div className="relative-div">
                               <img className="home-login-img" src={"/img/logo_tick.png"} />
                                <div className={"home-login-absolute-div-style"}><span>Login</span></div>
                            </div>
                            <div className="home-login-form">
                                <form>
                                    <div className="email">
                                        <input type="text" id="userEmail" className="type-email " placeholder="EMAIL" />
                                        <p>username</p>
                                    </div>
                                    <div className="password">
                                        <input type="password" id="userPassword" className="type-password" placeholder="********" />
                                        <p>password</p>
                                    </div>

                                    <div className="relative-div home-login-button-div">
                                        <button className="text-style home-login-btn" onClick={this._handleLogin.bind(this)}>
                                            <img className="logo-home-first-img" src={"/img/logo_tick.png"} />
                                        </button>
                                        <div className={"absolute-div-style"}>Log In</div>
                                    </div>

                                    <div className="relative-div relativeposition">
                                        <div className={"home-welcome-img-div"}><img  src={"/img/leaf.png"} /></div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;