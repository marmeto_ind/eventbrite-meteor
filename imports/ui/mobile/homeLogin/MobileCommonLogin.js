import React from 'react';
import './login.scss';
import {browserHistory} from 'react-router';
import { Accounts } from 'meteor/accounts-base';
import { Route, Switch } from 'react-router-dom';
import { Tracker } from 'meteor/tracker';
import {withTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
// import MyEvents from "../../pages/userMyEvents/MyEvents";

class MobileCommonLogin extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            loading: true,
            showPassword: false,
            radioChecked: false
        };
        this.toggleRadioChecked = this.toggleRadioChecked.bind(this);
    }

    // Validation before submit
    validateEmail = function (elementValue) {
        let emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };

    componentDidMount() {
        const self = this;
        $(document).ready(function () {

            $('#userEmail').change(function () {
                let value = $(this).val();
                let valid = self.validateEmail(value);
                if (!valid) {
                    $(".wrong-details").show();
                    $(".user-not-exist").hide();
                    $(".input_field_empty").hide();
                    $(".password_input_field_empty").hide();
                    $(".wrong-password").hide();
                } else {
                    $(".wrong-details").hide();
                    $(".user-not-exist").hide();
                    $(".input_field_empty").hide();
                    $(".password_input_field_empty").hide();
                    $(".wrong-password").hide();
                }
            });

            $('#userPassword').change(function () {
                let password = $("#userPassword").val();
                if (password.length < 4) {
                    $(".password_input_field_empty").show();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".user-not-exist").hide();
                    $(".input_field_empty").hide();
                } else {
                    $(".password_input_field_empty").hide();
                    $(".wrong-password").hide();
                    $(".wrong-details").hide();
                    $(".user-not-exist").hide();
                    $(".input_field_empty").hide();
                }
            });

        });
    }

    backToHome(e) {
        browserHistory.push('/mobile/home/first');
    }

    forgotPassoword(e) {
        browserHistory.push("/mobile/forgotPassword");
    };

    _handleLogin(e) {
        e.preventDefault();
        let email = $("#userEmail").val();
        let password = $("#userPassword").val();

        let value = $("#userEmail").val();
        let valid = this.validateEmail(value);

        if (email.length === 0 && password.length === 0) {
            $(".password_input_field_empty").hide();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".user-not-exist").hide();
            $(".input_field_empty").show();

        } else if (!valid) {
            if (!valid) {

                $(".wrong-password").hide();
                $(".user-not-exist").hide();
                $(".wrong-details").show();
                $(".input_field_empty").hide();
                $(".password_input_field_empty").hide();
            } else {
                $(".wrong-password").hide();
                $(".user-not-exist").hide();
                $(".wrong-details").hide();
                $(".input_field_empty").hide();
                $(".password_input_field_empty").hide();
            }
        } else if (password.length < 4) {
            $(".password_input_field_empty").show();
            $(".wrong-password").hide();
            $(".wrong-details").hide();
            $(".user-not-exist").hide();
            $(".input_field_empty").hide();
        } else {
            if (!this.state.radioChecked) {
                $(window).on('beforeunload', function () {
                    // Alternatively you should be able to log out Meteor here (not tested), eg:
                    Meteor.logout();
                });
            }

            // Login if validated
            Meteor.loginWithPassword({email: email}, password, function (err) {
                if (!err) {
                    const user = Meteor.user();
                    // For User Login
                    if (user.profile.role === "user") {
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set('userId', userId);
                        Session.set('userProfile', userProfile);
                        browserHistory.push("/mobile/event");
                    }
                    // For Vendor Login
                    else if (user.profile.role === "vendor") {
                        const user = Meteor.user();
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set({
                            userId: userId,
                            userProfile: userProfile,
                        });
                        browserHistory.push("/mobile/vendor/chats");
                    }
                    // For Admin Login
                    else if (user.profile.role === "admin") {
                        const userId = user._id;
                        const userProfile = user.profile.role;
                        Session.set({
                            userId: userId,
                            userProfile: userProfile,
                        });
                        browserHistory.push("/mobile/admin/chats");
                    } else {
                        Meteor.logout();
                        alert("Incorrect Credential");
                    }
                } else {
                    console.error(err);
                    if (err.reason === 'User not found') {
                        $("#userEmail").select();
                        $(".wrong-password").hide();
                        $(".user-not-exist").show();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                    } else if (err.reason === 'Incorrect password') {
                        $("#userPassword").select();
                        $(".user-not-exist").hide();
                        $(".wrong-password").show();
                        $(".wrong-details").hide();
                        $(".input_field_empty").hide();
                        $(".password_input_field_empty").hide();
                    } else if (err.reason === 'Match failed') {
                        $(".user-not-exist").hide();
                        $(".wrong-password").hide();
                        $(".wrong-details").hide();
                        $(".input_field_empty").show();
                        $(".password_input_field_empty").hide();
                    }
                }
            });
        }
    }

    toggleShowPassword() {
        this.setState({showPassword: !this.state.showPassword})
    }

    toggleRadioChecked() {
        this.setState({radioChecked: !this.state.radioChecked})
    }

    render() {
        
        return (
            // <div className="home-login-mobile-wrapper">
            //     <div className="home-login-mobile-container">
            //         <div className="home-login-mobile-div-wrapper">
            //             <div className="home-login-mobile-login-info">
            //                 <div className="relative-div home-login-button-div  new_login_logo">
            //                     <div className="text-style">
            //                         <img className="logo-home-first-img" src={"/img/homeImg/Asset 4.svg"}/>
            //                     </div>
            //                     <h4  id="text1" className={"user-signup-login-text"}>Log in</h4>
            //                 </div>
            //                 <h5 className="mandatory_text">
            //                     Please enter username and password
            //                 </h5>
            //                 <h3 className="input_field_empty">Please enter username and password</h3>
            //                 <h3 className="user-not-exist">username Does not exist</h3>
            //                 <h3 className="wrong-password">Please enter your password correctly</h3>
            //                 <h3 className="wrong-details">Please enter valid email address</h3>
            //                 <h3 className="password_input_field_empty">You must provide atleast 4 characters</h3>
            //                 <div className="home-login-form">
            //                     <form>
            //                         <div className="email">
            //                             <input type="text" id="userEmail" className="type-email "
            //                                    placeholder="ENTER Username or Password"/>
            //                             <p>username</p>
            //                         </div>
                                    // <div className="password password_new">
                                    //     <input type="password" id="userPassword" type={!this.state.showPassword? "password": "text"} className="type-password"
                                    //            placeholder="********"/>
                                    //     <p>password</p>

                                    //     <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"} 
                                    //         alt="show password" 
                                    //         onClick={this.toggleShowPassword.bind(this)}
                                    //         className="show_password" />

                                    // </div>
            //                         <div className="remember_div">
            //                             <label className="radio" >
            //                                 <input name="radio" type="radio" />
            //                                 <span>Keep Me Logged In</span>
            //                             </label>
            //                         </div>
                                    // <div className="relative-div home-login-button-div">
                                    //     <button className="text-style home-login-btn"
                                    //             onClick={this.forgotPassoword.bind(this)}>
                                    //         <img className="logo-home-first-img"
                                    //              src={"/img/svg_img/forgotLock_small.svg"}/>
                                    //         <div className={"absolute-div-style"}>Forgot Password?</div>
                                    //     </button>
                                    // </div>
            //                         <div className="relative-div home-login-button-div">
            //                             <button className="text-style home-login-btn"
            //                                     onClick={this._handleLogin.bind(this)}>
            //                                 <img className="logo-home-first-img" src={"/img/homeImg/Asset 4.svg"}/>
            //                                 <div className={"absolute-div-style"}>Log In</div>
            //                             </button>
            //                         </div>
            //                         <div className="relative-div home-login-button-div">
            //                             <div className="text-style" onClick={this.backToHome.bind(this)}>
            //                                 <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"}/>
            //                                 <div className={"absolute-div-style back_top"}>Back</div>
            //                             </div>
            //                         </div>
            //                     </form>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>
            // </div>

            <div className="mobile_new_couple_signup">
                <div className="user-signup-wrapper">
                    <div className="user-signup-container new_couple_block">
                        <div className="user-signup-wrapper-content padding-top-0">
                            <div className="user-signup-login-info">
                                <div className="login-header">
                                    <span className="login-header-icon">
                                        <img className="logo-login-img"
                                            src={"/img/homeImg/Asset 4.svg"}/></span>
                                    <h4 id={"text1"} className={"user-signup-login-text"}>Sign in</h4>
                                </div>

                                <h3 className="input_field_empty">Please enter username and password</h3>
                                <h3 className="user-not-exist">username Does not exist</h3>
                                <h3 className="wrong-password">Please enter your password correctly</h3>
                                <h3 className="wrong-details">Please enter valid email address</h3>
                                <h3 className="password_input_field_empty">You must provide atleast 4 characters</h3>
                                <h3 className="username_field">Please Enter Username</h3>
                                <div className="login-form user-login-form-style">
                                    {/* <h5 className="mandatory_text">Please enter username and password</h5> */}
                                    <form>
                                        <div className="username-div">
                                            <input id="userEmail" type="text" className="type-username-sign type-email "
                                                    placeholder="ENTER Username"/>
                                            <p>username</p>
                                        </div>
                                        
                                        <div className="password password_new">
                                            <input id="userPassword" type={!this.state.showPassword? "password": "text"} className="type-password"
                                                placeholder="********"/>
                                            <p>password</p>

                                            <img src={!this.state.showPassword ? "/img/new_img/eye_line.svg" : "/img/eye.png"} 
                                                alt="show password" 
                                                onClick={this.toggleShowPassword.bind(this)}
                                                className="show_password" />
                                        </div>
                                        
                                        <div className="remember_div">
                                            <label className="radio" id={!this.state.radioChecked? "no-check": ""}>
                                                <input name="radio"                                                     
                                                    type="radio" checked="false" 
                                                    onClick={this.toggleRadioChecked} />
                                                <span>Keep Me Logged In</span>
                                            </label>
                                        </div>

                                        <div className="relative-div home-login-button-div">
                                            <button className="text-style home-login-btn"
                                                    onClick={this.forgotPassoword.bind(this)}>
                                                <img className="logo-home-first-img"
                                                    src={"/img/svg_img/forgotLock_small.svg"}/>
                                                <div className={"absolute-div-style"}>Forgot Password?</div>
                                            </button>
                                        </div>

                                        <div className="button_wrapper">
                                            <button className="create_button_signup" onClick={this._handleLogin.bind(this)}>
                                                <img className="logo-home-first-img" src="/img/homeImg/Asset 4.svg" />
                                                <div className="absolute_div_style_mobile letter_spacing_3">Sign In</div>
                                            </button>
                                            <button className="bck_button_signup" onClick={this.backToHome.bind(this)}>
                                                <img className="logo-home-first-img" src="/img/svg_img/backSvg.svg" />
                                                <div className="absolute_div_style_mobile">Back</div>
                                            </button>
                                        </div>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(MobileCommonLogin);

