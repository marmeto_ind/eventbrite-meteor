import React from 'react';
import './chats.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import ShowChatList from './ShowChatList';

class Chats extends React.Component {
    render() {

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-service-div"}>
                        <p  className={"chattext"}>CHAT</p>
                        <p className={"hanna-text"}>
                            <span className={"mobile_admin_dot"}>.</span>
                            Admin: {this.props.userName? this.props.userName: ''}
                        </p>
                    </div>
                </div>
                <ShowChatList/>
                <AdminTabBar/>
            </div>
        )
    }
}

export default withTracker(() => {
    return {
        userName: Meteor.user()? Meteor.user().profile.name: '',
    }
})(Chats);