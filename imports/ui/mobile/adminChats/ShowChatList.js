import React from 'react';
import ShowEachSeller from './ShowEachSeller';

class ShowChatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellers: [],
            users: []
        };
    }

    componentDidMount() {
        const self = this;
        let adminId = Meteor.userId();
        
        Meteor.call('seller.getAllSellers', function (err, result) {
            if(err) {
                console.error(err);
            } else {
                if(!result) {
                    self.setState({sellers: []})
                } else {
                    self.setState({sellers: result})
                }
            }
        });
        
        Meteor.call('chat_with.getUserChatListWithAdmin', adminId, function (error, result) {
            if (error) {
                console.log(error)
            } else {
                self.setState({users: result})
            }
        })
    }

    renderSellers() {
        return this.state.sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index} />
        ))
    }

    renderUsers() {
        return this.state.users.map((user, index) => (
            <ShowEachSeller user={user} key={index} />
        ))
    }

    render() {
        return (
            <div className={"mobile-adminChats-block"}>
                <ul>
                    {this.renderUsers()}
                    {this.renderSellers()}
                </ul>
            </div>
        )
    }
}

export default ShowChatList;