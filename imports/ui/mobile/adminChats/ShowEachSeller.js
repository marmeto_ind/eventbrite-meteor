import React from 'react';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';
import {Messages} from "../../../api/messages";
import {MessageView} from "../../../api/messageView";
import {browserHistory} from 'react-router';
import {emojify} from 'react-emojione';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerName: '',
            numOfMessage: '',
            sellerImage: '',
            showSlrDltBtn: false
        };
    }

    componentDidMount() {
        const self = this;
        const seller = this.props.seller;

        if (seller) {
            const sellerId = seller._id;
            const sellerName = seller.name;
            this.setState({sellerName: sellerName});

            Meteor.call('seller.findOne', sellerId, function (err, seller) {
                if (err) {
                    console.error(err);
                } else {
                    self.setState({sellerImage: seller.profile.logoImage})
                }
            })
        }
    }

    handleSellerClick(e) {
        e.preventDefault();
        if (e.target.id !== "crossIcon") {
            const {seller, user} = this.props;
            // console.log(seller);
            if (seller) {
                const sellerId = seller._id;
                const sellerName = seller.name;
                Meteor.call('chat_with.getRecordBySellerId', sellerId, function (err, result) {
                    if (err) {
                        console.error(err);
                    } else {
                        if (result) {
                            browserHistory.push("/mobile/admin/chat/" + Meteor.userId() + "/" + sellerId);
                        }
                        if (!result) {
                            const user = Meteor.user();
                            const userId = user._id;
                            const userName = user.profile.name;
                            Meteor.call('chat_with.insert', userId, sellerId, sellerName, userName, function (err, result) {
                                if (err) {
                                    console.error(err);
                                } else {
                                    browserHistory.push("/mobile/admin/chat/" + userId + "/" + sellerId);
                                }
                            })
                        }
                    }
                })
            } else {
                browserHistory.push("/mobile/admin/chat/" + Meteor.userId() + "/" + user.user+ "/user");
            }
        }
    }

    showMessage(message) {
        if (this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    handleImageError() {
        this.setState({sellerImage: 'https://via.placeholder.com/150'})
    }

    render() {
        let latestMessage = this.props.latestMessage ? this.props.latestMessage : '';
        let {seller, user} = this.props;

        if (seller) {
            return (
                <li
                    className={"mobile_chatbox_wrapper"}
                    onClick={this.handleSellerClick.bind(this)}
                >
                    <div className={"admin_chatbox_wrapper"}>
                        <div className={"admin-chatImg"}>
                            <img
                                src={this.state.sellerImage ? this.state.sellerImage : "/img/svg_img/user.svg"}
                                onError={this.handleImageError.bind(this)}
                                alt={"Img"}/>
                        </div>
                        <div className={"admin_chatBody_wrapper"}>
                            <p className={"firstPclass"}>{seller && seller.name}</p>
                            <p className={"secondPclass"}>
                                {this.showMessage(latestMessage)}
                            </p>
                        </div>
                        <div className={"admin-chatCount"}>
                            {
                                this.props.newMsgCount > 0 ?
                                    <span className="badge">{this.props.newMsgCount}</span> :
                                    <span>0</span>
                            }
                        </div>
                    </div>
                </li>
            )
        } else {
            return (
                <li
                    className={"mobile_chatbox_wrapper"}
                    onClick={this.handleSellerClick.bind(this)}
                >
                    <div className={"admin_chatbox_wrapper"}>
                        <div className={"admin-chatImg"}>
                            <img
                                src={this.state.sellerImage ? this.state.sellerImage : "/img/svg_img/user.svg"}
                                onError={this.handleImageError.bind(this)}
                                alt={"Img"}/>
                        </div>
                        <div className={"admin_chatBody_wrapper"}>
                            <p className={"firstPclass"}>{user.userName}</p>
                            <p className={"secondPclass"}>
                                {this.showMessage(latestMessage)}
                            </p>
                        </div>
                        <div className={"admin-chatCount"}>
                            {
                                this.props.newMsgCount > 0 ?
                                    <span className="badge">{this.props.newMsgCount}</span> :
                                    <span>0</span>
                            }
                        </div>
                    </div>
                </li>
            )
        }
        
    }
}

export default withTracker((data) => {    
    const {user, seller} = data;

    if (seller) {
        const sellerId = data.seller._id;
        const userId = Meteor.userId();

        Meteor.subscribe('messages.all');
        Meteor.subscribe('message_view.all');

        const latestMessageCollection = Messages.findOne({
            "user": userId,
            "seller": sellerId,
            "sender": "seller"
        }, {sort: {createdAt: -1}});

        let newMsgCount = 0;

        let msgView = MessageView.findOne({"user": userId, "seller": sellerId});

        let lastView = '';
        if (msgView) {
            lastView = msgView.updatedAt;
        }

        // Now check the number of message created after the time
        if (lastView) {
            newMsgCount = Messages.find({
                "user": userId,
                "seller": sellerId,
                "sender": "seller",
                "createdAt": {$gt: lastView}
            }).count()
        } else {
            newMsgCount = Messages.find({"user": userId, "seller": sellerId, "sender": "seller"}).count()
        }

        let latestMessage = "";
        if (latestMessageCollection) {
            latestMessage = latestMessageCollection.message;
        }

        return {
            latestMessage,
            newMsgCount
        }
    } else {
        return {}
    }    
})(ShowEachSeller)

