import React from 'react';
import './chats.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import {ChatWith} from "../../../api/chatWith";
import ShowEachChats from './ShowEachChats';

class MobileVendorChats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderChats() {
        const chats = this.props.chatUsers;
        const sellerId = this.props.sellerId;
        return chats.map((chat, index) => (
            <ShowEachChats chat={chat} key={index} sellerId={sellerId}/>
        ))
    }

    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-vendor-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"mobile_vendor_chattext"}>CHAT</p>
                        <p className={"hanna-text"}><span className={"mobile_vendor_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>
                    </div>
                </div>


                <div className={"VendorChatsBlock vendor_block_mobile"}>
                    <ul className={"list-group vendorchatsList"}>
                        {this.renderChats()}
                    </ul>
                </div>

                <VendorBottomTabBar/>
            </div>
        )
    }
}

export default withTracker(() => {
    const userId = Meteor.userId();
    let sellerId = '';
    let chatUsers = [];
    Meteor.subscribe('sellers.all');
    Meteor.subscribe('chat_with.all');
    if(userId) {
        const seller = Sellers.findOne({"user" : userId});
        if(seller) {
            sellerId = seller._id;
        }
    }
    if(sellerId) {
        chatUsers = ChatWith.find({"seller" : sellerId}).fetch();
    }
    return {
        sellerId: sellerId,
        chatUsers: chatUsers,
    }
})(MobileVendorChats);