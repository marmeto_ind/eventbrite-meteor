import React from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {emojify} from 'react-emojione';
import {withTracker} from 'meteor/react-meteor-data';
import {MessageView} from "../../../api/messageView";
import {Messages} from "../../../api/messages";

class ShowEachChats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageCount: '',
            lastMessage: '',
        };
    }

    componentDidMount() {
        const sellerId = this.props.sellerId;
        const user = this.props.chat.user;
        const self = this;

        try {
            Meteor.call('messages.getLatestMessageByUserIdSellerId', user, sellerId, function (err, result) {
                if (err) {
                    console.log(err)
                } else {
                    if (result) {
                        self.setState({lastMessage: result});
                    }
                }
            })
        } catch (e) {
            console.log(e)
        }
    }

    handleChatsClick = (e) => {
        e.preventDefault();
        const userId = this.props.chat.user;
        const sellerId = this.props.chat.seller;

        browserHistory.push("/mobile/vendor/chat/" + userId + "/" + sellerId);
    };

    showMessage(message) {
        if (this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    render() {
        const chat = this.props.chat;
        return (
            <li className="list-group-item mobile_user_chat_block_list_padding"
                onClick={this.handleChatsClick.bind(this)}>
                <div className="block1">
                    <span>
                        <img className={"chatsListIcons"} src={"/img/svg_img/user.svg"}/>
                    </span>
                </div>
                <div className={"chatPersonDetailsDiv"}>
                    <p className={"chatName"}>{chat.userName}</p>
                    <p className={"vendor_chatList_overflow"}>{this.showMessage(this.state.lastMessage && this.state.lastMessage.message)}</p>
                </div>
                <div className="block3">
                    {
                        this.props.newMsgCount > 0 ?
                            <span className="badge">{this.props.newMsgCount}</span> :
                            <span></span>
                    }
                </div>
            </li>
        )
    }
}

// Tracker
export default withTracker((options) => {
    let userId = options.chat.user;
    let sellerId = options.chat.seller;

    Meteor.subscribe('message_view.all');
    Meteor.subscribe('messages.all');

    let newMsgCount = 0;

    let msgView = MessageView.findOne({"user": userId, "seller": sellerId});

    let lastView = '';
    if (msgView) {
        lastView = msgView.updatedAt;
    }

    // Now check the number of message created after the time
    if (lastView) {
        newMsgCount = Messages.find({
            "user": userId,
            "seller": sellerId,
            "sender": "user",
            "createdAt": {$gt: lastView}
        }).count()
    } else {
        newMsgCount = Messages.find({"user": userId, "seller": sellerId, "sender": "user"}).count()
    }
    return {newMsgCount}
})(ShowEachChats);

