import React from 'react';
import './approval-service.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import ShowSeller from './ShowSeller';

class ApprovalService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seller: '',
            approveMsg:false
        };
    }

    handleApproveSeller(e) {
        e.preventDefault();
        const sellerId = this.props.seller._id;

        const result = Meteor.call('seller.approve', sellerId);
        if(!result) {
            this.setState({approveMsg:true})
            console.log("approved successfully");
        } else {
            console.log("some error occurred");
        }
    }



    render() {
        const seller = this.props.seller;

        let tickIcon = "/img/wendy_grey_tick.png";

        if(seller) {
            tickIcon = (seller.isApprove === true)? "/img/show-tick.png": "/img/wendy_grey_tick.png";
        }
        // console.log(this.state.approveMsg);
        const isApprove = this.state.approveMsg;

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-service-div"}>
                        <p className={"chattext"}>{seller? seller.name: ''}</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: {this.props.adminName}</p>
                    </div>
                </div>

                {/*Admin Approval Details*/}
                <div className={"mobile-admin-approval-service-block height_100"}>
                    <div className={"mobile-admin-approval-tick-div"}>
                        <div className={"block-one"}>
                            <p> Approve?</p>
                        </div>
                        <div className={"block-two"} onClick={this.handleApproveSeller.bind(this)}>
                            <img
                                src={tickIcon}
                                alt={"Img"}
                            />
                        </div>
                    </div>

                   <ShowSeller seller={seller} />
                   {isApprove? 
                    <div className="approve_msg">approved successfully</div>
                    :
                    ''}
                   <div className="relative-div news_service_button  home-login-button-div" onClick={this.handleApproveSeller.bind(this)}>
                        <div className="text-style">
                            <img className="service_back_logo-home-first-img" src="/img/newService/TICK.svg" />
                            <div className="back_top">approve</div>
                        </div>
                    </div>
                    
                </div>

                <AdminTabBar/>
            </div>
        )
    }
}

// export default ApprovalService;

export default withTracker((data) => {

    const sellerId = data.sellerId;
    const adminName = Meteor.user()? Meteor.user().profile.name: '';
    Meteor.subscribe('sellers.all');
    const seller = Sellers.findOne({"_id" : sellerId});

    return {
        adminName: adminName,
        seller: seller,
    }
})(ApprovalService);