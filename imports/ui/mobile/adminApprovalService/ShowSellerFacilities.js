import React from 'react';

class ShowEachFacility extends React.Component {
    render() {
        const facility = this.props.facility;

        // console.log(facility);

        return (
            <li  className="form-check SellerFacilityClass">
                <label className="form-check-label">
                    {facility.name}
                </label>
            </li>
        )
    }
}

class ShowSellerFacilities extends React.Component {
    renderFacilities() {
        return this.props.facilities.map((facility, index) => (
            <ShowEachFacility facility={facility} key={index}/>
        ))
    }

    render() {
        // console.log(this.props.facilities);

        return (
            <div className={"all-facility-dv admin-service-div admin-div-background"}>
                <ul>
                    {this.renderFacilities()}

                    {/*<li  className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                    {/*<li  className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                    {/*<li className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                    {/*<li  className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                    {/*<li  className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                    {/*<li  className="form-check SellerFacilityClass">*/}
                        {/*<label className="form-check-label">*/}
                            {/*Kitchen*/}
                        {/*</label>*/}
                    {/*</li>*/}
                </ul>
            </div>
        )
    }
}

export default ShowSellerFacilities;