import React from 'react';
import ShowOtherImages from './ShowOtherImages';
import ReactPlayer from 'react-player';
import ShowSellerFacilities from './ShowSellerFacilities';

class ShowSeller extends React.Component {
    render() {
        const seller = this.props.seller;
        // console.log(seller);

        if(!seller) {
            return (
                <div></div>
            )
        } else {
            return (
                <div className={"mobileAdminApprovalService"}>
                    <ul>
                        <li className={"list-group-item ulClass"}>
                            <div className={"mobile-adminSevice-first-div"}>
                                <img
                                    src={seller.profile.mainImage}
                                    alt={""}
                                />
                            </div>
                        </li>

                        <li className={"list-group-item scroll-div-margin ulClass"}>
                            <div className={"mobile-adminSevice-second-div"}>
                                <ShowOtherImages images={seller.profile.moreImages}/>
                                <div className={"mobile-admin-service-img-absolute-div"}>
                                    <img
                                        src={seller.profile.logoImage}
                                        alt={""}
                                    />
                                </div>

                            </div>
                        </li>
                        <li className={"list-group-item ulClass"}>
                            <div className={"mobile-adminSevice-first-div"}>
                                {/*<img src={"/img/venu.jpg"} alt={"logo"} />*/}
                                <ReactPlayer
                                    url={seller.profile.video}
                                    controls={true}
                                    width='100%'
                                    height='100%'
                                />
                            </div>
                        </li>

                        <li className={"list-group-item mobile-adminSevice-common-div username"}>
                            <div>
                                <p className={"nameClass"}>{seller.name}</p>
                                <p>Username</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div category"}>
                            <div>
                                <p className={"nameClass"}>{seller.category.name}</p>
                                <p>Category</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div credintilas"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.credential}</p>
                                <p>ORGANISATION NR</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div location"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.address}</p>
                                <p>Location</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div mail"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.email}</p>
                                <p>Mail</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div phone"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.phone}</p>
                                <p>Phonen</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div website"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.website}</p>
                                <p>Website</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div subscription"}>
                            <div>
                                <p className={"nameClass"}>{seller.profile.subscription}</p>
                                <p>Subscription</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div payment"}>
                            <div>
                                <p className={"nameClass"}>DEBIT CARD ****</p>
                                <p>Payment</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div timezone"}>
                            <div>
                                <p className={"nameClass"}>GMT</p>
                                <p>Timezone</p>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-adminSevice-common-div XXXX"}>
                            <div>
                                <p className={"nameClass"}>XXXX</p>
                                <p>XXXX</p>
                            </div>
                        </li>
                        <li className={"list-group-item my-business-li-class admin-facility-pack admin-facility-pack-mob"}>
                            <ShowSellerFacilities facilities={seller.profile.facilities} />
                        </li>
                        <li className="list-group-item pack-div packList">
                            <div>
                                <div className="heading-text">
                                    <h2>STANDARD PACK</h2>
                                </div>
                                <p>{seller.profile.standardPack}</p>
                            </div>

                            <div className={"tick-absolute"}>
                                <img src={"/img/wendy_grey_tick.png"} className={"admin-approve-tick"} />
                            </div>
                        </li>
                        <li className="list-group-item pack-div">
                            <div>
                                <div className="heading-text">
                                    <h2>GENEROUS PACK</h2>
                                </div>
                                <p>{seller.profile.generousPack}</p>
                            </div>
                            <div className={"tick-absolute"}>
                                <img src={"/img/wendy_grey_tick.png"} className={"admin-approve-tick"} />
                            </div>
                        </li>
                        <li className="list-group-item pack-div">
                            <div>
                                <div className="heading-text">
                                    <h2 className={"exclu-h2"}>EXCLUSIVE PACK</h2>
                                </div>
                                <p>{seller.profile.exclusivePack}</p>
                            </div>
                            <div className={"tick-absolute"}>
                                <img src={"/img/show-tick.png"} className={"admin-approve-tick"} />
                            </div>
                        </li>
                    </ul>

                </div>
            )
        }
    }
}

export default ShowSeller;