import React from 'react';

class ShowEachImage extends React.Component {
    render() {
        const image = this.props.image;
        // console.log(image);

        return (
            <div>
                <img src={image} alt={""} />
            </div>
        )
    }
}


class ShowOtherImages extends React.Component {
    renderImages() {
        return this.props.images.map((image, index) => (
            <ShowEachImage key={index} image={image} />
        ))
    }

    render() {
        // console.log(this.props.images);

        return (
            <div className={"mobile-admin-service-img-list-scroll"}>
                {this.renderImages()}
            </div>
        )
    }
}

export default ShowOtherImages