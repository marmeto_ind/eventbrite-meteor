import React from 'react';
import './mobileForgotPassword.scss';
import $ from 'jquery';
import { Accounts } from 'meteor/accounts-base'
import {browserHistory} from "react-router";


class MobileForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: '',
            successMessage: ''
        }
        this.backToHome = this.backToHome.bind(this);
        this.resetPassword = this.resetPassword.bind(this);
    }

    validateEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    backToHome(e) {
        browserHistory.push('/mobile/home/login');
    }

    resetPassword(e) {
        e.preventDefault();
        const userEmail = $("#userPasswordForgot").val();

        if(!userEmail) {
            this.setState({errorMessage: "Please enter valid username or mail"});
        }
        else if(!this.validateEmail(userEmail)) {
            this.setState({errorMessage: "Please enter valid username or mail"});
        } else {
            this.setState({errorMessage: ""});
            const self = this;
            Accounts.forgotPassword({email: userEmail}, function (err, result) {
                if (err) {
                    console.log(err);
                    self.setState({errorMessage: err.reason})
                } else {
                    // Send email to the customer regarding password change
                    let user_id = Meteor.userId();
                    let user_type=  Meteor.user().profile.role;
                    let emailOptions = {
                        user_type: user_type,
                        event_type: "password_reset",
                        user_id: user_id,
                        email: userEmail
                    };
                    Meteor.call('emails.send', emailOptions, function (err, result) {
                        if (err) {
                            console.error(err)
                        } else {
                            console.log(result)
                        }
                    });
                    self.setState({errorMessage: "We have sent one mail to reset your password"})
                    browserHistory.push('/mobile/home/login');
                }
            });

        }

    }

    render() {
        return (
            <div className="mobile-forgot-password-page page-login">
                <div className="main-body forgot-padding">
                    <div key="1">
                        <div className="">

                            {/* <div className="card card_background">
                                <div className="forgot-wrapper">
                                    <div className="forgot-container">
                                        <div className="forgot-div-wrapper">
                                            <div className="forgot-info">
                                                <div className="relative-div home-login-button-div  new_login_logo">
                               
                                                    <div className="text-style">
                                                        <img className="logo-home-first-img" src={"/img/svg_img/forgotLock.svg"} />
                                                    </div>
                                                    <div className={"absolute-div-style"}> Forgot Password</div>
                                                </div>
                                                <div className="login-form forgot_password_form">
                                                    <div className={"error-message"}>{this.state.errorMessage}</div>
                                                    <div className="email">
                                                        <input id="userEmail" type="text" className="type-email"  placeholder="ENTER USERNAME OR MAIL" />
                                                        <p>USERNAME</p>
                                                    </div>
                                                    <div style={{paddingTop:'40px'}} className="relativePosition">
                                                        <button className="text-style LogInbtnClass relative-div">
                                                            <img className="logo-home-first-img" src={"/img/svg_img/forgotLock_small.svg"} />
                                                            <div
                                                                className={"user-login-absolute-div-style forgot-top"}
                                                                onClick={this.resetPassword.bind(this)}
                                                            >
                                                                Reset
                                                            </div>
                                                        </button>
                                                    </div>

                                                    <div  className="relative-div home-login-button-div">
                                                        <div className="text-style relative-div" onClick={this.backToHome.bind(this)}>
                                                            <img className="logo-home-first-img" src={"/img/svg_img/backSvg.svg"} />
                                                            <div className={"absolute-div-style back_top"}>Back</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                            <div className="popup_wrapper">
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item delete_wrapper">
                                                <h2>Forgot Password</h2>
                                                <div className="text_content_wrapper padding-top-bottom-0">
                                                    <div className="text_container forgot_pasword">
                                                        {/* <p className="valid_username">Enter a valid email or username</p> */}
                                                        <p className={"error-message"}>{this.state.errorMessage}</p>
                                                        <input type="text" placeholder="Enter Mail"  id="userPasswordForgot" className="typePasswordForgot" maxLength="32"/>
                                                    </div>
                                                    <div className="button_wrapper">
                                                        <button className="first_button" onClick={this.backToHome}>back</button>
                                                        <button onClick={this.resetPassword.bind(this)}>reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MobileForgotPassword;
