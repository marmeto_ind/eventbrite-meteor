import React from 'react';
import './settings.scss';
import '../../pages/homePage/allpopup.scss';
import BottomTabBar from "../userBottomBar/BottomTabBar";
import {browserHistory} from 'react-router';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
            event: '',
            phoneEdit: false,
            usernameEdit: false,
            phoneUpdateValue: '',
            language: 'English',
            showDeletePopup: false,
            showLanguagePopup: false
        };
        this.removeUser = this.removeUser.bind(this);
        this.onLanguageChanged = this.onLanguageChanged.bind(this);
        this.setCookie = this.setCookie.bind(this);
        this.handleInviteFriendSubmitClick = this.handleInviteFriendSubmitClick.bind(this);
    }

    handleLogout(e) {
        e.preventDefault();
        Meteor.logout();
        browserHistory.push('/mobile/home/first');
    }

    invite_friends(e) {
        $("#invite_friends_create_popup").show();
        $('body').css('overflow', 'hidden');

        $("#invite_friends_create_popup_ok").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
        $(".deleteMeetingClose").click(function (e) {
            $("#invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
    };

    contact_us(e) {
        // $("#contact_us_create_popup").show();
        // $('body').css('overflow', 'hidden');
        // $("#contact_us_create_popup_ok").click(function (e) {
        //     $("#contact_us_create_popup").hide();
        //     e.preventDefault();
        //     $('body').css('overflow', 'auto');
        // });
        // $(".deleteMeetingClose").click(function (e) {
        //     $("#contact_us_create_popup").hide();
        //     e.preventDefault();
        //     $('body').css('overflow', 'auto');
        // });
        let userId = Meteor.userId();
        Meteor.call('users.getAdminId', function (error, admin) {
            if (error) {
                console.log(error)
            } else {
                let adminId = admin._id;
                browserHistory.push(`/mobile/user/chat/${userId}/${adminId}/admin`)
            }
        })
        // browserHistory.push("/mobile/user/chat/k6cXroii6mGqBjpbe/j33x3RFXnni2swspB/admin")
    };

    handleDeleteAccountClick() {
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    handleLanguageChangeClick() {
        this.setState({showLanguagePopup: !this.state.showLanguagePopup})
    }

    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    handleLanguageChange() {
        // for swiden language use this setcookie("googtrans", "/en/sv", 30, "/", "");
        // For dutch language use this setcookie("googtrans", "/en/nl", 30, "/", "");
        // For english use this setcookie("googtrans", "", -30, "/", "");
        this.setCookie("googtrans", "", 30);
    }

    componentDidMount() {
        this.handleLanguageChange();
        const userId = Meteor.userId();
        const self = this;
        Meteor.call('events.getUsersLatestEvent', userId, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                if (result) {
                    self.setState({event: result});
                }
            }
        })
    };

    handlePhoneNumberEdit(event) {
        this.setState({phoneEdit: true});
        $('#tabBarDiv').hide();
        $("#updatePhoneNumber").click()
    }

    handleUserNameEdit(event) {
        this.setState({usernameEdit: true});
        $('#tabBarDiv').hide();
        // $("#updatePhoneNumber").click()
    }

    savePhoneNumber(e) {
        const updatePhoneNumber = $("#updatePhoneNumber").val()
        this.setState({phoneEdit: false});
        $('#tabBarDiv').show();
        let userProfile = Meteor.user().profile;
        userProfile['phoneNumber'] = updatePhoneNumber;
        Meteor.users.update({_id: Meteor.userId()}, {
            $set: {
                profile: userProfile,
            }
        });
    }

    handlePhoneNUmberBlud() {
        const updatePhoneNumber = $("#updatePhoneNumber").val();
        this.setState({phoneEdit: false});
        $('#tabBarDiv').show();
        if (updatePhoneNumber) {
            let userProfile = Meteor.user().profile;
            userProfile['phoneNumber'] = updatePhoneNumber;
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    profile: userProfile,
                }
            });
        }
    }

    handleUserNameBlur() {
        const updateUserName = $("#updateUserName").val()
        this.setState({usernameEdit: false});
        $('#tabBarDiv').show();
        if (updateUserName) {
            let userProfile = Meteor.user().profile;
            userProfile['name'] = updateUserName;
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    profile: userProfile,
                }
            });
        }
    }

    handleContactUsSubmitClick() {
        let query = $("#contact_us").val();
        let user = Meteor.user();
        let email_msg = `email: ${ user && user.emails[0] && user.emails[0].address}, query: ${query}`;
        let customer_email = user && user.emails[0] && user.emails[0].address;
        Meteor.call(
            'sendEmail',
            'nipor87@hotmail.com',
            customer_email,
            'Customer Query',
            email_msg
        );
    }

    handleInviteFriendSubmitClick() {
        let invite_mail = $("#invite_mail").val();
        let user = Meteor.user();
        let userName = user && user.profile.name;
        let email_msg = `${userName} invited you to www.wendely.com`;
        
        Meteor.call(
            'sendEmail',
            invite_mail,
            'nipor87@hotmail.com',
            'Invite to www.wendely.com',
            email_msg
        );
    }

    showUserName(userName) {
        if (userName && userName.length > 20) {
            return userName.substring(0, 17) + "...";
        } else {
            return userName
        }
    }

    removeUser(){
        let user_id = Meteor.userId();
        Meteor.call('users.remove', user_id, function (err, result) {
            if (err) {
                console.error(err)
            } else {
                // Send mail to the user regarding removing of account
                let user_type=  Meteor.user().profile.role;
                let email = Meteor.user().emails[0].address;

                let emailOptions = {
                    user_type: user_type,
                    event_type: "account_remove",
                    user_id: user_id,
                    email: email
                };

                Meteor.call('emails.send', emailOptions, function (err, result) {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log(result)
                    }
                });
            }
        });
    }

    handleDeleteYes(){
        this.removeUser();
        this.setState({showDeletePopup: !this.state.showDeletePopup})
        window.location.reload();
    }

    handleDeleteNo(){
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }
    
    onLanguageChanged(e) {
        let language = e.currentTarget.lang;
        if (language === "english") {
            this.setState({language})
            this.setCookie("googtrans", "", -30, "/", "");
            window.location.reload();
        } else if (language === "swiden") {
            this.setState({language})
            this.setCookie("googtrans", "/en/sv", 30, "/", "");
            window.location.reload();
        } else if (language === "dutch") {
            this.setState({language})
            this.setCookie("googtrans", "/en/nl", 30, "/", "");
            window.location.reload();
        }
        this.setState({showLanguagePopup: !this.state.showLanguagePopup})
    }

    render() {

        const user = this.props.user;
        if (!user) {
            return (
                <div></div>
            )
        } else {
            return (
                <div>
                    <div className={"height_100"}>
                        <div className={"mobile-chat-conv-div-header"}>
                            <div className={"chat-conv-div"}>
                                <p className={"chattext"}>ALTERNATIVE</p>
                                <p className={"hanna-text paddingBottom15"}><span
                                    className={"mobile_user_dot"}>.</span>{this.showUserName(user.profile.name) || "GUEST" }</p>
                                {/* <p className={"usernameLine"}><img src={"/img/homeImg/usernameLine.png"} alt={""}/></p> */}
                            </div>
                        </div>

                        {/*Body Part*/}
                        <div className={"mobile-settings-block"}>
                            <div className="mobile-settings-div set-Username">
                                {
                                    this.state.usernameEdit ?
                                        <input className="mobile_userSettingPhoneEdit_input" type="text"
                                               onBlur={this.handleUserNameBlur.bind(this)}
                                               placeholder={user.profile.name} id="updateUserName" autoFocus/> :
                                        <h2 className={"settings-h2"} onClick={this.handleUserNameEdit.bind(this)}>
                                            {user.profile.name || "GUEST"}
                                        </h2>
                                }
                                <img src={"/img/svg_img/user.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>UserName</p>
                            </div>

                            {
                                user.emails[0].address != "guest@wendely.com" &&
                                <div className="mobile-settings-div set-Mail">
                                    <h2 className={"settings-h2"}>{user.emails[0].address}</h2>
                                    <img src={"/img/new_img/at_mail.svg"} className={"mobile_user_settings_text_logo"}/>
                                    <p>Mail</p>
                                </div>
                            }
                            {
                                user.emails[0].address != "guest@wendely.com" &&
                            <div className="mobile-settings-div set-Phonenr">
                                {
                                    this.state.phoneEdit ?
                                        <input className="mobile_userSettingPhoneEdit_input" type="text"
                                               onBlur={this.handlePhoneNUmberBlud.bind(this)}
                                               placeholder={user.profile.phoneNumber} id="updatePhoneNumber"/> :
                                        <h2 className={"settings-h2"} onClick={this.handlePhoneNumberEdit.bind(this)}>
                                            {user.profile.phoneNumber || "NO PHONE NUMBER"}
                                        </h2>
                                }
                                <img src={"/img/svg_img/phone.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Phonenr</p>
                            </div>
                            }

                            <div className="mobile-settings-div set-Mail" onClick={this.handleLanguageChangeClick.bind(this)}>
                                <h2 className={"settings-h2"}>{this.state.language}</h2>
                                <img src={"/img/new_img/language.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Language</p>
                            </div>
                            {
                                user.emails[0].address != "guest@wendely.com" &&
                                <div className="mobile-settings-div2 set-Invite" onClick={this.invite_friends.bind(this)}>
                                    <h2 className={"settings-h2"}>Invite Friends</h2>
                                    <img src={"/img/svg_img/invite.svg"} className={"mobile_user_settings_text_logo"}/>
                                </div>
                            }
                            {
                                user.emails[0].address != "guest@wendely.com" &&
                            <div className="mobile-settings-div2 set-Contact" onClick={this.contact_us.bind(this)}>
                                <h2 className={"settings-h2"}>Contact Us</h2>
                                <img src={"/img/svg_img/support.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>
                            }
                            {
                                user.emails[0].address != "guest@wendely.com" &&
                            <div className="mobile-settings-div2 set-Contact" onClick={this.handleDeleteAccountClick.bind(this)}>
                                <h2 className={"settings-h2"}>Delete Account</h2>
                                <img src={"/img/new_img/delete_account.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>
                            }

                            <div className="mobile-settings-div2 set-Signout settings-margin-bottom">
                                <h2 className={"settings-h2"} onClick={this.handleLogout.bind(this)}>Sign out</h2>
                                <img src={"/img/tabBarImg/red_logout.svg"}
                                     className={"mobile_user_settings_signOut_text_logo"}/>
                            </div>
                        </div>

                        <div id={"invite_friends_create_popup"}>
                            <div className={"inner_div"}>
                                <span className="deleteMeetingClose">&times;</span>
                                <p className={"invite"}>Invite friends</p>
                                <div className="invite_email">
                                    <input type={"email"} id={"invite_mail"} placeholder="ENTER EMAIL"/>
                                    <p className={"subtext_P"}>Mail</p>
                                </div>
                                <button id={"invite_friends_create_popup_ok"} onClick={this.handleInviteFriendSubmitClick}>Invite</button>
                            </div>
                        </div>

                        <div id={"contact_us_create_popup"}>
                            <div className={"inner_div"}>
                                <span className="deleteMeetingClose">&times;</span>
                                <p className={"invite"}>Contact Us</p>
                                <div className="invite_email">
                                    <textarea name="contact_us" id="contact_us" cols="30" rows="5"></textarea>
                                    {/* <p className={"subtext_P"}>Enter Query</p> */}
                                </div>
                                <button id={"contact_us_create_popup_ok"} onClick={this.handleContactUsSubmitClick.bind(this)}>Submit</button>
                            </div>
                        </div>
                        
                        {
                            this.state.showDeletePopup?                            
                            <div className="popup_wrapper">
                            {/* Delete Account popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item delete_wrapper">
                                                <h2>Delete Account</h2>
                                                <div className="text_content_wrapper">
                                                    <div className="text_container">
                                                        <p>Are you sure you want to delete your account</p>
                                                    </div>
                                                    <div className="button_wrapper">
                                                        <button className="first_button" onClick={this.handleDeleteYes.bind(this)}>Yes</button>
                                                        <button onClick={this.handleDeleteNo.bind(this)}>No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                        }

                        {
                            this.state.showLanguagePopup?
                            <div className="popup_wrapper">
                                {/* alt Language popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item">
                                                <h2>Language</h2>
                                                <div className="radio_button_wrapper">
                                                    <div className="remember_div padding_top_inial">
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="on" lang={"english"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>English</span>
                                                        </label>
                                                        <label className="radio" >
                                                            <input type="radio" name="radio" value="off" lang={"swiden"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>Sevenska</span>
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="off" lang={"dutch"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>Nederland</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :""
                        }                        
                        
                        <BottomTabBar/>
                    </div>
                </div>
            )
        }
    }
}

export default withTracker(() => {
    const user = Meteor.user() ? Meteor.user() : '';
    return {
        user: user,
    }
})(Settings);

