import React from 'react';
import './createEvent.scss';
import BottomTabBar from "../userBottomBar/BottomTabBar";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {Regions} from "../../../api/regions";
import {EventType} from "../../../api/eventType";
import {withTracker} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';
import {Sellers} from "../../../api/sellers";

class AddBooking extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: 'Date',
            guestNumber: '',
            regions: [],
            events: [],
            selectedRegion: '',
            selectedEventType: '',
            eventId: '',
        }
    }

    componentDidMount() {
        $('.region-dropdown .Select-input input').attr('readonly', true);
        $('.DayPickerInput input').attr('readonly', true);

        // Get date value from the url parameter
        let url_string = window.location.href;
        let url = new URL(url_string);
        let date = url.searchParams.get("date");
        if(date) {
            this.setState({date: date});
        }
    }

    handleDayChange(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;

        if (day !== currentDate) {
            this.setState({date: day});
        }
    }

    handleInputChange(event) {
        const value = event.target.value;

        if (value < 999999) {
            this.setState({guestNumber: event.target.value})
        }
    }

    handleRegionChange(data) {
        this.setState({selectedRegion: data.value});
    }

    handleEventTypeChange(data) {
        this.setState({selectedEventType: data.value})
    }

    _handleEventCreate() {
        const user = Meteor.userId();
        const sellerId = this.props.sellerId;

        const eventName = $('#eventName').val();
        const region = this.state.selectedRegion;
        const date = this.state.date.toString();
        const eventType = this.state.selectedEventType;
        const numberOfGuest = this.state.guestNumber;

        const self = this;

        if (!user) {
            browserHistory.push("/mobile/login");
        } else if (!eventName) {
            $(".event-name-err-msg").show().text("Event Name cannot be empty")
        } else if (!region) {
            $(".event-region-err-msg").show().text("Region is not selected")
        } else if (date.length <= 4) {
            $(".event-date-err-msg").show().text("Date can not be empty")
        } else if (!numberOfGuest) {
            $(".event-guest-err-msg").show().text("Number of guest cannot be empty")
        } else {
            Meteor.call('events.insert',
                eventName,
                region,
                eventType,
                date,
                numberOfGuest,
                user, function (err, result) {
                    if (err) {
                        console.error(err);
                    } else {
                        self.setState({eventId: result});
                        // Now associate the event with the seller
                        Meteor.call('choices.insert', user, sellerId, "testcategory", result, function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                browserHistory.push("/mobile/vendor/agenda");
                            }
                        })
                    }
                });
        }
    }

    render() {
        const errorStyle = {
            "display": "none",
            "color": "red",
            "textAlign": "center"
        };

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>

                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>Book Event</p>
                        <p className={"hanna-text"}><span className={"mobile_user_dot"}>.</span>{this.props.userName}
                        </p>
                    </div>
                </div>

                {/*Body Part*/}
                <div className={"mobile-create-event-block"}>
                    <div className="create-event-div name">
                        <input
                            type="text"
                            className="create-event-input-class"
                            placeholder="Enter Event"
                            id={"eventName"}
                        />
                        <p className={"create-event-p"}>Name</p>
                    </div>
                    <p className={"event-name-err-msg"}
                       style={errorStyle}></p>

                    {/* <div className="create-event-div event">
                        <div className={"mobile-event-dropdown-class region-dropdown"}>
                            <Select
                                name="form-field-name"
                                id={"eventTypeName"}
                                placeholder={"Event"}
                                onChange={this.handleEventTypeChange.bind(this)}
                                options={this.props.eventDropdown}
                                value={this.state.selectedEventType}
                            />
                        </div>
                        <p className={"create-event-p"}>Type</p>
                    </div>
                    <p className={"event-type-err-msg"}
                       style={errorStyle}></p> */}

                    <div className="create-event-div mobile-date-class date">
                        <DayPickerInput
                            onDayChange={day => this.handleDayChange(day)}
                            value={this.state.date}
                            dayPickerProps={{disabledDays: {before: new Date()}}}
                        />
                        <p className={"create-event-p"}>Date</p>
                    </div>
                    <p className={"event-date-err-msg"}
                       style={errorStyle}></p>

                    <div className="create-event-div guest">
                        <input
                            type="text"
                            className="create-event-input-class"
                            placeholder="Enter Guest"
                            value={this.state.guestNumber}
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <p className={"create-event-p"}>Guest</p>
                    </div>
                    <p className={"event-guest-err-msg"}
                       style={errorStyle}></p>

                    <div className={"mobile-create-button-div"}>
                        <button
                            className="mobile-create-event-button"
                            onClick={this._handleEventCreate.bind(this)}
                        >
                            <span>Book</span>
                        </button>
                    </div>
                </div>

                <BottomTabBar/>

            </div>
        )
    }
}

export default withTracker(() => {
    const currentUserId = Meteor.userId();
    Meteor.subscribe('sellers');
    const seller = Sellers.findOne({"user": currentUserId});
    let sellerId = '';
    if (seller) {
        sellerId = seller._id
    }

    Meteor.subscribe('regions.all');
    Meteor.subscribe('event_types.all');

    const regions = Regions.find({}).fetch();
    const eventTypes = EventType.find({}).fetch();

    let regionDropdown = [];
    let eventDropdown = [];

    regions.map((region) => {
        const value = {value: region.name, label: region.name};
        regionDropdown.push(value);
    });

    eventTypes.map((eventType) => {
        const value = {value: eventType.name, label: eventType.name};
        eventDropdown.push(value);
    });

    const userName = Meteor.user() ? Meteor.user().profile.name : '';

    return {
        regionDropdown: regionDropdown,
        eventDropdown: eventDropdown,
        userName: userName,
        sellerId: sellerId
    }
})(AddBooking);