import React from 'react';
import './createEvent.scss';
import BottomTabBar from "../userBottomBar/BottomTabBar";
import Select from 'react-select';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Regions } from "../../../api/regions";
import { EventType } from "../../../api/eventType";
import { withTracker } from 'meteor/react-meteor-data';
import { browserHistory } from 'react-router';
import { Events } from "../../../api/events";
import { ToastContainer, toast } from 'react-toastify';


class EditEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: 'Date',
            guestNumber: '',
            regions: [],
            events: [],
            selectedRegion: '',
            selectedEventType: '',
            eventId: '',
            country: '',
            region: ''
        }
    }

    componentDidMount() {
        $('.region-dropdown .Select-input input').attr('readonly', true);
        $('.DayPickerInput input').attr('readonly', true);
    }

    componentDidUpdate(prevProps) {
        // check when event data gets loaded then set the value
        if (this.props.event !== prevProps.event) {
            this.setState({ selectedRegion: this.props.event.region })
            this.setState({ date: this.props.event.date })
            this.setState({ selectedEventType: this.props.event.eventType })
            this.setState({ guestNumber: this.props.event.numOfGuest })
            $('#eventName').val(this.props.event.name);
        }
    }

    handleDayChange(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;
        if (day !== currentDate) {
            this.setState({ date: day });
        }
    }

    handleInputChange(event) {
        const value = event.target.value;
        if (value < 999999) {
            this.setState({ guestNumber: event.target.value })
        }
    }

    handleRegionChange(data) {
        this.setState({ selectedRegion: data.value });
    }

    handleEventTypeChange(data) {
        this.setState({ selectedEventType: data.value })
    }

    selectCountry(val) {
        this.setState({ country: val });
    }

    selectRegion(val) {
        this.setState({ region: val });
    }

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.TOP_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.TOP_CENTER
        });
    };
 
    _handleEventEdit(event) {
        const eventId = this.props.eventId;
        const user = Meteor.userId();
        const eventName = $('#eventName').val();
        const region = this.state.selectedRegion;
        const date = this.state.date.toString();
        const eventType = this.state.selectedEventType;
        const numberOfGuest = this.state.guestNumber;
        const self = this;

        if (!user) {
            browserHistory.push("/mobile/login");
        } else if (!eventName) {
            //alert("Event Name cannot be empty");
            this.errorNotify("Event Name cannot be empty");
        } else if (!region) {
            //alert("Region is not selected");
            this.errorNotify("Region is not selected");
        } else if (!eventType) {
            //alert("Event is not selected");
            this.errorNotify("Event is not selected");
        }
        else if (date.length <= 4) {
            //alert("Date can not be empty");
            this.errorNotify("Date can not be empty");
        }
        else if (!numberOfGuest) {
            //alert("Number of guest cannot be empty");
            this.errorNotify("Number of guest cannot be empty");
        } else {
            console.log({ eventId, eventName, region, eventType, date, numberOfGuest, user })
            this.successNotify("Saving Changes");
            // Meteor.call('events.updateEvent',
            //     eventId, eventName, region, eventType, date, numberOfGuest, user,
            //     function (err, result) {
            //         if (err) {
            //             console.log(err);
            //             this.errorNotify("Can't save.");
            //         } else {
            //             console.log("Status Update ", result);
            //             browserHistory.push("/mobile/event");
            //         }
            //     });

            Meteor.call('events.updateEvent', eventId, eventName, region, eventType, date, numberOfGuest, user, function (err, result) {
                if (err) {
                    toast.error("Can't save.", {
                        position: toast.POSITION.TOP_CENTER
                    });
                } else {
                    if (result == 1) {
                        toast.success("Details saved.", {
                            position: toast.POSITION.TOP_CENTER
                        });
                        browserHistory.push("/mobile/event");
                    } else {
                        toast.success("Please try after some time.", {
                            position: toast.POSITION.TOP_CENTER
                        });
                    }
                }


            });


            // mobile/event
        }
    }

    render() {
        const { selectedRegion, selectedEventType } = this.state;

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>Edit Event</p>
                        <p className={"hanna-text"}><span className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                    </div>
                </div>

                {/*Body Part*/}

                <div className={"mobile-create-event-block"}>
                    <div className="create-event-div name">
                        <input
                            type="text"
                            className="create-event-input-class"
                            placeholder="Enter Event"
                            id={"eventName"}
                        />
                        <p className={"create-event-p"}>Name</p>
                    </div>

                    <div className="create-event-div event">
                        <div className={"mobile-event-dropdown-class region-dropdown"}>
                            <Select
                                name="form-field-name"
                                id={"eventTypeName"}
                                placeholder={"Event"}
                                onChange={this.handleEventTypeChange.bind(this)}
                                options={this.props.eventDropdown}
                                value={this.state.selectedEventType}
                            />
                        </div>
                        <p className={"create-event-p"}>Type</p>
                    </div>

                    <div className="create-event-div region">
                        <div className={"mobile-region-dropdown-class region-dropdown"}>
                            <Select
                                name="form-field-name"
                                onChange={this.handleRegionChange.bind(this)}
                                id={"regionName"}
                                placeholder={"Region"}
                                options={this.props.regionDropdown}
                                value={this.state.selectedRegion}
                            />
                        </div>
                        <p className={"create-event-p"}>Region</p>
                    </div>


                    <div className="create-event-div mobile-date-class date">
                        <DayPickerInput
                            onDayChange={day => this.handleDayChange(day)}
                            value={this.state.date}
                            dayPickerProps={{ disabledDays: { before: new Date() } }}
                        />
                        <p className={"create-event-p"}>Date</p>
                    </div>
                    <ToastContainer />
                    <div className="create-event-div guest">
                        <input
                            type="text"
                            className="create-event-input-class"
                            placeholder="Enter Guest"
                            value={this.state.guestNumber}
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <p className={"create-event-p"}>Guest</p>
                    </div>

                    <div className={"mobile-create-button-div"}>
                        <button
                            className="mobile-create-event-button"
                            onClick={this._handleEventEdit.bind(this)}
                        >
                            <span>Edit</span>
                        </button>
                    </div>
                </div>


                <BottomTabBar />

            </div>
        )
    }
}

export default withTracker((data) => {
    // check what is coming in data
    let { eventId } = data;

    Meteor.subscribe('regions.all');
    Meteor.subscribe('event_types.all');
    Meteor.subscribe('events.all');

    const event = Events.findOne({ "_id": eventId });
    const regions = Regions.find({}).fetch();
    const eventTypes = EventType.find({}).fetch();
    const userName = Meteor.user() ? Meteor.user().profile.name : '';

    let regionDropdown = [];
    let eventDropdown = [];

    Meteor.call('event_types.checkIfBlank', function (err, result) {
        if (err) { console.log(err) }
    });

    regions.map((region) => {
        const value = { value: region.name, label: region.name };
        regionDropdown.push(value);
    });

    eventTypes.map((eventType) => {
        const value = { value: eventType.name, label: eventType.name };
        eventDropdown.push(value);
    });

    return { regionDropdown, eventDropdown, userName, event }
})(EditEvent);
