import React from 'react';
import './createEvent.scss';
import BottomTabBar from "../userBottomBar/BottomTabBar";
import Select from 'react-select';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {Regions} from "../../../api/regions";
import {EventType} from "../../../api/eventType";
import { withTracker } from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

class CreateEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: 'Date',
            guestNumber: '',
            regions: [],
            events: [],
            selectedRegion: '',
            selectedEventType: 'Wedding',
            eventId: '',
            time: '10:00',
        }
    }

    componentDidMount() {
        $('.region-dropdown .Select-input input').attr('readonly', true);
        $('.DayPickerInput input').attr('readonly', true);
    }

    handleDayChange(date) {
        const day = date.toString().substring(0, 15);
        const currentDate = this.state.date;

        if(day !== currentDate) {
            this.setState({date: day});
        }
    }

    handleInputChange(event) {
        const value = event.target.value;

        if(value < 999999) {
            this.setState({guestNumber: event.target.value})
        }
    }

    handleRegionChange(data) {
        this.setState({selectedRegion: data.value});
    }

    handleEventTypeChange(data) {
        this.setState({selectedEventType: data.value})
    }

    _handleEventCreate(event){
        const user = Meteor.userId();

        const eventName = $('#eventName').val();
        const region = this.state.selectedRegion;
        const date = this.state.date.toString();
        const eventType = this.state.selectedEventType;
        const numberOfGuest = this.state.guestNumber;

        const self = this;

        if(!user) {
            browserHistory.push("/mobile/login");
        } else if (!eventName) {
            alert("Event Name cannot be empty");
        } else if (!region) {
            alert("Region is not selected");
        } else if(date.length <= 4) {
            alert("Date can not be empty");
        } else if (!numberOfGuest) {
            alert("Number of guest cannot be empty");
        } else {
            Meteor.call('events.insert',
                eventName,
                region,
                eventType,
                date,
                numberOfGuest,
                user, function (err, result) {
                    if(err) {
                        console.error(err);
                    } else {
                        self.setState({eventId: result});
                        browserHistory.push("/mobile/user/my-event/" + result);
                    }
                });
        }
    }
    onChange = time => this.setState({ time })
    render() {
        return (
           <div>
               {/*Header Bar*/}
               <div className={"mobile-chat-conv-div-header"}>
                   <div className={"chat-conv-div"}>
                       <p className={"chattext"}>Create Event</p>
                       <p className={"hanna-text"}><span className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                   </div>
               </div>

               {/*Body Part*/}

               <div className={"mobile-create-event-block"}>
                   <div className="create-event-div name">
                       <input
                           type="text"
                           className="create-event-input-class"
                           placeholder="Enter Event"
                           id={"eventName"}
                       />
                       <p className={"create-event-p"}>Name</p>
                   </div>

                   {/* <div className="create-event-div event">
                       <div className={"mobile-event-dropdown-class region-dropdown"}>
                           <Select
                               name="form-field-name"
                               id={"eventTypeName"}
                               placeholder={"Event"}
                               onChange={this.handleEventTypeChange.bind(this)}
                               options={this.props.eventDropdown}
                               value={this.state.selectedEventType}
                           />
                       </div>
                       <p className={"create-event-p"}>Type</p>
                   </div> */}

                   <div className="create-event-div mobile-date-class date">
                       <DayPickerInput
                           onDayChange={day=> this.handleDayChange(day)}
                           value={this.state.date}
                           dayPickerProps={{ disabledDays: {before: new Date()} }}
                       />
                       <p className={"create-event-p"}>Date</p>
                   </div>

                   <div className="create-event-div region">
                       <div className={"mobile-region-dropdown-class region-dropdown"}>
                           <Select
                               name="form-field-name"
                               onChange={this.handleRegionChange.bind(this)}
                               id={"regionName"}
                               placeholder={"Region"}
                               options={this.props.regionDropdown}
                               value={this.state.selectedRegion}
                           />
                       </div>
                       <p className={"create-event-p"}>Region</p>
                   </div>

                   <div className="create-event-div guest">
                       <input
                           type="text"
                           className="create-event-input-class"
                           placeholder="Enter Guest"
                           value={this.state.guestNumber}
                           onChange={this.handleInputChange.bind(this)}
                       />
                       <p className={"create-event-p"}>Guest</p>
                   </div>                 
                   
                   <div className="create-event-div name">
                        <div className="event_content_fixed">
                            <input
                                type="text"
                                className="create-event-input-class"
                                placeholder="11:42"
                                id={"evenTime"}
                            />
                            <p className={"create-event-p"}>Time</p>
                       </div>
                   </div>
                   <div className="create-event-div name">
                        <div className="event_content_fixed">
                            <input
                                type="text"
                                className="create-event-input-class"
                                placeholder="Enter Event"
                                id={"eventNoteText"}
                            />
                            <p className={"create-event-p"}>Notes</p>
                       </div>
                   </div>

                   <div className={"mobile-create-button-div"}>
                       <button
                           className="mobile-create-event-button"
                           onClick={this._handleEventCreate.bind(this)}
                       >
                            <span>Create</span>
                       </button>
                   </div>

                   {/* <TimePicker
                    onChange={this.onChange}
                    value={this.state.time}
                    /> */}
               </div>

               <BottomTabBar/>

           </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('regions.all');
    Meteor.subscribe('event_types.all');

    const regions = Regions.find({}).fetch();
    const eventTypes = EventType.find({}).fetch();

    Meteor.call('event_types.checkIfBlank', function (err, result) {
        if (err) {console.log(err)}
    });

    let regionDropdown = [];
    let eventDropdown = [];

    regions.map((region) => {
        const value = { value: region.name, label: region.name };
        regionDropdown.push(value);
    });

    eventTypes.map((eventType) => {
        const value = { value: eventType.name, label: eventType.name };
        eventDropdown.push(value);
    });

    const userName = Meteor.user()? Meteor.user().profile.name: '';

    return {
        regionDropdown: regionDropdown,
        eventDropdown: eventDropdown,
        userName: userName,
    }
})(CreateEvent);