import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import CreateEvent from './createEvent';
import EditEvent from './editEvent';

class editEventContainer extends React.Component {
    render() {
        const eventId = this.props.params.eventId;
        
        if(this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    browserHistory.push("/mobile/login");
                    return (<div></div>);
                } else {
                    return (
                        <EditEvent eventId={eventId}/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(editEventContainer);

