import React from 'react';
// import MobileReviewsComponent from "../../mobile/mobileReviewComponent/mobileReviewComponent";
import {browserHistory} from 'react-router';

class ShowEachEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventName: '',
            eventDate: '',
            eventLocation: '',
            numOfGuest: '',
            eventImage: '',
        }
    }

    componentDidMount() {
        const event = this.props.event;
        const eventId = event._id;
        const self = this;
        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                self.setState({eventImage: result});
            }
        })
    }

    handleEventClick() {
        const event = this.props.event;
        browserHistory.push("/mobile/user/my-event/" + event._id);
    }

    handleButtonPress() {
        const event = this.props.event;
        this.buttonPressTimer = setTimeout(() => browserHistory.push("/mobile/event/edit/" + event._id), 1000);
    }

    handleButtonRelease() {
        clearTimeout(this.buttonPressTimer);
    }

    render() {
        const event = this.props.event;
        let dateStr = event.date.substring(0, 3) + event.date.substring(7, 10) + event.date.substring(3, 7) + event.date.substring(10, 15);

        return (
            <div className={"mobile-user-myEvents-div"}
                        onTouchStart={this.handleButtonPress.bind(this)}
                        onTouchEnd={this.handleButtonRelease.bind(this)}
                        onMouseDown={this.handleButtonPress.bind(this)}
                        onMouseUp={this.handleButtonRelease.bind(this)}
            >
                <ul className="list-group mobile-myevents-list-background mobile-upcoming-event-list"
                    onClick={this.handleEventClick.bind(this)}
                >
                    <li className="list-group-item div-left-icon  ">
                        <img
                            className="create-add all-div-icon"
                            src={this.state.eventImage}
                            alt=""/>
                    </li>
                    <li className="list-group-item text-list-div-style  event-img">
                        {event.name}
                    </li>
                    <li className="list-group-item text-list-div-style event-list1 ">
                        <p className={"mobile_eventLine"}><img src="/img/mobile_searcBox_line.png"/></p>
                    </li>
                    <li className="list-group-item text-list-div-style  event-list2 ">
                        {dateStr}
                    </li>
                    <li className="list-group-item text-list-div-style  event-list3">
                        {event.region}
                    </li>
                    <li className="list-group-item text-list-div-style  event-list4">
                        ({event.numOfGuest})
                    </li>
                    {/*<li className="list-group-item text-list-div-style  event-list4">*/}
                    {/*<MobileReviewsComponent />*/}
                    {/*</li>*/}
                </ul>
            </div>
        )
    }
}

export default ShowEachEvent;