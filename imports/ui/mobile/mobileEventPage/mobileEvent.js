import React from 'react';
import './mobileEvent.scss';
import MobileReviewsComponent from "../../mobile/mobileReviewComponent/mobileReviewComponent";
import BottomTabBar from '../userBottomBar/BottomTabBar';
import {browserHistory} from 'react-router';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Events} from "../../../api/events";
import ShowEachEvent from './ShowEachEvent';

class MobileEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state={};
    }

    handleNewEventClick = () => {
        browserHistory.push('/mobile/event/create');
    };


    renderEvents() {
        const events = this.props.events;
        return events.map((event, index) => (
            <ShowEachEvent event={event} key={index} />
        ))
    }

    render() {
        return (
            <div>
                <div className={"height_100"}>
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>MY EVENTS</p>
                        <p className={"hanna-text paddingBottom15"}><span className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                    </div>
                </div>

                {
                    !this.props.events?
                    <div className={"mobile-no-event-div"}>
                        <div className="mobile_no_event" >
                            No Event
                        </div>
                    </div>:
                    ''
                }
                
                {/*Body Part*/}
                
                <div className={"mobile-user-myEvents-block "}>
                
                    {this.renderEvents()}
                </div>
                

                

                <div className="create_button" onClick={this.handleNewEventClick.bind(this)}>
                    {/* <img src="/img/plus.svg" onClick={this.handleNewEventClick.bind(this)} /> */}
                    <a></a>
                </div>

                <BottomTabBar/>
                </div>

            </div>
        )
    }
}

export default withTracker(() => {
    const userId = Meteor.userId();
    Meteor.subscribe('events.all');
    const events = Events.find({"user" : userId}).fetch();
    const userName = Meteor.user()? Meteor.user().profile.name: '';

    return {
        events: events,
        userName: userName,
    }
})(MobileEvent);