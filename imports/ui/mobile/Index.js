import React from 'react';


class MobileIndex extends React.Component {
    render() {
        return (
            <div>
                <h2>Mobile App Page Link</h2>
            <ul>
                    <li><a href="/mobile/home/first">Mobile Home First</a></li>
                    <li><a href="/mobile/home/login">Mobile Home Login</a></li>
                    <li><a href="/mobile/vendor/login">Mobile Vendor Login</a></li>
                    <li><a href="/mobile/vendor/signup">Mobile Vendor Signup</a></li>
                    <li><a href="/mobile/home/new-couple">Mobile Home New Couple</a></li>
                    <li><a href="/mobile/home/new-service">Mobile Home New Service</a></li>
                    <li><a href="/mobile/user/category">Mobile User Category</a></li>
                    <li><a href="/mobile/user/chat">Mobile User Chat</a></li>
                    <li><a href="/mobile/user/chats">Mobile User Chats</a></li>
                    <li><a href="/mobile/user/guest-list">Mobile User Guest List</a></li>
                    <li><a href="/mobile/user/my-event">Mobile User My Event</a></li>
                    <li><a href="/mobile/user/search-event">Mobile User Search Event</a></li>
                    <li><a href="/mobile/user/service">Mobile User Service</a></li>
                    <li><a href="/mobile/user/settings">Mobile User Settings</a></li>
                    <li><a href="/mobile/event">Mobile Event Page</a></li>
                    <li><a href="/mobile/event/create">Mobile Create Event</a></li>
                    <li><a href="/mobile/vendor/agenda">Mobile Vendor Agenda</a></li>
                    <li><a href="/mobile/vendor/chat">Mobile Vendor Chat</a></li>
                    <li><a href="/mobile/vendor/chats">Mobile Vendor Chats</a></li>
                    <li><a href="/mobile/vendor/date">Mobile Vendor Date</a></li>
                    <li><a href="/mobile/vendor/my-business">Mobile Vendor My Business</a></li>
                    <li><a href="/mobile/vendor/service-edit">Mobile Vendor Service Edit</a></li>
                    <li><a href="/mobile/vendor/settings">Mobile Vendor Settings</a></li>
                    <li><a href="/mobile/vendor/statistics">Mobile Vendor Statistics</a></li>
                    <li><a href="/mobile/admin/login">Mobile Admin Login</a></li>
                    <li><a href="/mobile/admin/approvals">Mobile Admin Approvals</a></li>
                    <li><a href="/mobile/admin/approval-service">Mobile Admin Approval Services</a></li>
                    <li><a href="/mobile/admin/chat">Mobile Admin Chat</a></li>
                    <li><a href="/mobile/admin/chats">Mobile Admin Chats</a></li>
                    <li><a href="/mobile/admin/contact-us">Mobile Admin Contact Us</a></li>
                    <li><a href="/mobile/admin/new-services">Mobile Admin New Services</a></li>
                    <li><a href="/mobile/admin/new-users">Mobile Admin New Users</a></li>
                    <li><a href="/mobile/admin/settings">Mobile Admin Settings</a></li>
                    <li><a href="/mobile/admin/statistics">Mobile Admin Statistics</a></li>
                    <li><a href="/mobile/admin/mail">Mobile Admin Mail</a></li>
            </ul>

            </div>
        )
    }
}

export default MobileIndex;