import React from 'react';
import './settings.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import {browserHistory} from 'react-router';
import {Sellers} from "../../../api/sellers";
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seller: '',
            phoneEdit: false,
            adminId: '',
            vendorNameEdit: false,
            language: 'English',
        };
        this.removeUser = this.removeUser.bind(this);
        this.onLanguageChanged = this.onLanguageChanged.bind(this);
        this.setCookie = this.setCookie.bind(this);
    }

    handleLogout(e) {
        e.preventDefault();
        Meteor.logout();
        browserHistory.push('/mobile/home/first');
    }

    vendor_invite_friends(e) {
        $("#vendor_invite_friends_create_popup").show();
        $('body').css('overflow', 'hidden');

        $("#vendor_invite_friends_create_popup_ok").click(function (e) {
            $("#vendor_invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
        $(".deleteMeetingClose").click(function (e) {
            $("#vendor_invite_friends_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
    }

    contact_us(e) {
        $("#contact_us_create_popup").show();
        $('body').css('overflow', 'hidden');

        $("#contact_us_create_popup_ok").click(function (e) {
            $("#contact_us_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
        $(".deleteMeetingClose").click(function (e) {
            $("#contact_us_create_popup").hide();
            e.preventDefault();
            $('body').css('overflow', 'auto');
        });
    };

    componentDidMount() {
        this.handleLanguageChange();
        // const userId = Meteor.userId();
    }

    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        console.log(document.cookie)
    }

    handleLanguageChange() {
        // for swiden language use this setcookie("googtrans", "/en/sv", 30, "/", "");
        // For dutch language use this setcookie("googtrans", "/en/nl", 30, "/", "");
        // For english use this setcookie("googtrans", "", -30, "/", "");
        this.setCookie("googtrans", "", 30);
    }

    handlePhoneNumberEdit() {
        this.setState({phoneEdit: true});
        $('#tabBarDiv').hide();
    }

    savePhoneNumber(e) {
        const updatePhoneNumber = $("#updatePhoneNumber").val();
        this.setState({phoneEdit: false});
        $('#tabBarDiv').show();
        const sellerId = this.props.seller._id;
        Meteor.call('seller.updatePhoneNumber', sellerId, updatePhoneNumber)
    }

    handleAddBooking() {
        browserHistory.push('/mobile/vendor/add-booking');
    }

    handleContactUs(e) {
        const self = this;
        Meteor.call('users.getAdminId', function (err, result) {
            if (err) {
                console.log(err)
            } else {
                const adminId = result._id;
                Meteor.call('seller.findOneByUserId', Meteor.userId(), function (err, seller) {
                    if (err) {
                        console.log(err)
                    } else {
                        browserHistory.push('/mobile/vendor/chat/' + adminId + '/' + seller._id);
                    }
                })
            }
        })
    }

    handleContactUsSubmitClick() {
        let query = $("#contact_us").val();
        let email_msg = `email: ${Meteor.user().emails[0].address}, query: ${query}`;
        Meteor.call(
            'sendEmail',
            'nipor87@hotmail.com',
            'suvojit@marmeto.com',
            'Customer Query',
            email_msg
        );
    }

    // handleUserNameBlur handle click on some other place
    handleSellerNameBlur() {
        const updateSellerName = $("#updateSellerName").val();
        this.setState({vendorNameEdit: false});
        const sellerId = this.props.seller._id;
        if (updateSellerName) {
            Meteor.call('seller.updateSellerName', sellerId, updateSellerName)
        }
    }

    // handle name edit
    handleSellerNameEdit(event) {
        this.setState({vendorNameEdit: true});
        $('#tabBarDiv').hide();
        // $("#updateSellerName").focus()
    }

    handleDeleteAccountClick() {
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    handleLanguageChangeClick() {
        this.setState({showLanguagePopup: !this.state.showLanguagePopup})
    }

    // Function to remove user and send email
    removeUser(){
        let user_id = Meteor.userId();
        Meteor.call('users.remove', user_id, function (err, result) {
            if (err) {
                console.error(err)
            } else {
                // Send mail to the user regarding removing of account
                let user_type=  Meteor.user().profile.role;
                let email = Meteor.user().emails[0].address;

                let emailOptions = {
                    user_type: user_type,
                    event_type: "account_remove",
                    user_id: user_id,
                    email: email
                };

                Meteor.call('emails.send', emailOptions, function (err, result) {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log(result)
                    }
                });
            }
        });
    }

    handleDeleteYes(){
        this.removeUser();
        this.setState({showDeletePopup: !this.state.showDeletePopup})
        window.location.reload();
    }

    handleDeleteNo(){
        this.setState({showDeletePopup: !this.state.showDeletePopup})
    }

    onLanguageChanged(e) {
        let language = e.currentTarget.lang;
        if (language === "english") {
            this.setState({language})
            this.setCookie("googtrans", "", -30, "/", "");
            window.location.reload();
        } else if (language === "swiden") {
            this.setState({language})
            this.setCookie("googtrans", "/en/sv", 30, "/", "");
            window.location.reload();
        } else if (language === "dutch") {
            this.setState({language})
            this.setCookie("googtrans", "/en/nl", 30, "/", "");
            window.location.reload();
        }
        this.setState({showLanguagePopup: !this.state.showLanguagePopup})
    }

    render() {
        const seller = this.props.seller;

        if (!seller) {
            return (
                <div></div>
            )
        } else {
            return (
                <div>
                    <div className={"height_100"}>
                        {/*Header Bar*/}
                        <div className={"mobile-chat-conv-div-header"}>
                            <div className={"chat-conv-div"}>
                                <p className={"chattext"}>Settings</p>
                                {/*<p className={"hanna-text paddingBottom15"}><span*/}
                                    {/*className={"mobile_vendor_dot"}>.</span>{this.props.userName}</p>*/}
                                <p className={"hanna-text paddingBottom15"}><span
                                    className={"mobile_vendor_dot"}>.</span>{seller.name}</p>
                                {/* <p className={"usernameLine"}><img src={"/img/homeImg/usernameLine.png"} alt={""}/></p> */}
                            </div>
                        </div>

                        {/*Body Part*/}
                        <div className={"vendor-mobile-settings-block"}>
                            <div className="mobile-settings-div set-Username">
                                {/*<h2 className={"settings-h2"}>{seller.name}</h2>*/}
                                {
                                    this.state.vendorNameEdit ?
                                        <input className="mobile_userSettingPhoneEdit_input" type="text"
                                               onBlur={this.handleSellerNameBlur.bind(this)}
                                               placeholder={seller.name}
                                               id="updateSellerName" autoFocus /> :
                                        <h2 className={"settings-h2"} onClick={this.handleSellerNameEdit.bind(this)}>
                                            {seller.name}
                                        </h2>
                                }
                                <img src={"/img/svg_img/user.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Business Name</p>
                            </div>

                            <div className="mobile-settings-div2 set-Invite"
                                 onClick={this.vendor_invite_friends.bind(this)}>
                                <h2 className={"settings-h2"}>Invite Friends</h2>
                                <img src={"/img/svg_img/invite.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>
                            <div className="mobile-settings-div set-Mail" onClick={this.handleLanguageChangeClick.bind(this)}>
                                <h2 className={"settings-h2"}>{this.state.language}</h2>
                                <img src={"/img/new_img/language.svg"} className={"mobile_user_settings_text_logo"}/>
                                <p>Language</p>
                            </div>

                            <div className="mobile-settings-div2 set-Contact" onClick={this.handleDeleteAccountClick.bind(this)}>
                                <h2 className={"settings-h2"}>Delete Account</h2>
                                <img src={"/img/new_img/delete_account.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>

                            <div className="mobile-settings-div2 set-Contact" onClick={this.contact_us.bind(this)}>
                                <h2 className={"settings-h2"}>Contact Us</h2>
                                <img src={"/img/svg_img/support.svg"} className={"mobile_user_settings_text_logo"}/>
                            </div>
                            <div className="mobile-settings-div2 set-Signout settings-margin-bottom"
                                 onClick={this.handleLogout.bind(this)}>
                                <h2 className={"settings-h2"}>Sign out</h2>
                                <img src={"/img/tabBarImg/red_logout.svg"}
                                     className={"mobile_user_settings_signOut_text_logo"}/>
                            </div>
                        </div>

                        <div id={"vendor_invite_friends_create_popup"}>
                            <div className={"inner_div"}>
                                <span className="deleteMeetingClose">&times;</span>
                                <p className={"invite"}>Invite friends</p>
                                <div className="invite_email">
                                    <input type={"email"} id={"invite_mail"} placeholder="ENTER MAIL"/>
                                    <p className={"subtext_P"}>Mail</p>
                                </div>
                                <button id={"vendor_invite_friends_create_popup_ok"}>Invite</button>
                            </div>
                        </div>

                        <div id={"contact_us_create_popup"}>
                            <div className={"inner_div"}>
                                <span className="deleteMeetingClose">&times;</span>
                                <p className={"invite"}>Contact Us</p>
                                <div className="invite_email">
                                    <textarea name="contact_us" id="contact_us" cols="30" rows="5"></textarea>
                                    <p className={"subtext_P"}>Enter Query</p>
                                </div>
                                <button id={"contact_us_create_popup_ok"} onClick={this.handleContactUsSubmitClick.bind(this)}>Submit</button>
                            </div>
                        </div>
                        {
                            this.state.showDeletePopup?                            
                            <div className="popup_wrapper">
                            {/* Delete Account popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item delete_wrapper">
                                                <h2>Delete Account</h2>
                                                <div className="text_content_wrapper">
                                                    <div className="text_container">
                                                        <p>Are you sure you want to delete your account</p>
                                                    </div>
                                                    <div className="button_wrapper">
                                                        <button className="first_button" onClick={this.handleDeleteYes.bind(this)}>Yes</button>
                                                        <button onClick={this.handleDeleteNo.bind(this)}>No</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                        }

                        {
                            this.state.showLanguagePopup?
                            <div className="popup_wrapper">
                                {/* alt Language popup design */}
                                <div className="alt_chat_wrapper">
                                    <div className="popup_content">
                                        <div className="popup_content_conatiner">
                                            <div className="popup_item">
                                                <h2>Language</h2>
                                                <div className="radio_button_wrapper">
                                                    <div className="remember_div padding_top_inial">
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="on" lang={"english"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>English</span>
                                                        </label>
                                                        <label className="radio" >
                                                            <input type="radio" name="radio" value="off" lang={"swiden"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>Sevenska</span>
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" name="radio" value="off" lang={"dutch"} onChange={this.onLanguageChanged.bind(this)} />
                                                            <span>Nederland</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :""
                        }  
                        <VendorBottomTabBar/>
                    </div>
                </div>
            )
        }
    }
}

export default withTracker(() => {
    const userId = Meteor.userId();
    Meteor.subscribe('sellers.all');
    const seller = Sellers.findOne({"user": userId});
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    return {
        seller: seller,
        userName: userName
    }
})(Settings);