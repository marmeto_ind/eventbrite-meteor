import React from 'react';

class ShowEachFacility extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facilityImage: '',
        }
    }

    componentDidMount() {
        const facility = this.props.facility;
        const self = this;
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if (err) {
                console.log(err)
            } else {
                if (result) {
                    self.setState({ facilityImage: result.image });
                }
            }
        });
    }

    render() {
        const sellerFacility = this.props.facility;
        const mobileVendorAddFacilitymainImage = this.state.facilityImage;
        const mobileVendorAddFacilitydivStyle = {
            backgroundImage: 'url(' + mobileVendorAddFacilitymainImage + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '15px 15px',
            backgroundPosition: '45% 25%',
            textOverflow: 'ellipsis',
            // overflow: 'hidden',
            // whiteSpace: 'nowrap'
        };
        const textFix = {
            'white-space': 'nowrap',
            'text-overflow': 'ellipsis',
            'overflow': 'hidden'
        }
        
        return (
            // <li style={mobileVendorAddFacilitydivStyle} className='form-check SellerFacilityClass paddingBottom active' >
            //     <input className="form-check-input facility-check-box mobite-checkbox hidden" type="checkbox" />
                <li style={textFix}>
                    <img className="facility-img" src={mobileVendorAddFacilitymainImage}></img>
                    
                    <span>{sellerFacility.name}</span>
                </li>
            //     <img className="facility-img" src={mobileVendorAddFacilitymainImage}></img>
            //     <label className="form-check-label" htmlFor={sellerFacility._id}>
            //         {sellerFacility.name}
            //     </label>
            // </li>
        )
    }
}

export default ShowEachFacility;