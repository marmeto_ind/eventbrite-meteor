import React from 'react';
import './my-business.scss';
import ReactPlayer from 'react-player';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import { Sellers } from "../../../api/sellers";
import ShowEachImage from './ShowEachImage';
import ShowEachFacility from './ShowEachFacility';
import { Carousel } from 'react-responsive-carousel';

class MyBusiness extends React.Component {

    renderOtherPictures(images) {
        return images.map((image, index) => (
            <ShowEachImage image={image} key={index} />
        ))
    }

    renderFacilities(facilities) {
        return facilities.map((facility, index) => (
            <ShowEachFacility facility={facility} key={index} />
        ))
    }

    render() {
        const seller = this.props.seller;



        if (!seller) {
            return (
                <div></div>
            )
        } else {

            const img1 = seller.profile.logoImage;
            const img2 = seller.profile.mainImage;

            const LogoBackStyle = {
                backgroundImage: 'url(' + img1 + ')',
                padding: "20px 0px",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                overflow: "hidden",
                width: "100%",
                height: "171px"
            };
            const MainBackStyle = {
                backgroundImage: 'url(' + img2 + ')',
                padding: "20px 0px",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                overflow: "hidden",
                width: "100%",
                height: "171px"
            };

            // console.log(seller)

            return (
                <div>
                    <div className={"height_100"}>
                        {/*Header Bar*/}
                        <div className={"mobile-chat-conv-div-header"}>
                            <div className={"chat-conv-div"}>
                                <p className={"chattext"}>My Service</p>
                                <p className={"hanna-text"}><span className={"mobile_vendor_dot"}>.</span>{this.props.userName}</p>
                            </div>
                        </div>

                        {/*BodyPart*/}
                        <div className={"mobile-vendor-MyBusiness-block mobile_newservice_wrapper"}>
                            <div className={"mobile-vendor-myBusiness"}>
                                <form className={"vendor-my-business-form "}>
                                    <ul className={"list-group"}>
                                        <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                    {seller.name}
                                                </p>
                                                <img src={"/img/tabBarImg/grey_vendor.svg"} className={"mobile_user_settings_text_logo"} />
                                                <p className="mobile_myBuss_shortTitle">SERVICE NAME</p>
                                            </div>
                                        </li>
                                        <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                    {seller.category.name}
                                                </p>
                                                <img src={"/img/new_img/guest_list.svg"} className={"mobile_user_settings_text_logo"} />
                                                <p className="mobile_myBuss_shortTitle">CATEGORY</p>
                                            </div>
                                        </li>
                                        <li style={LogoBackStyle} className={"list-group-item  my-business-li-class"}>
                                        </li>
                                        <li style={MainBackStyle} className={"list-group-item  my-business-li-class"}>

                                        </li>
                                        <li style={{ padding: '0px', height: '171px' }} className={"list-group-item  my-business-li-class"}>
                                            <div>
                                                <div>
                                                    <div>
                                                        <Carousel
                                                            showArrows={true}
                                                            showIndicators={false}
                                                            autoPlay={true}
                                                            stopOnHover={true}
                                                            interval={5000}
                                                            transitionTime={350}>
                                                            {
                                                                seller.profile.moreImages.map((url, index) => (
                                                                    <div style={{ height: '171px' }} className={"carousel_img"} key={index}>
                                                                        <img height={"100%"} src={url} alt={""} />
                                                                    </div>
                                                                ))
                                                            }
                                                        </Carousel>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                        <li style={{ padding: '0px' }} className={"list-group-item  my-business-li-class"}>
                                            <div style={{ width: '100%' }} className="picture-upload">
                                                <div style={{ width: '100%' }} className="add-more-pic">
                                                    <div className="vendor-video-div">
                                                        <ReactPlayer
                                                            url={seller.profile.video}
                                                            controls={true}
                                                            width='100%'
                                                            height='100%'
                                                            playing
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="list-group-item vendor-leftSideBar vendor_myBusiness_detailsdiv">
                                            <div className={"vendorleftSide-text-div"}>
                                                <div>
                                                    <p className="">
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"} />
                                                    </p>
                                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                                    <div className="heading-text">
                                                        <h2 style={{ fontSize: '24px' }}>STANDARD MENU</h2>
                                                        {/* <img style={{marginTop:'-25px'}} src={"/img/mobile_searcBox_line.png"} /> */}
                                                    </div>
                                                    <p style={{ marginTop: '12px' }}>{seller.profile.standardPack}</p>
                                                </div>
                                            </div>
                                        </li>

                                        {/* <li className="list-group-item vendor-leftSideBar vendor_myBusiness_detailsdiv">
                                            <div className={"vendorleftSide-text-div"}>
                                                <div>
                                                    <p className="">
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                                    </p>
                                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                                    <div className="heading-text">
                                                        <h2 style={{fontSize:'24px'}} className={"vendor-genorous-pack"}>Generous MENU</h2>
                                                        <img style={{marginTop:'-25px'}}  src={"/img/mobile_searcBox_line.png"} />
                                                    </div>
                                                    <p style={{marginTop:'-18px'}}>{seller.profile.generousPack}</p>
                                                </div>
                                            </div>
                                        </li> */}
                                        <li className="list-group-item vendor-leftSideBar admin-approvals-list">
                                            <div className={"vendorleftSide-text-div"}>
                                                <div>
                                                    <div className="heading-text">
                                                        <h2 className={"vendor-genorous-pack"}>Generous PACK</h2>
                                                        {/* <img src={"/img/mobile_searcBox_line.png"} /> */}
                                                    </div>
                                                    {/* <h4>Baptzr/Main Courses/Dessert</h4>
                                                <p className="first_paragraph"> Welcome drink / chocolate</p> */}
                                                    <p className="second_paragraph"> {seller.profile.generousPack}</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li className="list-group-item vendor-leftSideBar vendor_myBusiness_detailsdiv">
                                            <div className={"vendorleftSide-text-div"}>
                                                <div>
                                                    <p className="">
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"} />
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"} />
                                                        <img className={"vendor-star"} src={"/img/star.png"} alt={"star"} />
                                                    </p>
                                                    <span className={"vendor-right-logo"}><i className="far fa-star"></i></span>
                                                    <div className="heading-text">
                                                        <h2 style={{ fontSize: '24px' }} className={"vendor-exclusive-pack"}>Exclusive MENU</h2>
                                                        {/* <img style={{marginTop:'-25px'}} src={"/img/mobile_searcBox_line.png"} /> */}
                                                    </div>
                                                    <p style={{ marginTop: '12px' }}>{seller.profile.exclusivePack}</p>
                                                </div>
                                            </div>
                                        </li>
                                        {/* <li className={"list-group-item my-business-li-class mobile_facility "}>
                                            <div className="facility-dv new_service_position">
                                                <div className="box-container same-icon new_service_facility">
                                                    <div className={"all-facility-dv mobile-facility-div facility-div-background"}>
                                                    {this.renderFacilities(seller.profile.facilities)}
                                                    </div>
                                                </div>
                                            </div>
                                        </li> */}
                                        <li className={"list-group-item my-business-li-class mobile_facility vendor-facility-pack"}>
                                            <div className="all-facility-dv vendor-business-div">
                                                {/* <div className="box-container same-icon new_service_facility">
                                                    <div className={"all-facility-dv mobile-facility-div facility-div-background"}> */}
                                                        <ul>
                                                            {this.renderFacilities(seller.profile.facilities)}
                                                        </ul>
                                                    {/* </div>
                                                </div> */}
                                            </div>
                                        </li>


                                        {seller.profile.area ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.area}
                                                    </p>
                                                    <img src={"/img/svg_img/credintials.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">AREA (M2)</p>
                                                </div>
                                            </li>
                                            : ''}
                                        {seller.profile.ceilingHeight ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.ceilingHeight}
                                                    </p>
                                                    <img src={"/img/svg_img/credintials.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">CEILING HEIGHT (M)</p>
                                                </div>
                                            </li>
                                            : ''}
                                        {seller.profile.maxPeople ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.maxPeople}
                                                    </p>
                                                    <img src={"/img/svg_img/credintials.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">MAXIMUM PEOPLE</p>
                                                </div>
                                            </li>
                                            : ''}
                                        {seller.profile.eventType && seller.profile.eventType.name ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.eventType.name}
                                                    </p>
                                                    <img src={"/img/svg_img/credintials.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">TYPES OF EVENTS VENUE IS SUITED FOR</p>
                                                </div>
                                            </li>
                                            : ''}
                                        {seller.profile.country ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.country}
                                                    </p>
                                                    <img src={"/img/svg_img/location_svg.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">COUNTRY</p>
                                                </div>
                                            </li>
                                            : ''}
                                        {seller.profile.region ?
                                            <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorCredintialsLogo vendor_myBusiness_detailsdiv">
                                                <div className=" no-padding vendor-SideBarContctDetails vendor-service-credentials">
                                                    <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                        {seller.profile.region}
                                                    </p>
                                                    <img src={"/img/svg_img/location_svg.svg"} className={"mobile_user_settings_text_logo"} />
                                                    <p className="mobile_myBuss_shortTitle">REGION</p>
                                                </div>
                                            </li>
                                            : ''}
                                        <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorLocationLogo vendor_myBusiness_detailsdiv">
                                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-location">
                                                <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                    {seller.profile.address}
                                                </p>
                                                <img src={"/img/svg_img/location_svg.svg"} className={"mobile_user_settings_text_logo"} />
                                                <p className="mobile_myBuss_shortTitle">ADDRESS</p>
                                            </div>
                                        </li>
                                        <li className="list-group-item vendor_info_button vendor-leftSideBar mobileVendorWebLogo vendor_myBusiness_detailsdiv">
                                            <div className=" no-padding vendor-SideBarContctDetails vendor-service-website">
                                                <p style={{ fontSize: '20px !important' }} className={"vendor_list_name_size"}>
                                                    {seller.profile.website}
                                                </p>
                                                <img src={"/img/svg_img/web_svg.svg"} className={"mobile_user_settings_text_logo"} />
                                                <p className="mobile_myBuss_shortTitle">Website</p>
                                            </div>
                                        </li>
                                        <li>
                                            {/* <div
                                                className="save_button vendor_page">
                                                <div className="text-style">
                                                    <img className="service_back_logo-home-first-img"
                                                        src={"/img/tabBarImg/new_service_icon.svg"} />
                                                    <div className={"absolute-div-style line-height-32"}>Save</div>
                                                </div>
                                            </div> */}
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <VendorBottomTabBar />
                    </div>
                </div >
            )
        }
    }
}

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    const userId = Meteor.userId();

    const seller = Sellers.findOne({ "user": userId });

    return {
        userName: userName,
        seller: seller,
    }
})(MyBusiness)