import React from 'react';

class ShowEachImage extends React.Component {
    render() {
        const image = this.props.image;

        return (
            <div className={"vendor-more-pictures-scroll-div"}>
                <img className="vendor-main-picture-size" src={image} />
            </div>
        )
    }
}

export default ShowEachImage;