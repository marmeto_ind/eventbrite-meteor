import React from 'react';
import './login.scss';
import {browserHistory} from 'react-router';

class MobileAdminLogin extends React.Component {
    _handleLogin(e) {
        e.preventDefault();

        let email = $("#adminEmail").val();
        let password = $("#adminPassword").val();

        Meteor.loginWithPassword({email: email}, password, function (err) {
            if(!err) {
                const user = Meteor.user();
                // console.log("right credential");
                const userId = user._id;
                const userProfile = user.profile.role;
                Session.set({
                    userId: userId,
                    userProfile: userProfile,
                });
                browserHistory.push('/mobile/admin/chats');
            } else {
                Meteor.logout();
                console.error(err);
                alert("Invalid Credential");
            }
        });
    }
    render() {
        return(
            <div className="admin-login-mobile-wrapper">
                <div className="admin-login-mobile-container">
                    <div className="admin-login-mobile-div-wrapper">
                        <div className="admin-login-mobile-login-info">
                            <div className="relative-div">
                                <img className="home-login-img" src={"/img/login-01.png"} />
                                <div className={"home-login-absolute-div-style"}><span>Login</span></div>
                            </div>
                            <div className="admin-login-form">
                                <form>
                                    <div className="email">
                                        <input type="text" id="adminEmail" className="type-email " placeholder="EMAIL" />
                                        <p>Mail</p>
                                    </div>
                                    <div className="password">
                                        <input type="password" id="adminPassword" className="type-password" placeholder="********" />
                                        <p>password</p>
                                    </div>

                                    <div className="relative-div home-login-button-div">
                                        <button  onClick={this._handleLogin.bind(this)} className="text-style home-login-btn">
                                            <img className="logo-home-first-img" src={"/img/login-01.png"} />
                                        </button>
                                        <div className={"absolute-div-style"}>Log In</div>
                                    </div>

                                    <div className="relative-div home-welcome-img-div">
                                        <div><img  src={"/img/leaf.png"} /></div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default MobileAdminLogin;