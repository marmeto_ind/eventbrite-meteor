import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';

class ShowEachGuest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleGuestDelete() {
        const guest = this.props.guest;
        Meteor.call('guests.remove', guest._id, function (err) {
            if(err) {
                console.error(err);
            }
        });
    }

    render() {
        const guest = this.props.guest;

        return (
            <li className={"list-group-item"}>
                <div className={"mobile-user-guestlist-box _mobile-guestlist"}>
                    <div className={"block1"}>
                        <span>
                            <img src={"/img/place_on_table.svg"} alt={"user-img"} />
                        </span>
                    </div>
                    <div className={"block2"}>
                        <span>
                            {guest.name? guest.name: ''}
                        </span>
                    </div>
                    <div className={"block3"} onClick={this.handleGuestDelete.bind(this)}>
                        <span>
                            <img src={"/img/svg_img/cross_svg.svg"}  alt={"user-img"} />
                        </span>
                    </div>
                </div>
            </li>
        )
    }
}

export default ShowEachGuest;