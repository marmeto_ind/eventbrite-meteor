import React from 'react';
import './guest-list.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import ShowGuestlist from './ShowGuestlist';
import {Meteor} from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

class GuestList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        };
    }

    // handleAddGuest = () => {
    //     const guest = $("#messageText").val();
    //     const userId = Meteor.userId();
    //     if(guest) {
    //         Meteor.call('guests.insertWithOutTable', userId, guest, function (err, result) {
    //             if (err) {
    //                 console.error(err);
    //             } else {
    //                 $("#messageText").val('');
    //             }
    //         });
    //     }
    // };

    _handleAddGuest(event) {
        const name = $("#messageText").val();
        const tableId = this.props.tableId;
        const user = Meteor.userId()? Meteor.userId(): '';

        if (name) {
            if(!user) {
                console.log('User is not signed up');
            } else if (!tableId) {
                alert("no table selected");
            } else {
                Meteor.call('guests.insert', name, user, tableId);
                $("#messageText").val("");
            }
        }
    }


    handleClick(event) {
        if(event.target.id == "messageText") {
            $('#tabBarDiv').hide();
            $("#mobileUserGuestListBox").css("bottom","30px");
        } else {
            $('#tabBarDiv').show();
            $("#mobileUserGuestListBox").css("bottom","65px");
        }
    }

    componentDidMount () {
    }

    render() {
        const element = $(".list-group-item:last-child").offset();
        if(element) {
            $('html, body').animate({
                scrollTop: ($(".list-group-item:last-child").offset().top)
            },50);
        }

        return (
            <div onClick={this.handleClick.bind(this)}>
            
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>{this.props.eventName}</p>
                        <p className={"hanna-text paddingBottom15"}><span className={"mobile_user_dot"}>.</span>{this.props.name}</p>
                    </div>
                </div>
                <ShowGuestlist eventId={ this.props.eventId } tableId={ this.props.tableId } />
                <div className="mobileUserChatBoxParent">
                    <div id={"mobileUserGuestListBox"} className={"mobile-user-guest-list-type-box _mobile_guestlist_input"}>
                        <div className={"block1"}>
                            <span><img className={"list-img"} src="/img/place_on_table.svg" alt="" /></span>
                        </div>
                        <div className={"block2"}>
                            <input id={"messageText"}
                                className="type-search"
                                type="text"
                                placeholder="WRITE A NAME"
                                name="message"
                            />
                        </div>
                        <div className={"block3"}>
                            <span className={"btn"} id={"userGuestListSendButton"} onClick={this._handleAddGuest.bind(this)}>
                                <img className={"mobile-table-list-Img"}  src="/img/svg_img/guest_plus_svg.svg" alt="" />
                            </span>
                        </div>
                    </div>
                </div>
                <BottomTabBar/>
            </div>
        )
    }
}

export default withTracker(() => {
    const user = Meteor.user();
    if(user){
        return {
            name: user.profile.name,
        }
    } else {
        return {
            name: ''
        }
    }
})(GuestList);