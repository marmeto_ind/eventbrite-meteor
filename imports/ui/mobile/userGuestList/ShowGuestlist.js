import React, {Component} from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Guests} from "../../../api/guests";
import ShowEachGuest from './ShowEachGuest';

class ShowGuestlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event: '',
            eventImage: '',
        };
    }

    renderGuests() {
        const guests = this.props.guests;
        return guests.map((guest, index) => (
            <ShowEachGuest guest={guest} key={index} />
        ))
    }

    componentDidMount() {
        const userId = Meteor.userId();
        const self = this;
        const eventId = this.props.eventId;
        console.log("event id for guest list is ", eventId);
        // added by NXP
        Meteor.call('events.getUsersEventById', eventId, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                if (result) {
                    self.setState({ event: result });
                }
            }
        })
    }

    getEventImage() {
        const eventId = this.state.eventId;
        const self = this;

        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                if(result) {
                    self.setState({eventImage: result});
                }
            }
        })
    }

    render() {
        return (
            <div className={"mobile-user-guestlist background-none"}>
            <div className="relativePosition">
                <div>
                <img
                     src={this.state.eventImage}
                     alt={""}
                    className="mobile_user_guestListEvent_img"
                 />
                </div>
                <div className="chat_header event-name">{this.state.event? this.state.event.name: ''}</div>
            </div>
                <ul className={"user-mobile-guest-list-view"}>
                    {this.renderGuests()}
                </ul>
            </div>
        )
    }
}


export default withTracker((data) => {
    const userId = Meteor.userId();
    //Meteor.subscribe('guests.all');
    // const guests = Guests.find({"user" : userId}).fetch();
    // return {
    //     guests: guests
    // }

    Meteor.subscribe('guests');
    const tableId = data.tableId;
    // console.log(tableId);
    const user = Meteor.userId();
    return {
        guests: Guests.find({"user" : user, "tableId" : tableId}, {sort: {createdAt: -1}}).fetch(),
    }

})(ShowGuestlist);