import React, {Component} from 'react';
import './vendorTabBar.scss';
import {browserHistory} from 'react-router';

class VendorBottomTabBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            IsIos: false,
        };
    }

    isIOSDevice() {
        return (!!navigator.userAgent.match(/(iPad|iPhone|iPod)/g));
    }

    handleChatClick = (e) => {
        e.preventDefault();
        browserHistory.push("/mobile/vendor/chats");
    };

    handleAgendaClick = (e) => {
        e.preventDefault();
        browserHistory.push("/mobile/vendor/agenda");
    };

    handleMyBusinessClick = (e) => {
        e.preventDefault();
        browserHistory.push("/mobile/vendor/my-business");
    };

    handleStatisticsClick = (e) => {
        e.preventDefault();
        browserHistory.push("/mobile/vendor/statistics");
    };

    handleSettingsClick = (e) => {
        e.preventDefault();
        browserHistory.push("/mobile/vendor/settings");
    };

    handleBackButtonClick = () => {
        let pathname = browserHistory.getCurrentLocation().pathname;
        if (pathname !== "/mobile/vendor/chats") {
            browserHistory.goBack();
        }
    };

    componentDidMount () {

        const isIOS = this.isIOSDevice();
        this.setState({IsIos: isIOS});
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length-1];


        if(page_url.indexOf("chat") > -1) {
            $('#vendorChatImg').attr('src', '/img/tabBarImg/whiteChat.svg');
            
            $(".vendor-chats-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".vendor-chats-tab").css("color", "white");
            $(".vendor-chats-tab").css("height","45px");
            $(".vendor-chats-tab").css("width","auto");
            $(".vendor-chats-tab").css("background-position","50%");
            $(".vendor-chats-tab").css("background-size","contain");
            $(".vendor-chats-tab").css("background-repeat","no-repeat");

        }

        else  if(page_url.indexOf("agenda") > -1) {
            $('#vendorAgendaImg').attr('src', '/img/tabBarImg/white_calendar.svg');
            
            $(".vendor-agenda-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".vendor-agenda-tab").css("color", "white");
            $(".vendor-agenda-tab").css("height","45px");
            $(".vendor-agenda-tab").css("width","auto");
            $(".vendor-agenda-tab").css("background-position","50%");
            $(".vendor-agenda-tab").css("background-size","contain");
            $(".vendor-agenda-tab").css("background-repeat","no-repeat");

        }
       
        else  if(page_url.indexOf("statistics") > -1) {
            $('#vandorStatImg').attr('src', '/img/tabBarImg/white_stat.svg');
            
            $(".vendor-statistics").css('background','url("/img/lowerbar_background.svg")');
            $(".vendor-statistics").css("color", "white");
            $(".vendor-statistics").css("height","45px");
            $(".vendor-statistics").css("width","auto");
            $(".vendor-statistics").css("background-position","50%");
            $(".vendor-statistics").css("background-size","contain");
            $(".vendor-statistics").css("background-repeat","no-repeat");
        }
     
        else  if(page_url.indexOf("my-business") > -1) {
            $('#vendorMyBusiness').attr('src', '/img/tabBarImg/white_vendor.svg');
            
            $(".vendor-my-business-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".vendor-my-business-tab").css("color", "white");
            $(".vendor-my-business-tab").css("height","45px");
            $(".vendor-my-business-tab").css("width","auto");
            $(".vendor-my-business-tab").css("background-position","50%");
            $(".vendor-my-business-tab").css("background-size","contain");
            $(".vendor-my-business-tab").css("background-repeat","no-repeat");
        } else  if(page_url.indexOf("settings") > -1) {
            $('#vandorSettImg').attr('src', '/img/tabBarImg/white_sett.svg');
            
            $(".vendor-settings").css('background','url("/img/lowerbar_background.svg")');
            $(".vendor-settings").css("color", "white");
            $(".vendor-settings").css("height","45px");
            $(".vendor-settings").css("width","auto");
            $(".vendor-settings").css("background-position","50%");
            $(".vendor-settings").css("background-size","contain");
            $(".vendor-settings").css("background-repeat","no-repeat");
        } else {
            $(".icon-size").css("width","20px");
            $(".icon-size").css("height","20px");
            $('#vendorChatImg').attr('src', '/img/chat-notification.png');
            $('#vendorAgendaImg').attr('src', '/img/tabBarImg/grey_calendar.svg');
            $('#vandorStatImg').attr('src', '/img/tabBarImg/greys_statistics.svg');
            $('#vandorSettImg').attr('src', '/img/tabBarImg/grey_settings.svg');
            $('#vendorMyBusiness').attr('src', '/img/handshake-01.png');
        }
    }
    
    render() {
        var iconTextVendor ={
            // 'width':'30px',
            // 'height':'30px'
        }
        return(
            <div className={"VendorBottomBar"}>
                <div id="tabBarDiv">               
                    <div  className="ios_phone" id="tabBarInnerContainer">
                        <div className="tab">
                            <button className="tablinks" onClick={this.handleBackButtonClick.bind(this)}>
                                <span><img className={"icon-size"} src={"/img/svg_img/left-arrow_svg.svg"} /></span>
                            </button>
                            <button className="tablinks vendor-chats-tab" onClick={this.handleChatClick.bind(this)}>
                                <span><img  id="vendorChatImg" className={"icon-size"} src={"/img/chat-notification.png"} /></span>
                            </button>
                            <button className="tablinks vendor-agenda-tab" onClick={this.handleAgendaClick.bind(this)}>
                                <span><img id="vendorAgendaImg"  className={"icon-size"} src={"/img/tabBarImg/grey_calendar.svg"} /></span>
                            </button>
                            <button className="tablinks vendor-my-business-tab" onClick={this.handleMyBusinessClick.bind(this)}>
                                <span><img  id="vendorMyBusiness" style={iconTextVendor} className={"icon-size"} src={"/img/tabBarImg/grey_vendor.svg"} /></span>
                            </button>
                            <button className="tablinks vendor-statistics" onClick={this.handleStatisticsClick.bind(this)}>
                                <span><img id="vandorStatImg" className={"icon-size"} src={"/img/tabBarImg/grey_statistics.svg"} /></span>
                            </button>
                            <button className="tablinks vendor-settings" onClick={this.handleSettingsClick.bind(this)}>
                                <span><img id="vandorSettImg" className={"icon-size"} src={"/img/tabBarImg/grey_settings.svg"} /></span>
                            </button>
                        </div>                    
                    </div>             
                </div>
            </div>
        )
    }
}

export default VendorBottomTabBar;