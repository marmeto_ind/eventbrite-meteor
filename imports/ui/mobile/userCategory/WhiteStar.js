import React from 'react';

class WhiteStar extends React.Component {
    render() {
        return (
            <span>
                <img className="mobile-star-logo" src="/img/star.png" alt="" />
            </span>
        )
    }
}

export default WhiteStar;