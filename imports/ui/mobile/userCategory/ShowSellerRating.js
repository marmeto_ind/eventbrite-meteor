import React from 'react';
import GoldenStar from './GoldenStar';
import WhiteStar from './WhiteStar';

class ShowSellerRating extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: '',
        };
    }

    renderRating() {
        const review = this.state.review;
        const extra = 5 - review;
        var reviewComponent = [];

        for (var i = 0; i < review; i++) {
            reviewComponent.push(<GoldenStar key={i} />);
        }
        for (var j = 0; j < extra; j++) {
            reviewComponent.push(<WhiteStar key={i + j} />);
        }

        return <span>{reviewComponent}</span>;
    }

    componentDidMount() {
        const self = this;
        const seller = this.props.seller;
        const sellerId = seller._id;
        // console.log(sellerId);
        Meteor.call('reviews.getSellerRating', sellerId, function (err, result) {
            if (err) {
                console.error(err)
            } else {
                // console.log(result);
                self.setState({ review: result });
            }
        })
    }

    render() {
        // console.log(this.state.review);
        return (
            <div className="circle-star-margin">
                {/*{this.renderRating()}*/}
                <div className="rating">
                    <span><img className="star-logo" src="/img/10.png" alt="" /></span>
                    <span><img className="star-logo" src="/img/10.png" alt="" /></span>
                    <span><img className="star-logo" src="/img/10.png" alt="" /></span>
                    <span><img className="star-logo" src="/img/11.png" alt="" /></span>
                    <span><img className="star-logo" src="/img/11.png" alt="" /></span>
                </div>
            </div>
        )
    }
}

export default ShowSellerRating;