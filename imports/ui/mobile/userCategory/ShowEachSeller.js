import React from 'react';
import ShowSellerRating from './ShowSellerRating';
import ShowFacilityIcons from './ShowFacilityIcons';
import {browserHistory} from 'react-router';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            logoImage: '',
            checkIcon:true,
        };
    }

    componentDidMount() {
        const seller = this.props.seller;
        this.setState({logoImage: seller.profile.logoImage})
    }

    renderSellerFacilities(facilities) {
        return facilities.map((facility, index) => (
            <ShowFacilityIcons facility={facility} key={index}/>
        ))
    }
 
    handleSellerClick(e) {
        e.preventDefault();
        const seller = this.props.seller;
        const eventId = this.props.eventId;
        browserHistory.push("/mobile/user/service/" + seller._id + "/" + eventId);
    }

    handleImageError() {
        this.setState({logoImage: 'https://via.placeholder.com/150'})
    }

    render() {
        const seller = this.props.seller;
        const seller_background_img = seller.profile.mainImage;
        console.log(seller_background_img);
        const venue_choiceDiv = {
            'background-image': 'url(' + seller_background_img + ')',
            // backgroundImage: 'url("https://via.placeholder.com/150")',
            'background-repeat': 'no-repeat',
            'background-size': 'cover',
            'margin-bottom': '10px',
            'border-radius': '15px',
            'box-shadow': '1px 2px 12px 0px #22222C'
            // minHeight: '167px',
            // height: '167px',
        };

        const margin = {
            'margin': '-10px'
        }

        var showOrNot = false;
        if((this.props.choice) && (this.props.choice.seller) && (this.props.choice.seller == seller._id)){
            showOrNot = true;
        }
        return (
            <div style={venue_choiceDiv} className="vale_container">
                <div className="mychoice_data  my-data" onClick={this.handleSellerClick.bind(this)}>
                    {/* <div className="tag_wrapper">
                        <img src="/img/hot_deals.png" />
                    </div>
                    <div className="best_seller">
                        <p className="best_sold_img">best seller</p>
                        <p className="best_seller_img">best seller</p>
                        <p className="best_sold_img">best seller</p>
                    </div> */}
                    <div className="value-main-content">
                        <div className={this.props.choice? "shadow-box-main-div value-main-content-tick": "shadow-box-main-div"}>
                            <div className={"mobile_grey_background"}>
                            </div>
                            <div className={"shadow-box-div verticallyCenter"}>
                                <div>
                                    <img src={this.state.logoImage}
                                         onError={this.handleImageError.bind(this)}
                                         className={"mobile_vendor_logo"}/>
                                </div>
                                <ShowSellerRating seller={seller}/>
                                <p style={margin}>
                                    <img src="/img/mobile_searcBox_line.png"/>
                                </p>
                                <h3 className={"choice-text-style"}>{seller.name}</h3>
                                <p className={"choice-text-style"}>{seller.profile.address}</p>
                                <div className="icon_div">
                                    {this.renderSellerFacilities(seller.profile.facilities)}
                                </div>
                                {showOrNot ?
                                <div className="chceked_icon">
                                    <img src="/img/tick_golden.svg" />
                                </div>
                                :''}
                            </div>
                        </div>
                        {/* <div className={"mobile_category_seller_tick"}>
                            <img
                                src={tickImage}
                            />
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowEachSeller;