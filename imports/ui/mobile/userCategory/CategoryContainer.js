import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import Category from './Category';

class CategoryContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: ""
        };
        this.handleSellerSearch = this.handleSellerSearch.bind(this)
    }

    handleSellerSearch(searchKey) {
        this.setState({searchKey: searchKey});
    }

    render() {
        const categoryId = this.props.params.categoryId;
        const eventId = this.props.params.eventId;
        //console.log("Top event Categories", categoryId)
        if (this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if (!Meteor.user()) {
                browserHistory.push("/mobile/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if (user.profile.role !== "user") {
                    browserHistory.push("/mobile/login");
                    return (<div></div>);
                } else {
                    return (
                        <Category searchKey={this.state.searchKey} handleSellerSearch={this.handleSellerSearch}
                                  categoryId={categoryId} eventId={eventId}/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(CategoryContainer);

