import React from 'react';

class GoldenStar extends React.Component {
    render() {
        return (
            <span>
                <img className="star-logo" src="/img/10.png" alt="" />
            </span>
        )
    }
}

export default GoldenStar;