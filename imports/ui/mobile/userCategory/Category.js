import React from 'react';
import './category.scss';
import BottomTabBar from "../userBottomBar/BottomTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import {Categories} from "../../../api/categories";
import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import ShowEachSeller from './ShowEachSeller';
import {Choices} from "../../../api/choice";

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            event: '',
            eventImage: ''
        };
    }


    componentDidMount() {
        // const userId = Meteor.userId();
        // const self = this;
        // Meteor.call('events.getUsersLatestEvent', userId, function (err, result) {
        //     if(err) {
        //         console.log(err);
        //     } else {
        //         if(result) {
        //             self.setState({event: result});
        //         }
        //     }
        // })

        const self = this;
        const eventId = this.props.eventId;
        //console.log("event id for guest list is ", eventId);
        Meteor.call('events.getUsersEventById', eventId, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                if (result) {
                    self.setState({ event: result });
                }
            }
        })
        
        //const eventId = this.props.eventId;
        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                if(result) {
                    self.setState({eventImage: result});
                }
            }
        })
    }


    renderSellers() {
        const eventId = this.props.eventId;
        const choice = this.props.choice;
        const categoId = this.props.categoryId;
        //console.log("Event , choice, categories ======", eventId, choice, categoId);
        //const selectedOp = (choice._id == categoId)
        //console.log("dsadasdasd", selectedOp);


        if(this.props.sellers.length > 0) {
            return this.props.sellers.map((seller, index) =>(
                <ShowEachSeller key={index} seller={seller} eventId={eventId} choice={choice} categoId={categoId} />
            ))
        } else {
            return (
                <div className={"no-seller-present"}><p>NO SERVICES AVAILABLE YET</p></div>
            )
        }
    }

    handleSearchKeyChange(e){
        const searchKey = e.target.value;
        this.props.handleSellerSearch(searchKey);
    }

    render() {
        // const choice = this.props.choice;
    
        // const venue_choiceDiv = {
        //     backgroundColor: 'black',
        //     marginBottom: '20px',
        // };
        console.log(this.state.eventImage)

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-my-events-header"}>
                    <div className={"chat-div"}>
                        <p className={"chattext"}>{this.props.category? this.props.category.name: ''}</p>
                        <p className={"hanna-text paddingBottom15"}><span className={"mobile_user_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>
                    </div>
                </div>
                

                <div className={"mobile-user-category-block"}>
                    <div className={"mobile-user-category"}>
                        <div className={"admin-venue-box"}>
                            <div className={"admin-venue-name"}>
                                <p className={"event-date"}>{this.state.event? this.state.event.date: 'date'}</p>
                                <p className={"event-region"}> {this.state.event? this.state.event.region: 'region'}</p>
                            </div>
                        </div>
                    </div>

                    <div className="relativeposition category-search">
                        <form>
                            <div className="mobile-search-div">
                                <ul className="list-group li-box">
                                    <li className="list-group-item paddingBottom">
                                        <div className="row">
                                            <input type="text" className="mobile-user-search"
                                                   placeholder="Search Service"
                                                   onChange={this.handleSearchKeyChange.bind(this)}/>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>

                    <div className={"mobile-venue-results-div mobile-venue-results-user-div"}>
                        <div className="mobile-search-top-body-container relativeposition">
                            <div className={"mobile-venue-div"}>
                                {this.renderSellers()}
                            </div>
                        </div>
                    </div>
                </div>
                <BottomTabBar/>
            </div>
        )
    }
}

export default withTracker((data) => {
    const categoryId = data.categoryId;
    const eventId = data.eventId;
    const userId = Meteor.userId();

    let searchKey = data.searchKey;

    Meteor.subscribe('categories.all');
    Meteor.subscribe('sellers.all');
    Meteor.subscribe('choices.all');

    const category = Categories.findOne({"_id" : categoryId});
    let query = {};
    if(searchKey) {
        query["name"] = {$regex : searchKey, '$options' : 'i'};
        query["category._id"] = categoryId;
    }else {
        query["category._id"] = categoryId;
    }

    const sellers = Sellers.find(query).fetch();
    const choice = Choices.findOne({
        "user" : userId,
        "categoryId" : categoryId,
        "eventId" : eventId,
    });

    return {
        category: category,
        sellers: sellers,
        choice: choice,
    }
})(Category);


