import React from 'react';
import {Meteor} from 'meteor/meteor';

class ShowFacilityIcons extends React.Component {
    constructor(props){
        super(props);
        this.state={
            image: '',
        };
    }

    componentDidMount(){
        const self = this;
        const facility = this.props.facility;
        // console.log(facility);
        const facilityId = facility._id;
        Meteor.call('seller_facilities.findOne', facilityId, function (err, result) {
            if(err) {
                console.log(err);
            } else {
                if (result) {
                    self.setState({image: result.image})
                }

            }
        })
    }

    render() {
        return (
            <span>
                <img className="icon-img" src={this.state.image} alt="" />
            </span >
        )
    }
}

export default ShowFacilityIcons;