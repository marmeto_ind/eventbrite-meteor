import React, {Component} from 'react';
import {browserHistory} from 'react-router';

class BottomTabBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            IsIos: false,
        };
    }

    isIOSDevice() {
        return (!!navigator.userAgent.match(/(iPad|iPhone|iPod)/g));
    }

    handleChatClick = () => {
        browserHistory.push("/mobile/user/chats");
    };

    handleSearchClick = () => {
        browserHistory.push('/mobile/user/search-category');
    };

    handleMyEventClick = () => {
        browserHistory.push("/mobile/event");
    };

    handleGuestListClick = () => {
        // browserHistory.push("/mobile/user/guest-list");
        browserHistory.push("/mobile/user/categories");
    };

    handleSettingsClick = () => {
        browserHistory.push("/mobile/user/settings");
    };

    handleBackButtonClick = () => {
        let pathname = browserHistory.getCurrentLocation().pathname;
        // console.log(pathname)
        if (pathname !== "/mobile/event") {
            browserHistory.goBack();
        }
    };

    componentDidMount () {

        const isIOS = this.isIOSDevice();
        this.setState({IsIos: isIOS});
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length-1];

        if(page_url.indexOf("chat") > -1) {
            $('#userchatWhiteImg').attr('src', '/img/tabBarImg/whiteChat.svg');
            $(".chat").css('background','url("/img/lowerbar_background.svg")');
            $(".chat").css("color", "white");
            $(".chat").css("height","45px");
            $(".chat").css("width","auto");
            $(".chat").css("background-position","50%");
            $(".chat").css("background-size","contain");
            $(".chat").css("background-repeat","no-repeat");
        } else if(page_url.indexOf("settings") > -1) {
            $('#userSettingsImg').attr('src', '/img/tabBarImg/white_sett.svg');
            $(".settings").css('background','url("/img/lowerbar_background.svg")');
            $(".settings").css("color", "white");
            $(".settings").css("height","45px");
            $(".settings").css("width","auto");
            $(".settings").css("background-position","50%");
            $(".settings").css("background-size","contain");
            $(".settings").css("background-repeat","no-repeat");
        }
      
        else if(page_url.indexOf("categories") > -1) {
            $('#userGuestImg').attr('src', '/img/new_img/white_guestList.svg');
            $(".guest_list").css('background','url("/img/lowerbar_background.svg")');
            $(".guest_list").css("color", "white");
            $(".guest_list").css("height","45px");
            $(".guest_list").css("width","auto");
            $(".guest_list").css("background-position","50%");
            $(".guest_list").css("background-size","contain");
            $(".guest_list").css("background-repeat","no-repeat");
        }
        else if(page_url.indexOf("search") > -1) {
            $('#usersearchImg').attr('src', '/img/tabBarImg/white_search.svg');
            $(".search").css('background','url("/img/lowerbar_background.svg")');
            $(".search").css("color", "white");
            $(".search").css("height","45px");
            $(".search").css("width","auto");
            $(".search").css("background-position","50%");
            $(".search").css("border-radius","50%");
            $(".search").css("background-size","contain");
            $(".search").css("background-repeat","no-repeat");
        }       
        else if(page_url.indexOf("event") > -1) {
            $('#userEventHome').attr('src', '/img/tabBarImg/whiteHouse.svg');
            $(".my_event").css('background','url("/img/lowerbar_background.svg")');
            $(".my_event").css("color", "white");
            $(".my_event").css("height","45px");
            $(".my_event").css("width","auto");
            $(".my_event").css("background-position","50%");
            $(".my_event").css("border-radius","50%");
            $(".my_event").css("background-size","contain");
            $(".my_event").css("background-repeat","no-repeat");
        }
        else {
            $(".icon-size").css("width","20px");
            $(".icon-size").css("height","20px");
            $('#chatWhiteImg').attr('src', '/img/chat-notification.png');
            $('#searchImg').attr('src', '/img/vendor-search.png');
            $('#userEventHome').attr('src', '/img/tabBarImg/grey_eventHome.svg');
            $('#userSettingsImg').attr('src', '/img/tabBarImg/grey_settings.svg');
            $('#userGuestImg').attr('src', '/img/new_img/guest_list.svg');
        }
    }

    render() {
        return (
            <div id="tabBarDiv" className="z-index-9999">
                <div  className="" id="tabBarInnerContainer">
                    <div className="tab">
                         <button className="tablinks" onClick={this.handleBackButtonClick.bind(this)}>
                            <span><img  className={"icon-size"} src={"/img/svg_img/left-arrow_svg.svg"} /></span>
                            {/* <span>  Back</span> */}
                        </button>

                        <button className="tablinks chat" onClick={this.handleChatClick.bind(this)}>
                            <span>
                                <img id="userchatWhiteImg" className={"icon-size"} src={"/img/chat-notification.png"} />
                                {/* <div className="chat_count_icon_style"><span></span></div> */}
                            </span>
                            {/* <span>  Chat</span> */}
                        </button>

                        {/*<button className="tablinks search" onClick={this.handleSearchClick.bind(this)}>*/}
                            {/*<span><img id="usersearchImg" className={"icon-size"} src={"/img/vendor-search.png"} /></span>*/}
                            {/*/!* <span> Search</span> *!/*/}
                        {/*</button>*/}

                        <button className="tablinks my_event" onClick={this.handleMyEventClick.bind(this)}>
                            <span><img id="userEventHome" className={"icon-size"} src={"/img/tabBarImg/grey_eventHome.svg"} /></span>
                            {/* <span>MyEvent</span> */}
                        </button>

                        <button className="tablinks guest_list" onClick={this.handleGuestListClick.bind(this)}>
                            <span><img id="userGuestImg" className={"icon-size"} src={"/img/new_img/guest_list.svg"} /></span>
                            {/* <span> GuestLists</span> */}
                        </button>

                        <button className="tablinks settings" onClick={this.handleSettingsClick.bind(this)}>
                            <span><img id="userSettingsImg" className={"icon-size"} src={"/img/tabBarImg/grey_settings.svg"} /></span>
                            {/* <span>Settings</span> */}
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default BottomTabBar;
