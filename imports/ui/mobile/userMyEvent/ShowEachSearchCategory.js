import React from 'react';
import {browserHistory} from 'react-router';
import { withTracker } from 'meteor/react-meteor-data';
import {Choices} from "../../../api/choice";
import {Sellers} from "../../../api/sellers";

class ShowEachCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleClick(e) {
        e.preventDefault();
        const category = this.props.category;
        // console.log(category._id)
        browserHistory.push('/mobile/user/search-event/' + category._id);
    }

    render() {
        const category = this.props.category;
        const margin = {
            marginTop: '-10px',
            marginBottom:'-5px',
        };

        return (
            <li className={"list-group-item user-venue-my-wedding-div"} onClick={this.handleClick.bind(this)}>
                <div className={"myevents-main-div"}>
                    <div className={"my-events-img-div"}>
                        <img src={category.image} className={"search-img"} alt={"eventcenter-img"} />
                    </div>
                    <p style={margin}>
                        <img src="/img/mobile_searcBox_line.png" />
                    </p>
                    <p className={"mobile-p-class-style2"}>{category.name}</p>
                    <p className={"mobile-p-class-style3"}>{this.props.sellerName}</p>
                    {/*<p className={"mobile-p-class-style1"}>Panorama Eventcenter</p>*/}
                    {/*<p className={"mobile-p-class-style1"}>Exclusive Pack</p>*/}
                </div>
                <div className={"mobile_myEventCategory_seller_tick"}>
                    <img
                        src={this.props.choice? "/img/show-tick.png": ''}
                    />
                </div>
            </li>
        )
    }
}

// export default ShowEachCategory;

export default withTracker((data) => {
    Meteor.subscribe('choices.all');
    Meteor.subscribe('sellers.all');

    const categoryId = data.category._id;
    const eventId = data.eventId;
    const userId = Meteor.userId();
    const choice = Choices.findOne({
        "user" : userId,
        "categoryId" : categoryId,
        "eventId" : eventId
    });

    let sellerName = '';
    if(choice) {
        const sellerId = choice.seller;
        const seller = Sellers.findOne({"_id" : sellerId});
        if(seller) {
            sellerName = seller.name;
        }
    }
    return {
        choice: choice,
        sellerName: sellerName,
    }
})(ShowEachCategory);