import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import MyEvent from './ShowCategories';

class MyEventContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: ''
        }
        this.changeSearchKey = this.changeSearchKey.bind(this)
    }

    changeSearchKey(data) {
        this.setState({searchKey: data})
    }

    render() {
        const eventId = "nExzsHhBCuYnbbqhx";

        if (this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if (!Meteor.user()) {
                browserHistory.push("/mobile/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if (user.profile.role !== "user") {
                    browserHistory.push("/mobile/login");
                    return (<div></div>);
                } else {
                    return (
                        <MyEvent
                            eventId={eventId}
                            parentCallback={this.changeSearchKey}
                            searchKey={this.state.searchKey}
                        />
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(MyEventContainer);

