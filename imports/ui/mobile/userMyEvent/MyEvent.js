import React from 'react';
import './my-event.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Categories} from "../../../api/categories";
import ShowEachCategory from './ShowEachCategory';
import {Sellers} from "../../../api/sellers";
import {browserHistory} from 'react-router';

class MyEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            eventImage: '',
            event: ''
        }
    }

    componentDidMount() {
        const eventId = this.props.eventId;
        const self = this;
        Meteor.call('events.getEventImage', eventId, function (err, result) {
            if(err) {
                console.error(err);
            } else {
                self.setState({eventImage: result});
            }
        })

        Meteor.call('events.getUsersLatestEvent', Meteor.userId(), function (err, result) {
            if(err) {
                console.error(err);
            } else {
                self.setState({event: result});
            }
        })
    }
 
    renderCategories() {
        const eventId = this.props.eventId;
        const categories = this.props.categories;
        return categories.map((category, index) => (
            <ShowEachCategory category={category} key={index} eventId={eventId} />
        ))
    }

    handleImageError() {
        this.setState({eventImage: 'https://via.placeholder.com/150'})
    }

    handleTableListClick = () => {
        console.log("Table list clicked");
        const eventId = this.props.eventId;
        browserHistory.push('/mobile/user/tables/' + eventId +'/');
    };


    render() {
        const event = this.props.event || this.state.event;
        const date = event? event.date.toString().substring(8, 10): '';
        const day = event? event.date.toString().substring(0, 3): '';
        const month = event? event.date.toString().substring(4, 7): '';
        const year = event? event.date.toString().substring(11, 15): '';

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-my-events-header"}>
                    <div className={"chat-div"}>
                        <p className={"chattext"}>
                            {"CATEGORYS"}
                        </p>
                        <p className={"hanna-text"}><span className={"mobile_user_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>
                    </div>
                </div>

                {/* <div className={"img-box-width"}>
                        <img className={"search-img"} src={this.state.eventImage} onError={this.handleImageError.bind(this)} />
                </div> */}

                <div className={"mobile-user-myevents-body-part"}>
                    <div className={"mobile-user-my-events-search-div"}>
                        <div className={"main-box-width height_auto"}>
                           <div className={"date-box-width"}>
                               <div className={"p-date-search margin-0"}>
                                   {/* <p className={"date-search"}>{day+' '+date+' '+month+' '+year}</p> */}
                                   <p className={"date-search padding-20-top"}>{event? event.region: ''}</p>
                               </div>
                               {/* <p className={"region-search"}>{event? event.region: ''}</p> */}
                               <div className="right_button" onClick={this.handleTableListClick.bind(this)}>&nbsp;</div>
                           </div>
                       </div>
                    </div>

                    <div className={"mobile_event_top mobile-user-my-events-search-results-div"}>
                       <ul className={"list-group"}>
                           {this.renderCategories()}
                       </ul>
                    </div>

                </div>

                <BottomTabBar/>

            </div>
        )
    }
}

export default withTracker((data) => {
    const eventId = data.eventId;
    Meteor.subscribe('events.all');
    Meteor.subscribe('categories.all');
    Meteor.subscribe('sellers.all');

    let event = Events.findOne({"_id" : eventId});
    let categories = Categories.find({}).fetch();

    categories = categories.map((category) => {
        let category_id = category._id;
        category['seller_count'] = Sellers.find({"category._id": category_id}).count();
        return category;
    });

    return {
        event: event,
        categories: categories,
    }
})(MyEvent);

