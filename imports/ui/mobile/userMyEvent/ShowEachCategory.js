import React from 'react';
import {browserHistory} from 'react-router';
import {withTracker} from 'meteor/react-meteor-data';
import {Choices} from "../../../api/choice";
import {Sellers} from "../../../api/sellers";

class ShowEachCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryImage: ''
        };
    }

    componentDidMount() {
        const category = this.props.category;
        this.setState({categoryImage: category.image})
    }

    handleClick(e) {
        e.preventDefault();
        const category = this.props.category;
        const eventId = this.props.eventId;
        browserHistory.push('/mobile/user/category/' + category._id + "/" + eventId);
    }

    handleImageError() {
        this.setState({categoryImage: 'https://via.placeholder.com/150'})
    }

    render() {
        const category = this.props.category;
        const margin = {
            marginTop: '-10px',
            marginBottom: '-5px',
        };

        return (
            <li className={"list-group-item user-venue-my-wedding-div"} onClick={this.handleClick.bind(this)}>
                <div className={ this.props.choice? "myevents-main-div myevents-main-div-tick": "myevents-main-div"}>
                    <div className={"my-events-img-div"}>
                        <img src={this.state.categoryImage} onError={this.handleImageError.bind(this)}
                             className={"search-img"} alt={"eventcenter-img"}/>
                    </div>
                    <p style={margin}>
                        <img src="/img/mobile_searcBox_line.png"/>
                    </p>
                    <p className={"mobile-p-class-style2"}>{category.name}</p>
                    <p className={"mobile-p-class-style3"}>{this.props.sellerName? this.props.sellerName: `(${category.seller_count})`}</p>
                    {/*<p className={"mobile-p-class-style1"}>Panorama Eventcenter</p>*/}
                    {/*<p className={"mobile-p-class-style1"}>Exclusive Pack</p>*/}
                </div>
                {/* <div className={"mobile_myEventCategory_seller_tick"}>
                    <img
                        src={this.props.choice ? "/img/show-tick.png" : ''}
                    />
                </div> */}
            </li>
        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('choices.all');
    Meteor.subscribe('sellers.all');

    const categoryId = data.category._id;
    const eventId = data.eventId;
    const userId = Meteor.userId();
    //console.log("choice data  is " , categoryId,eventId, userId )
    const choice = Choices.findOne({
        "user": userId,
        "categoryId": categoryId,
        "eventId": eventId
    });

    let sellerName = '';
    if (choice) {
        const sellerId = choice.seller;
        const seller = Sellers.findOne({"_id": sellerId});
        if (seller) {
            sellerName = seller.name;
        }
    }

    //console.log("choises and event are")
    //console.log(choice, sellerName)
    return {
        choice: choice,
        sellerName: sellerName,
    }
})(ShowEachCategory);