import React from 'react';
import './my-event.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Categories} from "../../../api/categories";
import ShowEachCategory from './ShowEachSearchCategory';

class MyEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: ''
        }
    }

    renderCategories() {
        const eventId = this.props.eventId;
        const categories = this.props.categories;
        return categories.map((category, index) => (
            <ShowEachCategory category={category} key={index} eventId={eventId}/>
        ))
    }

    handleSearchKeyChange(e) {
        const searchKey = e.target.value;
        this.props.parentCallback(searchKey)
    }

    render() {
        const event = this.props.event;
        const categories = this.props.categories;

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-my-events-header"}>
                    <div className={"chat-div"}>
                        <p className={"chattext"}>
                            {/* {this.props.event? this.props.event.name: ''} */}
                            HOME
                        </p>
                        <p className={"hanna-text"}>
                            <span className={"mobile_user_dot"}>.</span>
                            {Meteor.user() ? Meteor.user().profile.name : ''}
                        </p>
                    </div>
                </div>

                <div className="relativeposition">
                    <form>
                        <div className="mobile-search-div">
                            <ul className="list-group li-box">
                                <li className="list-group-item paddingBottom">
                                    <div className="row">
                                        <input type="text" className="mobile-user-search"
                                               placeholder="Search Vendor"
                                               onChange={this.handleSearchKeyChange.bind(this)}
                                        />
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="mobile-search-icon-div mobile_user_search_vendor_block_img">
                            <img className="mobile-search-img" src={"/img/new_img/white_search.svg"} alt="search"/>
                        </div>
                    </form>
                </div>

                <div className={"mobile-user-myevents-body-part"}>
               
                    <div className={"mobile-user-my-events-search-results-div"}>
                        <ul className={"list-group"}>
                            {this.renderCategories()}
                        </ul>
                    </div>

                </div>

                <BottomTabBar/>

            </div>
        )
    }
}

export default withTracker((data) => {
    const eventId = data.eventId;
    Meteor.subscribe('events.all');
    Meteor.subscribe('categories.all');

    const searchKey = data.searchKey;
    const query = {};
    query["name"] = {$regex: searchKey, '$options': 'i'};
    const event = Events.findOne({"_id": eventId});
    const categories = Categories.find(query).fetch();

    return {
        event: event,
        categories: categories,
    }
})(MyEvent);

