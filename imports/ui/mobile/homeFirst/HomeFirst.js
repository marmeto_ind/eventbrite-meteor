import React from 'react';
import './homeFirst.scss';
import {browserHistory} from 'react-router';
import {Meteor} from "meteor/meteor";
import { withTracker } from 'meteor/react-meteor-data';

class HomeFirst extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // console.log(Meteor.user());
        // console.log(Session.get('userId'))
        // console.log(Session.get('userProfile'))
    }

    handleNewCoupleClick = () => {
        browserHistory.push('/mobile/home/new-couple')
    };

    handleNewService = () => {
        browserHistory.push('/mobile/home/new-service');
    };

    handleLoginButtonClick = () => {
        browserHistory.push("/mobile/home/login");
    };

    handleContinueAsGuestClick = () => {
        let email = "guest@wendely.com";
        let password = "guest";
        Meteor.loginWithPassword({email: email}, password, function (err) {
            if (!err) {
                const user = Meteor.user();
                const userId = user._id;
                const userProfile = "user";
                Session.set('userId', userId);
                Session.set('userProfile', userProfile);
                browserHistory.push("/mobile/event");
            }
        })
    };

    render() {
        console.log(this.props.logginIn);
        if(this.props.logginIn === true) {
            console.log("kgjgjhgj")
            return (
                
            <div className="loader_wrapper">
                <div className="loader_mobile"></div>
            </div>);
        } else {
            let user = Meteor.user();
            if(user) {
                // For User Login
                if (user.profile.role === "user") {
                    browserHistory.push("/mobile/user/chats");
                    return (<div></div>);
                }
                // For Vendor Login
                else if (user.profile.role === "vendor") {
                    browserHistory.push("/mobile/vendor/chats");
                    return (<div></div>);
                }
                // For Admin Login
                else if (user.profile.role === "admin") {
                    browserHistory.push("/mobile/admin/chats");
                    return (<div></div>);
                }
            } else {
                return (
                    <div className="home-first-mobile-wrapper">
                         
                        <div className="home-first-mobile-container_mobile">                    
                            <div className="home-first-div-wrapper">
                                <div className="heading-wrap">
                                    <img className="logo-img1" src={"/img/new_img/logo.svg"}/> 
                                    <h1 className={"head-logo1"}> WENDELY</h1>
                                </div>
                                <p className="headPara1">Welcome to your new weddingplanner!</p>
                                <div className="home-first-login-info">
        
                                    <div className={"mobile-first-div"}>
                                        <div className="new-couple-div relative-div">
                                            <button className="text-style FloatbtnClass"
                                                    onClick={this.handleNewCoupleClick.bind(this)}>
                                                <img className="logo-home-first-img" src={"/img/new_user.svg"}/>
                                                <div className={"absolute-div-style mobile_home_first"}>IM GETTING MARRIED</div>
                                            </button>
                                        </div>
        
                                        <div className=" new-service-div relative-div">
                                            <button className="text-style FloatbtnClass"
                                                    onClick={this.handleNewService.bind(this)}>
                                                <img className="logo-home-first-img" src={"/img/new_service.svg"}/>
                                                <div className={"absolute-div-style mobile_home_first"}>
                                                    ADD NEW SERVICE
                                                </div>
                                            </button>
                                        </div>
                                        <div className="relative-div">
                                            <button
                                                className="text-style FloatbtnClass home-first-button home_first_continue_guest_button"
                                                onClick={this.handleContinueAsGuestClick.bind(this)}>
                                                <img className="logo-home-first-img" src={"/img/new_img/visit_logo.svg"}/>
                                                <div className={"absolute-div-style mobile_home_first  home_first_continue_guest_button"}>JUST VISITING
                                                </div>
                                            </button>
                                        </div>
                                        <div className=" new-couple-div relative-div">
                                            <button
                                                className="text-style home-first-button FloatbtnClass home_first_login_button "
                                                onClick={this.handleLoginButtonClick.bind(this)}>
                                                <img className="logo-home-first-img" src={"/img/new_img/sign_in_logo.svg"}/>
                                                <div
                                                    className={"absolute-div-style home_first_login_button mobile_home_first"}>IM A MEMBER
                                                    
                                                </div>
                                            </button>
                                        </div>
        
                                    </div>
        
                                </div>
                            </div>
                            <h1 className={"wendy_com"}><span className="footer_copyright">wendely.</span><span>com</span></h1>
                        </div>
                    </div>
                )
            }            
        }        
    }
}

// export default HomeFirst;

export default withTracker((data) => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(HomeFirst);