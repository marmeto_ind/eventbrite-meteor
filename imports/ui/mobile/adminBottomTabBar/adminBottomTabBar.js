import React, {Component} from 'react';
import './adminBottomTabBar.scss';
import {browserHistory} from 'react-router';

class AdminTabBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            IsIos: false,
        };
    }

    isIosFunction(e) {
        e.preventDefault();
        this.setState({IsIos: !this.state.IsIos})
    }

    handleChatClick() {
        browserHistory.push("/mobile/admin/chats");
    }

    handleMailClick() {
        browserHistory.push("/mobile/admin/mail");
    }

    handleApprovalClick() {
        browserHistory.push("/mobile/admin/approvals");
    }

    handleStatisticsClick() {
        browserHistory.push("/mobile/admin/statistics");
    }

    handleSettingsClick() {
        browserHistory.push("/mobile/admin/settings");
    }

    isIOSDevice() {
        return (!!navigator.userAgent.match(/(iPad|iPhone|iPod)/g));
    }

    handleBackButtonClick = () => {
        let pathname = browserHistory.getCurrentLocation().pathname;
        if (pathname !== "/mobile/admin/chats") {
            browserHistory.goBack();
        }
    };

    componentDidMount() {
        const url = browserHistory.getCurrentLocation();
        const isIOS = this.isIOSDevice();
        this.setState({IsIos: isIOS});
        const page_url = window.location.href;
        const url_parts = page_url.split("/");
        const url_last_part = url_parts[url_parts.length - 1];

        if (page_url.indexOf("chat") > -1) {
            $('#adminChatImg').attr('src', '/img/tabBarImg/whiteChat.svg');
            
            $(".admin-chats-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".admin-chats-tab").css("color", "white");
            $(".admin-chats-tab").css("height","45px");
            $(".admin-chats-tab").css("width","auto");
            $(".admin-chats-tab").css("background-position","0");
            $(".admin-chats-tab").css("background-size","contain");
            $(".admin-chats-tab").css("background-repeat","no-repeat");
            $(".admin-chats-tab").css("background-position","50%");

        } else if (page_url.indexOf("mail") > -1) {
            $(".admin-mail-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".admin-mail-tab").css("color", "white");
            $(".admin-mail-tab").css("height","45px");
            $(".admin-mail-tab").css("width","auto");
            $(".admin-mail-tab").css("background-position","0");
            $(".admin-mail-tab").css("background-size","contain");
            $(".admin-mail-tab").css("background-repeat","no-repeat");
            $(".admin-mail-tab").css("background-position","50%");

        } else if (page_url.indexOf("approval") > -1) {

            $(".admin-approvals-tab").css({
                'background': 'linear-gradient(to right, rgb(107, 62, 96) 0%, rgb(35, 35, 34) 50%, rgb(107, 62, 96) 100%)',
                'color': 'white',
                'height': '40px',
                'width': '40px',
                'border-radius': '1000px',
                'line-height':'40px',
            });

            $('#adminApprvImg').attr('src', '/img/tabBarImg/whiteApproval.svg');

            $(".admin-approvals-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".admin-approvals-tab").css("color", "white");
            $(".admin-approvals-tab").css("height","45px");
            $(".admin-approvals-tab").css("width","auto");
            $(".admin-approvals-tab").css("background-position","0");
            $(".admin-approvals-tab").css("background-size","contain");
            $(".admin-approvals-tab").css("background-repeat","no-repeat");
            $(".admin-approvals-tab").css("background-position","50%");

        } else if (page_url.indexOf("statistics") > -1) {

            $('#adminStatImg').attr('src', '/img/tabBarImg/white_stat.svg');

            $(".admin-statistics-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".admin-statistics-tab").css("color", "white");
            $(".admin-statistics-tab").css("height","45px");
            $(".admin-statistics-tab").css("width","auto");
            $(".admin-statistics-tab").css("background-position","0");
            $(".admin-statistics-tab").css("background-size","contain");
            $(".admin-statistics-tab").css("background-repeat","no-repeat");
            $(".admin-statistics-tab").css("background-position","50%");

        } else if (page_url.indexOf("settings") > -1) {
            $('#adminSettImg').attr('src', '/img/tabBarImg/white_sett.svg');

            $(".admin-settings-tab").css('background','url("/img/lowerbar_background.svg")');
            $(".admin-settings-tab").css("color", "white");
            $(".admin-settings-tab").css("height","45px");
            $(".admin-settings-tab").css("width","auto");
            $(".admin-settings-tab").css("background-position","0");
            $(".admin-settings-tab").css("background-size","contain");
            $(".admin-settings-tab").css("background-repeat","no-repeat");
            $(".admin-settings-tab").css("background-position","50%");

        } else {
            $(".icon-size").css("width", "20px");
            $(".icon-size").css("height", "20px");

            $('#adminChatImg').attr('src', '/img/chat-notifi.png');
            $('#adminApprvImg').attr('src', '/img/tabBarImg/appr.svg');
            $('#adminStatImg').attr('src', '/img/svg_img/Graph.svg');
            $('#adminSettImg').attr('src', '/img/tabBarImg/grey_settings.svg');

        }
    }

    render() {
        return (
            <div>
                <div className={"AdminBottomBar"}>
                    <div id="tabBarDiv">
                        <div id="tabBarInnerContainer">
                            <div className="tab">
                                <button className="tablinks backButton" onClick={this.handleBackButtonClick.bind(this)}>
                                    <span><img className={"icon-size"} src={"/img/svg_img/left-arrow_svg.svg"}/></span>
                                </button>

                                <button className="tablinks" onClick={this.handleChatClick.bind(this)}>
                                    <div className="admin-chats-tab">
                                        <span><img id="adminChatImg" className={"icon-size"}
                                                   src={"/img/tabBarImg/greyChat.svg"}/></span>
                                    </div>
                                </button>

                                <button className="tablinks" onClick={this.handleApprovalClick.bind(this)}>
                                    <div className="admin-approvals-tab ">
                                        <span><img id="adminApprvImg" className={"icon-size"}
                                                   src={"/img/tabBarImg/appr.svg"}/></span>
                                    </div>
                                </button>

                                <button className="tablinks" onClick={this.handleStatisticsClick.bind(this)}>
                                    <div className="admin-statistics-tab">
                                        <span><img id="adminStatImg" className={"icon-size"}
                                                   src={"/img/svg_img/Graph.svg"}/></span>
                                    </div>
                                </button>

                                <button className="tablinks" onClick={this.handleSettingsClick.bind(this)}>
                                    <div className="admin-settings-tab">
                                        <span><img id="adminSettImg" className={"icon-size"}
                                                   src={"/img/tabBarImg/grey_settings.svg"}/></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default AdminTabBar;
