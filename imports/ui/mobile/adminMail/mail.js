import React from 'react';
import './mail.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";

class MobileAdminMail extends React.Component {
    render() {
        return (
            <div>
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-service-div"}>
                        <p className={"chattext"}>Mail</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: Mati</p>
                    </div>
                </div>
                <AdminTabBar/>
            </div>
        )
    }
}

export default MobileAdminMail;