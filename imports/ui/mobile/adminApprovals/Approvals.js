import React from 'react';
import './approvals.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import ShowSellersForApproval from './ShowSellersForApproval';
import { withTracker } from 'meteor/react-meteor-data';

class Approvals extends React.Component {
    render() {
        return (
            <div className="status_div_wrapper_mobile">
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-admin-approval-div"}>
                        <p className={"chattext"}>Approvals</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>{this.props.userName}</p>
                    </div>

                </div>

                <ShowSellersForApproval/>

                <AdminTabBar/>
            </div>
        )
    }
}

// export default Approvals;

export default withTracker(() => {
    const userName = Meteor.user()? Meteor.user().profile.name: '';
    return {
        userName: userName,
    }
})(Approvals);