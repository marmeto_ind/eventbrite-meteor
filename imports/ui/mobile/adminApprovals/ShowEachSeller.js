import React from 'react';
import {browserHistory} from 'react-router';

class ShowEachSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleSellerClick = this.handleSellerClick.bind(this);
    }

    handleSellerClick() {
        const seller = this.props.seller;
        browserHistory.push("/mobile/admin/approval-service/" + seller._id);
    }

    render() {
        const seller = this.props.seller;
        const sellerImageCount = seller.profile.moreImages.length;
        const createdAt = seller.createdAt.toString();
        
        return (
            // <div className={"second-div"}>
            //     <ul
            //         className="list-group admin-approvals-listBackground"
            //         onClick={this.handleSellerClick.bind(this)}>

            //         <li className="list-group-item div-left-icon mobile-admin-left-icon">
            //             <img className="create-add all-div-icon " src="/img/john-fashion.png" alt="" />
            //             <span className={"time-text"}>11:48</span>
            //         </li>
            //         <li className="list-group-item text-list-div-style all-div-text1">
            //             {seller.name}
            //         </li>
            //         <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
            //         <p style={margin}><img src="/img/mobile_searcBox_line.png" /></p>
            //         </li>
            //         <li className="list-group-item text-list-div-style all-div-text2 ">
            //             {seller.profile.address}
            //         </li>
            //         <li className="list-group-item text-list-div-style all-div-text3">
            //             {sellerImageCount + 2} pictures
            //         </li>
            //     </ul>
            // </div>
            <div className="third-div" onClick={this.handleSellerClick}>
                <ul className="list-group vendorstats-listBackground 
                    reviews-background approval_wrapper_section">
                    <div className="left_div">
                        <img className="" src="/img/cake.png" alt="" /><span>{ createdAt.substring(16, 21) }</span>
                    </div>
                    <div className="center_div">
                        <h2>{seller.name}</h2>
                        <p className="address">{seller.profile.address}</p>
                        <p className="image_details">{sellerImageCount + 2} pictures</p>
                    </div>
                    <div className="right_div"><img src="/img/tick_golden.svg" /></div>
                </ul>
            </div>
        )
    }
}

export default ShowEachSeller;