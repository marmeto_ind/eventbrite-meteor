import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import ShowEachSeller from './ShowEachSeller';

class ShowSellersForApproval extends React.Component {

    renderSellers() {
        return this.props.sellers.map((seller, index) => (
            <ShowEachSeller seller={seller} key={index}/>
        ))
    }

    render() {
        return (
            <div className={"mobile-admin-approvals "}>

                {this.renderSellers()}
                {/*<div className={"second-div"}>*/}
                    {/*<ul className="list-group admin-approvals-listBackground">*/}

                        {/*<li className="list-group-item div-left-icon mobile-admin-left-icon">*/}
                            {/*<img className="create-add all-div-icon " src="/img/john-fashion.png" alt="" />*/}
                            {/*<span className={"time-text"}>11:48</span>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text1">*/}
                            {/*Castello Bakery*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                            {/*<hr className={"hr-width"}/>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                            {/*Amsterdam*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                            {/*9 Month Subscription*/}
                        {/*</li>*/}

                    {/*</ul>*/}
                {/*</div>*/}

                {/*<div className={"second-div"}  >*/}
                    {/*<ul className="list-group admin-approvals-listBackground">*/}

                        {/*<li className="list-group-item div-left-icon">*/}
                            {/*<img className="create-add all-div-icon mobile-admin-left-icon " src="/img/john-fashion.png" alt="" />*/}
                            {/*<span className={"time-text"}>11:48</span>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text1">*/}
                            {/*Johns Fashion*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                            {/*<hr className={"hr-width"}/>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                            {/*Stockholm*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                            {/*8 pictures*/}
                        {/*</li>*/}

                    {/*</ul>*/}
                {/*</div>*/}

                {/*<div className={"second-div"}  >*/}
                    {/*<ul className="list-group admin-approvals-listBackground">*/}
                        {/*<li className="list-group-item div-left-icon mobile-admin-left-icon">*/}
                            {/*<img className="create-add all-div-icon " src="/img/john-fashion.png" alt="" />*/}
                            {/*<span className={"time-text"}>11:48</span>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text1">*/}
                            {/*DJ Henry*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style hr-padding stats-hr-line ">*/}
                            {/*<hr className={"hr-width"}/>*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text2 ">*/}
                            {/*StockHolem*/}
                        {/*</li>*/}
                        {/*<li className="list-group-item text-list-div-style all-div-text3">*/}
                            {/*8 Pictures*/}
                        {/*</li>*/}

                    {/*</ul>*/}
                {/*</div>*/}
            </div>
        )
    }
}

// export default ShowSellersForApproval;

export default withTracker(() => {
    Meteor.subscribe('sellers.all');
    const sellers = Sellers.find({"isApprove" : false}).fetch();

    return {
        sellers: sellers,
    }
})(ShowSellersForApproval);