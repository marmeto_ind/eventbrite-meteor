import React from 'react';
import './date.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';

class Date extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        // console.log(this.props.params);
    }

    render() {
        // console.log(this.props.event);
        const event = this.props.event;
        // console.log(event);

        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>{event? event.date: ''}</p>
                        <p className={"hanna-text"}><span className={"mobile_user_dot"}>.</span>{Meteor.user()? Meteor.user().profile.name: ''}</p>
                    </div>
                </div>

                <div className={"mobile-vendor-date-block"}>
                    <ul className={"list-group vendor-date-list-box"}>
                        <li className={"list-group-item vendor-date-list"}>
                            <div className={"vendor-date-box-width"}>
                                <div className={"vendor-date-img-box"}>
                                    <img src={"/img/greyStatue.png"} className={"vendor-date-img"} />
                                </div>
                                <div className={"vendor-date-text-box"}>
                                    <p className={"datePClass"}>{event? event.name: ''}</p>
                                    <p className={"date-sub-pClass"}>Couple</p>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item vendor-date-list"}>
                            <div className={"vendor-date-box-width"}>
                                <div className={"vendor-date-img-box"}>
                                    <img src={"/img/clock.png"} className={"vendor-date-img"} />
                                </div>
                                <div className={"vendor-date-text-box"}>
                                    <p className={"datePClass"}>KL 21:48</p>
                                    <p className={"date-sub-pClass"}>Couple</p>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item vendor-date-list"}>
                            <div className={"vendor-date-box-width"}>
                                <div className={"vendor-date-img-box"}>
                                    <img src={"/img/greyPack.png"} className={"vendor-date-img"} />
                                </div>
                                <div className={"vendor-date-text-box"}>
                                    <p className={"datePClass"}>Exclusive Pack</p>
                                    <p className={"date-sub-pClass"}>Menu</p>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item vendor-date-list"}>
                            <div className={"vendor-date-box-width"}>
                                <div className={"vendor-date-img-box"}>
                                    <img src={"/img/guest1.png"} className={"vendor-date-img"} />
                                </div>
                                <div className={"vendor-date-text-box"}>
                                    <p className={"datePClass"}>{event? event.numOfGuest: ''}</p>
                                    <p className={"date-sub-pClass"}>Guests</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <VendorBottomTabBar/>
            </div>
        )
    }
}

export default withTracker((data) => {

    const eventId = data.params.eventId;
    // console.log(eventId);
    Meteor.subscribe('events.all');
    const event = Events.findOne({"_id" : eventId});
    return {
        event: event
    }
})(Date);