import React from 'react';
import './statistics.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import { withTracker } from 'meteor/react-meteor-data';
import ChatStatistics from './ChatStatistics';
import ViewStatistics from './ViewStatistics';
import UserStatistics from './UserStatistics';
import ChoiceStatistics from './ChoiceStatistics';
import ServiceStatistics from './ServiceStatistics';
import ReviewStatistics from './ReviewStatistics';

class Statistics extends React.Component {
    render() {
        return (
            <div className={"height_100"}>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"admin-stats-text-div"}>
                        <p className={"chattext"}>Statistics</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: {this.props.userName}</p>
                    </div>
                </div>

                <div className={"mobile-admin-statistics-block"}>

                    <ChatStatistics/>

                    <ServiceStatistics/>

                    <UserStatistics/>

                    <ViewStatistics/>               

                    <ChoiceStatistics/>
                
                    <ReviewStatistics/>

                </div>
                <AdminTabBar/>

            </div>
        )
    }
}

export default withTracker(() => {
    const userName = Meteor.user()? Meteor.user().profile.name: '';
    return {
        userName: userName,
    }
})(Statistics);
