import React from 'react';

class ReviewStatistics extends React.Component {
    render() {
      
        return (
            <div className={"common-div"}>
                <ul className="list-group reviews-background adminstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/agenda-star.png" alt="Logo" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        4 R/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                    <p className="mobile_admin_line"><img src="/img/mobile_searcBox_line.png" /></p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        3 REVIEWS LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        4 IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default ReviewStatistics;