import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {ChatWith} from "../../../api/chatWith";
import {Meteor} from 'meteor/meteor';

class ChatStatistics extends React.Component {
    render() {
        const totalNumOfChat = this.props.totalNumOfChat;
        const numOfChatInLastWeek = this.props.numOfChatInLastWeek;
        const numOfChatInMonth = this.props.numOfChatInMonth;
       

        return (
            <div className={"common-div active-chat-background"}>
                <ul className="list-group adminstats-listBackground">
                    <li className="list-group-item div-left-icon">
                        <img className="create-add all-div-icon " src="/img/new_img/red_chat.svg" alt="Logo" />
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text1">
                        {numOfChatInMonth} A C/M
                    </li>
                    <li className="list-group-item text-list-div-style hr-padding stats-hr-line ">
                    <p className="mobile_admin_line"><img src="/img/mobile_searcBox_line.png" /></p>
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text2 ">
                        {numOfChatInLastWeek} ACTIVE CHATS LAST WEEK
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text3">
                        {totalNumOfChat} IN TOTAL SINCE START
                    </li>
                    <li className="list-group-item text-list-div-style all-div-text4">
                        (1)
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('chat_with.all');

    const oneDay = 24 * 60 * 60 * 1000;
    const oneWeekAgo = Date.now() - (7 * oneDay);
    const oneMonthAgo = Date.now() - (30 * oneDay);

    const totalNumOfChat = ChatWith.find().count();
    const numOfChatInLastWeek = ChatWith.find({"createdAt": {"$gt": new Date(oneWeekAgo) }}).count();
    const numOfChatInMonth = ChatWith.find({"createdAt": {"$gt": new Date(oneMonthAgo) }}).count();

    return {
        totalNumOfChat: totalNumOfChat,
        numOfChatInLastWeek: numOfChatInLastWeek,
        numOfChatInMonth: numOfChatInMonth,
    }
})(ChatStatistics);