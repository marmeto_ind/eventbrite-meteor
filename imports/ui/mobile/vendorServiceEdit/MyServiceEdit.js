import React from 'react';
import './my-service-edit.scss';
import VendorBottomTabBar from "../vendorTabBar/vendorTabBar";

class MobileVendorServiceEdit extends React.Component {
    render() {
        const servlogoImg ="/img/event_banner.jpg";
        const servLogoBackStyle = {
            backgroundImage: 'url(' + servlogoImg + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow:"hidden",
            width: "100%",
            cursor:"pointer"
        };

        const servMainImg ="/img/event_banner.jpg";
        const servMainBackStyle = {
            backgroundImage: 'url(' + servMainImg + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow:"hidden",
            width: "100%",
            cursor:"pointer"
        };

        const servVideo ="/img/event_banner.jpg";
        const servVideoBackStyle = {
            backgroundImage: 'url(' + servVideo + ')',
            padding: "20px 0px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            overflow:"hidden",
            width: "100%",
            cursor:"pointer"
        };


        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>My Business</p>
                        <p className={"hanna-text"}><span className={"mobile_vendor_dot"}>.</span>Panorama Eventcenter</p>
                    </div>
                </div>

                {/*BodyPart*/}
                <div className={"mobile-vendor-MyBusinessEdit-block"}>
                    <div className={"mobile-vendor-myBusiness"}>
                        <form className={"vendor-my-business-form "}>
                            <ul className={"list-group"}>
                                <li style={servLogoBackStyle} className={"list-group-item  my-business-li-class"}>
                                    <div  className="picture-upload">
                                        <div className={"mobile-main-camera-abs"}>
                                            <img id="hi" className="white-add-img" src="/img/photo-camera.png" alt="" />
                                        </div>
                                        <div className="add-more-pic">
                                            <label className="add-file img-border-label" htmlFor={"mobileVendorBusinessLogo"}>
                                                <img id="hi" className="white-add-img" src="/img/white-add.png" alt="" />

                                            </label>

                                            <input className="form-control-file" id="mobileVendorBusinessLogo" type="file" />
                                            <h4 className={"h4Class"}>Logo Picture</h4>
                                            <p className={"pClass"}>(Max 100 Mb)</p>
                                        </div>
                                    </div>
                                </li>
                                <li style={servMainBackStyle} className={"list-group-item  my-business-li-class"}>
                                    <div className="picture-upload">
                                        <div className={"mobile-main-camera-abs"}>
                                            <img id="hi" className="white-add-img" src="/img/photo-camera.png" alt="" />
                                        </div>
                                        <div className="add-more-pic">
                                            <label className="add-file img-border-label" htmlFor={"mobileVendorBusinessMain"}>
                                                <img id="hi" className="white-add-img" src="/img/white-add.png" alt="" />

                                            </label>

                                            <input className="form-control-file" id="mobileVendorBusinessMain" type="file" />
                                            <h4>Main Picture</h4>
                                            <p>(Max 100 Mb)</p>
                                        </div>
                                    </div>
                                </li>
                                <li className={"list-group-item  my-business-li-class"}>
                                    <div className="picture-upload more-pictures-relative-div">
                                        <div className={"more-pictures-absolute-div"}>
                                        <div className={"mobile-main-camera-abs"}>
                                            <img id="hi" className="white-add-img" src="/img/photo-camera.png" alt="" />
                                        </div>
                                        <div className="add-more-pic">
                                            <label className="add-file img-border-label" htmlFor={"mobileVendorBusinessMore"}>
                                                <img id="hi" className="white-add-img" src="/img/white-add.png" alt="" />

                                            </label>

                                            <input className="form-control-file" id="mobileVendorBusinessMore" type="file" />
                                            <h4 className={"h4Class"}>More Picture</h4>
                                            <p className={"pClass"}>(Max 100 Mb)</p>
                                        </div>
                                        </div>

                                        <div className={"mobile-vendor-more-pictures-scroll"}>
                                            <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} />
                                            </div>
                                            <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} />
                                            </div>
                                            <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} />
                                            </div>
                                            <div className={"mobile-vendor-more-pictures-scroll-div"}>
                                                <img src={"/img/logo_new.png"} className={"mobile-more-pictures-img"} />
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li style={servVideoBackStyle} className={"list-group-item  my-business-li-class"}>
                                    <div className="picture-upload">
                                        <div className={"mobile-main-camera-abs"}>
                                            <img id="hi" className="white-add-img" src="/img/white-play.png" alt="" />
                                        </div>
                                        <div className="add-more-pic">
                                            <label className="add-file img-border-label" htmlFor={"mobileVendorBusinessVideo"}>
                                                <img id="hi" className="white-add-img" src="/img/white-play.png" alt="" />

                                            </label>

                                            <input className="form-control-file" id="mobileVendorBusinessVideo" type="file" />
                                            <h4>Video</h4>
                                            <p>(Max 25 Mb)</p>
                                        </div>
                                    </div>
                                </li>
                                <li className={"list-group-item my-business-li-class"}>
                                    <div className="activated-pack vendor-business-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                            </p>
                                            <div className="heading-text business-pack">
                                                <h2>STANDARD PACK</h2>
                                            </div>
                                            <div className={"business-textarea-div"}>
                                        <textarea className={"business-text-area-box"}>
											</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className={"list-group-item my-business-li-class"}>
                                    <div className="activated-pack vendor-business-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                            </p>
                                            <div className="heading-text business-pack">
                                                <h2>GENEROUS PACK</h2>
                                            </div>
                                            <div className={"business-textarea-div"}>
                                        <textarea className={"business-text-area-box"}>
											</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className={"list-group-item my-business-li-class"}>
                                    <div className="activated-pack vendor-business-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print">
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                                <img className={"vendor-star"} src={"/img/star.png"} alt={"star"}/>
                                            </p>
                                            <div className="heading-text business-pack">
                                                <h2>EXCLUSIVE PACK</h2>
                                            </div>
                                            <div className={"business-textarea-div"}>
                                        <textarea className={"business-text-area-box"}>
											</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li className={"list-group-item my-business-li-class vendor-facility-pack"}>
                                    <div className={"all-facility-dv vendor-business-div vendor-business-div-background"}>
                                        <ul>
                                        <li  className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        <li  className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        <li className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        <li  className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        <li  className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        <li  className="form-check SellerFacilityClass">
                                            <input  className="form-check-input facility-check-box mobite-checkbox" type="checkbox" value="" />
                                            <label className="form-check-label">
                                                Kitchen
                                            </label>
                                        </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </form>

                    </div>
                </div>

                <VendorBottomTabBar/>
            </div>
        )
    }
}

export default MobileVendorServiceEdit;