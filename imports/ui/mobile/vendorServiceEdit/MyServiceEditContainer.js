import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import MyServiceEdit from './MyServiceEdit';

class MyServiceEditContainer extends React.Component {
    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/vendor/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "vendor") {
                    // Meteor.logout();
                    browserHistory.push("/mobile/vendor/login");
                    return (<div></div>);
                } else {
                    return (
                        <MyServiceEdit/>
                    )
                }
            }
        }
    }
}


export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(MyServiceEditContainer);
