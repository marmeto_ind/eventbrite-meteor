import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import Chat from './Chat';

class ChatContainer extends React.Component {
    render() {
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/vendor/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "admin") {
                    browserHistory.push("/mobile/vendor/login");
                    return (<div></div>);
                } else {
                    return (
                        <Chat userId={this.props.userId} user={this.props.user} sellerId={this.props.sellerId} />
                    )
                }
            }
        }
    }
}


export default withTracker((data) => {
    const logginIn = Meteor.loggingIn();
    const userId = data.params.userId;
    const sellerId = data.params.sellerId;
    const user = data.params.user;
    
    return {
        logginIn: logginIn,
        userId: userId,
        sellerId: sellerId,
        user: user
    }
})(ChatContainer);

