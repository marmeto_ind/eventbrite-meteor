import React from 'react';
import ShowEachMessage from './ShowEachMessage';

class ShowChat extends React.Component {
    renderMessages() {
        return this.props.messages.map((message, index) => (
            <ShowEachMessage message={message} key={index} />
        ));
    }

    render() {
        return (
            <ul className="list-group chats-conv-List">
                {this.renderMessages()}
            </ul>
        )
    }
}

export default ShowChat;