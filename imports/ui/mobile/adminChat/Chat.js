import React from 'react';
import './chat.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";
import {withTracker} from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Meteor} from 'meteor/meteor';
import ShowChat from './ShowChat';
import {Messages} from "../../../api/messages";
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerName: '',
            userId: '',
            sellerId: '',
        };
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;
        const existingValue = $("#messageText").val();
        if (existingValue) {
            $("#messageText").val(existingValue + " " + emojiCode);
        } else {
            $("#messageText").val(emojiCode);
        }
    }

    _handleOpenEmoji() {
        this.setState({openEmoji: !this.state.openEmoji});
    }

    componentWillMount() {
        const userId = this.props.userId;
        const sellerId = this.props.sellerId;
        const user = this.props.user;

        this.setState({userId: userId});
        this.setState({sellerId: sellerId});
        this.setState({user: user});
    }

    handleSendMessage = () => {
        const message = $("#messageText").val();
        const {userId, sellerId, user} = this.state;

        // Insert message into database only if message is not blank
        if (message) {
            if (sellerId) {
                Meteor.call('messages.insert', message, userId, sellerId, 'user', function (err, result) {
                    if (err) {
                        console.error(err);
                    }
                });
                $("#messageText").val("");
            } else {
                Meteor.call('messages.adminInsert', message, user, userId, 'admin', function (err, result) {
                    if (err) {
                        console.error(err);
                    }
                });
                $("#messageText").val("");
            }
        }
    };

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#mobileAdminAttach")[0].files[0];

        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                console.log(uploader.progress());
            }
        })
    }

    componentDidMount() {
        const {userId, sellerId, user} = this.props;
        const self = this;
        
        if (sellerId) {
            Meteor.call('seller.findOne', sellerId, function (err, seller) {
                if (err) {
                    console.log(err);
                } else {
                    if (seller) {
                        const sellerName = seller.name;
                        self.setState({sellerName: sellerName});
                    }
                }
            });
    
            Meteor.call('messages.addLastCheckedByUser', userId, sellerId, function (err, result) {
                if (err) {
                    console.log("error occured")
                }
            });
    
            Meteor.call('message_view.upsert', userId, sellerId, "admin", function (err, result) {
                if (err) {
                    console.log("error occured")
                } else {
                    console.log(result)
                }
            })
        }        
    }

    handleClick(event) {
        if (event.target.id == "messageText" || event.target.id == "mobile_emoji_img") {
            // $('#tabBarDiv').hide();
            // $("#mobileAdminTypeBox").css("bottom", "-20px");
        } else if (event.target.value != "") {
            // $('#tabBarDiv').hide();
            // $("#mobileAdminTypeBox").css("bottom", "-20px");
        } else {
            // $('#tabBarDiv').show();
            // $("#mobileAdminTypeBox").css("bottom", "40px");
            $('.mobile_user_emoji_block').hide();
        }
    }

    render() {
        const element = $(".list-group-item:last-child").offset();
        if (element) {
            $('html, body').animate({
                scrollTop: ($(".list-group-item:last-child").offset().top)
            }, 50);
        }

        return (
            <div onClick={this.handleClick.bind(this)}>
                {/*Header Bar*/}
                <div className={"mobile-admin-approval-service-div-header"}>
                    <div className={"mobile-active-chat-div"}>
                        {/*<p className={"chattext"}>Hanna Montana</p>*/}
                        <p className={"chattext"}>
                            {this.state.sellerName ? this.state.sellerName : ''}
                        </p>
                        <p className={"hanna-text"}><span
                            className={"mobile_admin_dot"}>.</span>Admin: {this.props.userName}</p>
                    </div>
                </div>

                <div>
                    <div className={"mobileAdminActiveChatBlock"}>
                        <div className={" float-chat-conv-div"}>
                            <ShowChat messages={this.props.messages}/>
                        </div>

                        {this.state.openEmoji === true ? <div className={"mobile_user_emoji_block"}>
                                <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)}/>
                            </div> :
                            ''
                        }

                        <div id={"mobileAdminTypeBox"} className={"chat-conv-type-box mobile_admin_type_box"}>
                            <div className={"block1"}>
                                <span><img className={"list-img"} id="mobile_emoji_img" src="/img/svg_img/smile.svg"
                                           alt="" onClick={this._handleOpenEmoji.bind(this)}/></span>
                            </div>
                            <div className={"block2"} onChange={this._handleFileUpload.bind(this)}>
                                <label htmlFor={"mobileAdminAttach"}>
                                   <span>
                                   <img
                                       className={"list-img"}
                                       src="/img/svg_img/attach.svg"
                                       alt=""
                                   />
                                   </span>
                                </label>
                                <input className="form-control-file" id="mobileAdminAttach" type="file"/>
                            </div>
                            <div className={"block3"}>
                                <input id={"messageText"}
                                       className="type-search"
                                       type="text"
                                       placeholder="WRITE A MESSAGE"
                                       name="message"
                                />
                            </div>
                            <div className={"block4"}>
                               <span className={"btn"} id={"adminChatSendButton"}
                                     onClick={this.handleSendMessage.bind(this)}>
                                   <img className={"list-img"} src="/img/svg_img/send.svg" alt=""/>
                               </span>
                            </div>
                        </div>
                    </div>
                </div>

                <AdminTabBar/>
            </div>
        )
    }
}

export default withTracker((data) => {
    const {user, userId, sellerId} = data;    
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    Meteor.subscribe('messages.all');
    
    if (sellerId) {        
        const messages = Messages.find({"user": userId, "seller": sellerId}).fetch();
        return {
            userName: userName,
            messages: messages,
        }
    } else {
        const messages = Messages.find({"admin": userId, "user": user}).fetch();
        return {
            messages: messages
        }
    }
})(Chat);

