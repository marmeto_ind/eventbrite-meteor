import React from 'react';
import Chats from './Chats';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';

class ChatsContainer extends React.Component {
    render() {
        if(this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    browserHistory.push("/mobile/login");
                    return (<div></div>);
                } else {
                    return (
                        <Chats/>
                    )
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(ChatsContainer);

