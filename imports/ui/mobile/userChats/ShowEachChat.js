import React from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {emojify} from 'react-emojione';
import {withTracker } from 'meteor/react-meteor-data';
import {MessageView} from "../../../api/messageView";
import {Messages} from "../../../api/messages";

class ShowEachChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            latestMessage: '',
            messageCount: '',
            seller: '',
            sellerLogoImage: '',
            placeholderImg: '/img/agenda-user.png'
        }
    }

    componentDidMount() {
        const self = this;
        const chat = this.props.chat;

        const userId = chat.user;
        const sellerId = chat.seller;

        if (sellerId) {
            Meteor.call('messages.getLatestMessageByUserIdSellerIdSeller',
            userId,
            sellerId,
            function (err, result) {
                if (err) {
                    console.error(err);
                } else {
                    if (!result) {
                        self.setState({latestMessage: ""});
                    } else {
                        self.setState({latestMessage: result.message});
                    }
                }
            }
        );

        Meteor.call('messages.getLatestCountBySellerId', userId, sellerId, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                self.setState({messageCount: result});
            }
        });

        Meteor.call('seller.findOne', sellerId, function (err, result) {
            if (err) {
                console.error(err);
            } else {
                self.setState({seller: result});
                self.setState({sellerLogoImage: result.profile.logoImage});
            }
        })

        }        
    }

    handleChatClick = (e) => {
        e.preventDefault();
        const chat = this.props.chat;
        const userId = chat.user;
        const sellerId = chat.seller;
        const adminId = chat.admin;

        if (adminId) {
            browserHistory.push('/mobile/user/chat/' + userId + '/' + adminId + "/admin");
        } else {
            browserHistory.push('/mobile/user/chat/' + userId + '/' + sellerId);
        }
    };

    showMessage(message) {
        if (this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    handleImageError() {
        this.setState({sellerLogoImage: 'https://via.placeholder.com/150'})
    }

    render() {
        const chat = this.props.chat;

        return (
            <li className="list-group-item mobile_user_chat_block_list_padding"
                onClick={this.handleChatClick.bind(this)}>
                <div className={"mobileUserChat"}>
                    <div className={"mobileuserChatIcon"}>
                <span>
                    <img className={"chatsListIcons"}
                         src={this.state.seller ? this.state.sellerLogoImage : this.state.placeholderImg}
                         onError={this.handleImageError.bind(this)}
                         />
                </span>
                    </div>
                    <div className={"chatPersonDetailsDiv"}>
                        <p className={"chatName"}>{chat.sellerName || chat.adminName}</p>
                        <p className={"lastConvMessage"}>{this.showMessage(this.state.latestMessage || "")}</p>
                    </div>
                    <div className={"mobileuserChatNumber"}>
                        {
                            this.props.newMsgCount > 0 ?
                                <span className="badge">{this.props.newMsgCount}</span> :
                                <span></span>
                        }
                    </div>
                </div>
            </li>
        )
    }
}

// Tracker
export default withTracker((options) => {
    Meteor.subscribe('message_view.all');
    Meteor.subscribe('messages.all');

    let userId = options.chat.user;
    let sellerId = options.chat.seller;

    let newMsgCount = 0;

    let msgView = MessageView.findOne({"user": userId, "seller": sellerId});

    let lastView = '';
    if (msgView) {
        lastView = msgView.updatedAt;
    }

    // Now check the number of message created after the time
    if (lastView) {
        newMsgCount = Messages.find({
            "user": userId,
            "seller": sellerId,
            "sender": "seller",
            "createdAt": {$gt: lastView}
        }).count()
    } else {
        newMsgCount = Messages.find({"user": userId, "seller": sellerId, "sender": "seller"}).count()
    }
    return {newMsgCount}
})(ShowEachChat);