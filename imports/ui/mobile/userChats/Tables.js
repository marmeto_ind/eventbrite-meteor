import React from 'react';
import './chats.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import { Meteor } from 'meteor/meteor';
import ShowEachTable from './ShowEachTable';
import { withTracker } from 'meteor/react-meteor-data';
import { GuestTable } from "../../../api/guestTable";
class Tables extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadComplete: false,
            event: '',
            tableName: '',
            eventId: ''
        }
        this.handleAddTable = this.handleAddTable.bind(this);
        this.handleTableInputChange = this.handleTableInputChange.bind(this);
    }

    handleTableInputChange(e) {
        let tableName = e.target.value;
        this.setState({ tableName: tableName })
    }

    componentDidMount() {
        //const userId = Meteor.userId();
        const self = this;
        const eventId = this.props._eventId;
        console.log("event id for guest list is ", eventId);
        Meteor.call('events.getUsersEventById', eventId, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                if (result) {
                    self.setState({ event: result });
                }
            }
        })
        self.setState({ loadComplete: true });

        // Meteor.call('events.getUsersLatestEvent', userId, function (err, result) {
        //     if(err) {
        //         console.log(err);
        //     } else {
        //         if(result) {
        //             self.setState({event: result});
        //         }
        //     }
        // })
        // added by NXP
        
    }

    renderTables() {
        //const tableList = this.state.tableList;
        console.log("table is ")
        // console.log(this.props.tableList)
        // console.log(this.props.tableList.name)
        return this.props.tableList.map((table, index) => (
            <ShowEachTable table={table} key={index} eventName={table.name} eventId={this.props._eventId} tableId={table._id} />
        ))
    }

    renderTableList() {
        if (this.state.loadComplete) {
            if (this.props.tableList.length > 0) {
                return (
                    this.renderTables()
                )
            } else {
                return (
                    <div className={"mobile_new_couple_message"}>
                        <p className="white_box">Wecome to wendely {this.props.userName} Please Add Table</p>
                    </div>
                )
            }
        } else {
            return (
                <div></div>
            )
        }
    }

    handleAddTable() {
        let tableName = this.state.tableName;
        let userId = Meteor.userId();
        const eventId = this.props._eventId;

        console.log({ msg: "inside handle add table function" });
        console.log({ tableName })

        if (tableName) {
            Meteor.call('guests_table.insert', eventId, tableName, userId, function (err, result) {
                if (err) {
                    console.error(err)
                } else {
                    console.log(result);
                }
            });
        }
        $("#tableName").val('');
    }

    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chats-header"}>
                    <div className={"chat-div"}>
                        <p className={"chattext"}>TABLE</p>
                        <p className={"mobile_userChat_hanna_text"}><span
                            className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                    </div>
                </div>

                {/*Body Part*/}
                <div className={"mobile_userChatsList mobileuserChatsBlockPosition mobile-user-guestlist background_none"}>
                    <div className="relativePosition">
                        <div className="chat_header table-header">{this.state.event.name || "no event"}</div>
                    </div>
                    <div className={"float-chat-div "}>
                        <ul className="list-group chatsList guestList">
                            {this.renderTableList()}
                        </ul>
                    </div>
                </div>
                <div className={"tableListParent update_class"}>
                    <div id={"mobileUserGuestListBox"} className={"mobile-user-guest-list-type-box table-list"}>
                        <div className="block1 guestlist_table">
                            <img src="/img/table.svg" />
                        </div>
                        <div className="block2 guelstlist_wrap">
                            <input id="tableName"
                                className="type-search"
                                type="text"
                                placeholder="WRITE A NAME"
                                name="message"
                                onChange={this.handleTableInputChange.bind(this)}
                            />
                        </div>
                        <div className={"block3"}>
                            <span className={"btn"} id={"userGuestListSendButton"} onClick={this.handleAddTable.bind(this)}>
                                <img className={"mobile-table-list-Img"} src="/img/svg_img/guest_plus_svg.svg" alt="" />
                            </span>
                        </div>
                    </div>
                </div>
                <BottomTabBar />
            </div>
        )
    }
}

export default withTracker((data) => {
    Meteor.subscribe('guests_table.all');

    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    const userId = Meteor.userId();
    const eventId = data._eventId;
    if (eventId) {
        const tables = GuestTable.find({ $and: [{ "user": userId }, { "eventId": eventId }] }).fetch();
        return {
            tableList: tables,  userName: userName,
        }
    } else {
        return {
            tableList: [],  userName: userName,
        }
    }
})(Tables);
