import React from 'react';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import {emojify} from 'react-emojione';
import {withTracker } from 'meteor/react-meteor-data';
// import {MessageView} from "../../../api/messageView";
// import {Messages} from "../../../api/messages";

class ShowEachTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            latestMessage: '',
            messageCount: '',
            seller: '',
            sellerLogoImage: ''
        }
    }

    componentDidMount() {
        // const self = this;
        // const table = this.props.table;
        // console.log({table})
    }
    // why it is here by NXP
    handleChatClick = (e) => {
        e.preventDefault();
        const chat = this.props.chat;
        const userId = chat.user;
        const sellerId = chat.seller;
        browserHistory.push('/mobile/user/chat/' + userId + '/' + sellerId);
    };

    //Added by NXP
    handleTableListClick = (e) => {
        e.preventDefault();
        const eventId = this.props.eventId;
        const tableId = this.props.tableId;
        const tableName = this.props.eventName;
        // console.log(this.props.eventName);
        browserHistory.push('/mobile/user/guestlist/'+ eventId + '/' + tableId + '/'+tableName+'/');
    };

    showMessage(message) {
        if (this.isUrl(message)) {
            return <a href={message} className={"attachUrl"} target="_blank" download="attachment">download file</a>;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    }

    handleImageError() {
        this.setState({sellerLogoImage: 'https://via.placeholder.com/150'})
    }

    render() {
        const table = this.props.table;

        return (
            <li className="list-group-item mobile_user_chat_block_list_padding"
                onClick={this.handleTableListClick.bind(this)}>
                <div className={"mobileUserChat table-list"}>
                    <div className={"mobileuserChatIcon"}>
                    <span>
                        <img className={"chatsListIcons tableListIcons"}
                            src={'/img/table.svg'}
                            onError={this.handleImageError.bind(this)}
                            />
                    </span>
                    </div>
                    <div className={"chatPersonDetailsDiv updateDesign"}>
                        <p className={"chatName"}>{table.name}</p>
                    </div>
                    <div className={"mobileuserChatNumber"}>
                        {
                            table.guestCount > 0 ?
                                <span className="badge">{table.guestCount}</span> :
                                <span></span>
                        }
                    </div>
                </div>
            </li>
        )
    }
}

// Tracker
export default withTracker((options) => {
    return {}
})(ShowEachTable);