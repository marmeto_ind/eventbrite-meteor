import React from 'react';
import './chats.scss';
import {browserHistory} from 'react-router';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import {Meteor} from 'meteor/meteor';
import ShowEachChat from './ShowEachChat';
import {withTracker} from 'meteor/react-meteor-data';

class Chats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatWithList: [],
            loadComplete: false,
        }
    }

    componentWillMount() {
        const self = this;
        const userId = Meteor.userId();

        Meteor.call('chat_with.getRecordByUserId', userId, function (err, result) {
            if (err) {
                console.log(err);
                self.setState({
                    loadComplete: true
                });
            } else {
                self.setState({
                    chatWithList: result,
                    loadComplete: true
                });
            }
        })

        // check chatwith record between user and admin
        Meteor.call('chat_with.insertChatWithAdmin', userId, function(err, result) {
            if (err) {
                console.log(err)
            } else {
                // console.log(result)
            }
        })
    }

    renderChats() {
        const chatWithList = this.state.chatWithList;
        return chatWithList.map((chat, index) => (
            <ShowEachChat chat={chat} key={index}/>
        ))
    }

    renderMessageOrEmptyText() {
        if (this.state.loadComplete) {
            if (this.state.chatWithList.length > 0) {
                return (
                    this.renderChats()
                )
            } else {
                return (
                    <div className={"mobile_new_couple_message"}>
                        {/* <p>Wecome to wendely</p>
                        <p>{this.props.userName}</p>
                        <br/> */}
                        <p className="no_chat_text">NO CHATS YET</p>
                    </div>
                )
            }
        } else {
            return (
                <div></div>
            )
        }

    }

    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chats-header"}>
                    <div className={"chat-div"}>
                        <p className={"chattext"}>CHAT</p>
                        <p className={"mobile_userChat_hanna_text"}><span
                            className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                    </div>
                </div>

                {/*Body Part*/}
                <div className={"mobile_userChatsList mobileuserChatsBlockPosition"}>
                    <div className={"float-chat-div "}>
                        <ul className="list-group chatsList">
                            {this.renderMessageOrEmptyText()}
                        </ul>
                    </div>
                </div>
                <BottomTabBar/>
            </div>
        )
    }
}

export default withTracker(() => {
    const userName = Meteor.user() ? Meteor.user().profile.name : '';
    return {
        userName: userName,
    }
})(Chats);
