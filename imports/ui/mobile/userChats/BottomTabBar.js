import React, {Component} from 'react';
import {browserHistory} from 'react-router';

class BottomTabBar extends Component {
    constructor(props) {
        super(props);
    }

    handleChatClick = () => {
        console.log('chat clicked');
    };

    handleSearchClick = () => {
        // console.log('search clicked');
        browserHistory.push('/mobile/user/search-category');
    };

    handleMyEventClick = () => {
        console.log('my event clicked');
        browserHistory.push("/mobile/user/my-event");
    };

    handleGuestListClick = () => {
        console.log('guest list clicked');
        browserHistory.push("/mobile/user/guest-list");
    };

    handleSettingsClick = () => {
        console.log("settings clicked");
        browserHistory.push("/mobile/user/settings");
    };

    render() {
        return (
            <div id="tabBarDiv">
                <div id="tabBarInnerContainer">
                    <div className="tab">
                        <button className="tablinks" onClick={this.handleChatClick.bind(this)}>
                            <span><img className={"icon-size"} src={"/img/chat-notifi.png"} /></span>
                            <span>  Chat</span>

                        </button>
                        <button className="tablinks" onClick={this.handleSearchClick.bind(this)}>
                            <span><img className={"icon-size"} src={"/img/chat-search.png"} /></span>
                            <span> Search</span>

                        </button>
                        <button className="tablinks" onClick={this.handleMyEventClick.bind(this)}>

                            <span><img className={"icon-size"} src={"/img/chats-logo.png"} /></span>
                            <span>My Event</span>

                        </button>
                        <button className="tablinks" onClick={this.handleGuestListClick.bind(this)}>

                            <span><img className={"icon-size"} src={"/img/guest-list.png"} /></span>
                            <span> GuestLists</span>

                        </button>
                        <button className="tablinks" onClick={this.handleSettingsClick.bind(this)}>
                            <span><img className={"icon-size"} src={"/img/chat-settings.png"} /></span>
                            <span>Settings</span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default BottomTabBar;
