import React from 'react';
import './chat.scss';
import BottomTabBar from '../userBottomBar/BottomTabBar';
import ShowMessages from './ShowMessages';
import EmojiPicker from 'emojione-picker';
import 'emojione-picker/css/picker.css';
import './emoji.scss';
import {withTracker} from 'meteor/react-meteor-data';
import {Sellers} from "../../../api/sellers";
import {Events} from "../../../api/events";
import {Meteor} from 'meteor/meteor';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            sellerId: '',
            emojiCode: ''
        };
    }

    _toggeEmoji(data) {
        const emojiCode = data.shortname;
        const existingValue = $("#messageText").val();
        if (existingValue) {
            $("#messageText").val(existingValue + " " + emojiCode);
        } else {
            $("#messageText").val(emojiCode);
        }
    }

    _handleOpenEmoji() {
        this.setState({openEmoji: !this.state.openEmoji});
    }

    componentWillMount() {        
        let {userId, sellerId, adminId} = this.props;

        this.setState({userId: userId});
        this.setState({sellerId: sellerId});
        this.setState({adminId: adminId});
    }

    componentDidMount() {
        let {userId, sellerId, adminId} = this.props;

        if (sellerId) {
            Meteor.call('messages.addLastCheckedByUser', userId, sellerId, function (err, result) {
                if (err) {
                    console.log(err)
                } else {
                }
            })
    
            Meteor.call('message_view.upsert', userId, sellerId, "user", function (err, result) {
                if (err) {
                    console.log("error occured")
                } else {
                }
            })
        }
        
    }

    handleClick(event) {
        if (event.target.id == "messageText" || event.target.id == "mobile_emoji_img") {
            // $('#tabBarDiv').hide();
            // $("#mobileUserChatBox").css("bottom", "-20px");
        } else if (event.target.value != "") {
            // $('#tabBarDiv').hide();
            // $("#mobileUserChatBox").css("bottom", "-20px");
        } else {
            // $('#tabBarDiv').show();
            // $("#mobileUserChatBox").css("bottom", "35px");
            // $('.mobile_user_emoji_block').hide();
        }
    }

    handleSendMessage = () => {
        const message = $("#messageText").val();
        // Insert message into database only if message is not blank
        if (message) {
            if (this.state.sellerId) {
                Meteor.call('messages.insert', message, this.state.userId, this.state.sellerId, 'user', function (err, result) {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(result);
                    }
                });
                $("#messageText").val("");
            } else {
                Meteor.call('messages.adminInsert', message, this.state.userId, this.state.adminId, 'user', function (err, result) {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(result);
                    }
                });
                $("#messageText").val("");
            }         
        }
    };

    handleQuickBookButton() {
        let {userId, sellerId, seller, event} = this.props;
        let categoryId = seller && seller.category && seller.category._id;
        let eventId = event && event._id;
        
        Meteor.call(
            'choices.insertOrRemove',
            userId,
            sellerId,
            categoryId,
            eventId,
            function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    alert("successfully booked event")
                    console.log(result);
                }
            }
        );
    };

    // Upload file and set the url to send message text box
    _handleFileUpload() {
        const file = $("#mobileUserAttach")[0].files[0];
        const uploader = new Slingshot.Upload("myFileUploads");
        let that = this;
        uploader.send(file, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
            } else {
                alert("file uploaded");
                $("#messageText").val(downloadUrl);
            }
        });

        // Track progress
        let computation = Tracker.autorun(() => {
            if (!isNaN(uploader.progress())) {
                console.log(uploader.progress());
            }
        })
    }

    render() {
        const element = $(".list-group-item:last-child").offset();
        if (element) {
            $('html, body').animate({
                scrollTop: ($(".list-group-item:last-child").offset().top)
            }, 50);
        }

        return (
            <div id={"chatComponent"} onClick={this.handleClick.bind(this)}>
                <div>
                    {/*Header Bar*/}
                    <div className={"mobile-chat-conv-div-header"}>
                        <div className={"chat-conv-div"}>
                            <p className={"chattext"}>{this.props.sellerName}</p>
                            <p className={"mobile_userChat_hanna_text"}><span
                                className={"mobile_user_dot"}>.</span>{this.props.userName}</p>
                        </div>
                    </div>

                    {/*Body Part*/}
                    <div className={"mobileUserChatBlock"}>
                        <div>
                            <ShowMessages 
                                userId={this.state.userId} 
                                adminId={this.state.adminId} 
                                sellerId={this.state.sellerId} />
                        </div>
                        {this.state.openEmoji === true ? <div className={"mobile_user_emoji_block"}>
                                <EmojiPicker search={true} onChange={this._toggeEmoji.bind(this)}/>
                            </div> :
                            ''
                        }

                        <div className="mobileUserChatBoxParent background-none">
                            <div id={"mobileUserChatBox"} onClick={this.handleClick.bind(this)}
                                className={"chat-conv-type-box mobile_user_chatBox_pos"}>
                                <div className={"block1"}>
                                    <span>
                                        <img
                                            id="mobile_emoji_img"
                                            className={"list-img"}
                                            src="/img/svg_img/smile_white.svg"
                                            alt=""
                                            onClick={this._handleOpenEmoji.bind(this)}
                                        />
                                    </span>
                                </div>
                                <div className={"block2"} onChange={this._handleFileUpload.bind(this)}>
                                    <label htmlFor={"mobileUserAttach"}>
                                        <span>
                                            <img
                                                className={"list-img"}
                                                src="/img/svg_img/attach_white.svg"
                                                alt=""
                                            />
                                        </span>
                                    </label>
                                    <input className="form-control-file" id="mobileUserAttach" type="file"/>
                                </div>
                                <div className={"block3"}>
                                    <input id={"messageText"}
                                        className="type-search"
                                        type="text"
                                        placeholder="WRITE A MESSAGE"
                                        name="message"
                                    />
                                </div>
                                <div className={"block4"}>
                                    <span className={"btn"} id={"userChatSendButton"} onClick={this.handleSendMessage.bind(this)}>
                                        <img className={"list-img"} src="/img/send.svg" alt=""/>
                                    </span>
                                </div>
                                {/* <div className={"block5"}>
                                    <span className={"btn"} id={"userQuickBookButton"} onClick={this.handleQuickBookButton.bind(this)}>
                                        <img className={"list-img"} src="/img/svg_img/smile.svg" alt=""/>
                                    </span>
                                </div> */}
                            </div>
                        </div>
                        
                    </div>

                    {/*Bottom chat-conv-tabBar*/}
                    <div id="chat-conv-tabBarDiv">
                        <BottomTabBar/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker((data) => {
    const {userId, sellerId, adminId} = data;
    const userName = Meteor.user() ? Meteor.user().profile.name : '';

    if (sellerId) {
        let sellerName = '';

        Meteor.subscribe('sellers.all');    
        Meteor.subscribe('events.all');
        
        const seller = Sellers.findOne({"_id": sellerId});
        let event = Events.findOne({"user" : userId}, {sort:{createdAt:1}});

        if (seller) {
            sellerName = seller.name;
        }        

        return { userName, sellerName,seller, event };
    } else {
        return {userName};
    }

})(Chat)

