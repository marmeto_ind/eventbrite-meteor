import React from 'react';
import {emojify} from 'react-emojione';
import {Sellers} from "../../../api/sellers";
import $ from 'jquery';
import {Meteor} from "meteor/meteor";

class ShowEachMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sellerImage: '',
            showRmMsgBtn: false
        };
        this.handleButtonPress = this.handleButtonPress.bind(this);
        this.handleButtonRelease = this.handleButtonRelease.bind(this);
    }

    showMessage(message) {
        if (this.isUrl(message)) {
            const fileUrl = <a href={message} className={"attachUrl"} target="_blank" download="attachment">download
                file</a>;

            return fileUrl;
        } else {
            return emojify(message);
        }
    }

    isUrl(s) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(s);
    }

    componentDidMount() {
        const message = this.props.message;
        const sellerId = message.seller;
        const self = this;

        if (sellerId) {
            Meteor.call('seller.findOne', sellerId, function (err, result) {
                if (err) {
                    console.log(err)
                } else {
                    if (result) {
                        self.setState({sellerImage: result.profile.logoImage})
                    }
                }
            })
        }
        
    }

    handleButtonPress() {
        this.buttonPressTimer = setTimeout(() => this.setState({showRmMsgBtn: true}), 1000);
    }

    handleButtonRelease() {
        clearTimeout(this.buttonPressTimer);
    }

    handleCrossBtnClick() {
        let message_id = this.props.message._id;
        this.setState({showRmMsgBtn: false});
        Meteor.call('messages.remove', message_id, function (err, result) {
            if (err) {
                console.error(err);
            }
        })
    }

    componentWillMount() {
        document.addEventListener('mousedown', this.handleClick, false)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClick, false);
    }

    handleClick = (e) => {
        if (e.target.id === "crossIcon") {
            // If click is inside, continue whatever you are doing
            return;
        }
        // The click is outside, do whatever you want
        this.handleOutsideClick();
    };

    handleOutsideClick() {
        this.setState({showRmMsgBtn: false})
    }

    render() {

        const message = this.props.message;
        const date = message.createdAt.toString().substring(8, 10);
        const month = message.createdAt.toString().substring(4, 7);
        const time = message.createdAt.toString().substring(16, 21);

        if (message.sender === "seller" || message.sender === "admin") {
            return (
                <li className="list-group-item ">
                    <div className={"row chatconvdiv1"}
                         onTouchStart={this.handleButtonPress}
                         onTouchEnd={this.handleButtonRelease}
                         onMouseDown={this.handleButtonPress}
                         onMouseUp={this.handleButtonRelease}
                    >
                        <div className={"chatsconvListIcon1"}>
                            {
                                this.state.showRmMsgBtn ?
                                    <img style={{width: '25px', height: '25px'}}
                                         src={"/img/icons8-delete.svg"} onClick={this.handleCrossBtnClick.bind(this)}
                                         id={"crossIcon"} ref={node => this.node = node}/> :
                                    <img style={{width: '25px', height: '25px'}}
                                         src={this.state.sellerImage}/>
                            }
                        </div>
                        <div className={"chatconvPersonDetailsDivleft"}>
                            <p>{this.showMessage(message.message)}</p>
                            <p className={"conv1text"}>{date + " " + month + " " + time}</p>
                        </div>
                        <div className="blank_block1">
                            {/* This block is created for styling purpose, do not delete it */}
                        </div>
                    </div>
                </li>
            )
        } else {
            return (
                <li className="list-group-item ">
                    <div className={"row chatconvdiv2"}
                         onTouchStart={this.handleButtonPress}
                         onTouchEnd={this.handleButtonRelease}
                         onMouseDown={this.handleButtonPress}
                         onMouseUp={this.handleButtonRelease}
                    >
                        <div className="blank_block1">
                            {/* This block is created for styling purpose, do not delete it */}
                        </div>
                        <div className={"chatconvPersonDetailsDiv"}>
                            <p className={"chatconvName"}>{this.showMessage(message.message)}</p>
                            <p className={"conv2text"}>{date + " " + month + " " + time}</p>
                        </div>
                        <div className={"chatsconvListIcon2"}>
                            {/*<img style={{width: '25px', height: '25px'}} src={"/img/svg_img/user.svg"}/>*/}
                            {
                                this.state.showRmMsgBtn ?
                                    <img style={{width: '25px', height: '25px'}}
                                         src={"/img/icons8-delete.svg"} onClick={this.handleCrossBtnClick.bind(this)}
                                         id={"crossIcon"} ref={node => this.node = node} /> :
                                    <img style={{width: '25px', height: '25px'}}
                                         src={"/img/svg_img/user.svg"}/>
                            }
                        </div>
                    </div>
                </li>
            )
        }
    }
}

export default ShowEachMessage;