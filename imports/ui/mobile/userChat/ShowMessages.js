import React, {Component} from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {Messages} from "../../../api/messages";
import ShowEachMessage from './ShowEachMessage';

class ShowMessages extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderMessages() {
        return this.props.messages.map((message, index) => (
            <ShowEachMessage message={message} key={index} />
        ))
    }

    render() {
        return (
            <div className={" float-chat-conv-div"}>
                <ul className="list-group chats-conv-List">
                    {this.renderMessages()}
                </ul>
            </div>
        )
    }
}

export default withTracker((data) => {
    const {userId, sellerId, adminId} = data;

    Meteor.subscribe('messages.all');
    
    if (sellerId) {
        return {
            messages: Messages.find({"user" : userId, "seller" : sellerId}).fetch(),
        }
    } else {
        return {
            messages: Messages.find({"user" : userId, "admin" : adminId}).fetch(),
        }
    }
    
})(ShowMessages);
