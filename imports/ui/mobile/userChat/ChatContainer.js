import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
import Chat from './Chat';

class ChatContainer extends React.Component {
    render() {
        const userId = this.props.params.userId;
        const sellerId = this.props.params.sellerId;
        const adminId = this.props.params.adminId;

        if(this.props.logginIn === true) {
            return (
                <div></div>
            )
        } else {
            if(!Meteor.user()) {
                browserHistory.push("/mobile/login");
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "user") {
                    browserHistory.push("/mobile/login");
                    return (<div></div>);
                } else {
                    if (adminId) {
                        return (
                            <Chat userId={userId} adminId={adminId} />
                        )
                    } else {
                        return (
                            <Chat userId={userId} sellerId={sellerId} />
                        )
                    }                    
                }
            }
        }
    }
}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(ChatContainer);

