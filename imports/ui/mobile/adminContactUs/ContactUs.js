import React from 'react';
import './contact-us.scss';
import AdminTabBar from "../adminBottomTabBar/adminBottomTabBar";

class ContactUs extends React.Component {
    render() {
        return (
            <div>
                {/*Header Bar*/}
                <div className={"mobile-chat-conv-div-header"}>
                    <div className={"chat-conv-div"}>
                        <p className={"chattext"}>Chat</p>
                        <p className={"hanna-text"}><span className={"mobile_admin_dot"}>.</span>Admin: Mati</p>
                    </div>
                </div>

                {/*Body Part*/}
                <div className={"mobile-admin-contact-block"}>
                    <ul>
                        <li className={"list-group-item mobile-admin-chat-list"}>
                            <div className={"admin-chatbox"}>
                                <div className={"admin-chatImg"}>
                                    <img src={"/img/user-login.png"} alt={"Img"} />
                                </div>
                                <div className={"admin-chatBody"}>
                                    <p className={"firstPclass"}>Admin</p>
                                    <p className={"secondPclass"}>Click on the settings Icon</p>
                                </div>
                                <div className={"admin-chatCount"}>
                                    <span>345</span>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-admin-chat-list"}>
                            <div className={"admin-chatbox"}>
                                <div className={"admin-chatImg"}>
                                    <img src={"/img/greyStatue.png"} alt={"Img"} />
                                </div>
                                <div className={"admin-chatBody"}>
                                    <p className={"firstPclass"}>Toni & Alexandara</p>
                                    <p className={"secondPclass"}>Hi How Are You</p>
                                </div>
                                <div className={"admin-chatCount"}>
                                    <span>34</span>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-admin-chat-list"}>
                            <div className={"admin-chatbox"}>
                                <div className={"admin-chatImg"}>
                                    <img src={"/img/greyStatue.png"} alt={"Img"} />
                                </div>
                                <div className={"admin-chatBody"}>
                                    <p className={"firstPclass"}>Michael & Marrie</p>
                                    <p className={"secondPclass"}>Only Yellow and Pink</p>
                                </div>
                                <div className={"admin-chatCount"}>
                                    <span>3</span>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-admin-chat-list"}>
                            <div className={"admin-chatbox"}>
                                <div className={"admin-chatImg"}>
                                    <img src={"/img/greyStatue.png"} alt={"Img"} />
                                </div>
                                <div className={"admin-chatBody"}>
                                    <p className={"firstPclass"}>Castello Bakery</p>
                                    <p className={"secondPclass"}>Hi How are you</p>
                                </div>
                                <div className={"admin-chatCount"}>
                                    <span>3</span>
                                </div>
                            </div>
                        </li>
                        <li className={"list-group-item mobile-admin-chat-list"}>
                            <div className={"admin-chatbox"}>
                                <div className={"admin-chatImg"}>
                                    <img src={"/img/greyStatue.png"} alt={"Img"} />
                                </div>
                                <div className={"admin-chatBody"}>
                                    <p className={"firstPclass"}>Michael & MArrie</p>
                                    <p className={"secondPclass"}>Hi How are you</p>
                                </div>
                                <div className={"admin-chatCount"}>
                                    <span>3</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <AdminTabBar/>

            </div>
        )
    }
}

export default ContactUs;