import React from 'react';
import {Meteor} from 'meteor/meteor';
import CategoryDropDown from './CategoryDropDown';
import { ToastContainer, toast } from 'react-toastify';
import Progress from 'react-progressbar';
import ReactPlayer from 'react-player';
import SellerFacilityComponent from './ShowFacilities';
import './facilities.scss';
import {browserHistory} from 'react-router';

class AddServiceSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainPictureUrl: '',
            logoUrl: '',
            morePicture: [],
            videoUrl: '',
            mainImageUploadProgress: 0,
            logoImageUploadProgress: 0,
            videoUploadProgress: 0,
            morePictureUploadProgress: 0,
        };
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleMainImageUpload = this._handleMainImageUpload.bind(this);
        this._handleMorePictureUpload = this._handleMorePictureUpload.bind(this);
        this._handleVideoUpload = this._handleVideoUpload.bind(this);
        this._handleLogoUpload = this._handleLogoUpload.bind(this);
    }

    uploadSuccessNotify = (string) => {
        toast.success("Successfully uploaded " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    uploadErrorNotify = (string) => {
        toast.error("Error while uploading " + string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    successNotify = (string) => {
        toast.success(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    errorNotify = (string) => {
        toast.error(string, {
            position: toast.POSITION.BOTTOM_CENTER
        });
    };

    _handleSubmit(e) {
        e.preventDefault();

        // fetch all data from the form
        const name = $("#name").val();
        const credential = $("#credential").val();
        const address = $("#address").val();
        const subscription = $("#activatePack").val();

        const standardPack = $("#standardPack").val();
        const generousPack = $("#generousPack").val();
        const exclusivePack = $("#exclusivePack").val();

        const categoryId = $("#category").val();
        const categoryName = $("#category option:selected").text();

        const vendorEmail = $("#vendorEmail").val();
        const vendorPhone = $("#vendorNumber").val();
        const websiteName = $("#vendorWeb").val();

        const category = { _id: categoryId, name: categoryName};

        const mainImage = this.state.mainPictureUrl;
        const logoImage = this.state.logoUrl;
        const moreImages = this.state.morePicture;
        const sellerVideo = this.state.videoUrl;

        // Initialized seller facilities object
        const sellerFacilities = [];
        const selectedFacilities = $("input:checked");

        // get the id the the selected facilities and save it
        for (let index = 0; index < selectedFacilities.length; ++index) {
            let item = selectedFacilities[index];
            let itemId = item.id;

            let jquerySelector = "label[for='" + itemId.toString() + "']";
            let value = $(jquerySelector).text();
            let facility = {};

            facility["_id"] = item.id;
            facility["name"] = value;

            sellerFacilities.push(facility);
        }
        // console.log(sellerFacilities);

        const user = Meteor.userId();

        if(!user) {
            this.errorNotify("Please log in");
        } else if(!name) {
            this.errorNotify("User name is empty");
        } else if (!category) {
            this.errorNotify("Category is empty");
        } else if (!mainImage) {
            this.errorNotify("Main image is not uploaded");
        } else if (!logoImage) {
            this.errorNotify("Logo image is not uploaded");
        } else if (!moreImages) {
            this.errorNotify("Other Images are not uploaded");
        } else if (!sellerVideo) {
            this.errorNotify("Video is not uploaded");
        } else if (!address) {
            this.errorNotify("Please fill address field");
        } else if (!credential) {
            this.errorNotify("Please fill credential");
        } else if (!subscription) {
            this.errorNotify("Subscription is empty");
        } else if (!standardPack) {
            this.errorNotify("Standard pack field is empty");
        } else if (!generousPack) {
            this.errorNotify("Generous pack field is empty");
        } else if (!exclusivePack) {
            this.errorNotify("Exclusive pack field is empty");
        } else if(!vendorEmail) {
            this.errorNotify("Email field is empty");
        } else if(!vendorPhone) {
            this.errorNotify("Vendor phone is empty");
        } else if(!websiteName) {
            this.errorNotify("Website name is empty");
        } else {
            const result = Meteor.call('sellers.insert',
                user, name, category, {
                    mainImage: mainImage,
                    logoImage: logoImage,
                    moreImages: moreImages,
                    video: sellerVideo,
                    address: address,
                    website: websiteName,
                    email: vendorEmail,
                    phone: vendorPhone,
                    credential: credential,
                    subscription: subscription,
                    standardPack: standardPack,
                    generousPack: generousPack,
                    exclusivePack: exclusivePack,
                    facilities: sellerFacilities,
                });
            if(!result) {
                this.successNotify("Seller Service Added");
                browserHistory.push('/vendor/chat');

            } else {
                this.errorNotify("Please try again");
            }
        }

        // console.log(result);
    }

    // Upload main image and store the url to state variable
    _handleMainImageUpload() {
        const mainPictureFile = $("#vendorMainPicture")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(mainPictureFile, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify("Main Image");
            } else {
                // console.log('uploaded file available here: ' + downloadUrl);
                that.setState({mainPictureUrl: downloadUrl});
                // $("#mainImageUploadStatus").text("Image Successfully Uploaded");
                that.uploadSuccessNotify("Main Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ mainImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    // Upload logo and store the url to state variable
    _handleLogoUpload() {
        const logo = $("#vendorLogo")[0].files[0];
        const uploader = new Slingshot.Upload("myImageUploads");
        let that = this;
        uploader.send(logo, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                that.uploadErrorNotify("Logo Image");
                console.error(error);
            } else {
                // console.log('uploaded file available here: ' + downloadUrl);
                that.setState({logoUrl: downloadUrl});
                // $("#logoImageUploadStatus").text("image uploaded");
                that.uploadSuccessNotify("Logo Image");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ logoImageUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    // Upload video and store the url to state variable
    _handleVideoUpload() {
        const video = $("#vendorVideo")[0].files[0];
        // $("#videoUploadStatus").text("uploading");
        const uploader = new Slingshot.Upload("myVideoUploads");
        let that = this;
        uploader.send(video, function (error, downloadUrl) {
            computation.stop();
            if (error) {
                console.error(error);
                that.uploadErrorNotify("Video");
            } else {
                // console.log('uploaded file available here: ' + downloadUrl);
                that.setState({videoUrl: downloadUrl});
                // $("#videoUploadStatus").text("video uploaded");
                that.uploadSuccessNotify("Video");
            }
        });

        // Track progress
        let computation = Tracker.autorun (() => {
            if(!isNaN(uploader.progress())) {
                this.setState({ videoUploadProgress: uploader.progress() * 100 });
            }
        })
    }

    // Upload more pictures and store the picture url to state variable
    _handleMorePictureUpload() {
        const morePictures = $("#vendorMorePicture")[0].files;
        // $("#moreImageUploadStatus").text("uploading");
        // console.log($("#vendorMorePicture")[0].files.length);
        if(morePictures.length > 6) {
            this.errorNotify("you can upload max 6 other pictures")
        } else {
            const that = this;
            _.map(morePictures, function (file) {
                let uploader = new Slingshot.Upload("myImageUploads");
                uploader.send(file, function (err, downloadUrl) {
                    computation.stop();
                    if(err) {
                        that.setState({morePictureUploadProgress: 0});
                        that.uploadErrorNotify("Picture");
                        console.log(err)
                    } else {
                        // console.log(downloadUrl);
                        const morePictureArray = that.state.morePicture;
                        that.setState({morePictureUploadProgress: 0});
                        morePictureArray.push(downloadUrl);
                        that.uploadSuccessNotify("Picture");
                    }
                });
                // Track progress
                let computation = Tracker.autorun (() => {
                    if(!isNaN(uploader.progress())) {
                        that.setState({ morePictureUploadProgress: uploader.progress() * 100 });
                    }
                })
            });
        }
    }

    renderOtherImages() {
        return this.state.morePicture.map((imageUrl, index) => (
            <div className={" more-pictures-div"}> <img className={"more-pictures-img"} src={imageUrl} alt="" width={100} key={index}/> </div>
        ))
    }

    render() {
        // console.log(this.props);
        // console.log(this.state.morePicture);
        return (
            <div className="vendor-mobile-wrapper">
                <div className="vendor-mobile-container">
                    <div className="vendor-mobile-div-wrapper vendor-reg-body">
                        <div className="vendor-mobile-login-info">
                            <div className="login-header">
                                <h4 className={"add-new-service-text"}>Add New Service</h4>
                            </div>
                            <div id={"vendorAddServicePage"} className="login-form">
                                <form>
                                    <div className="picture-upload">
                                        {
                                            this.state.mainPictureUrl
                                            ?
                                            <div>
                                                <img className={"vendor-uploaded-image-size"} src={this.state.mainPictureUrl} alt=""/>
                                            </div>
                                            :
                                            <div className="add-more-pic" onChange={this._handleMainImageUpload.bind(this)}>
                                                <label className="add-file" htmlFor="vendorMainPicture" >
                                                    <img id="hi" className="" src="/img/add.png" alt="" />
                                                    <hr className={"vendor-registration-hr"} />
                                                </label>
                                                <input
                                                    type="file"
                                                    className="form-control-file"
                                                    id="vendorMainPicture"
                                                />
                                                <h4 className={"vendor-reg-text"}>Main Picture</h4>
                                                <p className={"vendor-reg-text maxMb-margin"}>(Max 100 Mb)</p>
                                                <Progress completed={this.state.mainImageUploadProgress} />
                                            </div>
                                        }

                                    </div>

                                    <div className="picture-upload">
                                        {
                                        this.state.logoUrl
                                        ?
                                        <div>
                                            <img className={"vendor-uploaded-image-size"} src={this.state.logoUrl} alt=""/>
                                        </div>
                                        :
                                        <div className="add-more-pic"
                                             onChange={this._handleLogoUpload.bind(this)} >
                                            <label className="add-file" htmlFor="vendorLogo">
                                                <img id="hi" className="" src="/img/add.png" alt="" />
                                                <hr className={"vendor-registration-hr"} />
                                            </label>
                                            <input
                                                type="file"
                                                className="form-control-file"
                                                id="vendorLogo"
                                            />
                                            <h4 className={"vendor-reg-text"}>Logo</h4>
                                            <p className={"vendor-reg-text maxMb-margin"}>(Max 10 MB)</p>
                                            <Progress completed={this.state.logoImageUploadProgress} />
                                        </div>
                                        }
                                    </div>

                                    <div className="picture-upload">
                                        {
                                            this.state.morePicture.length == 0
                                            ?
                                            <div className="add-more-pic more-pictures-box"
                                                 onChange={this._handleMorePictureUpload.bind(this)}>
                                                <label className="add-file" htmlFor="vendorMorePicture">
                                                    <img id="hi" className="" src="/img/add.png" alt="" />
                                                    <hr className={"vendor-registration-hr"} />
                                                </label>
                                                <input
                                                    type="file"
                                                    ref={"vendorMorePicture"}
                                                    className="form-control-file"
                                                    id="vendorMorePicture"
                                                    multiple
                                                />
                                                <h4 className={"vendor-reg-text"}>More Pictures</h4>
                                                <p className={"vendor-reg-text more-pictures-box-p"}>(Max 25 Mb)</p>
                                                {/*<Progress completed={this.state.morePictureUploadProgress} />*/}
                                            </div>
                                                :
                                                this.renderOtherImages()

                                        }

                                        <Progress completed={this.state.morePictureUploadProgress} />

                                    </div>

                                    <div className="picture-upload">
                                        {
                                            this.state.videoUrl?
                                                <div  className="vendor-video">
                                                    <ReactPlayer className={"vendor-video"} url={this.state.videoUrl} controls={true}/>
                                                </div>
                                                :
                                                <div className="add-more-pic"
                                                     onChange={this._handleVideoUpload.bind(this)}>
                                                    <label className="add-file" htmlFor="vendorVideo">
                                                        <img id="hi" className="" src="/img/add.png" alt="" />
                                                    </label>

                                                    <input
                                                        type="file"
                                                        ref={"vendorVideo"}
                                                        className="form-control-file"
                                                        id="vendorVideo"
                                                    />
                                                    <h4 className={"vendor-reg-text"}>Video</h4>
                                                    <p className={"vendor-reg-text maxMb-margin"}>(Max 25 Mb)</p>
                                                    <Progress completed={this.state.videoUploadProgress} />
                                                </div>
                                        }
                                    </div>

                                    <div className="username vendorname">
                                        <input
                                            id="name"
                                            type="text"
                                            className="type-username"
                                            placeholder="Enter username" />
                                        <p>username</p>
                                    </div>


                                    <div className="username vendoremail">
                                        <input
                                            id="vendorEmail"
                                            type="text"
                                            className="type-username"
                                            placeholder="Enter Email" />
                                        <p> mail id</p>
                                    </div>

                                    <div className="username vendornumber">
                                        <input
                                            id="vendorNumber"
                                            type="number"
                                            className="type-username"
                                            placeholder="Enter Number" />
                                        <p>number</p>
                                    </div>

                                    <div className="username vendorwebname">
                                        <input
                                            id="vendorWeb"
                                            type="text"
                                            className="type-username"
                                            placeholder="Enter website name" />
                                        <p>website</p>
                                    </div>

                                    <CategoryDropDown/>

                                    <div className="username vendorcredential">
                                        <input
                                            id="credential"
                                            type="text"
                                            className="type-username"
                                            placeholder="Enter credential" />
                                        <p>Credential</p>
                                    </div>

                                    <div className="username vendoraddress">
                                        <input
                                            id="address"
                                            type="text"
                                            className="type-username"
                                            placeholder="Enter Address" />
                                        <p>Address</p>
                                    </div>

                                    <div className="username vendorActivatedPack">
                                        <input type="text"
                                               className="type-username"
                                               placeholder="Activated Pack"
                                               id="activatePack" />
                                        <p>Activated Pack</p>
                                    </div>

                                    <div className="standardPack activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print"><i className="far fa-star"></i></p>
                                            <div className="heading-text">
                                                <h2>STANDARD PACK</h2>
                                                <hr className={"vendor-registration-hr"} />
                                            </div>
                                            <textarea className="form-control textarea-box" id="standardPack" rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div className=" generousPack activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print"><i className="far fa-star"></i><i className="far fa-star"></i></p>
                                            <div className="heading-text">
                                                <h2>Generous PACK</h2>
                                                <hr className={"vendor-registration-hr"} />
                                            </div>
                                            <textarea className="form-control textarea-box" id="generousPack" rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div className="exclusivePack activated-pack">
                                        <div className="box-container same-icon">
                                            <p className="star-print"><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i></p>
                                            <div className="heading-text">
                                                <h2>Exclusive PACK</h2>
                                                <hr className={"vendor-registration-hr"} />
                                            </div>
                                            <textarea className="form-control textarea-box" id="exclusivePack" rows="3"></textarea>
                                        </div>
                                    </div>

                                    {/*<div className="facility-dv">*/}
                                        {/*<div className="box-container same-icon">*/}
                                            {/*<div className="col-md-1 col-xs-1 checkbox-items-div">*/}
                                                {/*<input type="checkbox" className="facility-dv-checkbox reg-checkbox" />*/}
                                            {/*</div>*/}
                                            {/*<div className="col-md-11 col-xs-11">*/}
                                                {/*<div className="all-facility-dv">*/}
                                                    {/*<p>freeparking</p>*/}
                                                    {/*<p>Somking area</p>*/}
                                                    {/*<p>Free Wi-Fi</p>*/}
                                                    {/*<p>Elavator</p>*/}
                                                    {/*<p>4,5 m to celling</p>*/}
                                                    {/*<p>2000 m2</p>*/}
                                                    {/*<p>Bar</p>*/}
                                                    {/*<p>Stage</p>*/}
                                                    {/*<p>Kitchen</p>*/}
                                                {/*</div>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                    {/*</div>*/}

                                    <SellerFacilityComponent/>

                                    <button id={"seviceaddButton"}
                                        onClick={this._handleSubmit.bind(this)}
                                        type="button"
                                        className="btn btn-primary btn-w-md add-service-button"
                                    >ADD</button>

                                </form>
                                <ToastContainer/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddServiceSection;