import React from 'react';
import VendorAddService from './Registration';
import { withTracker } from 'meteor/react-meteor-data';
import {Meteor} from 'meteor/meteor';
import {browserHistory} from 'react-router';
// import {Redirect} from 'react-router-dom';


class VendorAddServiceContainer extends React.Component {

    render() {
        // return (
        //     <VendorAddService/>
        // )
        if(this.props.logginIn === true) {
            return (<div></div>);
        } else {
            if(!Meteor.user()) {
                // browserHistory.push('/login');
                return (<div></div>);
            } else {
                const user = Meteor.user();
                if(user.profile.role !== "vendor") {
                    browserHistory.push('/login');
                    return (<div></div>);
                } else {
                    return (
                        <VendorAddService/>
                    )
                }
            }
        }
    }

}

export default withTracker(() => {
    const logginIn = Meteor.loggingIn();
    return {
        logginIn: logginIn,
    }
})(VendorAddServiceContainer);

