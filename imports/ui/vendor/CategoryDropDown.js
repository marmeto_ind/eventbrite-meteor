import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {Categories} from "../../api/categories";

class EachCategory extends React.Component {
    render() {
        const category = this.props.category;

        return (
            <option value={category._id}>{category.name}</option>
        )
    }
}


class CategoryDropDown extends React.Component {
    renderCategories() {
        return this.props.categories.map((category) => (
            <EachCategory key={category._id} category={category}/>
        ));
    }



    render() {
        const style = {
            height: "100%",
        };

        return (
            <div className="form-group username vendor_selectBox">
                <select className="category-select select-category type-username vendor-category-div" id="category" >
                    {this.renderCategories()}
                </select>

            </div>



        )
    }
}


export default withTracker(() => {
    Meteor.subscribe('categories');
    const categories = Categories.find({}).fetch();

    return {
        categories: categories,
    };
})(CategoryDropDown);

