import React from 'react';
// import FileUploadComponent from './FileUpload';
import {Images} from "../../api/images";
import Dropzone from 'react-dropzone';

var Gallery = React.createClass({
    _handleUpload(files){ //this function is called whenever a file was dropped in your dropzone
        _.each(files, function (file) {
            file.owner = Meteor.userId(); //before upload also save the owner of that file
            Images.insert(file, function (err, fileObj) {
                if (err) {
                    // console.log(err); // in case there is an error, log it to the console
                } else {
                    //the image upload is done successfully.
                    //you can use this callback to add the id of your file into another collection
                    //for this you can use fileObj._id to get the id of the file
                    // console.log("Images uploaded successfully");
                    // console.log(Images.find({}).fetch())
                }
            });
        });
    },

    render(){
        return (
            <div>
                <Dropzone onDrop={this._handleUpload}>
                    <div>Upload Main Picture</div>
                </Dropzone>
            </div>
        )
    }
});

class UploadPicture extends React.Component {
    render() {
        return (
            <div>
                <Gallery/>
            </div>

        )
    }
}

export default UploadPicture;