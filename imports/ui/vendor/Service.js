import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import UploadPicture from './UploadPicture';
import TextField from 'material-ui/TextField';

const style = {
    margin:12,
}

class VendorService extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h2>Welcome to vendor services page</h2>
                <UploadPicture/>

                <UploadPicture/>

                <TextField
                    // hintText="Password Field"
                    floatingLabelText="Password"
                    type="password"
                /><br />

                <TextField
                    // hintText="Hint Text"
                    floatingLabelText="Business Name"
                /><br />

                <TextField
                    floatingLabelText="Venue"
                /><br />

                <TextField
                    floatingLabelText="Credentials"
                /><br />

                <TextField
                    floatingLabelText="Address"
                /><br />

                <TextField
                    floatingLabelText="Email"
                /><br />

                <TextField
                    floatingLabelText="Phone Number"
                /><br />

                <TextField
                    floatingLabelText="Website"
                /><br />

                <TextField
                    floatingLabelText="Subscription"
                /><br />

                <TextField
                    floatingLabelText="Debit Card"
                /><br />

            </div>
        )
    }
}

export default VendorService;