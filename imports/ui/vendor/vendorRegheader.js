import React from 'react';
import './vendor-registration.scss';

class Header extends React.Component {

    render() {
        return(
            <header className="home-header">
                <a href="#" className="home-logo"><img src="/img/logo_new.png" alt="" />
                </a>
                <h1 className="home-logo-text">WENDELY</h1>

                <nav className="home-header-nav home-header-nav-new">
                    <ul>
                        <li className="home-header-nav-item"><a href="#" className="home-header-nav-link key"><img className="home-logo2" src="/img/user-profile-icon.png" alt="" /></a></li>
                        <li className="home-header-nav-item"><a href="#" className="home-header-nav-link"></a><img className="home-logo1" src="/img/settings1.png" alt="" /></li>
                    </ul>
                </nav>
            </header>
        )

    }
}

export default Header;