import React from 'react';
import Header from '../pages/header-username/header';
import Footer from '../components/footer/Footer';
import './vendor-registration.scss';
import TopBar from '../components/vendorTopBar/TopBar';
// import LeftSideBar from './LeftSideBar';
import AddServiceSection from './AddServiceSection';

class VendorAddService extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="body-cntnr main-container">
                    <TopBar/>
                    <div className="clearfix"></div>
                    <div className="body_container chat-body">
                        <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div className={"col-md-12 padding"}>
                                <div className={"vendor-registration-block"}>
                                    <div className={"chat-text vendor-reg-left"}>
                                        Vendor Registration
                                    </div>
                                    <div className={"vendor-reg-right"}>
                                        <img className={"svg-img-style"} src={"/img/line.png"} />
                                    </div>
                                </div>
                                <AddServiceSection/>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                </div>

                <Footer/>
            </div>
        )
    }
}

export default VendorAddService;