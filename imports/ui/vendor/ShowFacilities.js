import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {SellerFacilities} from "../../api/sellerFacilities";

class EachSellerFacility extends React.Component {

    render() {
        const sellerFacility = this.props.sellerFacility;
        const vendorAddServiceFacilityImage = this.props.sellerFacility.image;
        console.log(this.props.sellerFacility.image);

        const vendorAddFacilityStyle = {
            backgroundImage: 'url(' + vendorAddServiceFacilityImage + ')',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '35px 35px',
            backgroundPosition: '50% 50%',
        };
        return (
            <li style={vendorAddFacilityStyle} id={"SellerFacility"} className="form-check SellerFacilityClass">
                <input  className="form-check-input facility-check-box" type="checkbox" value="" id={sellerFacility._id}/>
                <label className="form-check-label" htmlFor={sellerFacility._id}>
                    {sellerFacility.name}
                </label>
            </li>
        )
    }
}

class SelectSellerFacility extends React.Component {

    renderSellerFacilities() {
        return this.props.sellerFacilities.map((sellerFacility) => (
            <EachSellerFacility key={sellerFacility._id} sellerFacility={sellerFacility}/>
        ));
    }

    render() {
        return (
            <div className={"all-facility-dv facility-div-background"}>
                {this.renderSellerFacilities()}
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('seller_facilities');
    const facilities= SellerFacilities.find({}).fetch();
    return {
        sellerFacilities: facilities,
    };
})(SelectSellerFacility);