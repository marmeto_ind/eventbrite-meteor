import React from 'react';
import {Sellers} from "../../api/sellers";
import { withTracker } from 'meteor/react-meteor-data';

class ShowEachVendorData extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        // console.log(this.props.data);
        return(
            <div>
                <div>{this.props.data.index} : {this.props.data.value}</div>
            </div>
        )
    }
}

class MyBusiness extends React.Component {
    constructor(props) {
        super(props);
    }

    dummyVendorData() {
        return [
            {
                _id: 1,
                "bussiness_name": "Panorama Eventcenter",
                "category": "Venue",
                "credentials": "660575-2684",
                "address": "Viksangsvagen 15",
                "mail": "hanna.montana@gmail.com",
                "phone_number": "+46704394701",
                "website": "panoramafest.se",
                "subscription": "Exclusive Pack",
                "payment": "Debit Card **** 4584",
                "timezone": "Amsterdam gmt (+01.00)",
            }
        ];
    }

    // Fetch data from the database and show here
    getVendorDataFromDatabase(){
        let vendorData = Sellers.find({}).fetch();
        console.log(vendorData);
        return vendorData;

    }

    renderVenderData(){
        console.log(this.props);
        const vendor = this.dummyVendorData()[0];
        // const vendorData = this.getVendorDataFromDatabase();
        // const vendor = vendorData[0];
        // console.log(vendor._id);

        // console.log(vendorData);

        return Object.keys(vendor).map(function (key, index) {
            let data = {index: key, value: vendor[key]};

            return <ShowEachVendorData key={index} data={data}/>;
        });
    }

    render(){
        return (
            <div>
                {this.renderVenderData()}
            </div>
        )
    }
}

export default withTracker(() => {
    return {
        details: Sellers.find({}).fetch(),
    };
})(MyBusiness);