import { Meteor } from 'meteor/meteor';
import { Categories } from "../imports/api/categories";
import { Sellers } from "../imports/api/sellers";
import { Messages } from "../imports/api/messages";
import { MessageView } from "../imports/api/messageView";
import { Review } from "../imports/api/review";
import { ChatWith } from "../imports/api/chatWith";
import { check } from "meteor/check";
import { Email } from 'meteor/email';
import { Banners } from '../imports/api/banners';
import { Choices } from '../imports/api/choice';
import { EventType } from "../imports/api/eventType";
import { Events } from "../imports/api/events";
import { Guests } from "../imports/api/guests";
import { GuestTable } from "../imports/api/guestTable";
import { Regions } from "../imports/api/regions";
import { SellerFacilities } from '../imports/api/sellerFacilities';
import { SellerViews } from '../imports/api/sellerViews';


Meteor.methods({
    'seller_views.insert'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);
        SellerViews.insert({
            userId: userId,
            sellerId: sellerId,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
    },
    'seller_views.getReviewStats'(userId) {
        check(userId, String);
        const seller = Sellers.findOne({ "user": userId });

        if (seller) {
            const sellerId = seller._id;
            const oneDay = 24 * 60 * 60 * 1000;
            const oneWeekAgo = Date.now() - (7 * oneDay);
            const oneMonthAgo = Date.now() - (30 * oneDay);

            const totalNumOfView = SellerViews.find({ "sellerId": sellerId }).count();
            const numOfViewInLastWeek = SellerViews.find({ "sellerId": sellerId, "createdAt": { "$gt": new Date(oneWeekAgo) } }).count();
            const numOfViewInMonth = SellerViews.find({ "sellerId": sellerId, "createdAt": { "$gt": new Date(oneMonthAgo) } }).count();

            return {
                totalNumOfView: totalNumOfView,
                numOfViewInLastWeek: numOfViewInLastWeek,
                numOfViewInMonth: numOfViewInMonth,
            }
        }
    }
});

if (Meteor.isServer) {
    Meteor.publish('seller_views', function sellerViewsPublication() {
        return SellerViews.find();
    })

    Meteor.publish('seller_views.all', function sellerViewsPublication() {
        return SellerViews.find();
    })
}

if (Meteor.isServer) {
    Meteor.publish('sellers', function sellersPublication() {
        return Sellers.find();
    });

    Meteor.publish('sellers.all', function sellersPublication() {
        return Sellers.find();
    });
}

Meteor.methods({
    'sellers.insert'(user, name, category, profile) {
        check(user, String);
        check(name, String);
        check(category, Object);
        check(profile, Object);

        Sellers.insert({
            user: user,
            name: name,
            category: category,
            profile: profile,
            isApprove: false,
            createdAt: new Date(),
        }, function (data) {
            return data;
        });
    },
    'sellers.count'() {
        return Sellers.find().count();
    },
    'seller.approve'(sellerId) {
        check(sellerId, String);
        Sellers.update(
            { "_id": sellerId },
            { $set: { "isApprove": true } }
        );
    },
    'seller.approveWithTag'({ hotTag, newTag, goodDealTag, rating, sellerId }) {
        return Sellers.update(
            { "_id": sellerId },
            {
                $set: {
                    "isApprove": true,
                    "profile.newTag": newTag,
                    "profile.hotTag": hotTag,
                    "profile.goodDealTag": goodDealTag
                }
            });
    },
    'seller.updatePhoneNumber'(sellerId, phoneNumber) {
        check(sellerId, String);
        check(phoneNumber, String);

        Sellers.update(
            { "_id": sellerId },
            { $set: { "profile.phone": phoneNumber } }
        );
    },
    'seller.updateSellerName'(sellerId, sellerName) {
        check(sellerId, String);
        check(sellerName, String);

        Sellers.update(
            { "_id": sellerId },
            { $set: { "name": sellerName } }
        );
    },
    'seller.getAllSellers'() {
        const sellers = Sellers.find({}).fetch();
        return sellers;
    },
    'seller.findOne'(sellerId) {
        const seller = Sellers.findOne({ "_id": sellerId });
        return seller;
    },
    'seller.findOneByUserId'(userId) {
        check(userId, String);
        return Sellers.findOne({ "user": userId });
    }
});

Meteor.methods({
    'seller_facilities.insert'(facilityName, image) {
        check(facilityName, String);
        check(image, String);

        const result = SellerFacilities.insert({
            name: facilityName,
            image: image,
            createdAt: new Date(),
        });
        console.log(result)
    },
    'seller_facilities.remove'(facilityId) {
        check(facilityId, String);
        const result = SellerFacilities.remove(facilityId);
        console.log(result);
    },
    'seller_facilities.update'(facilityId, facilityName, image) {
        check(facilityId, String);
        check(facilityName, String);
        check(image, String);

        SellerFacilities.update({
            "_id": facilityId
        }, {
                $set: {
                    "name": facilityName,
                    "image": image,
                }
            });
    },
    'seller_facilities.updateOnlyName'(facilityId, facilityName) {
        check(facilityId, String);
        check(facilityName, String);

        SellerFacilities.update({
            "_id": facilityId
        }, {
                $set: {
                    "name": facilityName
                }
            });
    },

    'seller_facilities.findOne'(facilityId) {
        check(facilityId, String);
        return SellerFacilities.findOne({ "_id": facilityId });
    }
});

if (Meteor.isServer) {
    Meteor.publish('seller_facilities', function facilitiesPublication() {
        return SellerFacilities.find();
    });

    Meteor.publish('seller_facilities.all', function facilitiesPublication() {
        return SellerFacilities.find();
    });
};

Meteor.methods({
    'reviews.insert'(userId, sellerId, eventId, review) {
        check(userId, String);
        check(sellerId, String);
        check(eventId, String);
        check(review, Number);

        let result = '';

        const rating = Review.findOne({
            "userId": userId,
            "sellerId": sellerId,
            "eventId": eventId
        });

        if (rating) {
            result = Review.update(
                { "_id": rating._id },
                { $set: { "review": review } }
            )
        } else {
            result = Review.insert({
                userId: userId,
                sellerId: sellerId,
                eventId: eventId,
                review: review,
                createdAt: new Date(),
            });
        }

        return result;
    },
    'reviews.getDataById'(eventId) {
        check(eventId, String);

        return Review.findOne({ "eventId": eventId }, { sort: { createdAt: -1 } });
    },
    'reviews.getReviewStats'(userId) {
        check(userId, String);

        const seller = Sellers.findOne({ "user": userId });
        if (seller) {
            const sellerId = seller._id;
            const oneDay = 24 * 60 * 60 * 1000;
            const oneWeekAgo = Date.now() - (7 * oneDay);
            const oneMonthAgo = Date.now() - (30 * oneDay);

            const totalNumOfView = Review.find({ "sellerId": sellerId }).count();
            const numOfViewInLastWeek = Review.find({ "sellerId": sellerId, "createdAt": { "$gt": new Date(oneWeekAgo) } }).count();
            const numOfViewInMonth = Review.find({ "sellerId": sellerId, "createdAt": { "$gt": new Date(oneMonthAgo) } }).count();

            return {
                totalNumOfView: totalNumOfView,
                numOfViewInLastWeek: numOfViewInLastWeek,
                numOfViewInMonth: numOfViewInMonth,
            }
        }
    },
    'reviews.getSellerRating'(sellerId) {
        check(sellerId, String);

        const reviews = Review.find({ "sellerId": sellerId }).fetch();
        const totalCount = Review.find().count();

        let totalReview = 0;
        _.each(reviews, function (review) {
            totalReview = totalReview + parseInt(review.review);
        });

        const averageReview = Math.ceil(totalReview / totalCount);
        return averageReview;
    },
    'reviews.getSellerRatingByUserId'(userId) {
        check(userId, String);

        let seller = Sellers.findOne({ "user": userId });
        if (seller) {
            let sellerId = seller._id;
            const reviews = Review.find({ "sellerId": sellerId }).fetch();
            const totalCount = Review.find().count();

            let totalReview = 0;
            _.each(reviews, function (review) {
                totalReview = totalReview + parseInt(review.review);
            });

            const averageReview = Math.ceil(totalReview / totalCount);
            return averageReview;
        } else {
            return null;
        }

    }
});

if (Meteor.isServer) {
    Meteor.publish('reviews.all', function () {
        return Review.find({});
    });
}

Meteor.methods({
    'regions.insert'(regionName) {
        check(regionName, String);

        Regions.insert({
            name: regionName,
            createdAt: new Date(),
        });
    },
    'regions.remove'(regionId) {
        check(regionId, String);
        Regions.remove({ _id: regionId });
    },
    'regions.update'(regionId, regionName) {
        check(regionId, String);
        check(regionName, String);

        Regions.update({
            "_id": regionId
        }, {
                $set: {
                    "name": regionName
                }
            });
    }
});


if (Meteor.isServer) {
    Meteor.publish('regions', function regionsPublication() {
        return Regions.find({});
    });

    Meteor.publish('regions.all', function regionsPublication() {
        return Regions.find({});
    });
}


Meteor.methods({
    'messages.insert'(message, userId, sellerId, sender) {
        check(message, String);
        check(userId, String);
        check(sellerId, String);
        check(sender, String);

        Messages.insert({
            message: message,
            user: userId,
            seller: sellerId,
            sender: sender,
            createdAt: new Date(),
        });
    },
    'messages.adminInsert'(message, userId, adminId, sender) {
        check(message, String);
        check(userId, String);
        check(adminId, String);
        check(sender, String);

        Messages.insert({
            message: message,
            user: userId,
            admin: adminId,
            sender: sender,
            createdAt: new Date(),
        });
    },
    'messages.remove'(messageId) {
        check(messageId, String);
        Messages.remove(messageId);
    },
    'messages.getCountBySellerId'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);
        return Messages.find({ 'user': userId, 'seller': sellerId }).count();
    },
    // find the count of seller latest messages
    'messages.getLatestCountBySellerId'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);
        try {
            const message = Messages.findOne({ 'user': userId, 'seller': sellerId });
            const lastCheckedByUser = message.lastCheckedByUser;
            // Return new messages and otherwise return zero
            if (lastCheckedByUser) {
                return Messages.find({
                    'user': userId,
                    'seller': sellerId,
                    "createdAt": { $gte: new Date(lastCheckedByUser) }
                }).count();
            } else {
                return 0;
            }
        } catch (e) {

        }
    },

    // find the count of user latest messages
    'messages.getLatestCountByUserId'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);

        try {
            const message = Messages.findOne({ 'user': userId, 'seller': sellerId });
            const lastCheckedBySeller = message.lastCheckedBySeller;

            // Return new messages and otherwise return zero
            if (lastCheckedBySeller) {

                return Messages.find({
                    'user': userId,
                    'seller': sellerId,
                    "createdAt": { $gte: new Date(lastCheckedBySeller) }
                }).count();
            } else {
                return 0;
            }
        } catch (e) { }
    },

    'messages.getSellerMessageCountById'(loggedInSellerId, userId) {
        const sellerId = Sellers.findOne({ "user": loggedInSellerId })._id;
        return Messages.find({ "user": userId, "seller": sellerId }).count();
    },
    'messages.getLatestMessageByUserIdSellerId'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);

        return Messages.findOne({ "user": userId, "seller": sellerId }, { sort: { createdAt: -1 } });
    },
    'messages.getLatestMessageByUserIdSellerIdSeller'(userId, sellerId) {
        check(userId, String);
        check(sellerId, String);

        return Messages.findOne({ "user": userId, "seller": sellerId }, { sort: { createdAt: -1 } });
    },
    'messages.getUnseenMessageByUserId'(userId) {
        check(userId, String);

        const user = Meteor.users.findOne({ "_id": userId });

        const lastLoginToken = user.services.resume.loginTokens[user.services.resume.loginTokens.length - 1];

        let lastLoginTime = '';
        let numOfMessage = 0;

        if (lastLoginToken) {
            lastLoginTime = lastLoginToken.when;
        }

        if (lastLoginTime) {
            numOfMessage = Messages.find({ "user": userId, "createdAt": { $gt: lastLoginTime } }).count();
        }

        return numOfMessage;
    },
    'messages.addLastCheckedByUser'(userId, sellerId) {
        check(sellerId, String);
        check(userId, String);
    },
    'messages.addLastCheckedBySeller'(userId, sellerId) {
        check(sellerId, String);
        check(userId, String);

        let dynamicItem = {};
        dynamicItem["lastCheckedBySeller"] = new Date();
        Messages.update({ "user": userId, "seller": sellerId }, { $set: dynamicItem });
    }

});

if (Meteor.isServer) {
    Meteor.publish('messages', function messagesPublication() {
        return Messages.find();
    });

    Meteor.publish('messages.all', function messagesPublication() {
        return Messages.find();
    });

    Meteor.publish('getUnseenMessage', function (userId) {
        check(userId, String);

        const user = Meteor.users.findOne({ "_id": userId });

        const lastLoginToken = user.services.resume.loginTokens[user.services.resume.loginTokens.length - 1];

        let lastLoginTime = '';
        let numOfMessage = 0;

        if (lastLoginToken) {
            lastLoginTime = lastLoginToken.when;
        }

        if (lastLoginTime) {
            numOfMessage = Messages.find({ "user": userId, "createdAt": { $gt: lastLoginTime } }).count();
        }

        return numOfMessage;
    })
}

Meteor.methods({
    'getAllCategories'() {
        return Categories.find({}).fetch();
    },
    'getAllSellers'() {
        const sellers = Sellers.find({}).fetch();
        return sellers;
    },
    'printToServer'(data) {
        console.log(data);
    },
    'user.getChatWithList'(userId) {
        const sellerList = ChatWith.find({ "user": userId }).fetch();
        return sellerList;
    },
    'user.getUnseenMessage'({ userId, sellerId, admin, viewer }) {
        if (sellerId) {

            const seller = Sellers.findOne({ "_id": sellerId });
            const latestMessageCollection = Messages.findOne({ "user": userId, "seller": sellerId, "sender": "seller" }, { sort: { createdAt: -1 } });
            let latestMessage = "";
            if (latestMessageCollection) {
                latestMessage = latestMessageCollection.message;
            }

            let newMsgCount = 0;

            let msgView = MessageView.findOne({ "user": userId, "seller": sellerId, "viewer": viewer });

            let lastView = '';
            if (msgView) {
                lastView = msgView.updatedAt;
            }

            // Now check the number of message created after the time
            if (lastView) {
                newMsgCount = Messages.find({
                    "user": userId,
                    "seller": sellerId,
                    "sender": "seller",
                    "createdAt": { $gt: lastView }
                }).count()
            } else {
                newMsgCount = Messages.find({ "user": userId, "seller": sellerId, "sender": "seller" }).count()
            }

            return {
                latestMessage: latestMessage,
                logoImage: seller.profile.logoImage,
                newMsgCount: newMsgCount
            }
        } else {
            return {}
        }
    },
    'user.getAllUnseenMessageCountByUser'({ userId, viewer }) {
        let newMsgCount = 0;
        let msgView = MessageView.findOne({ "user": userId, "viewer": viewer });
        let lastView = '';
        if (msgView) {
            lastView = msgView.updatedAt;
        }
        // Now check the number of message created after the time
        if (lastView) {
            newMsgCount = Messages.find({
                "user": userId,
                "sender": "seller",
                "createdAt": { $gt: lastView }
            }).count()
        } else {
            newMsgCount = Messages.find({ "user": userId, "sender": "seller" }).count()
        }

        return {
            newMsgCount: newMsgCount
        }
    },
    'user.getAllUnseenMessageCountByAdmin'({ userId, viewer }) {
        let newMsgCount = 0;
        // console.log({ userId });
        let msgView = MessageView.findOne({ "user": userId });
        // console.log({ msgView })
        // let lastView = '';
        // if (msgView) {
        //     lastView = msgView.updatedAt;
        // }
        // // Now check the number of message created after the time
        // if (lastView) {
        //     newMsgCount = Messages.find({
        //         "user": userId,
        //         "sender": "seller",
        //         "createdAt": {$gt: lastView}
        //     }).count()
        // } else {
        //     newMsgCount = Messages.find({"user": userId, "sender": "seller"}).count()
        // }

        // return {
        //     newMsgCount: newMsgCount
        // }

        // Return 0 as dummy content
        return {
            newMsgCount: 0
        }

    },
    'message_view.upsert'(user, seller, viewer) {
        check(user, String);
        check(seller, String);
        check(viewer, String);

        console.log({ user, seller, viewer });

        try {
            const record = MessageView.findOne({ "user": user, "seller": seller, "viewer": viewer });
            if (!record) {
                return MessageView.insert({
                    user: user,
                    seller: seller,
                    viewer: viewer,
                    createdAt: new Date(),
                    updatedAt: new Date()
                });
            } else {
                record.updatedAt = new Date();
                return MessageView.update({ "user": user, "seller": seller, "viewer": viewer }, record);
            }
        } catch (e) {
            console.log(e);
        }
    }
});


Meteor.methods({
    'banners.insert'({ banner_name, banner_type, banner_image, region, seller }) {
        check(banner_name, String);
        check(banner_type, String);
        check(banner_image, String);
        check(region, String);
        check(seller, String);

        const result = Banners.insert({
            banner_name,
            banner_type,
            banner_image, region,
            seller,
            createdAt: new Date(),
            updatedAt: new Date()
        });
        console.log(result)
    },
    'banners.remove'(bannerId) {
        check(bannerId, String);
        const result = Banners.remove(bannerId);
        console.log(result);
    },
    'banners.update'({ bannerId, banner_name, banner_type, banner_image, region, seller }) {
        check(banner_name, String);
        check(banner_type, String);
        check(banner_image, String);
        check(region, String);
        check(seller, String);
        check(bannerId, String);

        let result = Banners.update({
            "_id": bannerId
        }, {
                $set: {
                    banner_name,
                    banner_type,
                    banner_image, region,
                    seller,
                    updatedAt: new Date()
                }
            });
        console.log(result);
    },
});



Meteor.methods({
    sendEmail(to, from, subject, text) {
        // Make sure that all arguments are strings.
        check([to, from, subject, text], [String]);

        // Let other method calls from the same client start running, without
        // waiting for the email sending to complete.
        this.unblock();

        Email.send({ to, from, subject, text });
    }
});

if (Meteor.isServer) {
    Meteor.publish('getAllCategories', function () {
        return Categories.find({}).fetch();
    });
}


Meteor.methods({
    'categories.insert'(category, image) {
        check(category, String);
        check(image, String);
        return Categories.insert({
            name: category,
            image: image,
            createdAt: new Date(),
        });
    },
    'categories.remove'(categoryId) {
        check(categoryId, String);
        Categories.remove(categoryId);
    },
    'categories.update'(categoryId, categoryName, image) {
        check(categoryId, String);
        check(categoryName, String);
        check(image, String);

        Categories.update({
            "_id": categoryId
        }, {
                $set: {
                    "name": categoryName,
                    "image": image
                }
            });
    },
    'categories.updateOnlyName'(categoryId, categoryName) {
        check(categoryId, String);
        check(categoryName, String);

        // Update only category name
        Categories.update({
            "_id": categoryId
        }, {
                $set: {
                    "name": categoryName
                }
            });
    },
    'categories.getCategoryById'(categoryId) {
        check(categoryId, String);
        return Categories.findOne({ "_id": categoryId });
    },
});

if (Meteor.isServer) {
    Meteor.publish('categories', function categoriesPublication() {
        return Categories.find();
    });

    Meteor.publish('categories.all', function categoriesPublication() {
        return Categories.find();
    });
}


Meteor.methods({
    'chat_with.insert'(user, seller, sellerName, userName) {
        check(user, String);
        check(seller, String);
        check(sellerName, String);
        check(userName, String);

        const existingUser = ChatWith.findOne({ "user": user, "seller": seller });
        if (!existingUser) {
            return ChatWith.insert({
                user: user,
                seller: seller,
                sellerName: sellerName,
                userName: userName,
                createdAt: new Date(),
            });
        }

    },
    'chat_with.remove'(chatWithId) {
        check(chatWithId, String);
        const result = ChatWith.remove(chatWithId);
        return result;
    },
    'chat_with.getRecordBySellerId'(sellerId) {
        check(sellerId, String);
        return ChatWith.findOne({ "seller": sellerId });
    },
    'chat_with.getRecordBySellerIdUserId'(sellerId, userId) {
        check(sellerId, String);
        check(userId, String);

        return ChatWith.findOne({ "seller": sellerId, "user": userId });
    },
    'chat_with.getRecordByUserId'(userId) {
        check(userId, String);
        return ChatWith.find({ "user": userId }).fetch();
    },
    'chat_with.addLastCheckedByUser'(userId, sellerId) {
        check(sellerId, String);
        check(userId, String);

        let dynamicItem = {};
        dynamicItem["lastCheckedByUser"] = new Date();
        ChatWith.update({ "user": userId, "seller": sellerId }, { $set: dynamicItem });
    },
    'chat_with.addLastCheckedBySeller'(userId, sellerId) {
        check(sellerId, String);
        check(userId, String);

        let dynamicItem = {};
        dynamicItem["lastCheckedBySeller"] = new Date();
        ChatWith.update({ "user": userId, "seller": sellerId }, { $set: dynamicItem });
    },
    'chat_with.insertChatWithAdmin'(userId) {
        check(userId, String);

        if (Meteor.isServer) {
            let admin = Meteor.users.findOne({ "profile.role": "admin" });
            let user = Meteor.users.findOne({ "_id": userId });
            let userName = user.profile.name;
            let adminId = admin._id;
            let adminName = admin.profile.name;

            let record = ChatWith.findOne({ user: userId, admin: adminId });
            if (!record) {
                let result = ChatWith.insert({
                    user: userId,
                    userName: userName,
                    admin: adminId,
                    adminName: adminName,
                    createdAt: new Date(),
                });
                return ChatWith.findOne({ _id: result });
            } else {
                return record;
            }
        }
    },
    'chat_with.insertSellerChatWithAdmin'(userId) {
        let sellerId = '';
        let sellerName = '';
        let seller = Sellers.findOne({ "user": userId });

        if (seller) {
            sellerId = seller._id;
            sellerName = seller.name;
        }

        let admin = Meteor.users.findOne({ "profile.role": "admin" });
        let adminName = admin.profile.name;

        const existingUser = ChatWith.findOne({ "user": userId, "seller": sellerId });

        if (!existingUser) {
            let _id = ChatWith.insert({
                user: userId,
                seller: sellerId,
                sellerName: sellerName,
                userName: adminName,
                createdAt: new Date(),
            });
            return ChatWith.findOne({ "_id": _id });
        }
        return existingUser;
    },
    'chat_with.getUserChatListWithAdmin'(adminId) {
        return ChatWith.find({ admin: adminId }).fetch();
    }
});

if (Meteor.isServer) {
    Meteor.publish('chat_with', function chatWithPublication() {
        return ChatWith.find();
    })
    Meteor.publish('chat_with.all', function chatWithPublication() {
        return ChatWith.find();
    })
}

Meteor.methods({
    'choices.insert'(user, seller, categoryId, eventId) {
        check(user, String);
        check(seller, String);

        const result = Choices.insert({
            user: user,
            seller: seller,
            categoryId: categoryId,
            eventId: eventId,
            createdAt: new Date(),
        });
        console.log(result);
    },
    'choices.getDataById'(eventId) {
        check(eventId, String);
        return Choices.findOne({ "eventId": eventId });
    },
    'choices.getLatestUsers'(userId) {
        check(userId, String);
        const seller = Sellers.findOne({ "user": userId });
        var users = [];
        if (seller) {
            const sellerId = seller._id;
            const choices = Choices.find({ "seller": sellerId }, { sort: { createdAt: -1 }, limit: 3 }).fetch();
            _.each(choices, function (choice) {
                const userId = choice.user;
                // console.log(choice);
                const userName = Meteor.users.findOne({ "_id": userId }).profile.name;
                users.push({ name: userName, createdAt: choice.createdAt });
            });
        }
        return users;
    },
    'choices.getDataByUserCategoryId'(userId, categoryId) {
        // check(userId, String);
        check(categoryId, String);

        if (userId) {
            const choice = Choices.findOne({ "user": userId, "categoryId": categoryId });
            if (choice) {
                // console.log(choice);
            }
        }

    },
    'choices.insertOrRemove'(userId, sellerId, categoryId, eventId) {
        check(userId, String);
        check(sellerId, String);
        check(categoryId, String);
        check(eventId, String);

        console.log("inside fujcntion", userId, sellerId, categoryId, eventId)
        // First check if any other seller is choiced
        const alreadyChoice = Choices.findOne({
            "user": userId,
            "categoryId": categoryId,
            "eventId": eventId,
            "seller": { $ne: sellerId },
        });

        console.log(alreadyChoice);

        if (alreadyChoice) {
            const choiceId = alreadyChoice._id;
            Choices.remove({ "_id": choiceId });
        }

        // Now check if the current seller is already choiced
        // If already selected then remove from choice
        // Else insert the record

        const choice = Choices.findOne({
            "user": userId,
            "seller": sellerId,
            "categoryId": categoryId,
            "eventId": eventId
        });

        if (choice) {
            const choiceId = choice._id;
            Choices.remove({ "_id": choiceId });
            console.log("Service Removed")
            return "Service Removed"
        } else {
            Choices.insert({
                user: userId,
                seller: sellerId,
                categoryId: categoryId,
                eventId: eventId,
                createdAt: new Date(),
            });
            console.log("Service Added")
            return "Service Added"
        }
    }
});

if (Meteor.isServer) {
    Meteor.publish('choices', function choicesPublication() {
        return Choices.find();
    });
    Meteor.publish('choices.all', function choicesPublication() {
        return Choices.find();
    });
}

Meteor.methods({
    //Added by NXP
    'events.updateEvent'(eventId, eventName, region, eventType, date, numOfGuest, user) {
        check(eventId, String)
        check(eventName, String);
        check(region, String);
        // check(eventType, String);
        check(date, String);
        check(numOfGuest, String);
        check(user, String);
        try {
            const results = Events.update({ "_id": eventId }, {
                $set: {
                    "name": eventName,
                    "region": region,
                    "eventType": eventType,
                    "date": date,
                    "numOfGuest": numOfGuest,
                    "user": user,
                }
            });
            return results;
        } catch (error) {
            return 0;
        }
    },
    'events.insert'(eventName, region, eventType, date, numOfGuest, user) {
        check(eventName, String);
        check(region, String);
        // check(eventType, String);
        check(date, String);
        check(numOfGuest, String);
        check(user, String);

        const result = Events.insert({
            name: eventName,
            region: region,
            eventType: eventType,
            date: date,
            numOfGuest: numOfGuest,
            user: user,
            createdAt: new Date(),
        });

        return result;
    },
    'events.insertBySeller'(eventName, region, eventType, date, numOfGuest, userId) {
        check(eventName, String);
        check(region, String);
        check(date, String);
        check(numOfGuest, String);

        const result = Events.insert({
            name: eventName,
            region: region,
            eventType: eventType,
            date: date,
            numOfGuest: numOfGuest,
            createdAt: new Date(),
        });

        if (result) {
            const eventId = result;
            const seller = Sellers.findOne({ "user": userId });

            if (seller) {
                const sellerId = seller._id;
                const choiceInsertResult = Choices.insert({
                    seller: sellerId,
                    eventId: eventId,
                    createdAt: new Date(),
                });
            }
        }
        return result;
    },
    'events.remove'(eventId) {
        check(eventId, String);
        Events.remove(eventId);
    },
    'events.getDates'(userId) {
        check(userId, String);
        const seller = Sellers.findOne({ "user": userId });
        var eventDates = [];
        if (seller) {
            const sellerId = seller._id;
            const choices = Choices.find({ "seller": sellerId }).fetch();
            if (choices.length > 0) {
                _.each(choices, function (choice) {
                    const eventId = choice.eventId;
                    const event = Events.findOne({ "_id": eventId });
                    if (event) {
                        const date = event.date;
                        eventDates.push(date);
                    }
                })
            }
        }
        return eventDates;
    },
    'events.getSellerName'(eventId) {
        check(eventId, String);

        let choice = "";
        let sellerId = "";
        let seller = "";

        choice = Choices.findOne({ "eventId": eventId });

        if (choice) {
            sellerId = choice.seller;
            if (sellerId) {
                seller = Sellers.findOne({ "_id": sellerId });
                if (seller) {
                    return seller.name;
                }
            }
        }
        return '';
    },
    'events.getDataById'(eventId) {
        check(eventId, String);
        return Events.findOne({ "_id": eventId });
    },
    'events.getBookedEvent'(userId, date) {
        // Now get the seller Id from the given userId
        const seller = Sellers.findOne({ "user": userId });
        let sellerId = '';
        let eventId = '';
        let eventIdList = [];
        if (seller) {
            sellerId = seller._id;
            const sellerChoice = Choices.find({ "seller": sellerId }).fetch();
            if (sellerChoice) {
                sellerChoice.map((choice) => {
                    const id = choice.eventId;
                    eventIdList.push(id);
                });
                // eventId = sellerChoice.eventId;
            }
        }

        let event = '';

        eventIdList.forEach(id => {
            const result = Events.findOne({ "_id": id, "date": date });
            if (result) {
                event = result;
            }
        });

        if (!event) {
            return '';
        } else {
            return event;
        }
    },
    'events.getBookedEvents'(userId, date) {

        // Now get the seller Id from the given userId
        const seller = Sellers.findOne({ "user": userId });
        let sellerId = '';
        let eventId = '';
        if (seller) {
            sellerId = seller._id;
            const sellerChoice = Choices.findOne({ "seller": sellerId });
            if (sellerChoice) {
                eventId = sellerChoice.eventId;
            }
        }

        if (!sellerId) {
            return '';
        }
        if (!eventId) {
            return '';
        }

        const events = Events.find({ "_id": eventId, "date": date }).fetch();
        if (!events) {
            return '';
        } else {
            return events;
        }
    },
    'events.getAllBookedEvents'(userId, date) {
        // Now get the seller Id from the given userId
        const seller = Sellers.findOne({ "user": userId });
        let sellerId = '';
        let events = [];
        if (seller) {
            sellerId = seller._id;
            const sellerChoices = Choices.find({ "seller": sellerId }).fetch();
            sellerChoices.map((choice) => {
                const eventId = choice.eventId;
                const event = Events.findOne({ "_id": eventId, "date": date });
                if (event) {

                    if (events.indexOf(event) === -1) {
                        events.push(event);
                    }
                }
            });
        }
        return events;
    },
    'events.getEventImage'(eventId) {
        const event = Events.findOne({ "_id": eventId });
        let eventImage = '';
        if (event) {
            const eventType = event.eventType;
            const userEventType = EventType.findOne({ "name": eventType });
            if (userEventType) {
                eventImage = userEventType.image;
            }
        }
        return eventImage;
    },
    'events.getEventsOfSeller'(userId) {
        let sellerId = '';
        const seller = Sellers.findOne({ "user": userId });
        if (seller) {
            sellerId = seller._id;
        }

        let events = [];

        if (sellerId) {
            const sellerChoice = Choices.find({ "seller": sellerId }).fetch();
            sellerChoice.map((choice) => {
                const eventId = choice.eventId;
                const event = Events.findOne({ "_id": eventId });
                events.push(event);
            });
        }
        return events;
    },
    'events.getUpcomingEventsOfSeller'(userId) {

        let sellerId = '';
        const seller = Sellers.findOne({ "user": userId });
        if (seller) {
            sellerId = seller._id;
        }

        let events = [];

        if (sellerId) {
            const sellerChoice = Choices.find({ "seller": sellerId }, { sort: { createdAt: -1 } }).fetch();

            sellerChoice.map((choice) => {
                const eventId = choice.eventId;
                const event = Events.findOne({ "_id": eventId });
                const today = new Date();
                const eventDate = new Date(event.date);

                if (today < eventDate) {
                    events.push(event);
                }
            });
        }

        const sortedEvents = events.sort(function (a, b) {
            return new Date(a.date) - new Date(b.date);
        });

        return sortedEvents;
    },
    'events.getUsersLatestEvent'(userId) {
        check(userId, String);
        return Events.findOne({ "user": userId }, { sort: { createdAt: 1 } });
    },
    //added by NXP
    'events.getUsersEventById'(eventId) {
        //console.log("calling fucntion");
        check(eventId, String);
        return Events.findOne({ "_id": eventId }, { sort: { createdAt: 1 } });
    }
});

if (Meteor.isServer) {
    Meteor.publish('events', function eventsPublication() {
        return Events.find();
    });

    Meteor.publish('events.all', function eventsPublication() {
        return Events.find();
    });
}

Meteor.methods({
    'event_types.insert'(eventType, image) {
        check(eventType, String);
        check(image, String);
        const result = EventType.insert({
            name: eventType,
            image: image,
            createdAt: new Date(),
        });
    },
    'event_types.remove'(eventTypeId) {
        check(eventTypeId, String);

        EventType.remove({
            _id: eventTypeId
        });
    },
    'event_types.update'(eventTypeId, eventTypeName, image) {
        check(eventTypeId, String);
        check(eventTypeName, String);
        check(image, String);

        EventType.update({
            "_id": eventTypeId
        }, {
                $set: {
                    "name": eventTypeName,
                    "image": image
                }
            });
    },
    'event_types.updateOnlyName'(eventTypeId, eventTypeName) {
        check(eventTypeId, String);
        check(eventTypeName, String);

        EventType.update({
            "_id": eventTypeId
        }, {
                $set: {
                    "name": eventTypeName
                }
            });
    },
    'event_types.checkIfBlank'() {
        let eventTypes = EventType.find({}).fetch();
        if (eventTypes.length === 0) {
            EventType.insert({
                name: "Wedding",
                image: "",
                createdAt: new Date(),
            });
        }
    }
});

if (Meteor.isServer) {
    Meteor.publish('event_types', function eventTypesPublication() {
        return EventType.find({});
    });

    Meteor.publish('event_types.all', function eventTypesPublication() {
        return EventType.find({});
    });
}


Meteor.methods({
    'guests.insert'(name, user, tableId) {
        check(name, String);
        check(user, String);
        check(tableId, String);
        Guests.insert({
            name: name,
            user: user,
            tableId: tableId,
            createdAt: new Date(),
        });
    },
    'guests.remove'(guestId) {
        check(guestId, String);
        Guests.remove(guestId);
    },
    'guests.insertWithOutTable'(userId, guest) {
        check(userId, String);
        check(guest, String);
        Guests.insert({
            user: userId,
            name: guest,
            createdAt: new Date(),
        });
    }
});

if (Meteor.isServer) {
    Meteor.publish('guests', function guestsPublication() {
        return Guests.find();
    });
    Meteor.publish('guests.all', function guestsPublication() {
        return Guests.find();
    })
}


Meteor.methods({
    'guests_table.insert'(eventId, name, user) {
        console.log("inserting a table");
        console.log(eventId, name, user);
        check(eventId, String)
        check(name, String);
        check(user, String);
        GuestTable.insert({
            eventId: eventId,
            name: name,
            user: user,
            createdAt: new Date(),
        });
    },
    'guests_table.remove'(tableId) {
        check(tableId, String);
        GuestTable.remove(tableId);
        Guests.remove({ "tableId": tableId });
    },
    'guests_table_with_count'(userId, eventId) {
        let guestTables = GuestTable.find({ "user": userId, "eventId": eventId }).fetch();
        guestTables.map((table) => {
            let guestCount = Guests.find({ "tableId": table._id }).count()
            table.guestCount = guestCount;
        })
        return guestTables
    }
});

if (Meteor.isServer) {
    Meteor.publish('guests_table.all', function guestTablesPublication() {
        return GuestTable.find();
    })
}
