import {Meteor} from 'meteor/meteor';
import '../imports/api/messages';
import '../imports/api/messageView';
import '../imports/api/categories';
import '../imports/api/sellers';
import '../imports/api/listings';
import '../imports/api/events';
import '../imports/api/notes';
import '../imports/api/services';
import '../imports/api/images';
import '../imports/api/videos';
import '../imports/api/regions';
import '../imports/api/sellerPacks';
import '../imports/api/eventType';
import '../imports/api/guests';
import '../imports/api/chatWith';
import '../imports/api/sellerViews';
import '../imports/api/choice';
import '../imports/api/sellerFacilities';
import '../imports/api/guestTable';
import '../imports/api/users';
import '../imports/api/review';
import '../imports/api/emails';
import '../imports/api/banners';
import {Sellers} from '../imports/api/sellers';
import {Categories} from '../imports/api/categories';
import {Regions} from '../imports/api/regions';
import {SellerFacilities} from '../imports/api/sellerFacilities';
import {EventType} from '../imports/api/eventType';

Slingshot.fileRestrictions("myImageUploads", {
    allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
    maxSize: 25 * 1024 * 1024,
});

Slingshot.createDirective("myImageUploads", Slingshot.S3Storage, {
    AWSAccessKeyId: "AKIAILY74S3GGVK2LLVQ",
    AWSSecretAccessKey: "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",
    bucket: "wendely-images",
    acl: "public-read",
    region: "us-east-2",
    authorize: function () {
        return true;
    },
    key: function (file) {
        return file.name;
    }
});

Slingshot.fileRestrictions("myVideoUploads", {
    allowedFileTypes: ["video/mp4"],
    maxSize: 25 * 1024 * 1024,
});

Slingshot.createDirective("myVideoUploads", Slingshot.S3Storage, {
    AWSAccessKeyId: "AKIAILY74S3GGVK2LLVQ",
    AWSSecretAccessKey: "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",
    bucket: "wendely-images",
    acl: "public-read",
    region: "us-east-2",
    authorize: function () {
        return true;
    },
    key: function (file) {
        return file.name;
    }
});

/************* Amazon S3 configuration for file upload ************************/

Slingshot.fileRestrictions("myFileUploads", {
    allowedFileTypes: /.*/i,
    maxSize: 25 * 1024 * 1024,
});

Slingshot.createDirective("myFileUploads", Slingshot.S3Storage, {
    AWSAccessKeyId: "AKIAILY74S3GGVK2LLVQ",
    AWSSecretAccessKey: "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",
    bucket: "wendely-images",
    acl: "public-read",
    region: "us-east-2",
    authorize: function () {
        return true;
    },
    key: function (file) {
        return file.name;
    }
});

Meteor.startup(() => {
    process.env.MAIL_URL = "smtp://nipor:admin_123@smtp.sendgrid.net:587";
    Accounts.urls.resetPassword = function (token) {
        console.log(token);
        return Meteor.absoluteUrl('reset-password/' + token);
    };

    try {
        let user = Accounts.findUserByEmail("guest@wendely.com");
        if (!user) {
            let guestUserData = {
                username: "guest",
                email: "guest@wendely.com",
                password: "guest",
                profile: {
                    role: "user",
                }
            };
            let result = Accounts.createUser(guestUserData);
            console.log(result)
        }
    } catch (e) {
        console.log(e)
    }
});

Picker.route('/api/sellers', function(params, req, res, next) {
    let {skip, limit} = params.query;
        let option = {};
        option['sort'] = { createdAt: -1 };
        if (skip) {
            option['skip'] =  Number(skip);
        }
        if (limit) {
            option['limit'] = Number(limit);
        }

    let data = Sellers.find({}, option).fetch();
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(data));
});

Picker.route('/api/sellers/category/:id', function(params, req, res, next) {
    var result;
    if(params.id !== undefined) {
        var data = Sellers.find({"category._id" : params.id}).fetch();
        if(data.length > 0) {
            result = data
        } else {
            result = {
                "error" : true,
                "message" : "User not found."
            }
        }
    }
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(result));
})

Picker.route('/api/categories', function(params, req, res, next) {
    let result = Categories.find().fetch();
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(result));
});

Picker.route('/api/event-types', function(params, req, res, next) {
    let result = EventType.find({}).fetch();
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(result));
});

Picker.route('/api/facilities', function(params, req, res, next) {
    let result = SellerFacilities.find({}).fetch();
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(result));
});

Picker.route('/api/get_sellers_regions', function(params, req, res, next) {
    let categories = Categories.find().fetch();
    for (let i = 0; i < categories.length; i++) {
        let category = categories[i];
        let sellers = Sellers.find({"category._id": category._id}).fetch();
        for (let j = 0; j < sellers.length; j++) {
            let seller = sellers[j];
            let facilities = seller.profile.facilities;
            for (let k = 0; k < facilities.length; k++) {
                let facility_id = facilities[k]._id;
                let facility = SellerFacilities.findOne({_id: facility_id});
                if (facility) {
                    facilities[k].image = facility.image;
                }
            }
        }
        categories[i].sellers = sellers;
    }
    res.setHeader('Content-Type','application/json');
    res.end(JSON.stringify(categories));
});
