// This section sets up some basic app metadata, the entire section is optional.
App.info({
    id: 'com.example.matt.wendely',
    version: "0.0.1",
    name: 'Wendely',
    description: 'wendely mobile app',
    author: 'Suvojit Mondal',
    email: 'msuvojit@gmail.com',
    website: 'http://suvojitmondal.com'
});

// Set up resources such as icons and launch screens.
App.icons({
    // 'iphone_2x': 'icons/icon-60@2x.png',
    // 'iphone_3x': 'icons/icon-60@3x.png',
    // 'iphone_2x': 'public/img/logo_new_mobile.png',
    // 'iphone_3x': 'public/img/logo_new_mobile.png',
    // More screen sizes and platforms...
    "app_store": 'public/img/logo_new@4x.png',
    "iphone_2x": 'public/img/logo_new@4x.png',
    "iphone_3x": 'public/img/logo_new@4x.png',
    "ipad_2x": 'public/img/logo_new@4x.png',
    "ipad_pro": 'public/img/logo_new@4x.png',
    "ios_settings_2x": 'public/img/logo_new@4x.png',
    "ios_settings_3x": 'public/img/logo_new@4x.png',
    "ios_spotlight_2x": 'public/img/logo_new@4x.png',
    "ios_spotlight_3x": 'public/img/logo_new@4x.png',
    "ios_notification_2x": 'public/img/logo_new@4x.png',
    "ios_notification_3x": 'public/img/logo_new@4x.png',
    "ipad": 'public/img/logo_new@4x.png',
    "ios_settings": 'public/img/logo_new@4x.png',
    "ios_spotlight": 'public/img/logo_new@4x.png',
    "ios_notification": 'public/img/logo_new@4x.png',
    "iphone_legacy": 'public/img/logo_new@4x.png',
    "iphone_legacy_2x": 'public/img/logo_new@4x.png',
    "ipad_spotlight_legacy": 'public/img/logo_new@4x.png',
    "ipad_spotlight_legacy_2x": 'public/img/logo_new@4x.png',
    "ipad_app_legacy": 'public/img/logo_new@4x.png',
    "ipad_app_legacy_2x": 'public/img/logo_new@4x.png',
    "android_mdpi": 'public/img/logo_new@4x.png',
    "android_hdpi": 'public/img/logo_new@4x.png',
    "android_xhdpi": 'public/img/logo_new@4x.png',
    "android_xxhdpi": 'public/img/logo_new@4x.png',
    "android_xxxhdpi": 'public/img/logo_new@4x.png',
});

App.launchScreens({
    // 'iphone_2x': 'splash/Default@2x~iphone.png',
    // 'iphone5': 'splash/Default~iphone5.png',
    'iphone_2x': 'public/img/start-screen.png',
    'iphone5': 'public/img/start-screen.png',
    // Android
    "android_mdpi_portrait": "public/img/start-screen.png",
    "android_mdpi_landscape": "public/img/start-screen.png",
    "android_hdpi_portrait": "public/img/start-screen.png",
    "android_hdpi_landscape": "public/img/start-screen.png",
    "android_xhdpi_portrait": "public/img/start-screen.png",
    "android_xhdpi_landscape": "public/img/start-screen.png",
    "android_xxhdpi_portrait": "public/img/start-screen.png",
    "android_xxhdpi_landscape": "public/img/start-screen.png",
    "android_xxxhdpi_portrait": "public/img/start-screen.png",
    "android_xxxhdpi_landscape": "public/img/start-screen.png",
    // More screen sizes and platforms...
});

// Set PhoneGap/Cordova preferences.
App.setPreference('BackgroundColor', '0xff0000ff');
App.setPreference('HideKeyboardFormAccessoryBar', true);
App.setPreference('Orientation', 'portrait');
// App.setPreference('Orientation', 'default');
// App.setPreference('Orientation', 'all', 'ios');
App.accessRule('data:*', { type: 'navigation' });
App.accessRule('*');

// Pass preferences for a particular PhoneGap/Cordova plugin.
// App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//     APP_ID: '1234567890',
//     API_KEY: 'supersecretapikey'
// });

// Add custom tags for a particular PhoneGap/Cordova plugin to the end of the
// generated config.xml. 'Universal Links' is shown as an example here.
// App.appendToConfig(`
//   <universal-links>
//     <host name="localhost:3000" />
//   </universal-links>
// `);

